import ly
import ly.document
import ly.music

songs = {
    "Basics": [
        "basics-happy-birthday.ly",
        "basics-san-francisco.ly",
        "basics-twinkle-little-star.ly",
        "basics-twinkle-little-star-higher.ly",
        "basics-frere-jacques-lower.ly",
        "basics-frere-jacques.ly",
        "basics-itsy-bitsy-spider.ly",
        "basics-london-bridge.ly",
        "basics-susana.ly",
        "basics-susana-higher.ly",
        "basics-flow.ly",
        "basics-blue-eyed-stranger.ly",
        "basics-ye-banks-and-braes.ly",
    ],
    "Christian / Christmas": [
        "xmas-silent-night.ly",
        "xmas-first-noel.ly",
        "xmas-amazing-grace.ly",
        "xmas-frosty.ly",
        "xmas-carol-bells.ly",
        "xmas-faithful.ly",
        "xmas-joy-to-the-world.ly",
    ],
    "Failiar Songs": [
        "familiar-land.ly",
        "familiar-saints.ly",
        "familiar-puff.ly",
        "familiar-blowin-in-the-wind.ly",
        "familiar-scarborough-fair.ly",
        "familiar-sound-of-silence.ly",
        "familiar-let-it-be.ly",
        "familiar-stand-by-me.ly",
        "familiar-sweet-caroline.ly",
    ],
    "Star Wars": [
        "star-wars-personal.ly",
        "star-wars-01-fanfare.ly",
        "star-wars-02-main-title.ly",
        "star-wars-03-duel-fates.ly",
        "star-wars-04-anakin.ly",
        "star-wars-11-cantina-band.ly",
    ],
    "Wizard of Oz": [
        "oz-01-brain.ly",
        "oz-02-rainbow.ly",
    ],
    "Disney": [
        "disney-beast.ly",
    ],
    "Super Mario Bros.": [
        "mario-01-ground-theme.ly",
        "mario-02-underground-theme.ly",
        "mario-03-underwater.ly",
        "mario-05-3.ly",
        "mario-10-64.ly",
        "mario-13-wii-title.ly",
    ],
    "Zelda": [
        "zelda-01-title.ly",
        "zelda-02-main.ly",
        "zelda-06-adventure-of-link.ly",
        "zelda-11-dark-world.ly",
        "zelda-16-lost-woods.ly",
        "zelda-17-gerudo-valley.ly",
        "zelda-22-dragon-roost-island.ly",
        "symphony-01-ocarina.ly",
        "symphony-02-wind-waker.ly",
    ],
    "Scales": [
        "scales-harmonica.ly",
        "scales-concertina.ly",
    ],
}


def ly_file_to_pdf_file(ly_file):
    return ly_file.replace(".ly", ".pdf").replace("ly/", "pdf/")


def ly_file_to_midi_file(ly_file):
    return ly_file.replace(".ly", ".midi").replace("ly/", "midi/")


def extract_header_block(ly_file):
    contents = open(ly_file).read()
    d = ly.document.Document(contents)
    m = ly.music.document(d)
    for i in m.iter_depth(1):
        if type(i) == ly.music.items.Header:
            return i

def extract_headers(header_block):
    headers = [_ for _ in header_block.descendants()]
    header_map = {}
    for i in range(0, int(len(headers) + 1 / 2), 2):
        header_map[headers[i].token.title().lower()] = headers[i+1].value()
    return header_map

with open("index.html", "w") as index:
    index.write("<!doctype html>\n")
    index.write('<html lang="en">\n')
    index.write("\n")
    index.write('<head>\n')
    index.write('  <meta charset="utf-8">\n')
    index.write('  <title>Music Lilypond</title>\n')
    index.write('  <script src="lib/html-midi-player.js"></script>\n')
    index.write('   <meta name="viewport" content="width=device-width, initial-scale=1.0">\n')
    index.write('</head>\n')
    index.write("\n")
    index.write("<body>\n")
    index.write('<embed id="pdf-viewer" src="./pdf/xmas-frosty.pdf" width="100%" height="800px" type="application/pdf"><br/>\n')
    index.write('<midi-visualizer type="piano-roll" id="myVisualizer"></midi-visualizer><br/>\n')
    index.write('<midi-player\n')
    index.write('  id="midi-player"\n')
    index.write('  src="./midi/xmas-frosty.midi"\n')
    index.write('  sound-font visualizer="#myVisualizer">\n')
    index.write('</midi-player><br/>\n')

    for category in songs:
        index.write(category + ":\n<ul>\n")
        for song in songs[category]:
            ly_file = "ly/" + song
            midi_file = ly_file_to_midi_file(ly_file)
            pdf_file = ly_file_to_pdf_file(ly_file)
            header_block = extract_header_block(ly_file)
            headers = extract_headers(header_block)
            title = headers.get("title", "Untitled")
            subtitle = headers.get("subtitle", "")
            fulltitle = title + (" - " if subtitle else "") + subtitle
            index.write("  <li>\n")
            index.write(f'    <button onclick="document.getElementsByTagName(\'embed\')[0].src=\'{pdf_file}\'; document.getElementById(\'midi-player\').src = \'{midi_file}\'">Load</button>\n')
            # index.write(f'    <button onclick="">Load PDF</button>\n')
            # index.write(f'    <a href="{ly_file_to_pdf_file(ly_file)}">PDF</a>\n')
            index.write("    " + fulltitle + "\n")
            # index.write()
            index.write("  </li>\n")
        index.write("</ul>\n\n")

    index.write("</body>")
    index.write("</html>")