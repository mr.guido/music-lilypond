# Commas indicate breaks, periods indicate page breaks
# python src/generate_lilypond.py A,B 1 3,3,3,4.5,3,3,3,3.
#
# % Breaks as in the score I'm transcribing from, but when I'm done I'll set this
# % To just the empty curly braces {} so lilypond can figure out the breaks.
# mbreak = { \break }
# pgbreak = { \pageBreak }
# 
# % measures 1 - 3 
# AI = { | r1 }
# AII = { | r1 }
# AIII = { | r1 \mbreak }
# BI = { | r1 }
# BII = { | r1 }
# BIII = { | r1 \mbreak }
# 
# AHalf = {
#   \tempo 4 = 220
#   \clef treble
#   \time 4/4
#   \AI \AII \AIII \AIV
# }

# BHalf = {
#   \clef bass
#   \time 4/4
#   \BI \BII \BIII \BIV
# }

import sys
from roman_numerals import to_roman

def compute_chunk(measures_output, halves_parts, prefixes_split, rest, how_to_break, lower, upper):
    measures_output.append(f"% measures {lower} - {upper}")
    for prefix in prefixes_split:
        halves_parts[prefix].append("  ")
        for i in range(lower, upper+1):
            r = to_roman(i)
            mstr = prefix + r + " = { | r" + rest + " "
            if i == upper:
                mstr += how_to_break
            mstr += " }"
            measures_output.append(mstr)
            halves_parts[prefix].append("\\" + prefix + r)
            halves_parts[prefix].append(" ")
        halves_parts[prefix].append("\n")
    measures_output.append("")

def to_lilypond(prefixes, rest, measures):
    output = []
    output.append('\\version "2.24.2"\n')
    output.append('\\header{')
    output.append('  title = "<Title>"')
    output.append('  subtitle = "<Subtitle>"')
    output.append('  composer = "Music by <Person>"')
    output.append('  arranger = "Arranged by <Person>"')
    output.append('  source = "online"')
    output.append('}\n')
    output.append("% Breaks as in the score I'm transcribing from, but when I'm done I'll set this")
    output.append("% to just the empty curly braces {} so lilypond can figure out the breaks.")
    output.append("mbreak = { \\break }")
    output.append("pgbreak = { \\pageBreak }")
    output.append("")
    measures_output = []
    halves_output = []
    prefixes_split = prefixes.split(",")
    halves_parts = {prefix:[] for prefix in prefixes_split}
    lower = 1
    for c in range(0, len(measures), 2):
        upper = lower + int(measures[c]) - 1
        how_to_break = "\\mbreak \\pgbreak" if "." == measures[c+1] else "\\mbreak"
        compute_chunk(measures_output, halves_parts, prefixes_split, rest, how_to_break, lower, upper)
        lower += int(measures[c])

    for prefix in prefixes_split:
        halves_output.append(prefix + "Part = {")
        halves_output.append("  \\tempo 4 = 220")
        halves_output.append("  \\clef treble")
        halves_output.append("  \\time 4/4\n")
        halves_output.append("".join(halves_parts[prefix]))
        halves_output.append("}\n")

    instrument_output = []
    for prefix in prefixes_split:
        instrument_output.append('Instrument' + prefix + 'Part = \\new Staff \\with {midiInstrument = "acoustic grand"} {')
        instrument_output.append('  \\' + prefix + 'Part')
        instrument_output.append('}\n')

    instrument_output.append("InstrumentCombined = {")
    instrument_output.append("  \\new PianoStaff <<")
    for prefix in prefixes_split:
        instrument_output.append("    \\Instrument" + prefix + "Part")
    instrument_output.append("  >>")
    instrument_output.append("}\n")

    instrument_output.append("\\score {")
    instrument_output.append("  \\InstrumentCombined")
    instrument_output.append("  \\layout {")
    instrument_output.append("    indent = 0.0")
    instrument_output.append("  }")
    instrument_output.append("}\n")

    instrument_output.append("\\score {")
    instrument_output.append("  <<")
    instrument_output.append("    \\unfoldRepeats {")
    instrument_output.append("      \\InstrumentCombined")
    instrument_output.append("    }")
    instrument_output.append("  >>")
    instrument_output.append("  \\midi { }")
    instrument_output.append("}\n")

    output.extend(measures_output)
    output.append("")
    output.extend(halves_output)
    output.extend(instrument_output)
    return "\n".join(output)

if __name__ == "__main__":
    prefixes = sys.argv[1]
    rest = sys.argv[2]
    measures = sys.argv[3]
    print(to_lilypond(prefixes, rest, measures))