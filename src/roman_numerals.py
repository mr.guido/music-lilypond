# Example command:
# python src/roman_numbers.py 9
# 
# Output:
# IX

import sys

mapping = {
    1000: "M",
    900: "CM",
    500: "D",
    400: "CD",
    100: "C",
    90: "XC",
    50: "L",
    40: "XL",
    10: "X",
    9: "IX",
    5: "V",
    4: "IV",
    1: "I",
}

def to_roman(decimal_value):
    for i in mapping:
        if decimal_value >= i:
            return mapping[i] + to_roman(decimal_value - i)
    return ""

if __name__ == "__main__":
    print(to_roman(int(sys.argv[1])))
