# $ python src/generate_measures.py A,B 1 6,10
# % measures 6 - 10
# AVI = { | r1 }
# AVII = { | r1 }
# AVIII = { | r1 }
# AIX = { | r1 }
# AX = { | r1 }
# BVI = { | r1 }
# BVII = { | r1 }
# BVIII = { | r1 }
# BIX = { | r1 }
# BX = { | r1 }

# \AVI \AVII \AVIII \AIX \AX
# \BVI \BVII \BVIII \BIX \BX

import sys
from roman_numerals import to_roman

if __name__ == "__main__":
    prefixes = sys.argv[1]
    rest = sys.argv[2]
    lower, upper = sys.argv[3].split(",")
    lower, upper = int(lower), int(upper) + 1
    print(f"% measures {lower} - {upper-1}")
    backslashes = {p:[] for p in prefixes.split(",")}
    for p in prefixes.split(","):
        for i in range(lower, upper):
            r = to_roman(i)
            backslashes[p].append("\\" + p + r)
            print(f"{p}{r} = " + "{ | r" + rest + " }")
    print("")
    for p in backslashes:
        print(" ".join(backslashes[p]))