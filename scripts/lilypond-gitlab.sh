mkdir public/midi
mkdir public/pdf

cd ly

lilypond *.ly
mv *.midi ../public/midi
mv *.pdf ../public/pdf
