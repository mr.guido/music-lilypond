mkdir pdf
mkdir midi

cd ly
lilypond -f svg *.ly

mv *.pdf ../pdf
mv *.midi ../midi
