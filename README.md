# music-lilypond

A collection of lilypond music in text format that, when compiled, will produce
PDFs and MIDI files. I try to make sure the MIDI files sound decent before
checking in the `*.ly` files.

## Harmonica

Some of the "lyrics" are tablature for the Harmonica. There are 10 holes on a
standard harmonica and blowing out or drawing in will produce a different tone.
Negative numbers mean draw in whereas postivie numbers mean blow out.

## Concertina

Some of the "lyrics" are tablature for the 20-button Concertina. Simialar to the
harmonica, two different tones are produced when pushing in or drawing out. A
horizontal line means draw out while pushing the button. If the horizonal line
is omitted then it means to push in while pushing the button. Speaking of
buttons, the ones on the left side will be rendered below the staff and 5 is
your left index finger. Above the staff are buttons for your right hand and 1 is
your right index finger.

## Timiditty

Here's a sample command:

```bash
timidity -A 150,0 disney-bare.midi
```
