\version "2.24.2"

\header{
  title = "Joy to the World"
  composer = "George F. Handel, 1741"
  source = "Christmas Concertina (Kindle)"
}

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = { \break }
pgbreak = { \pageBreak }

% measures 1 - 4
AI = { | c'''4 b''8. a''16 g''4. f''8 }
AII = { | e''4 d''4 c''4. g''8 }
AIII = { | a''4. a''8 b''4. b''8 }
AIV = { | c'''2.~ c'''8 c'''8 \mbreak }

% measures 5 - 8
AV = { | c'''8 b''8 a''8 g''8 g''8. f''16 e''8 c'''8 }
AVI = { | c'''8 b''8 a''8 g''8 g''8. f''16 e''8 e''8 }
AVII = { | e''8 e''8 e''8 e''16 f''16 g''4. e''8 }
AVIII = { | d''8 d''8 d''8 d''16 e''16 f''4. e''16 d''16 \mbreak }

% measures 9 - 10
AIX = { | c''8 c'''4 a''8 g''8. f''16 e''8 f''8 }
AX = { | e''4 d''4 c''2 \mbreak \pgbreak }

APart = {
  \tempo 4 = 110
  \clef treble
  \time 4/4

  \AI \AII \AIII \AIV 
  \AV \AVI \AVII \AVIII 
  \AIX \AX 
}

InstrumentAPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \APart
}

InstrumentCombined = {
  \new PianoStaff <<
    % \chords \with {midiInstrument = "acoustic grand"} { 
    %   \set noChordSymbol = "(N.C.)"
    %   \clef treble
    %   | c4 g8. r16 c4. f8 | c4 g4 c4. r8 | f4. r8 g4. r8 | c2. r4
    %   | c8 r8 r2. | c8 r8 r2. | c8 r8 r2. | g8 r8 r2.
    %   | c8 r4 f8 c8. r16 r8 f8 | c4 g4 c2
    % }
    \InstrumentAPart
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}
