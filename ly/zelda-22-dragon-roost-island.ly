\version "2.24.2"

\header{
  title = "The Legend of Zelda™: The Wind Waker™"
  subtitle = "Dragon Roost Island"
  composer = "Composed by Kenta Nagata"
  arranger = "Arranged by Shinobu Amayake"
  source = "The Legend of Zelda Series for Easy Piano"
}

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = {  }
pgbreak = {  }

% measures 1 - 6
AI = { | <gis' b' e''>4\f <gis' b' e''>8-. <gis' b' e''>8~ <gis' b' e''>4~ }
AII = { | <gis' b' e''>2. }
AIII = { | r2. }
AIV = { | r2. }
AV = { | r2. }
AVI = { | r2. \mbreak }
BI = { | r2.\f }
BII = { | r2. }
BIII = { | r2. }
BIV = { | r2. }
BV = { | a4~\mf a8 c'4 e'8 }
BVI = { | g4 b4 d'4 \mbreak }
CI = { | r2.\f }
CII = { | r2. }
CIII = { | \xNote {a,4 a,4 a,4} }
CIV = { | \xNote {a,4 a,8 a,8 a,4}  }
CV = { | r2.  }
CVI = { | r2. \mbreak }

% measures 7 - 11
AVII = { | r2. }
AVIII = { | r4 r4 r8 e'8 }
AIX = { | c''4. b'8 c''4 }
AX = { | d''4. b'8~ b'4 }
AXI = { | a'2. \mbreak }
BVII = { | a4~ a8 c'4 e'8 }
BVIII = { | g4 b4 d'4 }
BIX = { | a4~ a8 c'4 e'8 }
BX = { | g4 b4 d'4 }
BXI = { | a4~ a8 c'4 e'8 \mbreak }

% measures 12 - 17
AXII = { | e'2 r8 e'8 }
AXIII = { | c''4.\< b'8 c''4 }
AXIV = { | d''4. g''8~ g''4 }
AXV = { | \after 8 \! e''2.~ }
AXVI = { | e''2 r8 c''8 }
AXVII = { | d''4. d''4.~ \mbreak }
BXII = { | g4 b4 d'4 }
BXIII = { | a4~\< a8 c'4 e'8 }
BXIV = { | g4 b4 d'4 }
BXV = { | \clef treble | \after 8 \! c'4~ c'8 e'4 g'8 }
BXVI = { | c'4 e'4 g'4 }
BXVII = { | bes4~ bes8 d'4 f'8 \mbreak }

% measures 18 - 22
AXVIII = { | d''2 r8 d''8 }
AXIX = { | c''4. a'8~ a'4~ }
AXX = { | a'2 r8 c''8-. }
AXXI = { | d''4. d''4.~ }
AXXII = { | d''2 r8 c''8 \mbreak \pgbreak }
BXVIII = { | bes4 d'4 f'4 }
BXIX = { | \clef bass | a4~ a8 c'4 e'8 }
BXX = { | a4 c'4 e'4 }
BXXI = { | g4~ g8 b4 d'8 }
BXXII = { | g4 b4 d'4 \mbreak \pgbreak }

% measures 23 - 28
AXXIII = { | a'2.~\> }
AXXIV = { | a'2.~ }
AXXV = { | a'2. }
AXXVI = { | r2. }
AXXVII = { | r4\!\mf r8 e'8-. r8 a'8-. }
AXXVIII = { | r4 d'4-. g'4-. \mbreak }
BXXIII = { | f4~\> f8 a4 c'8 }
BXXIV = { | f4 a4 c'4 }
BXXV = { | f4~ f8 a4 c'8 }
BXXVI = { | a4 f4 c4 }
BXXVII = { | a,4~\!\mf a,8 c4 e8 }
BXXVIII = { | g,4 b,4 d4 \mbreak }

% measures 29 - 34
AXXIX = { | r4 r8 e'8-. r8 a'8-. }
AXXX = { | r4 d'4-. r8 e'8 }
AXXXI = { | c''4. b'8 c''4 }
AXXXII = { | d''4. b'8~ b'4 }
AXXXIII = { | a'2. }
AXXXIV = { | e'2 r8 e'8 \mbreak }
BXXIX = { | a,4~ a,8 c4 e8 }
BXXX = { | e,4 b,4 e4 }
BXXXI = { | a,4~ a,8 c4 e8 }
BXXXII = { | g,4 b,4 d4 }
BXXXIII = { | a,4~ a,8 c4 e8 }
BXXXIV = { | e,4 b,4 e4 \mbreak }

% measures 35 - 39
AXXXV = { | c''4.\< b'8 c''4 }
AXXXVI = { | d''4. <b' g''>8~\! <b' g''>4 }
AXXXVII = { | <c'' e''>2.~ }
AXXXVIII = { | <c'' e''>2 r8 c''8 }
AXXXIX = { | <bes' d''>4. <bes d''>4.~ \mbreak }
BXXXV = { | a,4~\< a,8 c4 e8 }
BXXXVI = { | g,4 b,4\! d4 }
BXXXVII = { | c4~ c8 e4 g8 }
BXXXVIII = { | c4 e4 g4 }
BXXXIX = { | bes,4~ bes,8 d4 f8 \mbreak }

% measures 40 - 44
AXL = { | <bes' d''>2 r8 d''8 }
AXLI = { | <a' c''>4. <e' a'>4.~ }
AXLII = { | <e' a'>2 r8 c''8 }
AXLIII = { | <b' d''>4. <b' d''>4.~ }
AXLIV = { | <b' d''>2 r8 c''8 \mbreak }
BXL = { | bes,4 d4 f4 }
BXLI = { | a,4~ a,8 c4 e8 }
BXLII = { | a,4 c4 e4 }
BXLIII = { | g,4~ g,8 b,4 d8 }
BXLIV = { | g,4 b,4 d4 \mbreak }

% measures 45 - 50
AXLV = { | <f' a'>2.~\< }
AXLVI = { | <f' a'>2.( }
AXLVII = { | <fis' a'>2.)~  }
AXLVIII = { | <fis' a'>2 r8\! g'8  }
AXLIX = { | <d' a'>2.~ }
AL = { | <d' a'>2.( \mbreak \pgbreak }
BXLV = { | f,4~\< f,8 a,4 c8 }
BXLVI = { | f4 c4 e4 }
BXLVII = { | d4~ d8 fis4 a8 }
BXLVIII = { | c'4 a4 \after 16 \! d4 }
BXLIX = { | bes,4~ bes,8 d4 f8 }
BL = { | bes4 f4 bes,4 \mbreak \pgbreak }

% measures 51 - 55
ALI = { | <c' a'>2.)( }
ALII = { | <b a'>2.) }
ALIII = { | r4 r8 e'8-. r8 a'8-. }
ALIV = { | r4 d'4-. g'4-. }
ALV = { | r4 r8 e'8-. r8 a'8-. \mbreak }
BLI = { | a,4~ a,8 c4 e8 }
BLII = { | g,4 b,4 d4 }
BLIII = { | a,4~ a,8 c4 e8 }
BLIV = { | e,4 b,4 e4 }
BLV = { | a,4~ a,8 c4 e8 \mbreak }

% measures 56 - 60
ALVI = { | r4 d'4-. g'4-. }
ALVII = { | r4 r8 e'8-. r8 a'8-. }
ALVIII = { | r4 d'4-. r8 e'8-. }
ALIX = { | a'4. g'8~ g'4 }
ALX = { | e'4. c'8~ c'4 \mbreak }
BLVI = { | g,4 b,4 d4 }
BLVII = { | a,4~ a,8 c4 e8 }
BLVIII = { | e,4 b,4 e4 }
BLIX = { | f,4~ f,8 a,4 c8 }
BLX = { | f,4 a,4 c4 \mbreak }

% measures 61 - 66
ALXI = { | d'2.~ }
ALXII = { | d'4. e'8-. a8-. c'8-. }
ALXIII = { | d'2.~ }
ALXIV = { | d'2 r8 c'8  }
ALXV = { | d'2.~ }
ALXVI = { | d'2 r8 e'8-. \mbreak }
BLXI = { | e,4~ e,8 g,4 b,8 }
BLXII = { | e,4 g,4 b,4 }
BLXIII = { | d,4~ d,8 a,4 c8 }
BLXIV = { | d,4 a,4 c4 }
BLXV = { | e,4~ e,8 g,4 b,8 }
BLXVI = { | e,4 g,4 b,4 \mbreak }

% measures 67 - 71
ALXVII = { | a'4. g'8~ g'4 }
ALXVIII = { | e'4. c'8~ c'4 }
ALXIX = { | d'2.~ }
ALXX = { | d'4. e'8-. a8-. c'8-. }
ALXXI = { | d'4.~ d'4 e'8 \mbreak }
BLXVII = { | f,4~ f,8 a,4 c8 }
BLXVIII = { | f,4 a,4 c4 }
BLXIX = { | e,4~ e,8 g,4 b,8 }
BLXX = { | e,4 g,4 b,4 }
BLXXI = { | d,4~ d,8 fis,4 a,8 \mbreak }

% measures 72 - 76
ALXXII = { | d'2 r8 c'8 }
ALXXIII = { | <a d'>2.~ }
ALXXIV = { | <a d'>2.~ }
ALXXV = { | <a d'>2.~ }
ALXXVI = { | <a d'>2. \mbreak \pgbreak }
BLXXII = { | d4 d,4 d4 }
BLXXIII = { | bes,4~ bes,8 d4 f8 }
BLXXIV = { | bes,4 d4 f4 }
BLXXV = { | bes,4~ bes,8 d4 f8 }
BLXXVI = { | bes,4 d4 f4 \mbreak \pgbreak }

APart = {
  \tempo 4 = 152
  \clef treble
  \time 3/4

  \AI \AII \AIII \AIV \AV \AVI 
  \AVII \AVIII \AIX \AX \AXI 
  \AXII \AXIII \AXIV \AXV \AXVI \AXVII 
  \AXVIII \AXIX \AXX \AXXI \AXXII 
  \AXXIII \AXXIV \AXXV \AXXVI 

  \repeat volta 2 {
    \AXXVII \AXXVIII 
    \AXXIX \AXXX \AXXXI \AXXXII \AXXXIII \AXXXIV 
    \AXXXV \AXXXVI \AXXXVII \AXXXVIII \AXXXIX 
    \AXL \AXLI \AXLII \AXLIII \AXLIV 
    \AXLV \AXLVI \AXLVII \AXLVIII \AXLIX \AL 
    \ALI \ALII \ALIII \ALIV \ALV 
    \ALVI \ALVII \ALVIII \ALIX \ALX 
    \ALXI \ALXII \ALXIII \ALXIV \ALXV \ALXVI 
    \ALXVII \ALXVIII \ALXIX \ALXX \ALXXI 
    \ALXXII \ALXXIII \ALXXIV \ALXXV \ALXXVI
  }
}

BPart = {
  \clef bass
  \time 3/4

  \BI \BII \BIII \BIV \BV \BVI 
  \BVII \BVIII \BIX \BX \BXI 
  \BXII \BXIII \BXIV \BXV \BXVI \BXVII 
  \BXVIII \BXIX \BXX \BXXI \BXXII 
  \BXXIII \BXXIV \BXXV \BXXVI 
  
  \repeat volta 2 {
    \BXXVII \BXXVIII 
    \BXXIX \BXXX \BXXXI \BXXXII \BXXXIII \BXXXIV 
    \BXXXV \BXXXVI \BXXXVII \BXXXVIII \BXXXIX 
    \BXL \BXLI \BXLII \BXLIII \BXLIV 
    \BXLV \BXLVI \BXLVII \BXLVIII \BXLIX \BL 
    \BLI \BLII \BLIII \BLIV \BLV 
    \BLVI \BLVII \BLVIII \BLIX \BLX 
    \BLXI \BLXII \BLXIII \BLXIV \BLXV \BLXVI 
    \BLXVII \BLXVIII \BLXIX \BLXX \BLXXI 
    \BLXXII \BLXXIII \BLXXIV \BLXXV \BLXXVI 
  }
}

CPart = {
  \clef bass
  \time 3/4

  \CI \CII \CIII \CIV \CV \CVI
}

InstrumentAPart = \new Staff \with {midiInstrument = "acoustic guitar (steel)"} {
  \APart
}

InstrumentBPart = \new Staff \with {midiInstrument = "acoustic bass"} {
  \BPart
}

InstrumentCPart = \new Staff \with {midiInstrument = "standard kit"} {
  \CPart
}

InstrumentCombined = {
  \new PianoStaff <<
    \InstrumentAPart
    \InstrumentBPart
    \InstrumentCPart
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}

