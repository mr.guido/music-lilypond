\version "2.24.2"

\header{
  title = "Stand By Me"
  composer = "Words and Music by Jerry Leiber, Mike Stoller, and Ben E. King"
}

mbreak = {  }

\score {
  \new Staff \with {midiInstrument = "acoustic guitar (steel)"} <<
  { \clef treble 
    \tempo 4 = 130
    \time 4/4
    % measures 1 - 3
    s2. g'8 b'8
    | c''4 r8 c''8~ c''4 g'8 b'8
    | c''4 r8 c''8~ c''4 c''8 b'8
    \mbreak

    % measures 4 - 6
    | a'4 r8 a'8~ a'4 g'4
    | a'4 r8 a'8~ a'4 a'8 g'8
    | f'4 r8 f'8~ f'4 f'8 a'8
    \mbreak

    % measures 7 - 9
    | g'4 r8 g'8~ g'4 g'8 b'8
    | c''4 r8 c''8~ c''4 g'8 b'8
    | c''4 r8 c''8~ c''4 e'8 g'8
    \mbreak

    % measures 10 - 12
    | a'4 r4 r4 e'8 g'8~
    | g'4 r4 r2
    | r4 c'8 d'8 e'4 d'4
    \mbreak

    % measures 13 - 15
    | c'4 r4 r4 c'8 d'8
    | e'8( c'4.~ c'4) c'8 e'8
    | d'4. c'8 d'4 d'4
    \mbreak

    % measures 16 - 18
    | c'1
    | r2 r4 e'8 g'8
    | a'4 r4 r8 e'8 g'8 a'8~
    \mbreak

    % measures 19 - 21
    | a'4 r8 g'8 f'8( e'8 d'16 c'16 d'8)
    | e'4 r4 r8 e'8 d'4
    | c'2. c'8 e'8
    \mbreak

    % measures 22 - 24
    | d'8( c'4.~ c'4) c'8 e'8
    | d'2 r8 e'8 d'8( c'8)
    | c'2. r8 g'8
    \mbreak

    % measures 25 - 27
    | a'4 g'4 c''4 b'4
    \section
    | a'8( g'8 a'2) g'8 a'8~
    | a'2. r4
    \mbreak

    % measures 28 - 30
    | r2 e'8( c'8) d'4
    | c'2. e'4
    | d'8( c'4.~ c'2)
    \mbreak

    % measures 31 - 33
    | r4 e'8 d'8~ d'8 c'4.
    | r4 e'8 d'8~ d'8 c'4.
    | r1
  }
  \addlyrics { 
    " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  
    " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  
    " "  "When" "the" "night" "has" "come" "and" "the" "land" "is" "dark,"
    "and" "the" "moon" "is" "the" "on" "ly" "light" "we" "see." "Oh," "I" "won't"
    "be" "a-" "fraid." "No," "I" "won't" "be" "a-" "fraid" "just" "as" "long"
    "as" "you" "stand," "stand" "by" "me."
    "So" "dar-" "ling," "dar-" "ling," "stand" "by" "me."
    "Stand" "by" "me." "Oh," "stand," "stand" "by" "me," "stand" "by" "me."
  }
  >>
  \layout {

  }
  \midi {

  }
}
