\version "2.24.2"

\header{
  title = "Super Mario Bros."
  subtitle = "Underwater Theme"
  composer = "Composed by Koji Kondo"
  arranger = "Arranged by Sakiko Masuda"
  source = "Super Mario Jazz Piano Arrangements (Kindle)"
}

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = { }
pgbreak = { }

% measures 1 - 4
AI = { | d'4 e'4 fis'4 }
AII = { | g'4. a'8 r8 ais'8 }
AIII = { | <f' b'>8 b'8 <g' b'>4 <ges' b'>4 }
AIV = { | <f' b'>2 g'4 \mbreak }
BI = { | r4 des'4 <c' ees'>4 }
BII = { | <b d'>4. <c' ees'>8 r8 <cis' e'>8 }
BIII = { | d'4 <a cis'>4 <aes c'>4 }
BIV = { | r8 f8 fis8 g4. \mbreak }

% measures 5 - 8
AV = { | <g' b' e''>2. }
AVI = { | <ges' c'' ees''>2. }
AVII = { | <g' b' e''>2. }
AVIII = { | r8 g'8 a'8[ b'8] c''8[ d''8] \mbreak }
BV = { | c4 r8 <g c' e'>4. }
BVI = { | r8 <a ees'>8 r8 <a ees'>4. }
BVII = { | <g c' e'>4 r8 <g c' e'>4. }
BVIII = { | <f a c' d'>2 <f aes c' d'>4 \mbreak }

% measures 9 - 12
AIX = { | <g' b' e''>2. }
AX = { | <ges' c'' ees''>4 r8 <aes' d'' f''>4. }
AXI = { | <g' b' e''>2. }
AXII = { | r4 r4 <f' g'>4 \mbreak \pgbreak }
BIX = { | c4 r8 <g c' e'>4. }
BX = { | r8 <a ees'>8 r8 <b f'>4. }
BXI = { | r4 r8 b8 c'8 fis8 }
BXII = { | g8 dis8 e4 <ees des'>4 \mbreak \pgbreak }

% measures 13 - 17
AXIII = { | <f' a' d''>2. }
AXIV = { | <f' a' cis''>2. }
AXV = { | <f' a' d''>2. }
AXVI = { | r8 g'8 a'8 b'8 c''8 cis''8 }
AXVII = { | <f' a' d''>2. \mbreak }
BXIII = { | d4 r8 <f d'>4. }
BXIV = { | r8 <f cis'>8 r4 <f cis'>4 }
BXV = { | <f c'>4. a8 r8 ais8 }
BXVI = { | b2 a4 }
BXVII = { | g4 r8 d8 des8 c8 \mbreak }

% measures 18 - 22
AXVIII = { | g'4. <aes' des'' f''>4. }
AXIX = { | <g' c'' e''>2. }
AXX = { | r4 r4 g'4 }
AXXI = { | \acciaccatura fis''8 g''2. }
AXXII = { | \acciaccatura fis''8 g''2. \mbreak }
BXVIII = { | b,4 r8 <f b ees'>4. }
BXIX = { | <e a d'>4. <e a d'>8 r4 }
BXX = { | <d f c'>2 <f b ees'>4 }
BXXI = { | r8 c8 r8 <e a d'>4. }
BXXII = { | b,4. <e g d'>8 r4 \mbreak }

% measures 23 - 27
AXXIII = { | g''2. } % TODO add \acciaccatura
AXXIV = { | g''4. a''8 r8 g''8 }
AXXV = { | <a' d'' f''>2. }
AXXVI = { | <a' cis'' f''>2. }
AXXVII = { | <a' c'' f''>2. \mbreak }
BXXIII = { | r8 bes,8 r8 <f bes d'>4. }
BXXIV = { | <e bes d'>4. <g cis' f'>8 r4 }
BXXV = { | r4 r8 a,8 d8 e8 }
BXXVI = { | f4 r8 a,8 c8 cis8 }
BXXVII = { | d4 r8 a4 aes8 \mbreak }

% measures 28 - 32
AXXVIII = { | <b' f''>4. g''8 r8 <aes' des' f''>8 }
AXXIX = { | <g' c'' e''>2. }
AXXX = { | <f' a'>4 r8 <a' des'' f''>4. }
AXXXI = { | <f' c'' e''>2 <f' aes' b'>4 }
AXXXII = { | \acciaccatura gis'8 <e' a' c''>2. \mbreak \pgbreak }
BXXVIII = { | g4. g,8 r8 <f b ees'>8 }
BXXIX = { | <e a d'>2. }
BXXX = { | <ees g des'>4 r8 <g des' f'>4. }
BXXXI = { | <d c'>2 <g d'>4 }
BXXXII = { | <c g>2. \mbreak \pgbreak }


APart = {
  \tempo 4 = 150
  \clef treble
  \time 3/4
  \repeat volta 2 {
    \AI \AII \AIII \AIV 
    \AV \AVI \AVII \AVIII 
    \AIX \AX \AXI \AXII 
    \AXIII \AXIV \AXV \AXVI \AXVII 
    \AXVIII \AXIX \AXX \AXXI \AXXII 
    \AXXIII \AXXIV \AXXV \AXXVI \AXXVII 
    \AXXVIII \AXXIX \AXXX \AXXXI \AXXXII 
  }
}

BPart = {
  \clef bass

  \repeat volta 2 {
    \BI \BII \BIII \BIV 
    \BV \BVI \BVII \BVIII 
    \BIX \BX \BXI \BXII 
    \BXIII \BXIV \BXV \BXVI \BXVII 
    \BXVIII \BXIX \BXX \BXXI \BXXII 
    \BXXIII \BXXIV \BXXV \BXXVI \BXXVII 
    \BXXVIII \BXXIX \BXXX \BXXXI \BXXXII 
  }
}

InstrumentAPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \APart
}

InstrumentBPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \BPart
}

InstrumentCombined = {
  \new PianoStaff <<
    \chords \with {midiInstrument = "acoustic grand"} { 
      \set noChordSymbol = "(N.C.)"
      \repeat volta 2 {
        | d4:7 r2 | g4. c8:dim r8 cis8:dim | g8 r8 a4:7 aes4:7 | g2:7 r4
        | c2.:maj7 | c2.:dim | c2.:maj7 | f4:6 r4 f4:m6
        | c2.:maj7 | c4.:dim g4.:7 | c2.:maj7 r2.
        | d2.:m | d2.:maj7 | d2.:m7 | g2.:7 | r2.
        | g4.:7 r4. | c2.:maj7 | d2:m7 g4:7 | c2.:6 | c2.:6
        | bes2. | e4.:m7 a8:7 r4 | d2.:m | d2.:maj7 | d2.:m7
        | g2.:7 | c2.:6 | ees2.:7 | d2:m7 g4:7 | c2.:6
      }
    }
    \InstrumentAPart
    \InstrumentBPart
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}

