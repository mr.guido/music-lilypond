\version "2.24.2"

\header{
  title = "Puff The Magic Dragon"
}

\score {
  \new Staff \with {midiInstrument = "acoustic grand"} <<
  { \clef treble 
    c''4. c''8 c''4 c''4
    b'2 g'2
    a'4. c''4. c''4
    % \break
    g'2. g'4
    f'8 f'4 g'4. f'4
    e'8 g'4 c''4. c''8 c''8
    % \break
    c''4 a'4 b'8 c''4 d''8~
    d''1
    c''8 c'' 4. c''4 c''4
    b'4 g'2.
    % \break
    a'4 a'4 c''4 c''4
    g'2. g'4
    f'4 f'4 g'4 f'4
    % \break
    e'8 g'4 c''2 c''8
    a'8 c''4. b'4 d''4
    c''4 r4 b'2
    % \break
    c''4. c''8 c''4 c''4
    b'2 g'2
    a'4. c''2 c''4
    % \break
    g'2. g'4
    f'8 f'4 g'4. f'4
    e'8 g'4 c''4. c''8 c''8
    % \break
    c''4 a'4 b'8 c''4 d''8~
    d''1
    c''4. c''8 c''4 c''4
    b'2 g'2
    % \break
    a'4. c''4. c''4
    g'2. g'4
    f'8 f'8 g'4. f'4
    % \break
    e'8 g'4 c''4. c''8 c''8
    a'4 c''4 b'4 d''4
    c''1
  }

  \addlyrics { 
    "Puff," "the" "Mag-" "ic" "Drag-" "on," "lived" "by" "the"
    "sea" "and" "frol-" "icked" "in" "the" "au-" "tumn" "mist" "in" "a"
    "land" "called" "Hon-" "a-" "lee."
    "Lit" "tle" "Jack" "ie" "Pa-" "per" "loved" "that" "ras-" "cal," "Puff,"
    "and" "brought" "him" "strings" "and" "seal-" "ing" "wax" "and" "oth-"
    "er" "fan-" "cy" "stuff." "Oh!"
    "Puff," "the" "Mag-" "ic" "Drag-" "on," "lived" "by" "the" "sea" "and"
    "frol-" "icked" "in" "the" "au" "tumn" "mist" "in" "a" "land" "called"
    "Hon-" "a-" "lee." "Puff," "the" "Mag-" "ic" "Drag" "on" "lived" "by" "the"
    "sea" "and" "frol" "icked" "in" "the" "au" "tumn" "mist" "in" "a" "land"
    "called" "Hon-" "a-" "lee."
  }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 140
  }
}
