\version "2.24.2"

\header{
  title = "Frosty The Snowman"
  composer = "Walter Rollins and Steve Nelson"
}

\score {
  \new Staff \with {midiInstrument = "acoustic guitar (nylon)"} <<
  \relative {
    \time 4/4
    g'2 e4. f8 g4 c2 b8 c8 d4 c4 b4 a4
    g2. b8 c8 d4 c4 b4 a8 a8 g4 c4 e,4 g8 a8
    g4 f4 e4 f4 g2. r4 g2 e4. f8
    g4 c2 b8 c8 d4 c4 b4 a4 g2. b8 c8
    d4 c4 b4 a8 a8 g4 c4 e,4 g8 a8 g4 f4 e4 d4
    c2. c4 a'4 a4 c4 c4 b4 a4 g4 e4
    f4 a4 g4 f4 e2. e4 d4 d4 g4 g4
    b4 b4 d4 d8 b8 d4 c4 b4 a4 g2 g2
    g2 e4. f8 g4 c2 b8 c8 d4 c4 b4 a4
    g2. b8 c8 d4 c4 b4 a8 a8 g4 c4 e,4 g8 a8
    g4 f4 e4 d4 c2. r4
    \break
    g'8 g8 g4 g4 g4 a8 g8 g4 g4 g4 a4 g4 e4 g4 d4
    g8 g8 g4 g4 g4 a8 g8 g4 g4 g4 g8 g8 g4 a4 b4 c2 r4 r4
  }

  \addlyrics {
    Frost y the snow man was a jol ly hap py soul, with a corn cob pipe and a
    but ton nose and two eyes made out of coal.
    Frost y the snow man is a fair y tale they say, He was made of snow but the
    child ren know how he came to life one day.
    There must have been some mag ic in that old silk hat they found, for when
    they placed it on his head he be gan to dance a round.
    Oh!
    Frost y the snow man was a live as he could be, and the chil dren say he could
    laugh and play just the same as you and me.
    Thump et y thump thump, thump et y thump thump, look at Frost y go.
    Thump et y thump thump, thump et y thump thump, o ver the hills of snow.
  }

  \addlyrics {
    Frost y the snow man knew the sun was hot that day, So he said, Lets run and
    we'll have some fun now be fore I melt a way.
    Down to the vil lage with a broom stick in his hand, run ning here and there
    all a round the square say in' Catch me if you can.
    He led them down the strees of town right to the traf fic cop, and he on ly
    paused a mo ment when he heard him hol ler Stop!
    For Frost y the snow man had to hur ry on his way, But he waved good bye say
    in' Don't you cry I'll be back a gain some day.
  }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 164
  }
}
