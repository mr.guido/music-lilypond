\version "2.24.2"

\header{
  title = "Flow Gently Sweet Afton"
}

\score {
  <<
    \new Staff = "staff" {
      \new Voice = "melody" {
        {
          s2 s4 g'4
          c''4 c''4 e''8 d''8
          c''4 c''4 g'4
          a'4 c''4 a'4
          g'2 g'4
          \break
          c''4 c''4 d''4
          e''4 e''4 g''4
          g''4 e''4 c''4
          d''2 g'4
          \break
          c''4 c''4 e''8 d''8
          c''4 c''4 g'4
          a'4 f''4 a'4
          g'2 g'4
          \break
          c''4 c''4 d''4
          e''4 g''4 f''4
          g'4 g'4 b'4
          c''2 r4
        }
      }
    }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 116
  }
}
