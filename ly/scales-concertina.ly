\version "2.24.2"

\header{
  title = "Concertina Scale"
  poet = "Below = left hand; above = right hand.\nCircles = draw; otherwise push."
}

LeftHand = {\set fingeringOrientations = #'(down) \set stringNumberOrientations = #'(down)}
RightHand = {\set fingeringOrientations = #'(up) \set stringNumberOrientations = #'(up)}

\score {
  <<
    \new Staff = "staff" {
      \new Voice = "melody" {
        {
          \LeftHand
          <c'-3>4 <d'\3>4 <e'-2>4 <f'\2>4
          <g'-1>4 <a'\1>4 \RightHand <b'\1>4 <c''-1>4
          <d''\2>4 <e''-2>4 <f''\3>4 <g''-3>4
          <a''\4>4 <b''\5>4 <c'''-4>4
        }
      }
    }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 90
  }
}
