\version "2.24.2"

\header{
  title = "Star Wars"
  subtitle = "Across the Stars"
  composer = "Music by John Williams"
  arranger = "Arranged by Dan Coates"
  source = "Star Wars: A Musical Journey"
}

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = { \break }
pgbreak = { \pageBreak }

% measures 1 - 3
AI = { | a''2(\p g''4 }
AII = { | a''4 d''4 e''4) }
AIII = { | r4 <f'' a''>4( <e'' g''>4 \mbreak }
BI = { | \tuplet 3/2 {d8(\p f8 a8} \tuplet 3/2 {d8 f8 a8} \tuplet 3/2 {d8 f8 a8)} }
BII = { | \tuplet 3/2 {d8( f8 a8} \tuplet 3/2 {d8 f8 a8} \tuplet 3/2 {d8 f8 a8)} }
BIII = { | \tuplet 3/2 {d8( f8 a8} \tuplet 3/2 {d8 f8 a8} \tuplet 3/2 {d8 f8 a8)} \mbreak }

% measures 4 - 7
AIV = { | <f'' a''>2)\< a'4( }
AV = { | f''2\!\mp d''4 }
AVI = { | \tuplet 3/2 {g''8 f''8 e''8} f''4 d''4 }
AVII = { | \tuplet 3/2 {f''8 e''8 d''8} e''4 c''4 \mbreak }
BIV = { | \tuplet 3/2 {d8(\< f8 a8} \tuplet 3/2 {d8 f8 a8)} \tuplet 3/2 {cis8( g8 a8)} }
BV = { | \tuplet 3/2 {d8(\!\mp f8 a8} \tuplet 3/2 {d8 f8 a8} \tuplet 3/2 {d8 f8 a8)} }
BVI = { | \tuplet 3/2 {d8( f8 a8} \tuplet 3/2 {d8 f8 a8} \tuplet 3/2 {d8 f8 a8)} }
BVII = { | \tuplet 3/2 {d8( f8 a8)} \tuplet 3/2 {e8( a8 c'8} \tuplet 3/2 {e8 a8 c'8)} \mbreak }

% measures 8 - 11
AVIII = { | d''2 c''4 }
AIX = { | a'2) a'4( }
AX = { | f''2 d''4 }
AXI = { | \tuplet 3/2 {d''8 e''8 f''8} g''4 e''4 \mbreak }
BVIII = { | \tuplet 3/2 {d8( f8 a8} \tuplet 3/2 {d8 f8 a8)} \tuplet 3/2 {cis8( g8 a8)} }
BIX = { | \tuplet 3/2 {d8( f8 a8} \tuplet 3/2 {d8 f8 a8} \tuplet 3/2 {d8 f8 a8)} }
BX = { | \tuplet 3/2 {d8( g8 bes8)} \tuplet 3/2 {d8( f8 a8} \tuplet 3/2 {d8 f8 a8)} }
BXI = { | \tuplet 3/2 {d8( f8 a8)} \tuplet 3/2 {e8( a8 c'8} \tuplet 3/2 {e8 a8 c'8)} \mbreak }

% measures 12 - 15
AXII = { | <f'' a''>4 <ees'' g''>2 }
AXIII = { | <f'' a''>2)\> a''4( }
AXIV = { | c'''4.\!\mp bes''8 a''8 g''8 }
AXV = { | \tuplet 3/2 {g''8 a''8 bes''8} bes''4) g''4( \mbreak \pgbreak }
BXII = { | \tuplet 3/2 {d8( f8 a8)} \tuplet 3/2 {ees8( g8 bes8} \tuplet 3/2 {ees8 g8 bes8)}  }
BXIII = { | \tuplet 3/2 {d8(\> f8 a8} \tuplet 3/2 {d8 f8 a8} \tuplet 3/2 {d8 f8 a8)} }
BXIV = { | \tuplet 3/2 {ees8(\!\mp g8 bes8} \tuplet 3/2 {ees8 g8 bes8} \tuplet 3/2 {ees8 g8 bes8)} }
BXV = { | \tuplet 3/2 {g8( bes8 d'8} \tuplet 3/2 {g8 bes8 d'8} \tuplet 3/2 {g8 bes8 d'8)} \mbreak \pgbreak }

% measures 16 - 18
AXVI = { | c''4.\< bes''8 a''8 g''8 }
AXVII = { | fis''8 a''8 d''4) d''4( }
AXVIII = { | f''4.\!\mf ees''8 d''8 c''8 \mbreak }
BXVI = { | \tuplet 3/2 {ees8(\< g8 bes8} \tuplet 3/2 {ees8 g8 bes8} \tuplet 3/2 {ees8 g8 bes8)} }
BXVII = { | \tuplet 3/2 {d8( fis8 a8} \tuplet 3/2 {d8 fis8 a8} \tuplet 3/2 {d8 fis8 a8)} }
BXVIII = { | \tuplet 3/2 {aes8(\!\mf c'8 ees'8} \tuplet 3/2 {aes8 c'8 ees'8} \tuplet 3/2 {aes8 c'8 ees'8)} \mbreak }

% measures 19 - 21
AXIX = { | \tuplet 3/2 {c''8 d''8 ees''8} ees''4) c''4( }
AXX = { | f''4. ees''8 d''8 c''8 }
AXXI = { | b'8 d''8 g''2) \mbreak }
BXIX = { | \tuplet 3/2 {g8( c'8 ees'8} \tuplet 3/2 {g8 c'8 ees'8} \tuplet 3/2 {g8 c'8 ees'8)} }
BXX = { | \tuplet 3/2 {aes8( c'8 ees'8} \tuplet 3/2 {aes8 c'8 ees'8} \tuplet 3/2 {aes8 c'8 ees'8)} }
BXXI = { | \tuplet 3/2 {g8( b8 d'8} \tuplet 3/2 {g8 b8 d'8} g'4) \mbreak }

% measures 22 - 25
AXXII = { \bar "||" \key ees \major \tuplet 3/2 {c'8(\p ees'8 g'8} \tuplet 3/2 {c'8 ees'8 g'8} \tuplet 3/2 {c'8 ees'8 g'8)} }
AXXIII = { | \tuplet 3/2 {c'8( ees'8 g'8} \tuplet 3/2 {c'8 ees'8 g'8} \tuplet 3/2 {c'8\< ees'8 g'8)} }
AXXIV = { | \tuplet 3/2 {c'8( ees'8 g'8} \tuplet 3/2 {c'8 ees'8 g'8} \tuplet 3/2 {c'8 ees'8 g'8)} }
AXXV = { | \tuplet 3/2 {c'8(\< ees'8 g'8} \tuplet 3/2 {c'8 ees'8 g'8)} \after 8. \! <b g'>4( \mbreak }
BXXII = { \bar "||" \key ees \major r4 r4 g4-- }
BXXIII = { | c4-- r4 g,4--\< }
BXXIV = { | c,4-- r4 g4-- }
BXXV = { | c4--\< r4 g,4-- \mbreak }

% measures 26 - 29
AXXVI = { | << { \voiceOne | ees''2\mf c''4 } \new Voice { \voiceTwo | g'2.\mf } >> \oneVoice }
AXXVII = { | << { \voiceOne | \tuplet 3/2 {f''8 ees''8 d''8} ees''4 c''4} \new Voice { \voiceTwo | aes'4 g'2 } >> \oneVoice }
AXXVIII = { | << { \voiceOne | \tuplet 3/2 {ees''8 d''8 c''8} d''4 bes'4} \new Voice { \voiceTwo | g'4 g'2 } >> \oneVoice }
AXXIX = { | <ees' c''>2 < d' bes'>4 \mbreak \pgbreak }
BXXVI = { | c8(\!\mf g8 c'8 g8 ees'4) }
BXXVII = { | <f c'>4 c8( g8 c'8 g8 }
BXXVIII = { | c'4) bes4( g4) }
BXXIX = { | aes,8( ees8 aes8 ees8) <g, d>4 \mbreak \pgbreak }

% measures 30 - 34
AXXX = { | <ees' g'>2) r4 }
AXXXI = { | r2. }
AXXXII = { | r2. }
AXXXIII = { | r2. }
AXXXIV = { | r2. \mbreak }
BXXX = { | r2. }
BXXXI = { | r2. }
BXXXII = { | r2. }
BXXXIII = { | r2. }
BXXXIV = { | r2. \mbreak }

% measures 35 - 38
AXXXV = { | r2. }
AXXXVI = { | r2. }
AXXXVII = { | r2. }
AXXXVIII = { | r2. \mbreak }
BXXXV = { | r2. }
BXXXVI = { | r2. }
BXXXVII = { | r2. }
BXXXVIII = { | r2. \mbreak }

% measures 39 - 42
AXXXIX = { | r2. }
AXL = { | r2. }
AXLI = { | r2. }
AXLII = { | r2. \mbreak }
BXXXIX = { | r2. }
BXL = { | r2. }
BXLI = { | r2. }
BXLII = { | r2. \mbreak }

% measures 43 - 45
AXLIII = { | r2. }
AXLIV = { | r2. }
AXLV = { | r2. \mbreak \pgbreak }
BXLIII = { | r2. }
BXLIV = { | r2. }
BXLV = { | r2. \mbreak \pgbreak }

% measures 46 - 48
AXLVI = { | r2. }
AXLVII = { | r2. }
AXLVIII = { | r2. \mbreak }
BXLVI = { | r2. }
BXLVII = { | r2. }
BXLVIII = { | r2. \mbreak }

% measures 49 - 52
AXLIX = { | r2. }
AL = { | r2. }
ALI = { | r2. }
ALII = { | r2. \mbreak }
BXLIX = { | r2. }
BL = { | r2. }
BLI = { | r2. }
BLII = { | r2. \mbreak }

% measures 53 - 55
ALIII = { | r2. }
ALIV = { | r2. }
ALV = { | r2. \mbreak }
BLIII = { | r2. }
BLIV = { | r2. }
BLV = { | r2. \mbreak }

% measures 56 - 59
ALVI = { | r2. }
ALVII = { | r2. }
ALVIII = { | r2. }
ALIX = { | r2. \mbreak \pgbreak }
BLVI = { | r2. }
BLVII = { | r2. }
BLVIII = { | r2. }
BLIX = { | r2. \mbreak \pgbreak }


APart = {
  \key f \major
  \tempo 4 = 46
  \clef treble
  \time 3/4

  \AI \AII \AIII 
  \AIV \AV \AVI \AVII 
  \AVIII \AIX \AX \AXI 
  \AXII \AXIII \AXIV \AXV 
  \AXVI \AXVII \AXVIII 
  \AXIX \AXX \AXXI 
  \AXXII \AXXIII \AXXIV \AXXV 
  \AXXVI \AXXVII \AXXVIII \AXXIX 
  \AXXX \AXXXI \AXXXII \AXXXIII \AXXXIV 
  \AXXXV \AXXXVI \AXXXVII \AXXXVIII 
  \AXXXIX \AXL \AXLI \AXLII 
  \AXLIII \AXLIV \AXLV 
  \AXLVI \AXLVII \AXLVIII 
  \AXLIX \AL \ALI \ALII 
  \ALIII \ALIV \ALV 
  \ALVI \ALVII \ALVIII \ALIX 
}

BPart = {
  \key f \major
  \clef bass

  \BI \BII \BIII 
  \BIV \BV \BVI \BVII 
  \BVIII \BIX \BX \BXI 
  \BXII \BXIII \BXIV \BXV 
  \BXVI \BXVII \BXVIII 
  \BXIX \BXX \BXXI 
  \BXXII \BXXIII \BXXIV \BXXV 
  \BXXVI \BXXVII \BXXVIII \BXXIX 
  \BXXX \BXXXI \BXXXII \BXXXIII \BXXXIV 
  \BXXXV \BXXXVI \BXXXVII \BXXXVIII 
  \BXXXIX \BXL \BXLI \BXLII 
  \BXLIII \BXLIV \BXLV 
  \BXLVI \BXLVII \BXLVIII 
  \BXLIX \BL \BLI \BLII 
  \BLIII \BLIV \BLV 
  \BLVI \BLVII \BLVIII \BLIX 
}

InstrumentAPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \APart
}

InstrumentBPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \BPart
}

InstrumentCombined = {
  \new PianoStaff <<
    \InstrumentAPart
    \InstrumentBPart
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}
