\version "2.20.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Star Wars"
  subtitle = "Main Title"
  composer = "Music by John Williams"
  arranger = "Arranged by Dan Coates"
}

\score {
  \new Staff \with {midiInstrument = "acoustic grand"} <<
    { \clef treble 
      \time 4/4
      s2 s4 {\tuplet 3/2 { d'8\f (d'8 d'8 }
      g'2 d''2
      \tuplet 3/2 {c''8 b'8 a'8} g''2 d''4)}
      {\tuplet 3/2 {c''8 (b'8 a'8} g''2 d''4
      \break
      \tuplet 3/2 {c''8 b'8 c''8} a'2)} {d'8. (d'16
      g'2\mf d''2
      \tuplet 3/2 {c''8 b'8 a'8} g''2 d''4)}
      \break
      {\tuplet 3/2 {c''8 (b'8 a'8} g''2 d''4
      \tuplet 3/2 {c''8 b'8 c''8} a'4)} r4 {d'8.\mp (d'16
      e'4. e'8 c''8 b'8 a'8 g'8
      \break
      \tuplet 3/2 {g'8 a'8 b'8} a'8 e'8 f'4)} {d'8. (d'16
      e'4. e'8 c''8 b'8 a'8 g'8
      <f' d''>4 <d' fis' a'>2)} {d'8.\mf (d'16
      \break
      e'4. e'8 c''8 b'8 a'8 g'8
      \tuplet 3/2 {g'8 a'8 b'8} a'8. e'16 f'4)} {d''8. (d''16
      g''8.\f f''16 ees''8. d''16 c''8. bes'16 a'8. g'16
      \break
      <f' a' d''>2.\sfz)} {\tuplet 3/2 {d'8 (d'8 d'8} g'2\f d''2
      \tuplet 3/2 {c''8 b'8 a'8} g''2 d''4)}
      \break
      {\tuplet 3/2 {c''8 (b'8 a'8} g''2 d''4
      \tuplet 3/2 {c''8 b'8 c''8} a'2)} {d'8. (d'16
      g'2\mf d''2
      \break
      \tuplet 3/2 {c''8 b'8 a'8} g''2 d''4)}
      {\tuplet 3/2 {g''8 (f''8 ees''8} <g'' bes''>2 <d'' fis'' a''>4
      <b' d'' g''>4\sfz)} \tuplet 3/2 {g'8 g'8 g'8} g'4
    }
    
    \new Staff \with {midiInstrument = "acoustic grand"} 
    { \clef bass 
      s2 s8 r4.
      r4 <g b d'> r4 <f b d'>
      <e g c'>2 <d g b>2
      <e g c'>2 <d g b>2
      \break
      <f a c'>4 <fis a d'>2 r4
      <g b d'>4 <g b d'>4 <f b d'>4 <f b d'>4
      <e g c'>4 <e g c'>4 <d g b>4 <d g b>4
      \break
      <e g c'>4 <e g c'>4 <d g b>4 <d g b>4
      <f a c'>4 <fis a d'>2 r4
      <d g c'>4 <d g c'>2 <d g c'>4~
      \break
      <d g c'>4 <d g c'>4 <d a>2
      <d g c'>4 <d g c'>2 <d g c'>4
      <bes, f>4 <d a>4 d,4 r4
      \break
      <d g c'>4 <d g c'>2 <d g c'>4~
      <d g c'>4 <d g c'>4 <d a>2
      <c ees g a>4 <c ees g a>2 <c ees g a>4
      \break
      <d a>4 \tuplet 3/2 {d,8 d,8 d,8} d,4 r4
      <g b d'>4 <g b d'>4 <f b d'>4 <f b d'>4
      <e g c>4 <e g c>4 <d g b>4 <d g b>4
      \break
      <e g c'>4 <e g c'>4 <d g b>4 <d g b>4
      <f a c'>4 <fis a d'>2 r4
      <g b d'>4 <g b d'>4 <f b d'>4 <f b d'>4
      \break
      <e g c'>4 <e g c'>4 <d g b>4 <d g b>4
      <ees g bes>4 <ees g bes>2 <d a>4
      <g b>4 \tuplet 3/2 {g,8 g,8 g,8} g,4
    }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 114
  }
}
