\version "2.24.2"

\header{
  title = "When The Saints Go Marching In"
  composer = "Words by Katherina Purvis; Music by James Black"
  source = "First 50 Songs You Should Play on Harmonica (Kindle)"
}

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = {  }
pgbreak = {  }

% measures 1 - 4
AI = { | s4 c'4 e'4 f'4 }
AII = { | g'1~ }
AIII = { | g'4 c'4 e'4 f'4 }
AIV = { | g'1~ \mbreak }

% measures 5 - 8
AV = { | g'4 c'4 e'4 f'4 }
AVI = { | g'2 e'2 }
AVII = { | c'2 e'2 }
AVIII = { | d'1~ \mbreak }

% measures 9 - 12
AIX = { | d'4 e'4 e'4 d'4 }
AX = { | c'2. c'4 }
AXI = { | e'2 g'4 g'4 }
AXII = { | g'4 f'2.~ \mbreak }

% measures 13 - 17
AXIII = { | f'2 e'4 f'4 }
AXIV = { | g'2 e'2 }
AXV = { | c'2 d'2 }
AXVI = { | c'1~ }
AXVII = { | c'4 \bar "|." \mbreak \pgbreak }

APart = {
  \tempo 4 = 180
  \clef treble
  \time 4/4

  \AI \AII \AIII \AIV 
  \AV \AVI \AVII \AVIII 
  \AIX \AX \AXI \AXII 
  \AXIII \AXIV \AXV \AXVI \AXVII 
}

InstrumentAPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \APart
}

InstrumentCombined = {
  \new PianoStaff <<
    \InstrumentAPart
    \addlyrics { 
      Oh, when the saints go march- ing in,
      oh, when the saints go march- ing in,
      oh, Lord, I want to be in the num- ber
      when the saints go march- ing in.
    }
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}
