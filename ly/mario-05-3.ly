\version "2.24.2"

\header{
  title = "Super Mario Bros. 3"
  subtitle = "GROUND THEME"
  composer = "Composed by Koji Kondo"
  arranger = "Jazz Piano arrangement by Sakiko Masuda"
  source = "Super Mario Jazz Piano Arrangements (Kindle)"
}

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = {  }
pgbreak = {  }

% measures 1 - 3
AI = { | r8 g'8 <a' d'' g''>8 <g' c'' f''>8 r8 a'8 ais'8 <ees' f' b'>8 }
AII = { | r4 r8 g''8 g'''4 r4 }
AIII = { | r8 <g' e''>8[ r8 dis''8] <g' e''>4 r8 dis''8 \mbreak }
BI = { | g,4 <f b e'>8 <ees a d'>8 r8 f8 fis8 g8 }
BII = { | r2 r4 g,4 }
BIII = { | c4 r4 r8 <bes e'>4. \mbreak }

% measures 4 - 6
AIV = { | <g' c'' e''>4 a'8 <e' a' c''>8~ <e' a' c''>2 }
AV = { | r8 <g' b' e''>8[ r8 dis''8] <g' bes' e''>8 e''8 c''8 <f' a' d''>8 }
AVI = { | r1 \mbreak }
BIV = { | <a f'>4 r4 r8 f8 dis8 e8-> }
BV = { | r2 <g cis'>4. <f c'>8-> }
BVI = { | r8 g,4.-> a,4-- b,4-- \mbreak }

% measures 7 - 9
AVII = { | r8 <g' e''>8[ r8 dis''8] <g' e''>4 r8 dis''8 }
AVIII = { | <g' c'' e''>4 a'8 <f' g' c''>8~-> <f' g' c''>2 }
AIX = { | r8 <f' a' e''>8[ r8 e''8] e''8 g'8 a'8 <g' c''>8-> \mbreak }
BVII = { | c4 r4 r8 <bes e'>4. }
BVIII = { | <a f'>4 r8 <ees des'>8~-> <ees des'>2 }
BIX = { | <d c'>4 r4 <f b e'>4. <e a d'>8-> \mbreak }

% measures 10 - 12
AX = { | r1 }
AXI = { | a'8 a'8 c''8 c''8 r8 <c'' f''>8 a''4  }
AXII = { | g'8 g'8 c''8 c''8 r8 <bes' e''>8 g''4 \mbreak \pgbreak }
BX = { | r8 c4 c8 d4 e4 }
BXI = { | <f c' e'>4 r8 <f c' e'>8 r8 <a ees' g'>4. }
BXII = { | <e b d'>4 r8 <g cis' f'>8 r8 <g cis' f'>4. \mbreak \pgbreak }

% measures 13 - 15
AXIII = { | f'8 f'8 a'8 <f' a'>8 r8 <aes' d''>8 f''4 }
AXIV = { | e'8 e'8 g'8 <e' g'>8 r8 <bes' c''>8 e''4 }
AXV = { | a'8 a'8 c''8 c''8 r8 <c'' f''>8 a''4 \mbreak }
BXIII = { | <d a c'>4 r8 <g c'>8 r8 <b f'>4. }
BXIV = { | <c g b>4 r8 <bes des'>8 r8 <cis' g'>4. }
BXV = { | <f c' e'>4 r8 <f c' e'>8 r8 <a ees' g'>4. \mbreak }

% measures 16 - 18
AXVI = { | a'8 a'8 c''8 c''8 r8 <c'' fis''>8 a''4 }
AXVII = { | <f' b'>8 b'8 <g' c''>8 c''8 <g' des''>8 des''8 <fis' d''>8 d''8 }
AXVIII = { | r8 <b' f'' g''>8 r4 <b' f'' g''>2 \mbreak }
BXVI = { | <fis c' e'>4 r8 <fis c' e'>8 r8 <a dis' fis'>4. }
BXVII = { | <g d'>4-- <a e'>4-- <ees bes>4-- <d c'>4-- }
BXVIII = { | g4 r4 des2 \mbreak }

% measures 19 - 21
AXIX = { | r8 <f' a' e''>8[ r8 e''8] e''8 g'8 a'8 <g' c''>8-> }
AXX = { | r2 r8 <c'' f''>8[ r8 <c'' e''>8]-> }
AXXI = { | r8 e''8[ r8 e''8] e''8 g'8 a'8 <g' c''>8-> \mbreak }
BXIX = { | <d c'>4 r4 <f b e'>4. <e bes d'>8 }
BXX = { | r2 <g cis' f'>4. <f c' e'>8-> }
BXXI = { | r2 <f b e'>4. <e bes d'>8-> \mbreak }

% measures 22 - 24
AXXII = { | r2 r8 <a' c'' f''>4.-> }
AXXIII = { | r8 <f' a' e''>8[ r8 e''8] e''8 g'8 a'8 <g' c''>8-> }
AXXIV = { | r1 \mbreak \pgbreak }
BXXII = { | r4 bes,4 a,8-. <g cis' f'>4.-> }
BXXIII = { | <d c'>4 r4 <f b e'>4. <e a d'>8-> }
BXXIV = { | r4 r8 c,8-.-> r2 \mbreak \pgbreak }


APart = {
  \tempo 4 = 150
  \clef treble
  \time 4/4

  \AI \AII \AIII 
  \AIV \AV \AVI 
  \AVII \AVIII \AIX 
  \AX \AXI \AXII 
  \AXIII \AXIV \AXV 
  \AXVI \AXVII \AXVIII 
  \AXIX \AXX \AXXI 
  \AXXII \AXXIII \AXXIV 
}

BPart = {
  \clef bass
  \time 4/4

  \BI \BII \BIII 
  \BIV \BV \BVI 
  \BVII \BVIII \BIX 
  \BX \BXI \BXII 
  \BXIII \BXIV \BXV 
  \BXVI \BXVII \BXVIII 
  \BXIX \BXX \BXXI 
  \BXXII \BXXIII \BXXIV 
}

InstrumentAPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \APart
}

InstrumentBPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \BPart
}

InstrumentCombined = {
  \new PianoStaff <<
    \chords \with {midiInstrument = "acoustic grand"} { 
      \set noChordSymbol = "(N.C.)"
      | g4.:7 f2:7 g8:7 | r1 | c1:7
      | f1:m7 | e2:m7 a4.:7 d8:m7 | r1
      | c1:7 | f4.:maj7 ees8:7~ ees2:7 | d2:m7 g4.:7 c8:6
      | r1 | f2:maj7 r8 f4.:7 | e4:m7 r8 a8:7 r2

      | d2:m7 r8 g4.:7 | c4.:maj7 bes8:dim r8 a4.:7 | f2:maj7 r8 f4.:7
      | fis2:m7 r8 fis4.:dim | g4:7 a4:m7 ees4:7 d4:7 | g2:7 des2:7
      | d2:m7 g4.:7 e8:m7 | r2 a4.:7 d8:m7 | r2 g4.:7 e8:m7
      | r2 r8 a4.:7 | d2:m7 g4.:7 c8:6
    }
    \InstrumentAPart
    \InstrumentBPart
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}

