\version "2.24.2"

\header{
  title = "Oh! Susana"
  subtitle = "Higher Octave"
}

\score {
  <<
    \new Staff = "staff" {
      \new Voice = "melody" {
        {
          r2 r4 c''8 d''8
          e''4 g''4 g''4 a''4
          g''4 e''4 c''4 d''4
          e''4 e''4 d''4 c''4
          d''2. c''8 d''8
          \break
          e''4 g''4 g''4 a''4
          g''4 e''4 c''4 d''4
          e''4 e''4 d''4 d''4
          c''2 d''4 e''4
          \break
          f''2 f''2
          a''4 a''2 a''4
          g''4 g''4 e''4 c''4
          d''2. c''8 d''8
          \break
          e''4 g''4 g''4 a''4
          g''4 e''4 c''4 d''4
          e''4 e''4 d''4 d''4
          c''2. r4
        }
      }
    }
    \new Lyrics {
      \lyricsto "melody" {
        ""
      }
    }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 150
  }
}
