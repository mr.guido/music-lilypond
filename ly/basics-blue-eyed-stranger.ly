\version "2.24.2"

\header{
  title = "The Blue Eyed Stranger"
  composer = "Traditional English"
}

% This is the first time I'm using variables.

Notes = {
  \clef treble
  \time 4/4
  \tempo 4 = 115

  \repeat volta 2 {
    % measures 1-4
    c''4 g'4 g'4 c''8 b'8
    | a'8 g'8 a'8 b'8 c''4 c''8 d''8
    | e''4 g''4 f''8 e''8 d''8 c''8
    | a'4 b'4 c''2
  }

  % measures 5-8
  \break
  | c''8 d''8 e''8 f''8 g''4 g''4
  | a''8 g''8 f''8 e''8 d''2
  | c''8 d''8 e''8 f''8 g''4 g''4
  | e''4. f''8 g''2

  % measures 9-12
  \break
  | c''8 d''8 e''8 f''8 g''4 e''8 g''8
  | a''8 g''8 f''8 e''8 d''4 c''8 d''8
  | e''4 g''4 f''8 e''8 d''8 c''8
  | a'4 b'4 c''2
}

Concertina = \new Staff \with {midiInstrument = "concertina"} {
  \Notes
}

% Using variables helps here because this song has a repeated section / volta.
% For the MIDI output the repeats are unfolded. But for the music sheet they
% are not.
\score {
  <<
    \Concertina
  >>
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \Concertina
    }
  >>
  \midi {
    \tempo 4 = 115
  }
}