\version "2.24.2"

\header{
  title = "San Francisco"
}

\score {
  \new Staff \with {midiInstrument = "acoustic guitar (nylon)"} <<
  \relative {
    \tempo 4 = 130
    c'4 c4 a'8 a4.~ a4. a8 g2 g2 e8 d4.~ \break
    d2 c4 c4 c4 a'2. a4 g4 a4 g8 e4. d1
  }

  \addlyrics { 
    "if" "you're" "go-" "ing" "to" "San" "Fran-" "cis" "co"
    "Be" "sure" "to" "wear" "some" "flow-" "ers" "in" "your" "hair"
  }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 130
  }
}
