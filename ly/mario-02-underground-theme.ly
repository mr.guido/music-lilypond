\version "2.24.2"

\header{
  title = "Super Mario Bros."
  subtitle = "UNDERGROUND THEME"
  composer = "Koji Kondo"
  arranger = "Sakiko Masuda"
}

\score {
  \new PianoStaff <<
    \chords \with {midiInstrument = "electric bass (finger)"} { 
      \set noChordSymbol = "(N.C.)"
      \clef bass
      c4:7 a4:7 bes4:7 r4 | % 1
      r1 | % 2
      c4:7 a4:7 bes4:7 r4 | % 3
      r1 | %4
      \break
      f4:7 d4:7 ees4:7 r4 | % 5
      r1 | % 6
      f4:7 d4:7 ees4:7 r4 | % 7
      r1 | % 8
      \break
      c4:7 ees4:7 d4:7 aes4:7 | % 9
      g4:7 des4:7 r2 | % 10
      % \tuplet 3/2 {aes4:7 aes4:7 aes4:7} aes4:7 aes8:7 aes8:7 | % 11 % revisit this measure
      % aes8:7 aes8:7 aes8:7 aes8:7 aes8:7 aes8:7 aes8:7 aes8:7 | % 12 % revisit this measure
      % \break
      % aes4:7 r2. | % 13
      % c4:7 a4:7 bes4:7 r4 | % 14
      % r1 | % 15
      % c4:7 a4:7 bes4:7 r4 | % 16
      \break
    }
    \new Staff \with {midiInstrument = "electric bass (finger)"} <<
    { \clef bass 
      \tempo 4 = 160
      r8 <e bes c'>8 [r8 <cis g a>8 r8 <d aes bes>8] r4 | % 1
      r1 | % 2
      r8 <e bes c'>8 [r8 <cis g a>8 r8 <d aes bes>8] r4 | % 3
      r1 | % 4
      \break
      r8 <a ees' f'>8 [r8 <fis c' d'>8 r8 <g des' ees'>8] r4 | % 5
      r1 | % 6
      r8 <a ees' f'>8 [r8 <fis c' d'>8 r8 <g des' ees'>8] r4 | % 7
      r2 r4 \tuplet 3/2 {ees'8 d'8 des'8} | % 8
      \break
      <e bes c'>4-- <g des' ees'>4-- <fis c' d'>4-- <c ges aes>4-- | % 9
      <b, f g>4-- <f ces' des'>4--
      \clef treble
      \tuplet 3/2 {c'8 ges'8 f'8} \tuplet 3/2 {e'8 bes'8 a'8} | % 10
      % \tuplet 3/2 {<c' ges' aes'>4-> ees'4 b4}
      % \clef bass
      % bes4 a8 <c ges aes>8-.-> | % 11
      % r1 | % 12
      % \break
      % r1 | % 13
      % r8 <e bes c'>8 [r8 <cis g a>8 r8 <d aes bes>8] r4 \clef treble | % 14
      % r8 <aes'' bes''>8 r4 <aes'' bes''>2 \clef bass | % 15
      % r8 <e bes c'>8 [r8 <cis g a>8 r8 <d aes bes>8] r4 \clef treble | % 16
    }
    
    \new Staff \with {midiInstrument = "electric bass (finger)"} 
    { \clef bass 
      c,4 a,,4 bes,,4 r4 | % 1
      r1 | % 2
      c,4 a,,4 bes,,4 r4 | % 3
      r1 | % 4
      \break
      f,4 d,4 ees,4 r4 | % 5
      r1 | % 6
      f,4 d,4 ees,4 r4 | % 7
      r2 r4 \tuplet 3/2 {ees,8 d,8 des,8} | % 8
      \break
      c,4-- ees,4-- d,4-- aes,,4 | % 9
      g,,4-- des,4-- \tuplet 3/2 {c,8 ges,8 f,8} \tuplet 3/2 {e,8 bes, a,} | % 10
      % \tuplet 3/2 {aes,4-> ees,4 b,,4} bes,,4 a,,8 aes,,8-.-> | % 11
      % r4 aes,4 r8 aes,4. | % 12
      % \break
      % aes,4 r8 aes,8~ aes,2 | % 13
      % c,4 a,,4 bes,,4 r4 | % 14
      % r1 | % 15
      % c,4 a,,4 bes,,4 r4 | % 16
    }
    >>
  >>
  \layout {
    indent = 0.0
  }
  \midi {
    \tempo 4 = 160
  }
}
