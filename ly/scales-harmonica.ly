\version "2.24.2"

\header{
  title = "Harmonica Scale"
  poet = "Blow -Draw"
}

\score {
  \new Staff \with {midiInstrument = "harmonica"} <<
  \relative {
    c4 d 
    e f g a b 
    c d 
    e f
    g \break a 
    b c 
    d e 
    f g 
    a b c
  }

  \addlyrics { 
    "1" "-1" 
    "2" "-" "-2,3" "-" "-3" 
    "4" "-4" 
    "5" "-5" 
    "6" "-6" 
    "-7" "7" 
    "-8" "8" 
    "-9" "9" 
    "-10" "-" "10"
  }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 120
  }
}
