\version "2.24.3"

\header{
  title = "Movement I: The Ocarina of Time"
  composer = "Music by Koji Kondo"
}

% TODOs:
% - add crescendos, \mp, \f to B/second-half/bass line; same for second voices
% - add squiggly lines for BXCIII (93rd measure)
% 
% Lilypond features being used:
% Variables - These let me better structure the source code.
% https://lilypond.org/doc/v2.25/Documentation/notation/using-variables
% 
% Chorded Notes - to play mulitiple notes at once.
% https://lilypond.org/doc/v2.25/Documentation/notation/chorded-notes
% 
% Articulations and ornamentations - these specify exactly how a note should be played.
% https://lilypond.org/doc/v2.25/Documentation/notation/articulations-and-ornamentations
% 
% Multiple voices - these allow two notes of different lengths to be played on
% the same staff at the same time.
% https://lilypond.org/doc/v2.25/Documentation/notation/single_002dstaff-polyphony
% 
% Tremolo - These are the three diagonal lines over the stem on the first few
% notes. I use the shorthand notation by adding colon & a number after the
% duration of a note for this. I'm still not sure if I'm specifying the right
% number, but the generated pdf looks as intended.
% https://lilypond.org/doc/v2.25/Documentation/notation/tremolo-repeats
% 
% Dynamics - these are the bold & italicized p, f, mf, mp, etc. In between the
% two staves that specify how loud or softly the music should be played. This
% documentation also has crescendo and decrescendo hairpins - the elongated
% less than or greater than symbols.
% https://lilypond.org/doc/v2.25/Documentation/notation/dynamics
% 
% I'm entering notes using absolute note names. This is opposed to relative note
% names which are recommended for when you're starting out in lilypond.
% https://lilypond.org/doc/v2.25/Documentation/learning/absolute-note-names
% 
% Tuplets - this allows for playing say, three eight notes in the span of two
% eight notes.
% https://lilypond.org/doc/v2.25/Documentation/learning/tuplets
% 
% Bar checks - these will help detect any errors if I get the duration wrong.
% https://lilypond.org/doc/v2.25/Documentation/learning/bar-checks
% 
% I'm using some notation to tell lilypond to create a .midi file, almost like
% an audio file so I can listen to my hard work at the end :)
% There are many midi players available, as they all work like syntehesizers.
% My small hosted website uses the following html midi player.
% https://lilypond.org/doc/v2.25/Documentation/notation/creating-midi-output
% https://cifkao.github.io/html-midi-player/
% 
% A note on variable names: the first letter indicates the part. i.e. in this
% piece there are only two, so the variable will either start with A or B.
% The second letter indicates the bar/measure using roman numerals. So AI is the
% very first bar/measure for the first part - typically the treble.
% https://www.rapidtables.com/convert/number/roman-numerals-converter.html?x1=&x2=215
% 
% Including articulate.ly to hopefully make the midi file sound more like what
% was intended.
% https://lilypond.org/doc/v2.25/Documentation/notation/the-articulate-script

\include "articulate.ly"

% TODO spacers. Remove this when finished transcribing:
TODOThreeFours = {r2.}
TODOFourFours = {r1}

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% To just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = { \break }
pgbreak = { \pageBreak }

% common chords
DAD = {<d, a, d>8->}
CGC = {<c, g, c>8->}

% Split voices
Voices_BXI_BXIV = { % Voices for part B for measures 11-14
  <<
  { \voiceOne
    cis2.~
    | cis2. \mbreak
    | cis2.~
    | cis2.
  }
  \new Voice
  { \voiceTwo
    <d, a,>8-> r8 r4 <d, a,>8-> r8
    | <d, a,>8-> r8 r4 <c, g,>8-> r8 \mbreak
    | <d, a,>8-> r8 r4 <c, g,>8-> r8
    | <d, a,>8-> r8 r4 <c, g,>8-> r8
  }
  >>
  \oneVoice
}

Voices_BXXVI_BXXX = {
  <<
  { \voiceOne
    ees2.~
    | ees2.
    | ees2.~
    | ees2.
  }
  \new Voice
  { \voiceTwo
    ees,8-. r8 ees,8-. ees,8-. ees,8-. ees,8-.
    | ees,8-. r8 ees,8-. ees,8-. ees,8-. ees,8-.
    | ees,8-. r8 ees,8-. ees,8-. ees,8-. ees,8-.
    | ees,8-. r8 ees,8-. ees,8-. ees,8-. ees,8-.
  }
  >>
  \oneVoice
}
Voices_AXXXV_AXXXVIII = {
  <<
  { \voiceOne
    | g'2( bes'4
    | f''2 ees''4 \mbreak
    | bes'2 g'4
    | f'2.)
  }
  \new Voice
  { \voiceTwo
    % <ais b cis' d'>2.~  % these two measures were in the original
    % | <ais b cis' d'>2. % but I didn't like how it sounded
    s2.
    | s2.
    | s2.
    | s2.
  }
  >>
  \oneVoice
}
Voices_ALIX_ALX = {
  <<
  { \voiceOne
    | bes8(\< aes8 bes2~ bes8 aes8
    | bes8\> des'8 \after 2 \! c'2.)
  }
  \new Voice
  { \voiceTwo
    | r4 ees8 des ees8 f8 ges8 aes8
    | g1
  }
  >>
  \oneVoice
}

Voices_BLXXXIV_BLXXXVI = {
  <<
  { \voiceOne
    | c4( g,4 c4 e4
    | g4 c'2.)
    | <f, c>8(\f a8 <f a c'>2.)
  }
  \new Voice
  { \voiceTwo
    | <c,>1~
    | <c,>1
    | s1
  }
  >>
  \oneVoice
}
Voices_AXCIII = {
  <<
  { \voiceOne
    | <e' e''>1)\>
  }
  \new Voice
  { \voiceTwo
    | r8 <c'' g'' b''>8-. <c'' g'' b''>8-. <c'' g'' b''>8-. <c'' g'' b''>8-. <c'' g'' b''>8-. <c'' g'' b''>8-. <c'' g'' b''>8-.
  }
  >>
  \oneVoice
}
Voices_BCVI = {
  <<
  { \voiceOne
    | e1
  }
  \new Voice
  { \voiceTwo
    | g,8-. r8 r8 g,8-. r8 g,8-. g,8-. d,8-.
  }
  >>
  \oneVoice
}
Voices_BCVIII = {
  <<
  { \voiceOne
    | f1
  }
  \new Voice
  { \voiceTwo
    | g,8-. r8 r8 g,8-. r8 g,8-. g,8-. r8
  }
  >>
  \oneVoice
}

AI   = { | <d'' e'' f''>2.:8~\p\<^"Enter Ganondorf:" }
AII  = { | \after 2 \f <d'' e'' f''>2.:8 }
AIII = { | <d' f'>8-.\! r8 <des' a'>8-. r8 <f' d''>8-. r8 }
AIV  = { | <bes' f''>8-. r8 <des'' a''>8-. r8 <f'' d'''>8-. r8 \mbreak }
BI   = { | <d' e' f'>2.:8~ }
BII  = { | <d' e' f'>2.:8 }
BIII = { | \DAD r8 r4 \DAD r8 }
BIV  = { | \DAD r8 r4 \CGC r8 \mbreak }

% measures 5-8
AV = { | <d' f'>8-. r8 <des' a'>8-. r8 <f' d''>8 r8 }
AVI = { | <bes' f''>8-. r8 <des'' a''>8-. r8 \tuplet 3/2 {cis'8 e'8 b'8} }
AVII = { | <f' aes' des''>2. }
AVIII = { | <f' aes' des''>8 r8 <dis' fis' b'>8 r8 <f' aes' des''>8 r8 \mbreak }
BV = { | \DAD r8 r4 \DAD r8 }
BVI = { | \DAD r8 r4 \CGC r8 }
BVII = { | \DAD r8 r4 \DAD r8 }
BVIII = { | \DAD r8 r4 \CGC r8 \mbreak }

% measures 9-14
AIX = { | <gis' b' e''>2. }
AX = { | <g' bes' ees''>2. }
AXI = { | e'2 g'4 }
AXII = { | d'2 c'8 d'8 \mbreak }
AXIII = { | e'2 g'4  }
AXIV = { | d'2. }
BIX = { | \DAD r8 r4 \DAD r8 }
BX = { | \DAD r8 r4 \CGC r8 }
BXI_XIV = \Voices_BXI_BXIV % voices are used for measures 11 to 14

% measures 15 - 16
AXV = { | <ees' bes' ees''>2~ <ees bes' ees''>8 <d' a' d''>8 }
AXVI = { | <f' c'' f''>2~ <f' c'' f''>8 <e' b' e''>8 \mbreak }
BXV = { | <ees, bes, ees>8-> r8 r4 <ees, bes, ees>8-> r8 }
BXVI = { | <ees, bes, ees>8-> r8 r4 <des, aes, des>8-> r8 \mbreak }

% measures 17 - 20
AXVII = { | <g' d'' g''>2~ <g' d'' g''>8 <ges' des'' ges''>8 }
AXVIII = { | <aes' ees'' aes''>2. }
AXIX = { | <bes, ees>2~ <bes, ees>8 <a, d>8 }
AXX = { | <c f>2~ <c f>8 <b, e>8 \mbreak \pgbreak }
BXVII = { | <ees, bes, ees>8-> r8 r4 <ees, bes, ees>8-> r8 }
BXVIII = { | <ees, bes, ees>8-> r8 r4 <des, aes, des>8-> r8 }
BXIX = { | des,4 des,4 r4 }
BXX = { | des,4 des,4 r4 \mbreak \pgbreak }

% measures 21 - 24
AXXI = { | <d g>2~ <d g>8 <cis fis>8 }
AXXII = { | <dis gis>2. }
AXXIII = { | <aes ees' aes'>2~\< <aes ees' aes'>8 <g d' g'>8 }
AXXIV = { | <bes f' bes'>2~ <bes f' bes'>8 <a e' a'>8 \mbreak }
BXXI = { | des,4 des,4 r4 }
BXXII = { | des,4 des,4 r4 }
BXXIII = { | <ees,, ees,>8 <d,, d,>8 <f,, f,>8 <e,, e,>8 <g,, g,>8 <fis,, fis,>8}
BXXIV = { | <aes,, aes,>8 <g,, g,>8 <a,, a,>8 <aes,, aes,>8 <bes,, bes,>8 <a,, a,>8 \mbreak }

% measures 25 - 30
AXXV = { | <c' g' c''>2~ <c' g' c''>8 <b fis' b'>8\! }
AXXVI = { | \repeat tremolo 6 { <gis' cis''>16\fp\< <cis'>16\fff\! } } % 
AXXVII = { | r2. }
AXXVIII = { | r2. \mbreak }
AXXIX = { | r2. }
AXXX = { | r2. }
BXXV = { | <c, c>8 <bes,, bes,>8 <des, des>8 <c, c>8 <d, d>8 <des, des>8 }
BXXVI = { | \repeat tremolo 6 { <ees,>16 <ees>16 } }
BXXVII_XXX = \Voices_BXXVI_BXXX

% measures 31 - 32
AXXXI = { | g'2(^"Zelda's Lullaby:" bes'4 }
AXXXII = { | f'2 ees'8 f'8 \mbreak }
BXXXI = { | <ees, bes, ees>8-. r8 <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. }
BXXXII = { | <ees, bes,>8-. r8 <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. }

% measures 33 - 38
AXXXIII = { | g'2 bes'4 }
AXXXIV = { | f'2.) }
AXXXV_XXXVIII = \Voices_AXXXV_AXXXVIII
BXXXIII = { | <ees, bes, ees>8-. r8 <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. }
BXXXIV = { | <ees, bes,>8-. r8 <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. }
BXXXV = { | <ees, bes, ees>8-. r8 <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. }
BXXXVI = { | <ees, bes,>8-. r8 <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. }
BXXXVII = { | <ees, bes, ees>8-. r8 <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. }
BXXXVIII = { | <ees, bes,>8-. r8 <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. }

% measures 39 - 40
AXXXIX = { | <g' g''>2( <bes' bes''>4 }
AXL = { | <f' f''>2  ees''8 f''8 \mbreak }
BXXXIX = { | <ees, bes, ees>8-. r8 <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. }
BXL = { | <ees, bes,>8-. r8 <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. }

% measures 41 - 45
AXLI = { | <g' g''>2 <bes' bes''>4 }
AXLII = { | f'2.) }
AXLIII = { | g'2 bes'4 }
AXLIV = { | f''2 ees''4 }
AXLV = { | g''4\< g''16 g''16 g''8 g''4 \mbreak \pgbreak }
BXLI = { | <ees, bes, ees>8-. r8 <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. }
BXLII = { | <ees, bes,>8-. r8 <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. <ees, bes,>8-. }
BXLIII = { | <a bes cis' d'>2. }
BXLIV = { | <b c' dis' e'>2. }
BXLV = { | <c' des' e' f'>2. \mbreak \pgbreak }

% measures 46 - 50
AXLVI = { | g''4 g''16 g''16 g''8 g''4\! }
AXLVII = { | <bes' bes''>4(\ff\> <g' g''>4 <ges' ges''>4 }
AXLVIII = { | <ees'' ees'''>4 <d'' d'''>4 <bes' bes''>4) }
AXLIX = { | <a' dis'' fis'' g'' a''>2. }
AL = { | <ges' ges''>2.\mp\!\fermata \mbreak }
BXLVI = { | <cis' d' f' g'>2. }
BXLVII = { | <g,, g,>2.~ }
BXLVIII = { | <g,, g,>2. }
BXLIX = { | <g,, g,>2.~ }
BL = { | <g,, g,>2.\fermata }

% measures 51 - 54
ALI = { | r1 }
ALII = { | <c'' g''>8( g'8 <c'' g''>8 g'8 <c'' g''>8 g'8 <c'' g''>8 g'8) }
ALIII = { | r4 <g c'>4(\mp\< <aes des'>4 <c' f'>4 }
ALIV = { | <b e'>4\> <g c'>2.) \mbreak }
BLI = { | c8( g8 c8 g8 c8 g8 c8 g8) }
BLII = { | c8( g8 c8 g8 c8 g8 c8 g8) }
BLIII = { | c,8( g,8 c,8 g,8 c,8 g,8 c,8 g,8) }
BLIV = { | c,8( g,8 c,8 g,8 c,8 g,8 c,8 g,8) }

% measures 55 - 58
ALV = { | r4\< <g c'>4( <aes des'>4 <c' f'>4 }
ALVI = { | <b e'>4\> <f' bes'>4 <e' g'>2) }
ALVII = { | r4\! <c' f'>4\< <des' e'>4 <c' f'>4 }
ALVIII = { | <b e'>4\> \after 2 \! <g c'>2. \mbreak }
BLV = { | cis,8( gis,8 cis,8 gis,8 cis,8 gis,8 cis,8 gis,8) }
BLVI = { | cis,8( gis,8 cis,8 gis,8 cis,8 gis,8 cis,8 gis,8) }
BLVII = { | c,8( g,8 c,8 g,8 c,8 g,8 c,8 g,8) }
BLVIII = { | c,8( g,8 c,8 g,8 c,8 g,8 c,8 g,8) \mbreak }

% measures 59 - 63
ALIX_LX = \Voices_ALIX_ALX
ALXI = { | <aes' c'' g''>8(\>\p g'8 <c'' g''>8 g'8 <aes' c'' g''>8 g'8 <c'' g''>8 g'8) }
ALXII = { | <aes'( c'' g''>8 g'8 <c'' g''>8 g'8 <aes' c'' g''>8 g'8 <c'' g''>8 g'8) }
ALXIII = { | f''1\fermata\!\pp \mbreak }
BLIX = { | cis,8( gis,8 cis,8 gis,8 cis,8 gis,8 cis,8 gis,8) }
BLX = { | c,8( g,8 c,8 g,8 c,8 g,8 c,8 g,8) }
BLXI = { | c,8( g,8 c,8 g,8 c,8 g,8 c,8 g,8) }
BLXII = { | c,8( g,8 c,8 g,8 c,8 g,8 c,8 g,8) }
BLXIII = { | f1\fermata \mbreak }

% measures 64 - 67
ALXIV = { | r1^"Title Theme:" }
ALXV = { | r1 }
ALXVI = { | r1 }
ALXVII = { | r2 e''4( f''4 \mbreak \pgbreak}
BLXIV = { | f,8( c8 <f a d' e'>2.) }
BLXV = { | c,8( g,8 <g b d' e'>2.) }
BLXVI = { | f,8( c8 <f a d' e'>2.) }
BLXVII = { | c,8( g,8 <g b d' e'>2.) \mbreak \pgbreak }

% measures 68 - 71
ALXVIII = { | d'''4. des'''16 c'''16 b''4.~ b''16 a''16 }
ALXIX = { | g''2.) d''8( e''8 }
ALXX = { | d'''4~ d'''16 des'''16 c'''16 b''16~ b''4. c'''8 }
ALXXI = { | b''2.) d''8( e''8  \mbreak }
BLXVIII = { | f,8( c8 <f a d' e'>2.) }
BLXIX = { | c,8( g,8 <g b d' e'>2.) }
BLXX = { | f,8( c8 <f a d' e'>2.) }
BLXXI = { | c,8( g,8 <g b d' e'>2.) \mbreak }

% measures 72 - 75
ALXXII = { | d'''4. des'''16 c'''16 b''4.~ b''16 a''16 }
ALXXIII = { | g''2.) d''8( e''8 }
ALXXIV = { | d'''4~ d'''16 des'''16 c'''16 b''16~ b''4. e'''8 }
ALXXV = { | b''1) \mbreak }
BLXXII = { | f,8( c8 <f a d' e'>2.) }
BLXXIII = { | c,8( g,8 <g b d' e'>2.) }
BLXXIV = { | f,8( c8 <f a d' e'>2.) }
BLXXV = { | c,8( g,8 <g b d' e'>2.) \mbreak }

% measures 76 - 79
ALXXVI = { | a''4( e''2.) }
ALXXVII = { | d''4( g''2.) }
ALXXVIII = { | a''4( e''2.) }
ALXXIX = { | b''4( g''2.) \mbreak }
BLXXVI = { | f8( a8 <f a c' e'>2.) }
BLXXVII = { | g8( b8 <g b d'>2.) }
BLXXVIII = { | a8( c'8 <a c' e'>2.) }
BLXXIX = { | g8( b8 <g b d'>2.) \mbreak }

% measures 80 - 83
ALXXX = { | <a' a''>4( <e' e''>2.) }
ALXXXI = { | <d' d''>4( <g' g''>2.) }
ALXXXII = { | <c'' c'''>4( <g' g''>2.) }
ALXXXIII = { | <f' f''>4( <g' g''>2 <f' f''>4 \mbreak }
BLXXX = { | <f, f>8( a8 <f a c'>2.) }
BLXXXI = { | g,8( b8 <g b d'>2.) }
BLXXXII = { | aes,8( ees8 <aes c' ees'>2.) }
BLXXXIII = { | bes,8( f8 <bes d' f'>2.) \mbreak }

% measures 84 - 87
ALXXXIV = { | <g' c'' e'' g''>1~\< }
ALXXXV = { | <g' c'' e'' g''>2) <d' d''>4( <e' e''>4 }
ALXXXVI = { | <d'' d'''>4.\!\f <des'' des'''>16 <c'' c'''>16 <b' b''>4.~ <b' b''>16 <a' a''>16 }
ALXXXVII = { | <g' g''>2.) <d' d''>8( <e' e''>8 \mbreak \pgbreak }
BLXXXIV_LXXXVI = \Voices_BLXXXIV_BLXXXVI
BLXXXVII = { | <c, g,>8( e8 <c e g>2.) \mbreak \pgbreak }

% measures 88 - 91
ALXXXVIII = { | <d'' d'''>4~ <d'' d'''>16 des'''16 c'''16 <b' b''>16~ <b' b''>4. <c'' c'''>8 }
ALXXXIX = { | <b' b''>2.) <d' d''>8( <e' e''>8 }
AXC = { | <e'' e'''>4 <b' b''>2 <a' a''>8 <b' b''>8 }
AXCI = { | <e' e''>4 <d' d''>4 <e' e''>2) \mbreak }
BLXXXVIII = { | <f, c>8( a8 <f a c'>2.) }
BLXXXIX = { | <c, g,>8( e8 <c e g>2.) }
BXC = { | <f, c>8( a8 <f a c'>2.) }
BXCI = { | <c, g,>8( e8 <c e g>2.) \mbreak }

% measures 92 - 95
AXCII = { | <e'' e'''>4( <b' b''>2 <a' a''>8 <b' b''>8 }
AXCIII = \Voices_AXCIII
AXCIV = { | <b' b''>2\!\p }
AXCV = { | e''4(\mp b'2 a'8 b'8 \mbreak }
BXCII = { | <f, c>8( a8 <f a c'>2.) }
BXCIII = { | <c, g, e>1\arpeggio }
BXCIV = { | r2 }
BXCV = { | <b e' g'>4\arpeggio <g b e'>2.\arpeggio }

% measures 96 - 99
AXCVI = { | e'2 <fis a d'>2)\arpeggio }
AXCVII = { | <e'' e'''>4( <b' b''>2 a''8 b''8}
AXCVIII = { | e''2 d''2) }
AIC = { | <g'' e'''>4(\mf\< <e'' b''>2 <d'' a''>8 <e'' b''>8 \mbreak }
BXCVI = { | <e g b>2 <d, d>2\arpeggio }
BXCVII = { | c,8( g,8 <e g c'>2.)\arpeggio }
BXCVIII = { | d,8( a,8 <d fis a>2.)\arpeggio }
BIC = { | c,8( g,8 <e g c'>2.)\arpeggio }

% measures 100 - 104
AC = { | <g' e''>2\> <a' d''>2) }
ACI = { | <g'' e'''>4\<\mf( <e'' b''>2 <d'' a''>8 <e'' b''>8 }
ACII = { | <g' e''>2\> <a' d''>2) }
ACIII = { | \repeat tremolo 8 { <g''>16\fp\< <e'>16 } }
ACIV = { | \repeat tremolo 8 { <g''>16 <e'>16 } \mbreak }
BC = { | d,8( a,8 <d fis a>2.)\arpeggio } % TODO add more squiggly lines
BCI = { | c,8( g,8 <e g c'>2.)\arpeggio } % TODO add more squiggly lines
BCII = { | d,8( a,8 <d fis a>2.)\arpeggio } % TODO add more squiggly lines
BCIII = { | r1 }
BCIV = { | r1 }

% measures 105 - 108
ACV = { | <g b>8-.\!\mf^"Hyrule Field:" r8 r8 <g b>8-. r8 <g b>8-. <g b>8-. <g b>8-.}
ACVI = { | <g c'>4. <c'' e''>8~ <c'' e''>8 d''16 c''16 b'16 c''16 <g' b'>8~ }
ACVII = { | <g' b'>4. <g b>8 r8 <g b>8 <g b>8 <g b>8 }
ACVIII = { | <a d'>4. <d'' f''>8~ <d'' f''>8 e''16 d''16 c''16 d''16 <g' b'>8~ \mbreak \pgbreak }
BCV = { | <g, d>8-. r8 r8 <g, d>8-. r8 <g, d>8-. <g, d>8-. <d, d>8-. }
BCVI = \Voices_BCVI
BCVII = { | <g, d>8-. r8 r8 <g, d>8-. r8 <g, d>8-. <g, d>8-. <d, d>8-. }
BCVIII = \Voices_BCVIII

% measures 109 - 112
ACIX = { | <g' b' d''>2 <b d' g'>2 }
ACX = { | <b d' g'>4 <c' e' a'>4 <d' g' b'>4 <e' g' c''>4 }
ACXI = { | <g' c'' d''>1~ }
ACXII = { | <g' c'' d''>8 r8 r4 r4 \tuplet 7/4 {g'16\<( a'16 b'16 c''16 d''16 e''16 f''16} \mbreak }
BCIX = { | d,8-. r8 r8 d,8-. r8 d,8-. d,8-. r8 }
BCX = { | d,8-. r8 r8 d,8-. r8 d,8-. d,8-. r8 }
BCXI = { | c,8 r8 r8 c,8-. r8 c,8-. c,8-. r8 }
BCXII = { | c,8-. r8 r4 r2 \mbreak }

% measures 113 - 116
ACXIII = { | <g' g''>8->)\f r8 <d' g'>4( g8) r8 <b' g''>4 }
ACXIV = { | <a' f''>2 \tuplet 3/2 { <g' e''>4 <f' d''>4 <e' c''>4 } }
ACXV = { | <d' b'>16 <e' c''>16 <f' d''>4.~ <f' d''>2 }
ACXVI = { | <a' c'' f''>2 <b' d'' g''>4 \tuplet 7/4 { g'16( a'16 b'16 c''16 d''16 e''16 f''16 } \mbreak }
BCXIII = { | <g, d>8-. r8 r8 <g, d>8-. r8 <g, d>8-. <g, d>8-. r8 }
BCXIV = { | <f, c>8-. r8 r8 <f, c>8-. r8 <f, c>8-. <f, c>8-. r8 }
BCXV = { | <g, d>8-. r8 r8 <g, d>8-. r8 <g, d>8-. <g, d>8-. r8 }
BCXVI = { | <f, c>8-. r8 r8 <f, c>8-. r8 <f, c>8-. <f, c>8-. r8 \mbreak }

% measures 117 - 120
ACXVII = { | <g' g''>8)-> r8 <d' g'>4 g8 r8 <b' g''>4 }
ACXVIII = { | <a' f''>2 \tuplet 3/2 { <g' e''>4 <f' d''>4 <e' c''>4 } }
ACXIX = { | <e' c''>16 <f' d''>16 <d' b'>4. <b d' g'>16 a'16 <d' g' b'>4. }
ACXX = { | <f' a' c''>2 <e' g' c''>4 \tuplet 7/4 { g'16( a'16 b'16 c''16 d''16 e''16 f''16 } \mbreak }
BCXVII = { | <g, d>8-. r8 r8 <g, d>8-. r8 <g, d>8-. <g, d>8-. r8 }
BCXVIII = { | <f, c>8-. r8 r8 <f, c>8-. r8 <f, c>8-. <f, c>8-. r8 }
BCXIX = { | <g, d>8-. r8 r8 <g, d>8-. r8 <g, d>8-. <g, d>8-. r8 }
BCXX = { | <f, c>8-. r8 r8 <f, c>8-. r8 <f, c>8-. <f, c>8-. r8 \mbreak }

% measures 121 - 124
ACXXI = { | <g' g''>8)-> r8 <b d' g'>4( <d' g' b'>8) r8 <g' b' d'' g''>4( }
ACXXII = { | <f' a' c'' f''>8) r8 <c' f' a' c''>4( <f' a' c'' f''>8) r8 <a' c'' f'' a''>4 }
ACXXIII = { | <b' d'' g'' b''>1 }
ACXXIV = { | b''16( a''16 g''16 f''16) a''16( g''16 f''16 e''16) g''16( f''16 e''16 d''16) f''16( e''16 d''16 c''16) \mbreak }
BCXXI = { | <g, d>8-. r8 r8 <g, d>8-. r8 <g, d>8-. <g, d>8-. r8 }
BCXXII = { | <f, c>8-. r8 r8 <f, c>8-. r8 <f, c>8-. <f, c>8-. r8 }
BCXXIII = { | <g, d>8-. r8 r8 <g, d>8-. r8 <g, d>8-. <g, d>8-. r8 }
BCXXIV = { | <f, c>8-. r8 r8 <f, c>8-. r8 <f, c>8-. <f, c>8-. r8 \mbreak }

% measures 125 - 128
ACXXV = { | b'8 r8 <b d' g'>4( <d' g' b'>8) r8 <b' d'' g''>4( }
ACXXVI = { | <a' c'' f''>8) r8 <c'' f'' a''>2~ \tuplet 3/2 { <c'' f'' a''>8 g''8 f''8 } }
ACXXVII = { | e''16 f''16 <g' b' d''>4.~ <g' b' d''>2~ }
ACXXVIII = { | <g' b' d''>2. r4 \mbreak }
BCXXV = { | <g, d>8-. r8 r8 <g, d>8-. r8 <g, d>8-. <g, d>8-. r8 } 
BCXXVI = { | <f, c>8-. r8 r8 <f, c>8-. r8 <f, c>8-. <f, c>8-. r8 } 
BCXXVII = { | <g, d>8-. r8 r8 <g, d>8-. r8 <g, d>8-. <g, d>8-. r8 }
BCXXVIII = { | <f, c>8-. r8 r8 <f, c>8-. r8 <f, c>8-. <f, c>8-. r8 \mbreak }

% measures 129 - 132
ACXXIX = { | b'8->\f r8 d'2-> d''16 b'16 c''16 d''16 }
ACXXX = { | c''8-> r8 e'2.-> }
ACXXXI = { | b'8-> r8 d'2-> d''16 b'16 c''16 d''16 }
ACXXXII = { | e''8-> r8 a'2.-> \mbreak \pgbreak }
BCXXIX = { | r1 }
BCXXX = { | r1 }
BCXXXI = { | r1 }
BCXXXII = { | r1 \mbreak \pgbreak }

AHalf = {
  \tempo 4 = 150
  \clef treble
  \time 3/4
  
  \AI \AII \section \AIII \AIV
  \AV \AVI \AVII \AVIII
  \AIX \AX \AXI \AXII
  \AXIII \AXIV \AXV \AXVI
  \AXVII \AXVIII \clef bass \AXIX \AXX
  \AXXI \AXXII \clef treble \AXXIII \AXXIV
  \AXXV \AXXVI \AXXVII \AXXVIII
  \AXXIX \AXXX \section \AXXXI \AXXXII
  \AXXXIII \AXXXIV \AXXXV_XXXVIII \AXXXIX \AXL
  \AXLI \AXLII \AXLIII \AXLIV \AXLV
  \AXLVI \AXLVII \AXLVIII \AXLIX \AL
  \section \time 4/4 \tempo 4 = 80
  \ALI \ALII \ALIII \ALIV
  \ALV \ALVI \ALVII \ALVIII
  \clef bass \ALIX_LX \clef treble \ALXI \ALXII \ALXIII
  \section \tempo 4 = 65
  \ALXIV \ALXV \ALXVI \ALXVII
  \ALXVIII \ALXIX \ALXX \ALXXI
  \ALXXII \ALXXIII \ALXXIV \ALXXV
  \ALXXVI \ALXXVII \ALXXVIII \ALXXIX
  \ALXXX \ALXXXI \ALXXXII \ALXXXIII
  \ALXXXIV \ALXXXV \ALXXXVI \ALXXXVII
  \ALXXXVIII \ALXXXIX \AXC \AXCI
  \AXCII \AXCIII \time 2/4 \AXCIV \section \time 4/4 \AXCV
  \AXCVI \AXCVII \AXCVIII \AIC
  \AC \ACI \ACII \ACIII \ACIV
  \tempo 4 = 130 \ACV \ACVI \ACVII \ACVIII
  \ACIX \ACX \ACXI \ACXII
  \ACXIII \ACXIV \ACXV \ACXVI
  \ACXVII \ACXVIII \ACXIX \ACXX
  \ACXXI \ACXXII \ACXXIII \ACXXIV
  \ACXXV \ACXXVI \ACXXVII \ACXXVIII
  \ACXXIX \ACXXX \ACXXXI \ACXXXII
}

BHalf = {
  \clef treble
  \time 3/4

  \BI \BII \clef bass \section \BIII \BIV
  \BV \BVI \BVII \BVIII
  \BIX \BX \BXI_XIV \BXV \BXVI
  \BXVII \BXVIII \BXIX \BXX
  \BXXI \BXXII \BXXIII \BXXIV
  \BXXV \BXXVI \BXXVII_XXX \section \BXXXI \BXXXII
  \BXXXIII \BXXXIV \BXXXV \BXXXVI
  \BXXXVII \BXXXVIII \BXXXIX \BXL
  \BXLI \BXLII \clef treble \BXLIII \BXLIV \BXLV
  \BXLVI \clef bass \BXLVII \BXLVIII \BXLIX \BL
  \section \time 4/4
  \BLI \BLII \BLIII \BLIV
  \BLV \BLVI \BLVII \BLVIII
  \BLIX \BLX \BLXI \BLXII \BLXIII
  \section
  \BLXIV \BLXV \BLXVI \BLXVII
  \BLXVIII \BLXIX \BLXX \BLXXI
  \BLXXII \BLXXIII \BLXXIV \BLXXV
  \BLXXVI \BLXXVII \BLXXVIII \BLXXIX
  \BLXXX \BLXXXI \BLXXXII \BLXXXIII
  \BLXXXIV_LXXXVI \BLXXXVII
  \BLXXXVIII \BLXXXIX \BXC \BXCI
  \BXCII \BXCIII \time 2/4 \BXCIV \section \time 4/4 \BXCV
  \BXCVI \BXCVII \BXCVIII \BIC
  \BC \BCI \BCII \BCIII \BCIV
  \BCV \BCVI \BCVII \BCVIII
  \BCIX \BCX \BCXI \BCXII
  \BCXIII \BCXIV \BCXV \BCXVI
  \BCXVII \BCXVIII \BCXIX \BCXX
  \BCXXI \BCXXII \BCXXIII \BCXXIV
  \BCXXV \BCXXVI \BCXXVII \BCXXVIII
  \clef treble \BCXXIX \BCXXX \BCXXXI \BCXXXII
}

InstrumentAHalf = \new Staff \with {midiInstrument = "acoustic grand"} { 
  \AHalf
}

InstrumentBHalf = \new Staff \with {midiInstrument = "acoustic grand"} { 
  \BHalf
}

InstrumentCombined = {
  \new PianoStaff <<
    \InstrumentAHalf
    \InstrumentBHalf
  >>
}

\score {
  \InstrumentCombined
  \layout {
    % indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats \articulate {
      \InstrumentCombined
    }
  >>
  \midi { }
}
