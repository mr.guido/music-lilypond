\version "2.24.2"

\header{
  title = "Super Mario Bros Wii"
  subtitle = "Title"
  composer = "Composed by Ryo Nagamatsu"
  arranger = "Arranged by Sakiko Masuda"
  source = "Super Mario Jazz Piano Arrangements"
}

% TODO Add chords
% TOOD Add voltas

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = { }
pgbreak = { }

Voices_AXX_AXXI = {
  <<
  { \voiceOne
    | s2 s4 c''4~ \mbreak
    | c''1
  }
  \new Voice
  { \voiceTwo
    | <g' ees''>4. d''8 r4 <e' a'>4~ \mbreak
    | <e' a'>2 <ees' aes'>2
  }
  >>
  \oneVoice
}
Voices_BXX_BXXI = {
  <<
  { \voiceOne
    | s2 s4 c'4~ \mbreak
    | c'1
  }
  \new Voice
  { \voiceTwo
    | <a ees'>4 r4 r4 fis4~ \mbreak
    | fis2 f2
  }
  >>
  \oneVoice
}

% measures 1 - 4
AI = { | <e' c''>8 c''8 b'4-- <f' a'>4-- <g' b'>4-- }
AII = { | <g b e'>4-. r4 <a c' f'>4-. r4 }
AIII = { | <ais cis' fis'>4. <b d' g'>8-> r2 }
AIV = { | <c' e' a'>4-. <d' f' b'>4-. <e' g' c''>4-. r4 \mbreak }
BI = { | d4 aes4 g4 des4 }
BII = { | <c, c>4-. r4 <d, d>4-. r4 }
BIII = { | <dis, dis>4. <e, e>8-> r2  }
BIV = { | <f, f>4-. <g, g>4-. <a, a>4-. r4 \mbreak }

% measures 5 - 8
AV = { | <a c' e'>4. <b d'>8-> r2 }
AVI = { | <g b e'>4-. r4 <a c' f'>4-. r4 }
AVII = { | <ais cis' fis'>4. <b d' g'>8-> r2 }
AVIII = { | <c' e' a'>4-. <d' f' b'>4-. <e' g' c''>4-. r4 \mbreak }
BV = { | <d f>4. <f aes>8-> r2 }
BVI = { | <c, c>4-. r4 <d, d>4-. r4 }
BVII = { | <dis, dis>4. <e, e>8-> r2 }
BVIII = { | <f, f>4-. <g, g>4-. <a, a>4-. r4 \mbreak }

% measures 9 - 12
AIX = { | <f' a' c'' e''>4. <f' aes' b' d''>8-> r2 }
AX = { | <g b e'>4-. r4 <a c' f'>4-. r4 }
AXI = { | <ais cis' fis'>4. <b d' g'>8-> r2 }
AXII = { | <c' e' a'>4-- <d' f' b'>4-- <e' g' c''>2-- \mbreak }
BIX = { | d'4. g8-> r8 g8 g,4 }
BX = { | c,4-. c4-. d,4-. d4-. }
BXI = { | dis,4-. dis8-. e,8-> r8 e8 e,4 }
BXII = { | f4-- g4-- a4-- a,4-- \mbreak }

% measures 13 - 16
AXIII = { | <a e'>4.-- <aes d'>8--~ <aes d'>2 }
AXIV = { | <g' c''>4-. r4 <g' c''>4-. r8 <a' e''>8->~ }
AXV = { | <a' e''>4 d''4-. <aes' c''>4-- <f' a'>4-- }
AXVI = { | <f' a' c''>4. <f' aes' d''>8 r4 <e' g' c''>4-> \mbreak \pgbreak }
BXIII = { | <b, f>4.-- <bes, f>8--~ <bes, f>2 }
BXIV = { | <a e'>4-. r4 <g e'>4-. r8 <fis e'>8->~ }
BXV = { | <fis e'>2 <f ees'>4-- <ees des'>4-- }
BXVI = { | <d c'>4. <g b>8 r4 <c a>4-> \mbreak \pgbreak }

% measures 17 - 20
AXVII = { | r1 }
AXVIII = { | <d' e' g'>4. a'8 r4 c''4-. }
AXIX = { | r8 a'4.-> c''4-- d''4-- }
AXX_AXXI = \Voices_AXX_AXXI
BXVII = { | r8 g,4 g,8 a,4-. b,4-. }
BXVIII = { | <c g>4 r4 r2 }
BXIX = { | r8 <bes e'>4.-> r8 <bes e'>8 r4 }
BXX_BXXI = \Voices_BXX_BXXI

% measures 21 - 24
AXXII = { | <d' g' b'>2 g'4. <cis' f' bes'>8-> }
AXXIII = { | r8 a'4.-> g'4-. <cis' e'>4 }
AXXIV = { | <d' f'>4 g'8 a'8 r8 <c' f' aes' c''>4.-> \mbreak }
BXXII = { | <e a>2. r8 <a, g>8-> }
BXXIII = { | r4 r8 <a, g>8 <a, g>4 <g bes>4 }
BXXIV = { | <f a>4 r4 r8 <aes, ges>4.-> \mbreak }

% measures 25 - 28
AXXV = { | <b dis' g' b'>4---> a'8 r8 g'8[-. r8 f'8]-. r8 }
AXXVI = { | <d' e' g'>4. a'8 r4 c''4-- }
AXXVII = { | r8 a'4.-> c''4-- d''4-- }
AXXVIII = { | <g' c'' ees''>4. f''8 r4 <bes' d'' g''>4~ \mbreak }
BXXV = { | <g, f>4---> r4 r2 }
BXXVI = { | <c g>4 r4 r2 }
BXXVII = { | r8 <bes e'>4.-> r8 <bes e'>8 r4 }
BXXVIII = { | <a ees'>4 r4 r2 \mbreak }

% measures 29 - 32
AXXIX = { | <bes' d'' g''>1 }
AXXX = { | <c'' e'' a''>2 c''4. <aes' ees''>8-> }
AXXXI = { | r8 d''4.-> <g' c''>4-- <f' a'>4-- }
AXXXII = { | <f' a' c''>4. <f' aes' d''>8 r4 <e' g' c''>4-.-> \mbreak }
BXXIX = { | r8 <g f'>4. <c' e'>8[ r8 <c' e'>8] r8 }
BXXX = { | <fis c' e'>2. r8 <f c' ees'>8-> }
BXXXI = { | r4 r8 <f c' ees'>8 <e d'>4-- <ees des'>4-- }
BXXXII = { | <d c'>4. <g b>8 r4 <c g a>4-.-> \mbreak }

% measures 33 - 36
AXXXIII = { | r1 }
AXXXIV = { | <f'' g''>4-. <e'' g''>4-. r8 <ees'' g''>4.-> }
AXXXV = { | <d'' f'' g''>4-. <c'' f'' g''>4-. <b' f'' g''>4-. r4 }
AXXXVI = { | <f' a' c''>4. <f' aes' d''>8 r4 <e' g' c''>4-.-> \bar "|." }
BXXXIII = { | r8 e4.-> f4-- fis4-- }
BXXXIV = { | g4-. g4-. r8 g4.-> }
BXXXV = { | g4-. g4-. g4-. g,4-. }
BXXXVI = { | <d c'>4. <g b>8 r4 <c g a>4-.-> \bar "|." }


APart = {
  \tempo 4 = 220
  \clef treble
  \time 4/4

  \AI 
  \repeat volta 2 {
    \AII \AIII \AIV
    \AV \AVI \AVII \AVIII 
    \AIX \AX \AXI \AXII 
    \AXIII \AXIV \AXV \AXVI 
    \AXVII \AXVIII \AXIX \AXX_AXXI
    \AXXII \AXXIII \AXXIV 
    \AXXV \AXXVI \AXXVII \AXXVIII 
    \AXXIX \AXXX \AXXXI 
    \alternative {
      \volta 1 { 
        \AXXXII 
        \AXXXIII \AXXXIV \AXXXV 
      }
      \volta 2 {
        \AXXXVI 
      }
    }
  }
}

BPart = {
  \clef bass
  \time 4/4

  \BI 
  \repeat volta 2 {
    \BII \BIII \BIV 
    \BV \BVI \BVII \BVIII 
    \BIX \BX \BXI \BXII 
    \BXIII \BXIV \BXV \BXVI 
    \BXVII \BXVIII \BXIX \BXX_BXXI
    \BXXII \BXXIII \BXXIV 
    \BXXV \BXXVI \BXXVII \BXXVIII 
    \BXXIX \BXXX \BXXXI 
    \alternative {
      \volta 1 {
        \BXXXII 
        \BXXXIII \BXXXIV \BXXXV
      }
      \volta 2 {
        \BXXXVI 
      }
    }
  }
}

InstrumentAPart = \new Staff \with {midiInstrument = "acoustic guitar (steel)"} {
  \APart
}

InstrumentBPart = \new Staff \with {midiInstrument = "acoustic guitar (steel)"} {
  \BPart
}

InstrumentCombined = {
  \new PianoStaff <<
    \chords \with {midiInstrument = "acoustic guitar (steel)"} { 
      \set noChordSymbol = "(N.C.)"
      | d2:m7 g2:7
      \repeat volta 2 {
        | c4:maj7 r4 d4:m7 r4 | dis4.:m7 e8:m7 r2 | f4:maj7 g4:7 a4:m7 r4
        | d4.:m7 g8:7 r2 | c4:maj7 r4 d4:m7 r4 | dis4.:m7 e8:m7 r2 | f4:maj7 g4:7 a4:m7 r4
        | d4.:m7 g8:7 r2 | c4:maj7 r4 d4:m7 r4 | dis4.:m7 e8:m7 r2 | f4:maj7 g4:7 a2:m7
        | b4.:m7 bes8:7 r2 | a4:m7 r4 a4:m7 r8 fis8:m7 | r2 f4:m7 ees4:7 | d4.:m7 g8:7 r4 c4:6

        | c2:6 r2 | c4.:7 r8 r2 | r1 | f4.:7 r8 r4 fis4:m7~
        | fis2:m7 f2:m7 | e2:m7 r4. a8:7 | r2. g4:dim | d4:m r4. aes4.:7
        | g4:7 r2. | c4.:7 r8 r2 | r1 | f2:7 r4 g4:m7~
        | g2:m7 c2:7 | d2:7 r4. f8:m7 | r2 c4 ees4:7
        \alternative {
          \volta 1 {
            | d4.:m7 g8:7 r4 c4:6 | r1 | g2:7 r2 | r1
          }
          \volta 2 {
            | d4.:m7 g8:7 r4 c4:6
          }
        }
      }
    }
    \InstrumentAPart
    \InstrumentBPart
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}
