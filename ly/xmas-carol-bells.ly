\version "2.24.2"

\header{
  title = "Carol of the Bells"
  subtitle = "Ukrainian Christmas Carol"
}

\score {
  \new PianoStaff <<
    <<
      \chords \with {midiInstrument = "acoustic guitar (steel)"} { 
        \set noChordSymbol = ""
        a2.:m a:m7 f a:m
        r a:m7 f a:m
        f a:m d:m7 a:m
        d:m7 a:m d:m7 a:m
        r a:m6 a:m7 a:m6
        a:m r d:m a:m
        e:7 r2 a4:m e2.:7 r2 a4:m
        r2. d:m7 a:m d:m7 a:m
        r2. r r r r
      }
      \new Staff \with {midiInstrument = "acoustic guitar (steel)"} {
        \time 3/4
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4
        \break
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4
        \break
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4
        \break
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4
        \break

        e''4 d''8 e''8 c''4
        e''4 d''8 e''8 c''4
        e''4 d''8 e''8 c''4
        e''4 d''8 e''8 c''4
        \break
        a''4 a''8 a''8 {g''8 (f''8)}
        e''4 e''8 e''8 {d''8 (c''8)}
        d''4 d''8 d''8 {e''8 (d''8)}
        a'4 a'8 a'8 a'4
        \break
        {e'8 (fis'8 gis'8 a'8 b'8 c''8)}
        {d''8 (e''8) d''4 c''4}
        {e'8 (fis'8 gis'8 a'8 b'8 c''8)}
        {d''8 (e''8) d''4 c''4}
        % \break
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4
        % \break
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4
        c''4 b'8 c''8 a'4\fermata
        e''4 {d''8 (e''8\fermata)} a'4~
        a'2.
      }
      \addlyrics { 
        Hark to the bells, hark to the bells, tell- ing us all Je- sus is King!
        Strong- ly they chime, sound with a rhyme, Chris- mas is here! Wel- come the King.
        Hark to the bells, hark to the bells, this is the day, day of the King!
        Peal out the news o'er hill and dale, and 'round the town tell- ing the tale.

        Hark to the bells, hark to the bells, tell- ing us all Je- sus is King!
        Come, one and all, hap- pi- ly sing songs of good will. Oh, let them sing!
        Ring, sil- v'ry bells. Sing, joy- ous bells!
        Strong- ly they chime, sound with a rhyme, Christ- mas is here. Wel- come the King.
        Hark to the bells, hark to the bells, tell- ing us all Je- sus is King! Ring! Ring! bells.
      }
    >>
  >>
  \layout {
    indent = 0.0
  }
  \midi {
    \tempo 4 = 140
  }
}
