\version "2.20.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "The Legend of Zelda: A Link to the Past"
  subtitle = "The Dark World"
  composer = "Koji Kondo"
}

\score {
  \new Staff \with {midiInstrument = "acoustic guitar (steel)"} <<
  { \clef treble 
    \repeat volta 2 {
      r8\mp <c' e'>8 <c' e'>8 <c' e'>16 <c' e'>16 <c' e'>8 <d' fis'>4 r8
      r8 <e' g'>8 <e' g'>8 <e' g'>16 <e' g'>16 <e' g'>8 <fis' a'>4 r8
      r8 <c' e'>8 <c' e'>8 <c' e'>16 <c' e'>16 <c' e'>8 <d' fis'>4 r8
      r8 <e' g'>8 <e' g'>8 <e' g'>16 <e' g'>16 <e' g'>8 <fis' a'>8 r8 a\mf
      \break
      <c' e'>2~ <c' e'>8 a8 e'8 a'8
      fis'16 g'16 <d' fis'>8~ <d' fis'>2 r8. d'16
      <c' e'>4 a2 r8 d'8
      b16 c'16 <b g>8~ <b g>2 r8. g16
      \break
      \alternative {
        \volta 1 { 
          a2. r8 a'16 e''16
          b'2. r8 a8 
        }
        \volta 2 { 
          a2.  d''16 c''16 b'16 a'16
          \break
          b'2. r8 e''16\f fis''16
          <e'' g''>2. e''8 g''8
          fis''16 e''16 d''8~ d''2 r8 c''16 d''16
          <c'' e''>2. a'8 e''8
          \break

          d''16 c''16 b'8~ b'2 r8 e''16 fis''16
          <e'' g''>2. e''8 g''8
          fis''16 g''16 a''8~ a''2.
          <e'' g''>2. fis''8 g''8
          \break
          fis''16 eis''16 fis''8~ fis''2.
          e''1
          r1
          a'16\mf e'16 d'16 e'16~ e'2 a'4
          \break
          g'16 e'16 d'16 e'16~ e'2.
          g'16 d'16 c'16 d'16~ d'2 g'4
          f'16 c'16 b16 c'16~ c'2.
          \break
          e'16 b16 a16 b16~ b2 e'4
          a'16 e'16 d'16 e'16~ e'2 <e' a'>4
          <dis' b'>1~\f
          <d' b'>2 r2
        }
      }
    }
  }
  
  \new Staff \with {midiInstrument = "acoustic guitar (steel)"} 
  { \clef bass 
    \repeat volta 2 {
      a,4 a,4 a,4 r8 a,8
      a,4 a,4 a,4 r8 a,8
      a,4 a,4 a,4 r8 a,8
      a,4 a,4 a,4 r8 a,8
      \break
      a,4 a,4 a,4 a,4
      d4 d4 d4 d4
      f,4 f,4 f,4 f,4
      g,4 g,4 g,4 g,4
      \break
      \alternative {
        \volta 1 { 
          a,8 <c e>8 <c e>8 <c e>8 <c e>8 <d fis>8 r8 a,8
          a,8 <e g>8 <e g>8 <e g>8 <e g>8 <fis a>8 r8 a,8
        }
        \volta 2 {
          a,8 <c e>8 <c e>8 <c e>8 <c e>8 <d fis>8 r8 a,8
          \break
          a,8 <e g>8 <e g>8 <e g>8 <e g>8 <fis a>8 r8 a,8
          <c g>8 <c g>8 r8 <c g>8 <c g>8 <c g>8 r8 <c g>8
          <d fis>8 <d fis>8 r8 <d fis>8 <d fis>8 <d fis>8 r8 <d fis>8
          <f a>8 <f a>8 r8 <f a>8 <f a>8 <f a>8 r8 <f a>8
          \break

          <g f>8 <g f>8 r8 <g f>8  <g f>8 <g f>8 r8 <g f>8
          <c g>8 <c g>8 r8 <c g>8  <c g>8 <c g>8 r8 <c g>8
          <d fis>8 <d fis>8 r8 <d fis>8 <d fis>8 <d fis>8 r8 <d fis>8
          <cis f>8 <cis f>8 r8 <cis f>8 <cis f>8 <cis f>8 r8 <cis f>8
          \break
          <b, fis>8 <b, fis>8 r8 <b, fis>8 <b, fis>8 <b, fis>8 r8 <b, fis>8
          e8 <g b>8 <g b>8 <g b>8 <a cis'>8 <a cis'>8 r8 e8
          e8 <g b>8 <g b>8 <g b>8 <a cis'>8 <a cis'>8 r8 e8
          <f a>4 r8 <f a>8~ <f a>8 <f a>8 <f a>4
          \break
          <e g>4 r8 <e g>8~ <e g>8 <e g>8 <e g>4
          <dis fis>4 r8 <dis fis>8~ <dis fis>8 <dis fis>8 <dis fis>4
          <d f>4 r8 <d f>8~ <d f>8 <d f>8 <d f>4
          \break
          <des f>4 r8 <des f>8~ <des f>8 <des f>8 <des f>4
          <c e>4 r8 <c e>8~ <c e>8 <c e>8 <c e>4
          <b, a>4 r8 <b, a>8~ <b, a>8 <b, a>8 <b, a>4
          <e a>4 r8 <e a>8~ <e a>8 <e gis>8 <e gis>4
        }
      }
    }
  }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 137
  }
}
