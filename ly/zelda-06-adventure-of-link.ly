\version "2.24.2"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Zelda II™: The Adventure of Link™"
  subtitle = "TITLE THEME"
  composer = "Composed by Akito Nakatsuka"
  arranger = "Piano arrangement by Shinobu Amayake"
  poet = "Music supervision by Nintendo"
}

Treble = {
  \tempo 4 = 100
  \clef treble
  \key f \major
  \time 4/4
  
  % measures 1-4
  bes''8\pp\< a''8 f''8 c''8 bes''8 a''8 f''8 c''8
  | bes''8 a''8 f''8 c''8 bes''8 a''8 f''8 c''8
  | bes''8 a''8 f''8 c''8 bes''8 a''8 f''8 c''8
  | bes''8 a''8 f''8 c''8 bes''8 a''8 f''8 c''8\!

  \repeat volta 2 {
    % measures 5-8
    \break
    | <a' c''>2~\mf <a' c''>8 a'4 c''8
    | <g' bes'>2. r4
    | <f' aes'>2~ <f' aes'>8 f'4 a'8
    | <e' g'>2. r4

    % measures 9-12
    \break
    | <a' c''>2~ <a' c''>8 f''4 ees''8
    | <g' ees''>2. r4
    | <f' des''>2~ <f' des''>8 aes'4 des''8
    | <e' c''>4. g'8~ g'4 e'4

    % measures 13-16
    \break
    | <f' c''>4. <f' c''>4 <f' c''>4 r8
    | <ges' des''>4. <ges' des''>4 <ges' des''>4 r8
    | <g' d''>4. <g' d''>4 <g' d''>4 r8
    | <aes' ees''>4 r8 <bes' e''>8~ <bes' e''>4 r4

    % measures 17-20
    \break
    | r4 bes'4 a'8 f'4 c'8~
    | c'8 a4. bes4 c'4
    | ees'4 d'4 c'4 bes4
    | c'8 bes4 a8~ a2

    % measures 21-24
    \break
    | r4 bes'4 a'8 f'4 c'8~
    | c'8 a4. bes4 c'4
    | ees'4 d'4 c'4 bes4
    | c'8 bes4 a8~ a2

    % measures 25-28
    \break
    | r4 des'8 ees'8 f'8 aes'4 g'8~
    | g'8 e'4 c''8~ c''8 e'8 g'4
    | r4 des'8 ees'8 f'8 aes'4 bes'8~
    | bes'8 aes'4 g'8~ g'2

    % measures 29-32
    \break
    | r4 des'8 ees'8 f'8 aes'4 g'8~
    | g'8 e'4 c''8~ c''8 e'8 g'4
    | <f' bes'>4. <f' aes'>8~ <f' aes'>8 <f' bes'>4.
    | <e' c''>1
  }
}

Bass = {
  \tempo 4 = 100
  \clef bass
  \key f \major
  \time 4/4

  % measures 1-4
  r1
  | r1
  | r1
  | r1
  
  \repeat volta 2 {
    % measures 5-8
    \break
    | f2~ f8 c4 f8
    | ees2. c4
    | des4. f8~ f4 aes4
    | g4. e8~ e4 c4

    % measures 9-12
    \break
    | f2~ f8 c4 f8
    | ees2. c4
    | des4. f8~ f4 aes4
    | g4. e8~ e4 c4

    % measures 13-16
    \break
    | <f a>4 r8 <f a>8 r8 <f a>4 <f a>8
    | <f bes>4 r8 <f bes>8 r8 <f bes>4 <f bes>8
    | <f b>4 r8 <f b>8 r8 <f b>4 <f b>8
    | <f c'>4 r8 c8 r8 c8 e8 g8

    % measures 17-20
    \break
    | <f, c>4 r8 <f, c>8 r8 <f, c>4 <f, c>8
    | <f, c>4 r8 <f, c>8 r8 <f, c>4 <f, c>8
    | <ees, bes,>4 r8 <ees, bes,>8 r8 <ees, bes,>4 <ees, bes,>8
    | <f, c>4 r8 <f, c>8 r8 <f, c>4 <f, c>8

    % measures 21-24
    \break
    | <f, c>4 r8 <f, c>8 r8 <f, c>4 <f, c>8
    | <f, c>4 r8 <f, c>8 r8 <f, c>4 <f, c>8
    | <ees, bes,>4 r8 <ees, bes,>8 r8 <ees, bes,>4 <ees, bes,>8
    | <f, c>4 r8 <f, c>8 r8 <f, c>4 <f, c>8

    % measures 25-28
    \break
    | des4 des4 f4 aes4
    | c4 e4 g4 e4
    | des4 des4 f4 aes4
    | c4 e4 g4 e4

    % measures 29-32
    \break
    | des4 des4 f4 aes4
    | c4 e4 g4 e4
    | des4 des4 f4 aes4
    | c4 e4 g4 e4
  }
}

AcousticGrandTreble = \new Staff \with {midiInstrument = "acoustic grand"} { 
  \Treble
}

AcousticGrandBass = \new Staff \with {midiInstrument = "acoustic grand"} { 
  \Bass
}

AcousticGrandCombined = {
  \new PianoStaff <<
    \AcousticGrandTreble
    \AcousticGrandBass
  >>
}

\score {
  \AcousticGrandCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \AcousticGrandCombined
    }
  >>
  \midi {
    \tempo 4 = 100
  }
}
