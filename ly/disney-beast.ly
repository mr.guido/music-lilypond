\version "2.24.2"

\header{
  title = "Beauty And The Beast"
  composer = "Music by Alan Menken"
  arranger = "Arranged by Mac Huff"
  source = "online"
}

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = { \break }
pgbreak = { \pageBreak }

Voices_DI_DIV = {
  <<
  { \voiceOne
    | r8 c8 c2.
    | r8 d8 f2 e4
    | r8 c8 c2 c4
    | r8 d8 f2 e4
  }
  \new Voice
  { \voiceTwo
    | f,1
    | f,1
    | f,1
    | f,1
  }
  >>
  \oneVoice
}
Voices_DV_DVIII = {
  <<
  { \voiceOne
    | r8 c8 c2.
    | r8 d8 f2 e4
    | r8 c8 c2 c4
    | r8 d8 f2 e4
  }
  \new Voice
  { \voiceTwo
    | f,1
    | f,1
    | f,1
    | f,1
  }
  >>
  \oneVoice
}
Voices_DIX_XII = {
  <<
  { \voiceOne
    | r8 c8~ c2 c4
    | r8 g8 f8 g8 c'4 e4
    | r8 f8 bes8 f8 r8 f8 bes8 f8
    | r8 f8 g8 bes8~ bes8 f8 r4
  }
  \new Voice
  { \voiceTwo
    | f,1
    | a,1
    | bes,1
    | c2. <c g>4
  }
  >>
  \oneVoice
}

% measures 1 - 4
AI = { | r1 }
AII = { | r1 }
AIII = { | r1 }
AIV = { | r1 \mbreak }
BI = { | r1 }
BII = { | r1 }
BIII = { | r1 }
BIV = { | r1 \mbreak }
CI = { | r4 g8 a8~ a8 c'8 e'8 f'8 }
CII = { | bes4. bes8 a'8 bes'8 g'8 a'8 }
CIII = { | <a f'>4 g8 a8~ a8 c'8 e'8 f'8 }
CIV = { | bes4. bes8 <c' a'>8 <d' bes'>8 <bes g'>8 <c' a'>8 \mbreak }
DI_DIV = \Voices_DI_DIV

% measures 5 - 8
AV = { | r2 a8 c'8 e'8. f'16 }
AVI = { | bes2. r4 }
AVII = { | r2 a8. c'16 e'8 f'8 }
AVIII = { | g'1 \mbreak }
BV = { | r1 }
BVI = { | r1 }
BVII = { | r1 }
BVIII = { | r1 \mbreak }
CV = { | f'4 g8 a8~ a8 c'8 e'8. f'16 }
CVI = { | bes4. bes8 a'8 bes'8 <c' g'>8 a'8 }
CVII = { | <a f'>4 g8 a8~ a4 e'8 f'8 }
CVIII = { | <bes g'>4. bes8 a'8 bes'8 <c' g'>8 a'8 \mbreak }
DV_DVIII = \Voices_DV_DVIII

% measures 9 - 12
AIX = { | r2 f'8 g'8 a'8 bes'8 }
AX = { | c''2 c''8. bes'16 a'8 g'8 }
AXI = { | f'2 bes'8 a'8 g'8 f'8 }
AXII = { | d'8( c'4.~ c'2) \mbreak \pgbreak }
BIX = { | r2 f'8 g'8 a'8 bes'8 }
BX = { | c''2 c''8. bes'16 a'8 g'8 }
BXI = { | f'2 bes'8 a'8 g'8 f'8 }
BXII = { | d'8( c'4.~ c'2) \mbreak \pgbreak }
CIX = { | <g a f'>4 g8 a8~ a8 g'8 a'8 bes'8 }
CX = { | <e' c''>2. <c' a'>8 g'8 }
CXI = { | <c' d' f'>2 <c' d' f' bes'>4 a'8 g'8 }
CXII = { | <d' f'>4 c'8 d'8 f'4 e'4 \mbreak \pgbreak }
DIX_XII = \Voices_DIX_XII

% measures 13 - 15
AXIII = { | r1 }
AXIV = { | r1 }
AXV = { | r2 a8 c'8 e'8 f'8 \mbreak }
BXIII = { | r2 a8. c'16 e'8 f'8 }
BXIV = { | bes2. r4 }
BXV = { | r1 \mbreak }
CXIII = { | <g a c' f'>4\arpeggio <g' a'>8 c'8 <f' a'>8 c'8 <e' a'>8 f'8 }
CXIV = { | d'8 bes8 f'8 <a' f''>8 <g' e''>8. <e' c''>16~ <e' c''>8 <c' g'>8 }
CXV = { | <g' a'>8 c'8 <f' a'>8 c'8 <f' a'>8 c'8 <f' a'>8 c'8 \mbreak }
DXIII = { | f,4. f,8 f,2 }
DXIV = { | f,4. f,8 f,4~ f,16 f8 c16 }
DXV = { | f,4. f,8 f,2 \mbreak }

% measures 16 - 18
AXVI = { | g'2 a'8 g'8 a'8 c''8 }
AXVII = { | f'2 f'8 e'8 f'8 a'8 }
AXVIII = { | bes2 a'8 bes'8 g'8 a'8 \mbreak }
BXVI = { | r2 a'8 g'8 a'8 c''8 }
BXVII = { | f'2 f'8 e'8 f'8 a'8 }
BXVIII = { | bes2 c'8 d'8 bes8 c'8 \mbreak }
CXVI = { | <ees' g'>8 bes8 <ees' g'>8 bes8 <ees' a'>8 c'8 <f' c''>8 c'8 }
CXVII = { | <c' d' f'>8 f8 <c' d' f'>8 f8 <c' d' f'>8 bes8 <f' c''>8 c'8 }
CXVIII = { | <d' f' bes'>8 bes8 <d' f' bes'>8 bes8 <c' a'>8 <d' bes'>8 <bes g'>8 <c' a'>8~ \mbreak }
DXVI = { | f,4. c8 f4 a,4 }
DXVII = { | bes,4. bes,8 bes,4 a,4 }
DXVIII = { | g,4. g,8 c2 \mbreak }

% measures 19 - 22
AXIX = { | f'1 }
AXX = { | r2 f'8 g'8 a'8 bes'8 }
AXXI = { | c''1 }
AXXII = { | r2 f'8 g'8 a'8 bes'8 \mbreak \pgbreak }
BXIX = { | a1 }
BXX = { | r2 f'8 g'8 a'8 g'8 }
BXXI = { | a'1 }
BXXII = { | r2 f'8 g'8 a'8 g'8 \mbreak \pgbreak }
CXIX = { | <c' a'>8 c'8 <a f'>4 <c'' a''>8 <d'' bes''>8 <bes' g''>8 <c'' a''>8 }
CXX = { | <a' f''>2 f'8 g'8 <f' a'>8 <d' bes'>8 }
CXXI = { | <e' g' c''>8 c'8 <e' g' c''>8 c'8 <e' g' c''>8 c'8 f''4-- }
CXXII = { | <bes' e''>8-- f'8 <bes' d''>8-- f'8~ f'8 g'8 <f' a'>8 <d' bes'>8 \mbreak \pgbreak }
DXIX = { | f,4. c8 f2 }
DXX = { << { \voiceOne | r8 c8 a8 c8 a4 f16 f16 g,8 } \new Voice { \voiceTwo | f,2. s4 } >> \oneVoice }
DXXI = { | a,4. a,8 a,4. a,8 }
DXXII = { | bes,4. bes,8 bes,2 \mbreak \pgbreak }

% measures 23 - 25
AXXIII = { | c''1 }
AXXIV = { | r2 f'8 g'8 a'8 bes'8 }
AXXV = { | c''4 r8 c''8 c''8 bes'8 a'8 g'8 \mbreak }
BXXIII = { | a'1 }
BXXIV = { | r2 f'8 g'8 a'8 bes'8 }
BXXV = { | c''4 r8 c''8 c''8 bes'8 a'8 g'8 \mbreak }
CXXIII = { | <e' g' c''>8 c'8 <e' g' c''>8 c'8 <e' g' c''>8 c'8 e'16 d'16 c'8 }
CXXIV = { | <c'' d''>8 f'8 <bes' d''>8 f'8~ f'8 g'8 <f' a'>8 <d' bes'>8 }
CXXV = { | <e' g' c''>8 c'8 <e' g' c''>8 c'8 <e' c''>8 bes'8 <c' a'>8 c''8 \mbreak }
DXXIII = { | a,4. a,8 a,4. a,8 }
DXXIV = { | bes,4. bes,8 bes,2 }
DXXV = { | a,4. a,8 a,2 \mbreak }

% measures 26 - 28
AXXVI = { | f'4 r4 f'8 g'8 a'8 f'8 }
AXXVII = { | g'1 }
AXXVIII = { | g'8.( f'16) bes'8.( a'16) c''2~ \mbreak }
BXXVI = { | f'4 r4 f'8 g'8 a'8 f'8 }
BXXVII = { | g'1 }
BXXVIII = { | g'8. f'16 bes'8. a'16 c''2~ \mbreak }
CXXVI = { | <d' f' a'>8 a8 <d' f' a'>8 a8 f'8 <bes g'>8 <c' a'>8 <a f'>8 }
CXXVII = { | <bes ees' g'>8 g8 <ees' g'>8 bes8 <ees' g' bes'>8 bes8 <ees' g' bes'>8 bes8 }
CXXVIII = { | <d' f' g'>8 bes8 <d' f' bes'>8 bes8 <f' g' c''>8 c'8 <e' g' c''>8 c'8 \mbreak }
DXXVI = { | d4. a,8 d,4. d,8 }
DXXVII = { | ees,4. ees,8 ees,4 ees,8 d,8 }
DXXVIII = { | c,4. c,8 c,8 c8 c8 c,8 \mbreak }

% measures 29 - 32
AXXIX = { | c''1 }
AXXX = { | r1 }
AXXXI = { | r1 }
AXXXII = { | r1 \mbreak \pgbreak }
BXXIX = { | c''1 }
BXXX = { | r1 }
BXXXI = { | r1 }
BXXXII = { | r1 \mbreak \pgbreak }
CXXIX = { | <g' a' c''>8 c'8 <f' a' c''>8 c'8 a'8-- c''8-- e''8-- f''8-- }
CXXX = { | <d' bes'>4-- <d' f' bes'>8 bes8 <d' f' bes'>8 bes8 <e' g' c''>8 c'8 }
CXXXI = { | <g' a' c''>8 c'8 <f' a' c''>8 c'8 a'8-- c''8-- e''8-- f''8-- }
CXXXII = { | <bes' d'' g''>8-- g'8 <bes' d'' g''>8 g'8 <bes' d'' g''>8 g'8 <a' e'' a''>4 \mbreak \pgbreak }
DXXIX = { | f,4. f,8 f,2 }
DXXX = { | f,4. f,8 f,4 f8 c8 }
DXXXI = { | f,4. f,8 f,2 }
DXXXII = { | f,4. f,8 f,4 <cis, cis>4 \mbreak \pgbreak }

% measures 33 - 35
AXXXIII = { | d''1( }
AXXXIV = { | c''2~ c''4. a'16 g'16 }
AXXXV = { | f'1) \mbreak }
BXXXIII = { | d''1( }
BXXXIV = { | c''2~ c''4. a'16 g'16 }
BXXXV = { | f'1) \mbreak }
CXXXIII = { | <a' d'' f''>8 f'8 <a' d'' f''>8 f'8 <d'' f'' a''>8 a'8 <a' d'' f''>8 f'8 }
CXXXIV = { | <g' c'' e''>8 e'8 <g' c'' e''>8 e'8 <e' g' c''>8 c'8 <g' c'' e''>8 e'8 }
CXXXV = { | <c'' d''>8 f'8 <bes' d''>8 g'8 f'8 g'8 a'8 bes'8 \mbreak }
DXXXIII = { | d,4. d,8 d,2 }
DXXXIV = { | a,4. a,8 a,4. a,8 }
DXXXV = { | bes,4. bes,8 bes,4 bes,16 g,16 f,8 \mbreak }

% measures 36 - 38
AXXXVI = { | r2 f'8 g'8 a'8 bes'8 }
AXXXVII = { | c''1 }
AXXXVIII = { | r2 d''8 c''8 c''8 a'8 \mbreak }
BXXXVI = { | r2 f'8 g'8 a'8 g'8 }
BXXXVII = { | a'1 }
BXXXVIII = { | r2 d''8 c''8 c''8 a'8 \mbreak }
CXXXVI = { | <f' bes' c''>8 c'8 <f' bes' c''>8 c'8 <bes d' f' a'>4 <g' c'' e''>4 }
CXXXVII = { | <e' g' c''>8 c'8 <e' g' c''>8 c'8 <e' g' c''>8 c'8 <e' g' c''>8 f''8-- }
CXXXVIII = { | <bes' e''>8-- f'8 <bes' d''>8-- f'8 <bes' d''>8 f'8 d''16 f'16 c''16 a'16 \mbreak }
DXXXVI = { | c4. g,8 c,4 bes,4 }
DXXXVII = { | a,4. a,8 a,4. a,8 }
DXXXVIII = { | bes,4. bes,8 bes,2 \mbreak }

% measures 39 - 41
AXXXIX = { | c''1 }
AXL = { | r2 f'8 g'8 a'8 bes'8 }
AXLI = { | c''4 r8 c''8 c''8 bes'8 a'8 g'8 \mbreak \pgbreak }
BXXXIX = { | a'1 }
BXL = { | r2 f'8 g'8 a'8 bes'8 }
BXLI = { | c''4 r8 c''8 c''8 bes'8 a'8 g'8 \mbreak \pgbreak }
CXXXIX = { | <e' g' c''>8 c'8 <e' g' c''>8 c'8 <e' g' c''>8 c'8 <e' g' c''>8 f'8 }
CXL = { | e'8 bes8 d'8 bes8 f'8 g'8 <f' a'>8 <d' bes'>8 }
CXLI = { | <e' g' c''>8 c'8 <e' g' c''>8 c'8 <e' c''>8 bes'8 <c' a'>8 c''8 \mbreak \pgbreak }
DXXXIX = { | a,4. a,8 a,4. a,8 }
DXL = { | bes,4. f,8 bes,,4. bes,8 }
DXLI = { | a,4. a,8 a,2 \mbreak \pgbreak }

% measures 42 - 44
AXLII = { | f'4 r4 f'8 g'8 a'8 f'8 }
AXLIII = { \time 3/4 g'2 r4 }
AXLIV = { \time 5/4 g'4 r8 aes'8~ aes'4 bes'4 r4 \mbreak }
BXLII = { | f'4 r4 f'8 g'8 a'8 f'8 }
BXLIII = { \time 3/4 g'2 r4 }
BXLIV = { \time 5/4 g'4 r8 aes'8~ aes'4 g'4 r4 \mbreak }
CXLII = { | <d' f' a'>8 a8 <d' f' a'>8 a8 f'8 <bes g'>8 <c' a'>8 <a f'>8 }
CXLIII = { \time 3/4 <bes ees' g'>8 g8 <bes ees' g'>8 g'16 a'16 <ees' g' bes'>4-^ }
CXLIV = { \time 5/4 g'8-> bes8 ees'8 aes'8-> c'8 ees'8 <ees' g' bes'>4-> s4 \mbreak }
DXLII = { | d4. a,8 d,4. d,8 }
DXLIII = { \time 3/4 ees,4. ees,8 ees,4 }
DXLIV = { \time 5/4 <des, des>4.-> <c, c>8->~ <c, c>4 <bes,, bes,>4-> <ees, ees>16 <des, des>16 <bes,, bes,>8-^ \mbreak }

% measures 45 - 47
AXLV = { \time 4/4 \key aes \major r2 c'8 ees'8 g'8. aes'16 }
AXLVI = { | des'1 }
AXLVII = { | r2 c''8 bes'16 bes'16 bes'8 c''8 \mbreak }
BXLV = { \time 4/4 \key aes \major r2 c'8 ees'8 g'8. aes'16 }
BXLVI = { | des'1 }
BXLVII = { | r2 c''8 bes'16 bes'16 bes'8 c''8 \mbreak }
CXLV = { \time 4/4 \key aes \major <ees' bes' c''>8 c'8 <ees' bes' c''>8 c'8 <ees' aes' c''>8 c'8 <ees' aes' c''>8 c'8 }
CXLVI = { | <f' aes' des''>8 des'8 <f' aes' des''>8 aes''8 <bes' g''>8. <g' ees''>16~ <g' ees''>8 <ees' bes'>8 }
CXLVII = { | <ees' bes' c''>8 c'8 <ees' bes' c''>8 c'8 <ees' aes' c''>8 c'8 <ees' aes' c''>8 c'8 \mbreak }
DXLV = { \time 4/4 \key aes \major aes,4. aes,8 aes,4. aes,8 }
DXLVI = { | aes,4. aes,8 aes,4~ aes,16 aes8 ees16 }
DXLVII = { | aes,4. aes,8 aes,4. aes,8 \mbreak }

% measures 48 - 50
AXLVIII = { | aes'2 r2 }
AXLIX = { | r2 aes'8 bes'8 c''8 des''8 }
AL = { | ees''2 ees''8 des''8 c''8 bes'8 \mbreak \pgbreak }
BXLVIII = { | aes'2 r2 }
BXLIX = { | r2 aes'8 bes'8 c''8 bes'8 }
BL = { | c''2 c''8 bes'8 aes'8 g'8 \mbreak \pgbreak }
CXLVIII = { | <f' aes' des''>8 des'8 <f' aes' des''>8 aes''8 <bes' g''>8. <g' ees''>16~ <g' ees''>8 <f' des''>8 }
CXLIX = { | <ees' aes' c''>8 c'8 <ees' aes' c''>8 c'8 <ees' aes' c''>8 c'8 <aes' c''>8-> <bes' des''>8-> }
CL = { | <aes' c'' ees''>8 ees'8 <aes' c'' ees''>8 ees'8 <g' bes' ees''>8 e'8 <g' c''>8 bes'8 \mbreak \pgbreak }
DXLVIII = { | aes,4. aes,8 aes,4. ees,8 }
DXLIX = { | aes,4. aes,8 aes,4 f,8-> fes,8-> }
DL = { | ees,4. ees,8 c,4. c8 \mbreak \pgbreak }

% measures 51 - 53
ALI = { | aes'2 des''8. c''16 bes'8 aes'8 }
ALII = { | aes'2( g'2) }
ALIII = { \bar "||" \key f \major r1 \mbreak }
BLI = { | f'2 des''8. c''16 bes'8 aes'8 }
BLII = { | ees'1 }
BLIII = { \bar "||" \key f \major r2 a8 c'8 e'8. f'16 \mbreak }
CLI = { | <f' aes' c''>8 c'8 <f' aes' c''>8 c'8 <f' aes' des''>8 des'8 <f' aes' des''>8 des'8 }
CLII = { | <aes' bes' ees''>8 ees'8 <aes' bes' ees''>8 aes''8 <bes' g''>8. <g' ees''>16~ <g' ees''>8 des''8 }
CLIII = { \bar "||" \key f \major <g' a' c''>4\arpeggio <g' a'>8 c'8 <f' a'>8 c'8 <e' a'>8 f'8 \mbreak }
DLI = { | f,4. ees,8 des,4. des,8 }
DLII = { | ees,4. ees,8 ees,4. ees,8 }
DLIII = { \bar "||" \key f \major f,4. f,8 f,2 \mbreak }

% measures 54 - 56
ALIV = { | r2 a'8 g'8 g'8. a'16 }
ALV = { | f'2 r2 }
ALVI = { | r2 a'8 g'8 a'8. c''16 \mbreak }
BLIV = { | bes16( a16 bes8~ bes4) r2 }
BLV = { | r2 a8 c'8 e'8 f'8 }
BLVI = { | g'2 a'8 g'8 a'8. c''16 \mbreak }
CLIV = { | d'8 bes8 f'8 <a' f''>8 <g' e''>8 d''16 c''16~ c''8 bes'8 }
CLV = { | <g' a'>8 c'8 <g' a'>8 c'8 <f' a'>8 c'8 e'8 f'8 }
CLVI = { | <ees' g'>8 bes8 <ees' g'>8 bes8 <ees' a'>8 c'8 <f' c''>8 c'8 \mbreak }
DLIV = { | f,4. f,8 f,4. f,8 }
DLV = { | f,4. f,8 f,2 }
DLVI = { | f,4. c8 f4 a,4 \mbreak }

% measures 57 - 60
ALVII = { | f'2 f'8 e'8 f'8 a'8 }
ALVIII = { | bes2 a'8 bes'8 g'8 a'8 }
ALIX = { | f'1 }
ALX = { | r1 \mbreak \pgbreak }
BLVII = { | f'2 f'8 e'8 f'8 a'8 }
BLVIII = { | bes2 c'8 d'8 bes8 c'8 }
BLIX = { | a1 }
BLX = { | r2 a'8 g'8 a'8. c''16 \mbreak \pgbreak }
CLVII = { | <c' d' f'>8 f8 <c' d' f'>8 f8 <c' d' f'>8 bes8 <d' f' a'>8 a8 }
CLVIII = { | <d' f' bes'>8 bes8 <d' f' bes'>8 bes8 <c' a'>8 <d' bes'>8 <bes g'>8 <c' a'>8~ }
CLIX = { | <c' a'>4 <a f'>4 <c' a'>8 g'8 a'8 c''8 }
CLX = { | <d' f'>8 a8 <d' f'>8 a8 <c' f'>8 a8 <f' a'>8 c'8 \mbreak \pgbreak }
DLVII = { | bes,4. bes,8 bes,4 d4 }
DLVIII = { | g,4. g,8 c2 }
DLIX = { | f,4. f,8 f,4 e,4 }
DLX = { | d,4. d,8 c,4. c,8 \mbreak \pgbreak }

% measures 61 - 64
ALXI = { | r2 bes'8. a'16 bes'8 d''8 }
ALXII = { | g'2 a'8 bes'8 g'8 a'8-- }
ALXIII = { | f'1 }
ALXIV = { | r1 \mbreak }
BLXI = { | f'2 r2  }
BLXII = { | r2 c'8 d'8 bes8 c'8-- }
BLXIII = { | a1 }
BLXIV = { | r1 \mbreak }
CLXI = { | <c' d' f'>8 f8 <c' d' f'>8 f8 <c' d' f'>8 f8 <d' f'>8 a'8 }
CLXII = { | <d' f' bes'>2 <c' a'>8 <d' bes'>8 <bes g'>8 <c' a'>8 }
CLXIII = { | <a f'>2 a'8 c''8 e''8 f''8 }
CLXIV = { | <f' bes'>4. f'8 <g' e''>8. <e' c''>16~ <e' c''>8 bes'8 \mbreak }
DLXI = { | bes,,4. f,8 bes,4 a,4 }
DLXII = { | g,2 c4 c,4 }
DLXIII = { | << { \voiceOne | r8 c8 a8 c8 f2 } \new Voice { \voiceTwo | f,1 } >> \oneVoice }
DLXIV = { | << { \voiceOne | r8 d8 f2 f4 } \new Voice { \voiceTwo | f,1 } >> \oneVoice \mbreak }

% measures 65 - 68
ALXV = { | r1 }
ALXVI = { | r1 }
ALXVII = { | r1 }
ALXVIII = { | r1 \mbreak }
BLXV = { | r1 }
BLXVI = { | r1 }
BLXVII = { | r1 }
BLXVIII = { | r1 \mbreak }
CLXV = { | <c' a'>2 a'8 c''8 e''8 f''8 }
CLXVI = { | <f' bes'>4. f''8 <g' e''>8. c''16~ c''8 bes'8 }
CLXVII = { | <c' f' a'>2 a8 c'8 e'8 f'8 }
CLXVIII = { | <d' g'>4 <des' f'>8 e'8 <c' f'>8 g'8 <g c' e'>8 <fis c' ees'>8 \mbreak }
DLXV = { | << { \voiceOne | r8 c8 a8 c8 f2 } \new Voice { \voiceTwo | f,1 } >> \oneVoice }
DLXVI = { | << { \voiceOne | r8 d8 f2 } \new Voice { \voiceTwo | f,2. } >> \oneVoice des4 }
DLXVII = { | c8 f,8 c,8 c8~ c2 }
DLXVIII = { | <bes, g>4 <bes, f>4 <a, f>4 <d, d>4 \mbreak }

% measures 69 - 71
ALXIX = { | r2 c''8 d''8 bes'8 c''8-- }
ALXX = { | a'1~ }
ALXXI = { | a'1\fermata \mbreak \pgbreak }
BLXIX = { | r2 a'8 bes'8 g'8 a'8-- }
BLXX = { | f'1~ }
BLXXI = { | f'1\fermata \mbreak \pgbreak }
CLXIX = { | <f bes d'>8\arpeggio f'8 g'8 bes'8 <f' c''>8 <g' bes'>8 <d' g'>8 <c' a'>8\arpeggio }
CLXX = { | <a c' f'>8\arpeggio a8 c'8 a8 <a f'>8 <c' a'>8 <f' c''>4 }
CLXXI = { | <a' c'' f'' a''>1\arpeggio\fermata \mbreak \pgbreak }
DLXIX = { | g,2 <a c'>8 <g c'>8 <f bes>8 <e bes>8\arpeggio }
DLXX = { | f,1~ }
DLXXI = { | f,1\fermata \mbreak \pgbreak }


APart = {
  \tempo 4 = 80
  \clef treble
  \time 4/4
  \key f \major

  \AI \AII \AIII \AIV 
  \AV \AVI \AVII \AVIII 
  \AIX \AX \AXI \AXII 
  \AXIII \AXIV \AXV 
  \AXVI \AXVII \AXVIII 
  \AXIX \AXX \AXXI \AXXII 
  \AXXIII \AXXIV \AXXV 
  \AXXVI \AXXVII \AXXVIII 
  \AXXIX \AXXX \AXXXI \AXXXII 
  \AXXXIII \AXXXIV \AXXXV 
  \AXXXVI \AXXXVII \AXXXVIII 
  \AXXXIX \AXL \AXLI 
  \AXLII \AXLIII \AXLIV 
  \AXLV \AXLVI \AXLVII 
  \AXLVIII \AXLIX \AL 
  \ALI \ALII \ALIII 
  \ALIV \ALV \ALVI 
  \ALVII \ALVIII \ALIX \ALX 
  \ALXI \ALXII \ALXIII \ALXIV 
  \ALXV \ALXVI \ALXVII \ALXVIII 
  \ALXIX \ALXX \ALXXI 
}

BPart = {
  \clef treble
  \key f \major

  \BI \BII \BIII \BIV 
  \BV \BVI \BVII \BVIII 
  \BIX \BX \BXI \BXII 
  \BXIII \BXIV \BXV 
  \BXVI \BXVII \BXVIII 
  \BXIX \BXX \BXXI \BXXII 
  \BXXIII \BXXIV \BXXV 
  \BXXVI \BXXVII \BXXVIII 
  \BXXIX \BXXX \BXXXI \BXXXII 
  \BXXXIII \BXXXIV \BXXXV 
  \BXXXVI \BXXXVII \BXXXVIII 
  \BXXXIX \BXL \BXLI 
  \BXLII \BXLIII \BXLIV 
  \BXLV \BXLVI \BXLVII 
  \BXLVIII \BXLIX \BL 
  \BLI \BLII \BLIII 
  \BLIV \BLV \BLVI 
  \BLVII \BLVIII \BLIX \BLX 
  \BLXI \BLXII \BLXIII \BLXIV 
  \BLXV \BLXVI \BLXVII \BLXVIII 
  \BLXIX \BLXX \BLXXI 
}

CPart = {
  \clef treble
  \key f \major

  \CI \CII \CIII \CIV 
  \CV \CVI \CVII \CVIII 
  \CIX \CX \CXI \CXII 
  \CXIII \CXIV \CXV 
  \CXVI \CXVII \CXVIII 
  \CXIX \CXX \CXXI \CXXII 
  \CXXIII \CXXIV \CXXV 
  \CXXVI \CXXVII \CXXVIII 
  \CXXIX \CXXX \CXXXI \CXXXII 
  \CXXXIII \CXXXIV \CXXXV 
  \CXXXVI \CXXXVII \CXXXVIII 
  \CXXXIX \CXL \CXLI 
  \CXLII \CXLIII \CXLIV 
  \CXLV \CXLVI \CXLVII 
  \CXLVIII \CXLIX \CL 
  \CLI \CLII \CLIII 
  \CLIV \CLV \CLVI 
  \CLVII \CLVIII \CLIX \CLX 
  \CLXI \CLXII \CLXIII \CLXIV 
  \CLXV \CLXVI \CLXVII \CLXVIII 
  \CLXIX \CLXX \CLXXI 
}

DPart = {
  \clef bass
  \key f \major

  \DI_DIV
  \DV_DVIII
  \DIX_XII
  \DXIII \DXIV \DXV 
  \DXVI \DXVII \DXVIII 
  \DXIX \DXX \DXXI \DXXII 
  \DXXIII \DXXIV \DXXV 
  \DXXVI \DXXVII \DXXVIII 
  \DXXIX \DXXX \DXXXI \DXXXII 
  \DXXXIII \DXXXIV \DXXXV 
  \DXXXVI \DXXXVII \DXXXVIII 
  \DXXXIX \DXL \DXLI 
  \DXLII \DXLIII \DXLIV 
  \DXLV \DXLVI \DXLVII 
  \DXLVIII \DXLIX \DL 
  \DLI \DLII \DLIII 
  \DLIV \DLV \DLVI 
  \DLVII \DLVIII \DLIX \DLX 
  \DLXI \DLXII \DLXIII \DLXIV 
  \DLXV \DLXVI \DLXVII \DLXVIII 
  \DLXIX \DLXX \DLXXI 
}

InstrumentAPart = \new Staff \with {midiInstrument = "acoustic guitar (steel)"} {
  \APart
}

InstrumentBPart = \new Staff \with {midiInstrument = "acoustic guitar (steel)"} {
  \BPart
}

InstrumentCPart = \new Staff \with {midiInstrument = "acoustic guitar (steel)"} {
  \CPart
}

InstrumentDPart = \new Staff \with {midiInstrument = "acoustic guitar (steel)"} {
  \DPart
}

InstrumentCombined = {
  \new PianoStaff <<
    \InstrumentAPart
    \InstrumentBPart
    \InstrumentCPart
    \InstrumentDPart
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}
