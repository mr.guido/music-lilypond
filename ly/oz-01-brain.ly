\version "2.24.2"

\header{
  title = "Wizard of Oz"
  subtitle = "If I Only Had a Brain"
  composer = "Music by Harold Arlen"
  meter = "128"
}

\score {
  \new ChoirStaff <<
    \new Staff \with {midiInstrument = "acoustic grand"} <<
      { \clef treble
        \key ees \major
        s2. g'8^"The Scarecrow:" aes'8
        bes'8 g'8 ees'8 f'8 {g'16 (ees'8.)} r8 f'8
        g'8 ees'8 c'8 d'8 ees'16 c'8. r8 c'8
        \break
        d'8 d'8 d'8 d'8 d'4 r4
        r2 r4 ees'8 d'8
        c'4 c''8 bes'8 aes'8 g'8 f'8 ees'8
        \break
        d'8 d'8 d''8 c''8 bes'8 aes'8 g'8 f'8
        ees'8 ees'8 \tuplet 3/2 {ees'8 ees'8 ees'8~} ees'4 r4
        % \pageBreak
        \break

        r2 r4 g'8 aes'8
        bes'8 g'8 ees'8 f'8 g'16 ees'8. r8 f'8
        g'8 ees'8 c'8 d'8 ees'16 c'8. r8 c'8
        \break
        bes16 bes8. bes16 bes8. bes4 r4
        r2 r8 ees'4^"Dorothy:" d'8
        c'4 c''8 bes'8 aes'8 g'8 f'8 ees'8
        \break
        d'8 d'8 d''8 ees''8 bes'8 aes'8 g'8 f'8
        ees'8 ees'8~ \tuplet 3/2 {ees'8 ees'8 ees'8} ees'2~
        ees'4 r2^"The Scarecrow:" ees'4
        \break
        c''2 r8 b'8 c''8 d''8
        bes'2 r4 r8 bes'8
        aes'4 aes'4 aes'4 bes'4
        \break
        g'2 r4 aes'8 g'8
        f'8 e'8 f'8 e'8 f'8 e'8 f'8 g'8
        ees'2 r8 f'8 g'16 bes'8.
        \break
        c''4 r4 r8 a'8 g'8 f'8
        bes'4 r4 r4 g'8 aes'8
        bes'8 g'8 ees'8 f'8 g'16 ees'8. r8 f'8
        \break
        g'16 ees'8.~ \tuplet 3/2 {ees'8 c'8 d'8} ees'16 c'8. r8 c'8
        bes8 bes8~ \tuplet 3/2 {bes8 bes8 bes8} bes4 r4
        \break
        r2 r4 ees'8 d'8
        c'4 c''8 bes'8 aes'8 g'8 f'8 ees'8
        d'8 d'8 d''8 c''8 bes'8 aes'8 g'8 f'8
        \break
        ees'8 ees'8 \tuplet 3/2 {ees'8 ees'8 ees'8~} ees'4 r4
        r2 r4
      }

      \addlyrics { 
        I could while a- way the hours con- fer- rin' with the flow- ers, con-
        sult- in' with the rain. And my head I'd be scratch- in' while my 
        thoughts were bus- y hatch- in' if I on- ly had a brain.

        I'd un- rav- el ev- 'ry rid- dle for an- y in- di- vid- 'al in
        trou- ble or in pain.

        With the thoughts you'd be think- in', you could
        be an- oth- er Lin- coln if you on- ly had a brain.

        Oh, I could tell you why the o- cean's near the
        shore.
        
        I could think of things I nev- er thought be- fore, and then I'd
        sit and think some more.

        I would not be just a nuff- in', my head all full of stuff- in',
        my heart all full of pain.

        I would dance and be mer- ry, life would be a ding- a- der- ry if I
        on- ly had a brain.
      }

      \new Staff \with {midiInstrument = "acoustic grand"} 
      { \clef treble
        \key ees \major
        s2. g'8 aes'8
        <g' bes'>8 <ees' g'>8 <c' ees'>8 <d' f'>8 <ees' g'>16 <c' ees'>8. r8 <d' f'>8
        <ees' g'>8 <c' ees'>8 <aes c'>8 <bes d'>8 <c' ees'>16 <aes c'>8. r8 <aes c'>8
        \break
        <aes bes>8 <aes bes>8 <aes bes>8 <aes bes>8 <g bes>4 r8 {c'''8
        (bes''8-.)} g''8 \tuplet 3/2{{f''8 (ees''8 c''8)}} bes'4 bes4
        <aes c'>4 <aes' c''>8 bes'8 c'2
        \break
        bes4 <aes' d''>8 c''8 d'2
        ees'8 ees'8 \tuplet 3/2 {ees'8 ees'8 ees'8~} ees'4 {\tuplet 3/2 {<c'' f''>8 (fes''8 ees''8)}}
        \break

        {\tuplet 3/2 {<bes' d''>8 (des''8 c''8)}} {\tuplet 3/2 {<aes' bes'>8 (beses'8 aes'8)}} <d' g'>8-> r8 g'8 aes'8
        <<
        { \voiceOne bes'2. }
        \new Voice
        { \voiceTwo r8 g'8 ees'8 f'8 g'16 ees'8. }
        >>
        \oneVoice
        r8 f'8

        <<
        { \voiceOne g'2. }
        \new Voice
        { \voiceTwo r8 ees'8 c'8 d'8 ees'16 c'8. }
        >>
        \oneVoice
        r8 c'8
        \break

        bes16 bes8. bes16 bes8. bes4 r4
        r4
        <<
        { \voiceOne {<ees'' g'' bes''>4-> (<g' bes'>8-.)} ees'4 d'8 }
        \new Voice
        { \voiceTwo r4 r4 bes4 }
        >>
        \oneVoice

        <aes c'>4 <aes' c''>8 bes'8
        <<
        { \voiceOne aes'8 g'8 f'8 ees'8 }
        \new Voice
        { \voiceTwo c'2 }
        >>
        \oneVoice

        <bes d'>8\< d'8 <aes' d''>8 c''8
        <<
        { \voiceOne bes'8 aes'8 g'8 f'8 }
        \new Voice
        { \voiceTwo d'2 }
        >>
        \oneVoice
        ees'8\!\> ees'8~ \tuplet 3/2 {ees'8 ees'8 ees'8} ees'8 f'4 ees'8
        des'4\! \tuplet 3/2 {f'8 aes'8 c''8}
        <<
        { \voiceOne <g' bes'>2 }
        \new Voice
        { \voiceTwo des'4 ees'4 }
        >>
        \oneVoice

        <ees' aes' c''>2 r8 {<d' g' b'>8-> (<ees' aes' c''>8)} d''8
        <<
        { \voiceOne 
          bes'2 r4 r8 bes'8
          aes'4 aes'4 aes'4 bes'4
          g'2 r4 aes'8 g'8
          f'8 e'8 f'8 e'8 f'8 e'8 f'8 g'8
          <d' ees'>2
        }
        \new Voice
        { \voiceTwo 
          {f'2 (ees'2
          ees'2 d'2
          d'2 c'2)}
          {c'2 (b2)}
          g2
        }
        >>
        \oneVoice
        c'8 f'8 ees'4
        
        <ees' g' c''>4-. {<a' ees'' g''>4-> (<ees' g' c''>8-.)} a'8 g'8 f'8
        bes'8-. <ees' c''>8-. r8 <d' bes'>8-. r4 g'8 aes'8
        bes'8 g'8 ees'8 f'8 g'16 ees'8.
        <<
        { \voiceOne  ees'8 f'8 }
        \new Voice
        { \voiceTwo <g bes>4 }
        >>
        \oneVoice
        <aes' c''>4-> r4
        <<
        { \voiceOne 
          ees'16 c'8. c'8 c'8
          bes8 bes8~ \tuplet 3/2 {bes8 bes8 bes8}
        }
        \new Voice
        { \voiceTwo 
          {<aes bes>4 (g4)}
          <ges d'>2
        }
        >>
        \oneVoice
        <g bes c'>4. {c'''8
        (bes''8-.)} g''8 {\tuplet 3/2 {f''8 (ees''8 c'')}} bes'4
        <<
        { \voiceOne ees'8 d'8 }
        \new Voice
        { \voiceTwo bes4 }
        >>
        \oneVoice
        <aes c'>4 <aes' c''>8 bes'8
        <<
        { \voiceOne 
          aes'8 g'8 f'8 ees'8
          d'8 d'8 <aes' d''>8 c''8
          bes'8 aes'8 g'8 f'8
        }
        \new Voice
        { \voiceTwo 
          c'2
          bes4 r4 d'2
        }
        >>
        \oneVoice
        ees'8 ees'8 \tuplet 3/2 {ees'8 ees'8 ees'8~} ees'8 <g' bes' ees''>4 des''8
        <fes' ges' beses' ces''>8 <fes' ges' beses' ces''>8 <fes' ges' beses' ces''>8 <fes' ges' beses' ces''>8 <ees' ges' beses' ces''>4 r4
      }

      \new Staff \with {midiInstrument = "acoustic grand"}
      { \clef bass
        \key ees \major
        s2. r4 ees,4-- <ees g>4-. ees,4-- <ees g>4-.
        f,4 f4 bes,4 f4
        \break
        ees,4  ees4 ees,4 ees4
        ees,4 <ees g>4 ees,4 <ees g>4
        aes,4 <ees aes>4 f,4 <aes c'>4
        \break
        aes,4 <f aes>4 aes,4 <aes c'>4
        <ees g bes>4-. <ees aes c'>4-. <ees g bes>4-.
        \clef treble
        {\tuplet 3/2 {ges'8 (f'8 fes'8)}}
        \break

        {\tuplet 3/2 {aes'8 (g'8 ges'8)}} {\tuplet 3/2 {f'8 (fes'8 ees'8)}} <aes bes>8-> r8 r4
        \clef bass
        <<
        { \voiceOne 
          {bes,4 (b,4 c d4)}
          {ees4 (e4 f4 ges4)}
          {<ees g>4 (aes4)}
        }
        \new Voice
        { \voiceTwo 
          ees,4 r4 ees,4 r4
          f,4 r4 bes,4 r4
          r2
        }
        >>
        \oneVoice
        ees,4 bes,4->~
        bes,2 bes,,8-. r8 g,4
        aes,4-- <ees aes>4-. f,4-- <aes c'>4-.
        \break
        
        bes,4-- <f aes>4-. bes,4-- <aes c'>4-.
        <ees g bes>4-. <ees aes c'>4-. <ees g bes>4-. r4
        <<
        { \voiceOne aes4 aes4 }
        \new Voice
        { \voiceTwo bes,2 }
        >>
        \oneVoice
        ees,4 <g bes>4

        <aes,>4-- <aes c'>4-. aes,8 {g8-> (aes4)}
        g,4-- <f bes>4-. c,4-- <ees bes>4-.
        f,4 <ees aes>4 bes,4 <d aes bes>4

        ees,4 <d g>4 ees,4 ees4
        d,4 <c f>4 g,4 <b, g>4
        c4 ees4 bes,2

        a,4-. r4 f,4-. r4
        bes,4-. f,4-. bes,,4-. r4
        <<
        { \voiceOne {bes,4 (c4 d4 ees4)} }
        \new Voice
        { \voiceTwo ees,4 r4 ees,4 r4 }
        >>
        \oneVoice

        f,4-- <f aes>4. bes,4-- bes,4--
        ees,4-- <d ges>4-. ees,4-- <c g>4-.

        ees,4-- <ees g>4-. ees,4-- <ees g>4-.
        {f,4 (g,4 aes,4 a,4 bes,4 b,4 c4 d4)}

        <ees g bes>4-. <ees aes c'>4-. <ees g bes>4-. r4
        des4 ges,4 ces4 r8
      }
    >>
  >>
  \layout {
    indent = 0.0
  }
  \midi {
    \tempo 4 = 128
  }
}
