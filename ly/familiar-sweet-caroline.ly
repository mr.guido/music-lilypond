\version "2.24.2"

\header{
  title = "Sweet Caroline"
  composer = "Words & Music by Neil Diamond"
  source = "First 50 Songs You Should Play on Harmonica (Kindle)"
}

mbreak = { }
pgbreak = { }

InstrumentCombined = {
  \new Staff \with {midiInstrument = "acoustic grand"} <<
  { \clef treble 
    \tempo 4 = 130
    \time 4/4

    % measures 1 - 3
    | c'8. e'16 f'2 d'8. e'16
    | f'8. e'16 d'4 f'8. e'16 d'4
    | f'8. g'16 a'2 f'8. g'16 \mbreak
    | a'8. g'16 f'4 a'8. g'16 f'4
    | a'8. b'16 c''2 a'8. b'16
    | c''2 b'2
    | e''2 d''2 \mbreak
    \repeat volta 2 {
      | r2 e'8. f'16 g'8. g'16~
      | g'4 r4 r2
      | r2 \tuplet 3/2 {d'4 e'4 f'4} \mbreak
      | f'4 f'8 e'8~ e'4 d'4
      | r2 \tuplet 3/2 {c'4 d'4 e'4}
      | e'4 e'4 d'4 c'4
      | g2. r4 \mbreak
      | r1 
      | r2 e'8. e'16 g'8. g'16~
      | g'4 r4 r2 \mbreak \pgbreak
      | r2 \tuplet 3/2 {d'4 e'4 f'4}
      | f'4 f'4 e'8 d'4.
      | r2 c'8. d'16 e'8. e'16~ \mbreak
      | e'4 e'4 d'4 c'4
      | d'1
      | r1
      | g'1
      | r1 \mbreak
      | r4 a'8 a'8 a'2
      | r1
      | r4 b'8 b'8 b'2
      | r1 \mbreak
      | r4 c''8 c''8 c''2
      | r2 c''4 d''4
      | d''1~ d''4. r8 r2 \mbreak
      | e''2 g'4 a'8. a'16~
      | a'2 r2
      | r2 a'8 f''4 f''8~
      | f''8 e''4. d''4 c''4 \mbreak
      | g'1
      | r1
      | e''2 g'4 a'8. a'16~
      | a'2 r2 \mbreak \pgbreak
      | r2 a'8 f''4 f''8~
      | f''8 e''4 d''4 c''4.
      | d''2 c''2
      | g'2 f'2 \mbreak
    }
    | d'8. e'16 f'2 d'8. e'16
    | f'8. e'16 d'4 f'8. e'16 d'4
    | f'8. g'16 a'2 f'8. g'16 \mbreak
    | a'8. g'16 f'4 a'8. g'16 f'4
    | a'8. b'16 c''2 a'8. b'16
    | c''2 b'2
    | e''2 d''2 \mbreak
    | e''2 g'4 a'8. a'16~
    | a'2 r2
    | r2 a'8 f''4 f''8~
    | f''8 e''4. d''4 c''4 \mbreak
    | g'1
    | r1
    | e''2 g'4 a'8. a'16~
    | a'2 r2
    | r2 a'8 f''4 f''8~ \mbreak
    | f''8 e''4 d''4 c''4.
    | d''1~
    | d''4 f''4 f''4 e''16( d''16 c''8)
    | c''1\fermata \bar "|."
  }
  \addlyrics { 
    " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "
    " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " " 
    " "
    <<
      \new Lyrics {
        \set associatedVoice = "singleVoice"
        Where it be- gan, I can't be- gin to know- in',
        but then I know it's grow- in strong.
        Was in the spring and spring be- came the sum- mer.
        Who'd have be- lieved you'd come a- long?
        Hands, touch- in' hands,
      }
      \new Lyrics {
        \set associatedVoice = "singleVoice"
        look at the night, and it don't seem so lone- ly.
        We fill it up with on- ly two.
        And when I hurt, hurt- in' runs off my shoul- ders. How can I hurt
        when hold- in' you? Warm, touch- in' warm.
      }
    >>
    reach- in' out, touch- in' me touch- in' you.
    Sweet Car- o- line, good times nev- er seemed so good.
    I've been in- clined to be- lieve they nev- er would.
    
    
    <<
      \new Lyrics {
        \set associatedVoice = "singleVoice"
        But now I
      }
      \new Lyrics {
        \set associatedVoice = "singleVoice"
        Oh, no, no.
      }
    >>

    " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "
    " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "  " "
    Sweet Car- o- line, good times nev- er seemed so good.
    I've been in- clined to be- lieve they nev- er would.
    Sweet Car- o- line.
  }
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}
