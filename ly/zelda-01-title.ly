\version "2.20.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "The Legend of Zelda"
  subtitle = "Title Theme"
  composer = "Composed by Koji Kondo"
}

\score {
  \new Staff \with {midiInstrument = "acoustic grand"} <<
  { \clef treble 
    \unfoldRepeats {
      \repeat volta 2 {
        c''2\mf r8 g'8 g'8 c''8
        bes'16 aes'16 bes'8~ bes'2 r4
        c''2 r8 aes'8 aes'8 c''8
        b'16 a'16 b'8~ b'2 r4
        \break
        <ees' g'>2\mp <ees' g'>2
        <ees' g'>2 <ees' g'>2
        c''4 g'4~ g'8 c''8 c''16 d''16 e''16 f''16
        g''2. r4
        \break
        c''4 g'4~ g'8 c''8 c''16 d''16 e''16 f''16
        g''2. r4
        c''4\staccato\mf g'4~ g'8 c''8 c''16 d''16 e''16 f''16
        \break
        g''2 r8 g''8 \tuplet 3/2 {g''8 aes'' bes''}
        c'''2 \tuplet 3/2 {r4 c'''8} \tuplet 3/2 {c'''8 bes'' aes''}
        bes''8.\staccato aes''16 g''2 g''4
        \break

        f''8 f''16 g''16 aes''2 g''8 f''8
        ees''8 ees''16 f''16 g''2 f''8 ees''8
        d''8 d''16\< e''16 fis''2 <c'' a''>4\!
        \break
        <b' g''>8 g'16 g'16\> g'8 g'16 g'16 g'8 g'16 g'16 g'8\! g'8
        c''4\staccato g'4~ g'8\< c''8 c''16 d''16 e''16 f''16
        g''2 r8 g''8 \tuplet 3/2 {g''8 aes'' bes''}\!
        \break
        c'''2 r4 <ges'' ees'''>4
        <f'' d'''>4\staccato b''2 <b' g''>4\f
        <c'' aes''>2 r4 <ees'' c'''>4
        \break
        <d'' b''>4\staccato g''4 r4 <b' g''>4
        <c'' aes''>2 r4 <ees'' c'''>4
        <d'' b''>4\staccato g''4 r4 e''4\mf
        \break
        <aes' f''>2 r4 <des'' aes''>4
        <c'' g''>4\staccato ees''4 r4 c''4
        d''8 d''16 e''16 f''2 <c'' a''>4
        <b' g''>8 g'16 g'16 g'8 g'16 g'16 g'8 g'16 g'16 g'8 g'8
      }
    }
  }
  
  \new Staff \with {midiInstrument = "acoustic grand"} 
  { \clef bass 
    \unfoldRepeats {
      \repeat volta 2 {
        c4 g4 c'2
        bes,4 f4 bes2
        aes,4 ees4 aes2
        g,4 d4 g2
        \break
        <c g>4 <c g>4 <c g>4 <c g>4
        <c g>4 <c g>4 <c g>4 <c g>4
        <c g>4 <c g>4 <c g>4 <c g>4
        <bes, f>4 <bes, f>4 <bes, f>4 <bes, f>4
        \break
        <aes, ees>4 <aes, ees>4 <aes, ees>4 <aes, ees>4
        <g, d>4 <g, d>4 <g, d>4 <g, d>4
        <c g>4 <c g>4 <c g>4 <c g>4
        \break
        <bes, f>4 <bes, f>4 <bes, f>4 <bes, f>4
        <aes, ees>4 <aes, ees>4 <aes, ees>4 <aes, ees>4
        <ees bes>4 <ees bes>4 <ees bes>4 <ees bes>4
        \break

        <des aes>4 <des aes>4 <des aes>4 <des aes>4
        <c g>4 <c g>4 <c g>4 <c g>4
        <des aes>4 <des aes>4 <des aes>4 <des aes>4
        \break
        <e b>4 <ees bes>4 <d a>4 <des aes>4
        <c g>4 <c g>4 <c g>4 <c g>4
        <bes f>4 <bes f>4 <bes f>4 <bes f>4
        \break
        <aes, ees>4 <aes, ees>4 <aes, ees>4 <aes, ees>4
        <g, d>4 <g, d>4 <g, d>4 <g, d>4
        fis,4 \tuplet 3/2 {fis,8 a,8 c8} ees4 r4
        \break
        g,4 \tuplet 3/2 {g,8 g,8 g,8} g,4 g,4
        fis,4 \tuplet 3/2 {fis,8 a,8 c8} ees4 r4
        g,4 \tuplet 3/2 {g,8 g,8 g,8} g,4 g,4
        \break
        <des aes>4 <des aes>4 <des aes>4 <des aes>4
        <c g>4 <c g>4 <c g>4 <c g>4
        <d a>4 <d a>4 <d a>4 <d a>4
        <e b>4 <ees bes>4 <d a>4 <des aes>4
      }
    }
  }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 80
  }
}
