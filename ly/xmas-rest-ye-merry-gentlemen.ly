\version "2.24.2"

\header{
  title = "God Rest Ye Merry, Gentlemen"
}

\score {
  \new StaffGroup <<
    \new Staff \with {midiInstrument = "trumpet"} \with {instrumentName = "Trumpet"} <<
    {
      \key f \major
      \time 4/4
      s2. d'4\mp
      {d'4 (a'4)} a'4 g'4
      {f'4 (e'4)} d'4 c'4
      d'4 e'4 f'4 g'4
      \break
      a'2. d'4
      {d'4 (a'4)} a'4 g'4
      {f'4 (e'4)} d'4 c'4
      \break
      d'4 e'4 f'4 g'4
      a'2.\< a'4\!
      {bes'4 (g'4)} a'4 bes'4
      \break

      {c''4 (d''4)} a'4 g'4
      f'4 d'4 e'4 f'4
      g'2\f {f'4 (g'4)}
      \break
      a'2 bes'4 a'4
      {a'4 (g'4)} f'4 e'4
      d'2 \tuplet 3/2 {f'4--\mf e'4-- d'4--}
      \break
      g'2\fermata {f'4 (g'4)}
      {a'4 (bes'4)} c''4 d''4
      {a'4 (g'4)} f'4 e'4
      d'2.\mp r4
    }
    \addlyrics { 
      % God rest ye mer- ry gen- tle- men, let noth- ing you di- may.
      % Re- mem- ber Christ our Sav- iour was born on Christ- mas day to
      % save us all from Sa- tan's pow'r when we were gone a- stray. O tid- ings
      % of com- fort and joy, com- fort and joy. O tid- ings of com- fort and joy.
    }
    >>
    \new PianoStaff \with {midiInstrument = "acoustic grand"} \with {instrumentName = "Piano"} <<
    {
      \key ees \major
      \clef treble
      \time 4/4
      <<
        {
          \voiceOne
          s2. c'4\mp
          c'4 g'4 g'4 f'4
          {ees'4 (d'4)} c'4 bes4
          c'4 d'4 ees'4 f'4
          \break
          g'2. c'4
          c'4 g'4 g'4 f'4
          ees'4 d'4 c'4 bes4
          \break
          c'4 d'4 ees'4 f'4
          g'2. g'4
          aes'4 f'4 g'4 aes'4
          \break
          {bes'4 (c''4)} g'4 f'4
          ees'4 c'4 d'4 ees'4
          f'2\f {ees'4 (f'4)}
          \break
          g'2 aes'4 g'4
          {g'4 (f'4)} ees'4 d'4
          c'2 \tuplet 3/2 {ees'4\mf d'4 c'4}
          \break
          f'2\fermata {ees'4 (f'4)}
          {g'4 (aes'4)} bes'4 c''4
          {g'4 (aes'4)} ees'4 d'4
          c'2.\mp r4
        }
        \new Voice {
          \voiceTwo
          s2. c'4\mp
          c'4 ees'4 d'4 c'4
          c'4 bes4 g4 f4
          g4 bes4 c'4 c'4
          \break
          b2. c'4
          c'4 c'4 g4 bes4
          c'4 bes4 g4 f4
          \break
          g4 bes4 c'4 d'4
          ees'2.\< d'4\!
          c'4 ees'4 d'4 f'4
          \break

          ees'4 d'4 c'4 g4
          c'4 g4 c'4 c'4
          c'2\f c'4 bes4
          \break
          ees'2 ees'4 ees'4
          ees'4 c'4 c'4 b4
          aes2 {aes4\mf (bes4)}
          \break
          c'2\fermata c'4 d'4
          ees'2 ees'4 c'4
          {ees'4 (c'4)} bes4 b4
          g2.\mp r4
        }
      >>
      \oneVoice
    }
    \new Staff \with {midiInstrument = "acoustic grand"} {
      \key ees \major
      \clef bass
      \time 4/4
      <<
        {
          \voiceOne
          s2. c4
          c4 c'4 bes4 aes4
        }
        \new Voice {
          \voiceTwo
          s2. c4
          c4 c4 c4 c4
        }
      >>
      \oneVoice
    }
    >>
  >>
  \layout {
    % indent = 0.0
  }
  \midi {
    \tempo 4 = 136
  }
}
