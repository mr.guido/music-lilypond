\version "2.24.2"

\header{
  title = "The First Noel"
}

\score {
  <<
    \new Staff = "staff" \with {midiInstrument = "concertina"} {
      \new Voice = "melody" {
        \relative {
            \time 3/4
            r2 e'8 d8 c4.
            d8 e8 f8 g2
            a8 b8 c4 b4
            a4 g2 a8 b8 \break
            c4 b4 a4 g4
            a4 b4 c4 g4
            f4 e4 r4 e8
            d8 c4. d8 e8
            f8 \break g2 a8 b8
            c4 b4 a4 g2
            a8 b8 c4 b4
            a4 g4 a4 b4
            c4 g4 f4 e4
            r4 e8 d8 c4.
            d8 e8 f8 g2
            c8 b8 a2 a4
            g2. c4 b4 a4
            g4 a4 b4 c4
            g4 f4 e2 r4
         }
      }
    }
    \new Lyrics {
      \lyricsto "melody" {
        Th e fir st no - el Th e an gels did say, Un to cer tain poor shep herds
        in fields where they lay, I n fie lds where they lay, a - keep ing their
        sheep, on a cold win ter's ni ght that w as so deep, No - e l No - el
        No - el No el. Born is the ki ng of Is - ra el
      }
    }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 100
  }
}
