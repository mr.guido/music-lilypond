\version "2.24.2"

\header{
  title = "Frère Jacques"
  subtitle = "Higher Octave"
}

\score {
  <<
    \new Staff = "staff" {
      \new Voice = "melody" {
        {
          c''4 d''4 e''4 c''4
          c''4 d''4 e''4 c''4
          e''4 f''4 g''2
          e''4 f''4 g''2
          \break
          g''8 a''8 g''8 f''8 e''4 c''4
          g''8 a''8 g''8 f''8 e''4 c''4
          c''4 g'4 c''2
          c''4 g'4 c''2
        }
      }
    }
    
    \new Lyrics {
      \lyricsto "melody" {
        "Fre-" "re" "Jac-" "ques,"
        "Fre-" "re" "Jac-" "ques,"
        "Dor-" "mez-" "vous,"
        "Dor-" "mez-" "vous,"
        "Son-" "nes" "les" "ma-" "ti-" "nes,"
        "Son-" "nes" "les" "ma-" "ti-" "nes,"
        "Ding," "dang," "dong."
        "Ding," "dang," "dong."
      }
    }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 116
  }
}
