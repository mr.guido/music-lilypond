\version "2.24.2"

\header{
  title = "The Legend of Zelda: Ocarina of Time"
  subtitle = "Lost Woods (Saria's Song)"
  composer = "Composed by Koji Kondo"
  arranger = "Arranged by Shinobu Amayake"
  source = "The Legend of Zelda Series for Piano (Kindle)"
}

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = {  }
pgbreak = {  }

% measures 1 - 6
AI = { | f'8-.\mp a'8-. b'4 }
AII = { | f'8-. a'8-. b'4 }
AIII = { | f'8( a'8-.) b'8-. e''8-. }
AIV = { | d''4( b'8)-. c''8-. }
AV = { | b'8( g'8-.) e'4~ }
AVI = { | e'4 r8 d'8( \mbreak }
BI = { | f8-.[\mp <a c'>8-. <a c'>8-. <a c'>8-.] }
BII = { | f8-.[ <a c'>8-. <a c'>8-. <a c'>8-.] }
BIII = { | f8-.[ <a c'>8-. <a c'>8-. <a c'>8-.] }
BIV = { | f8-.[ <a c'>8-. <a c'>8-. <a c'>8-.] }
BV = { | e8-.[ <g c'>8-. <g c'>8-. <g c'>8-.] }
BVI = { | e8-.[ <g c'>8-. <g c'>8-. c8-.] \mbreak }

% measures 7 - 12
AVII = { | e'8)-. g'8-. e'4~ }
AVIII = { | e'2 }
AIX = { | f'8-. a'8-. b'4 }
AX = { | f'8-. a'8-. b'4 }
AXI = { | f'8( a'8-.) b'8-. e''8-. }
AXII = { | d''4( b'8-.) c''8-. \mbreak }
BVII = { | e8-.[ <g c'>8-. <g c'>8-. <g c'>8-.] }
BVIII = { | e8-.[ <g c'>8-. <g c'>8-. c8-.] }
BIX = { | f8-.[ <a c'>8-. <a c'>8-. <a c'>8-.] }
BX = { | f8-.[ <a c'>8-. <a c'>8-. <a c'>8-.] }
BXI = { | f8-.[ <a c'>8-. <a c'>8-. <a c'>8-.] }
BXII = { | f8-.[ <a c'>8-. <a c'>8-. <a c'>8-.] \mbreak }

% measures 13 - 18
AXIII = { | e''8( b'8-.) g'4~ }
AXIV = { | g'4 r8 b'8-. }
AXV = { | g'8( d'8-.) e'4~ }
AXVI = { | e'2 }
AXVII = { | d'8( e'8-.) f'4 }
AXVIII = { | g'8( a'8-.) b'4 \mbreak \pgbreak }
BXIII = { | e8-.[ <g c'>8-. <g c'>8-. <g c'>8-.] }
BXIV = { | e8-.[ <g c'>8-. <g c'>8-. c8-.] }
BXV = { | e8-.[ <g c'>8-. <g c'>8-. <g c'>8-.] }
BXVI = { | e8-.[ <g c'>8-. <g c'>8-. c8-.] }
BXVII = { | d8-.[ <f a>8-. d8-. <f a>8-.] }
BXVIII = { | g,8-.[ <d g>8-. g,8-. <d g>8-.] \mbreak \pgbreak }

% measures 19 - 24
AXIX = { | c''8( b'8-.) e'4~ }
AXX = { | e'2 }
AXXI = { | <d' f'>8( <e' g'>8-.) <f' a'>4 }
AXXII = { | <g' b'>8( <a' c''>8-.) <b' d''>4 }
AXXIII = { | <c'' e''>8( <d'' f''>8-.) <e'' g''>4~ }
AXXIV = { | <e'' g''>2 \mbreak }
BXIX = { | c8-.[ <g c'>8-. c8-. <g c'>8-.] }
BXX = { | a,8-.[ <e a>8-. a,8-. <e a>8-.] }
BXXI = { | d8-.[ <f a>8-. d8-. <f a>8-.] }
BXXII = { | g,8-.[ <d g>8-. g,8-. <d g>8-.] }
BXXIII = { | c8-.[ <g c'>8-. c8-. <g c'>8-.] }
BXXIV = { | a,8-.[ <e a>8-. a,8-. <e a>8-.] \mbreak }

% measures 25 - 30
AXXV = { | d'8( e'8-.) f'4 }
AXXVI = { | g'8( a'8-.) b'4 }
AXXVII = { | c''8( b'8-.) e'4~ }
AXXVIII = { | e'2 }
AXXIX = { | <d' f'>8([\< <c' e'>8-.) <f' a'>8( <e' g'>8-.)] }
AXXX = { | <g' b'>8([ <f' a'>8-.) <a' c''>8( <g' b'>8-.)] \mbreak }
BXXV = { | d8-.[ <f a>8-. d8-. <f a>8-.] }
BXXVI = { | g,8-.[ <d g>8-. g,8-. <d g>8-.] }
BXXVII = { | c8-.[ <g c'>8-. c8-. <g c'>8-.] }
BXXVIII = { | a,8-.[ <e a>8-. a,8-. <e a>8-.] }
BXXIX = { | d8-.[ <f a>8-. <f a>8-.] r8 }
BXXX = { | d8-.[ <f a>8-. <f a>8-.] r8 \mbreak }

% measures 31 - 36
AXXXI = { | <b' d''>8( <a' c''>8-.) <c'' e''>8( <b' d''>8-.) }
AXXXII = { | <d'' f''>8( \after 64 \! <c'' e''>8-.) <b' e''>16[ <c'' f''>16 r16 <a' d''>16] }
AXXXIII = { | <b' e''>2~\mf }
AXXXIV = { | <b' e''>2 }
AXXXV = { | r2 }
AXXXVI = { | r4 e'4-. \mbreak \pgbreak }
BXXXI = { | c8-.[ <g b>8-. <g b>8-.] r8 }
BXXXII = { | c8-.[ <g b>8-. <g b>8-.] r8 }
BXXXIII = { | e8-.[ <a b>8-. r8 <a b>8-.] }
BXXXIV = { | e8-.[ <a b>8-. r8 <a b>8-.] }
BXXXV = { | e8-.[ <gis b>8-. <gis b>8-. <e gis b>8-.] }
BXXXVI = { | <e gis b>4-. e4-. \mbreak \pgbreak }


APart = {
  \tempo 4 = 140
  \clef treble
  \time 2/4

  \repeat volta 2 {
    \AI \AII \AIII \AIV \AV \AVI 
    \AVII \AVIII \AIX \AX \AXI \AXII 
    \AXIII \AXIV \AXV \AXVI \AXVII \AXVIII 
    \AXIX \AXX \AXXI \AXXII \AXXIII \AXXIV 
    \AXXV \AXXVI \AXXVII \AXXVIII \AXXIX \AXXX 
    \AXXXI \AXXXII \AXXXIII \AXXXIV \AXXXV \AXXXVI 
  }
}

BPart = {
  \clef bass

  \repeat volta 2 {
    \BI \BII \BIII \BIV \BV \BVI 
    \BVII \BVIII \BIX \BX \BXI \BXII 
    \BXIII \BXIV \BXV \BXVI \BXVII \BXVIII 
    \BXIX \BXX \BXXI \BXXII \BXXIII \BXXIV 
    \BXXV \BXXVI \BXXVII \BXXVIII \BXXIX \BXXX 
    \BXXXI \BXXXII \BXXXIII \BXXXIV \BXXXV \BXXXVI 
  }
}

InstrumentAPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \APart
}

InstrumentBPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \BPart
}

InstrumentCombined = {
  \new PianoStaff <<
    \InstrumentAPart
    \InstrumentBPart
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
 }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
   }
  >>
  \midi { }
}
