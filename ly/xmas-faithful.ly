\version "2.24.2"

\header{
  title = "O Come, All Ye Faithful"
}

\score {
  \new PianoStaff <<
    <<
      \chords \with {midiInstrument = "acoustic grand"} { 
        \set noChordSymbol = ""
        s2. c4 % check the duration of this
        c1
        g1:/b
        c4 g4:/b c4 f4
        c2:/g g4 a4:m
        a4:m fis4:dim/a g4 d4 % a2:m wrong modifier
        {g4 (c4:/e)} g4:/b a:m7
        {g2:/d (d4.:7)} g8 g2. r4
        c2:/e d4:m c4

        d4:m7 b4:dim/d c2
        g4:/b c4 a4:m d4:m/f
        g2 r2
        
        r1
        r2. c2
        g4:/b c4 g4:7/d
        c2 g4:/b c4

        g4:7/d c4 g4:/b d8:7sus4/a d8:7/a
        g2 {c4:/e (f4)}
        c2:/g g4.:7 c8 c2.
      }
      \new Staff \with {midiInstrument = "acoustic grand"} {
        \time 4/4
        s2. c''4
        c''2 g'4 c''4
        d''2 g'2
        e''4 d''4 e''4 f''4
        e''2 d''4 c''4
        \break
        c''2 b'4 a'4
        {b'4 (c''4)} d''4 e''4
        {b'2 (a'4.)} g'8
        g'2. r4
        g''2 f''4 e''4
        \break
        f''2 e''2
        d''4 e''4 c''4 d''4
        b'2 g'4 c''4
        \break
        c''4 b'4 c''4 d''4
        c''2 g'4 e''4
        e''4 d''4 e''4 f''4
        e''2 d''4 e''4
        \break
        f''4 e''4 d''4 c''4
        b'2 {c''4 (f''4)}
        {e''2 (d''4.)} c''8
        c''2. r4
      }
      \addlyrics { 
        
      }
    >>
  >>
  \layout {
    indent = 0.0
  }
  \midi {
    \tempo 4 = 130
  }
}
