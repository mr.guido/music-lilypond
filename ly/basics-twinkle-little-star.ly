\version "2.24.2"

\header{
  title = "Twinkle, Twinkle, Little Star"
  instrument = "For Concertina"
  subtitle = "Lower Octave"
}

\score {
  <<
    \new Staff = "staff" {
      \new Voice = "melody" {
        {
          c'4 c'4 g'4 g'4
          a'4 a'4 g'2
          f'4 f'4 e'4 e'4
          d'4 d'4 c'2
          \break
          g'4 g'4 f'4 f'4
          e'4 e'4 d'2
          g'4 g'4 f'4 f'4
          e'4 e'4 d'2
          \break
          c'4 c'4 g'4 g'4
          a'4 a'4 g'2
          f'4 f'4 e'4 e'4
          d'4 d'4 c'2
        }
      }
    }
    % Lyrics below are for the left hand
    % The horizontal line means draw; if omitted it means push
    \new Lyrics {
      \lyricsto "melody" {
        " "  " "  " "  " "
        "_"  "_"  " "
        "_"  "_"  " "  " "
        "_"  "_"  " "
        " "  " "  "_"  "_"
        " "  " "  "_"
        " "  " "  "_"  "_"
        " "  " "  "_"
        " "  " "  " "  " "
        "_"  "_"  " "
        "_"  "_"  " "  " "
        "_"  "_"  " "
      }
    }
    % 5 means index finger
    \new Lyrics {
      \lyricsto "melody" {
        "3"  "3"  "5"  "5"
        "5"  "5"  "5"
        "4"  "4"  "4"  "4"
        "3"  "3"  "3"
        "5"  "5"  "4"  "4"
        "4"  "4"  "3"
        "5"  "5"  "4"  "4"
        "4"  "4"  "3"
        "3"  "3"  "5"  "5"
        "5"  "5"  "5"
        "4"  "4"  "4"  "4"
        "3"  "3"  "3"
      }
    }
    \new Lyrics {
      \lyricsto "melody" {
        "Twin-" "kle," "twin-" "kle," "lit" "tle" "star,"
        "How" "I" "won-" "der" "what" "you" "are!"
        
        "Up" "a-" "bove" "the" "world" "so" "high,"
        "Like" "a" "dia-" "mond" "in" "the" "sky."

        "Twin-" "kle," "twin-" "kle," "lit" "tle" "star,"
        "How" "I" "won-" "der" "what" "you" "are!"
      }
    }
    % Lyrics above are for the right hand
    % Horizontal bar means draw; if omitted it means push
    \new Lyrics \with { alignAboveContext = "staff" } {
      \lyricsto "melody" {
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "  " "  " "
        " "  " "  " "
      }
    }
    % 1 means index finger
    \new Lyrics \with { alignAboveContext = "staff" } {
      \lyricsto "melody" {
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "  " "  " "
        " "  " "  " "
      }
    }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 100
  }
}
