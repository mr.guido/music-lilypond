\version "2.24.2"

\header{
  title = "The Legend of Zelda: A Link to the Past"
  subtitle = "The Dark World"
  composer = "Composed by Koji Kondo"
  arranger = "Arranged by Shinobu Amayake"
  source = "The Legend of Zelda Series for Piano (intermediate - advanced)"
}

% todo: consider adding dynamics to bass line

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = {  }
pgbreak = {  }

% measures 1 - 4
AI = { | r8 <ees' g'>8-.\mp <ees' g'>8-. <ees' g'>16 <ees' g'>16 <ees' g'>8-. <f' a'>4 r8 }
AII = { | r8 <g' bes'>8-. <g' bes'>8-. <g' bes'>16 <g' bes'>16 <g' bes'>8-. <a' c''>4 r8 }
AIII = { | r8 <ees' g'>8-. <ees' g'>8-. <ees' g'>16 <ees' g'>16 <ees' g'>8-. <f' a'>4 r8 }
AIV = { | r8 <g' bes'>8-. <g' bes'>8-. <g' bes'>16 <g' bes'>16 <g' bes'>8-. <a' c''>8 r8 <g c'>8 \mbreak }
BI = { | <c, c>4-.\mp <c, c>4-. <c, c>4-. r8 <c, c>8-. }
BII = { | <c, c>4-. <c, c>4-. <c, c>4-. r8 <c, c>8-. }
BIII = { | <c, c>4-. <c, c>4-. <c, c>4-. r8 <c, c>8-. }
BIV = { | <c, c>4-. <c, c>4-. <c, c>4-. r8 <c, c>8-. \mbreak }

% measures 5 - 8
AV = { | <ees' g'>2~\mf <ees' g'>8 <g c'>8 <ees' g'>8 <g' c''>8 }
AVI = { | <f' a'>16 <g' bes'>16 <f' a'>8~ <f' a'>2 r8. <d' f'>16 }
AVII = { | <ees' g'>4 <aes c'>2 r8 <d' f'>8 }
AVIII = { | <bes d'>16 <c' ees'>16 <bes d'>8~ <bes d'>2 r8. <f bes>16 \mbreak }
BV = { | <c, c>4-.\mf <c, c>4-. <c, c>4-. r8 <c, c>8-. }
BVI = { | <f, f>4-. <f, f>4-. <f, f>4-. r8 <f, f>8-. }
BVII = { | <aes,, aes,>4-. <aes,, aes,>4-. <aes,, aes,>4-. r8 <aes,, aes,>8-. }
BVIII = { | <bes,, bes,>4-. <bes,, bes,>4-. <bes,, bes,>4-. r8 <bes,, bes,>8-. \mbreak }

% measures 9 - 12
AIX = { << { \voiceOne | r8 <ees' g'>8-. <ees' g'>8-. <ees' g'>16 <ees' g'>16 <ees' g'>8-. <f' a'>4 c''16 g''16 } \new Voice { \voiceTwo | <g c'>1 } >> \oneVoice }
AX = { << { \voiceOne | d''2. r8 <g c'>8 } \new Voice { \voiceTwo | r8 <g' bes'>8-. <g' bes'>8-. <g' bes'>16 <g' bes'>16 <g' bes'>8-. <a' c''>8 r4 } >> \oneVoice }
AXI = { << { \voiceOne | r8 <ees' g'>8-. <ees' g'>8-. <ees' g'>16 <ees' g'>16 <ees' g'>8-. <f' a'>8 f''16 ees''16 d''16 c''16 } \new Voice { \voiceTwo | <g c'>1 } >> \oneVoice }
AXII = { | << { \voiceOne | d''2. r4 } \new Voice { \voiceTwo | r8 <g' bes'>8-. <g' bes'>8-. <g' bes'>16 <g' bes'>16 <g' bes'>8-. <a' c''>8 r8 <g' g''>16 a''16 } >> \oneVoice \mbreak }
BIX = { | <c, c>4-. <c, c>4-. <c, c>4-. r8 <c, c>8-. }
BX = { | <c, c>4-. <c, c>4-. <c, c>4-. r8 <c, c>8-. }
BXI = { | <c, c>4-. <c, c>4-. <c, c>4-. r8 <c, c>8-. }
BXII = { | <c, c>4-. <c, c>4-. <c, c>4-. r8 <c, c>8-. \mbreak }

% measures 13 - 15
AXIII = { | <bes' ees'' g'' bes''>2.\f <bes' ees'' g''>8 bes''8 }
AXIV = { | <c'' f'' a''>16 g''16 <a' c'' f''>8~ <a' c'' f''>2 r8 ees''16 f''16 }
AXV = { | <g' c'' ees'' g''>2. <g' c''>8 g''8 \mbreak \pgbreak }
BXIII = { | <ees, ees>8-. <ees, ees>8-. r8 <ees, ees>8-. <ees, ees>8-. <ees, ees>8-. \tuplet 3/2 {<ees, ees>8 <ees, ees>8 <ees, ees>8} }
BXIV = { | <f, f>8-. <f, f>8-. r8 <f, f>8-. <f, f>8-. <f, f>8-. \tuplet 3/2 {<f, f>8 <f, f>8 <f, f>8} }
BXV = { | <aes, aes>8-. <aes, aes>8-. r8 <aes, aes>8-. <aes, aes>8-. <aes, aes>8-. \tuplet 3/2 {<aes, aes>8 <aes, aes>8 <aes, aes>8} \mbreak \pgbreak }

% measures 16 - 18
AXVI = { | <bes' d'' f''>16 ees''16 <f' bes' d''>8~ <bes' d''>2 r8 <ees'' g''>16 <f'' a''>16 }
AXVII = { | <bes' ees'' g'' bes''>2.\< <bes' ees'' g''>8 <g'' bes''>8 }
AXVIII = { | <c'' f'' a''>16 <g'' bes''>16 <c'' a'' c'''>8~ \after 2 \! <c'' a'' c'''>2. \mbreak }
BXVI = { | <bes, bes>8-. <bes, bes>8-. r8 <bes, bes>8-. <bes, bes>8-. <bes, bes>8-. \tuplet 3/2 {<bes, bes>8 <bes, bes>8 <bes, bes>8} }
BXVII = { | <ees, ees>8-.\< <ees, ees>8-. r8 <ees, ees>8-. <ees, ees>8-. <ees, ees>8-. \tuplet 3/2 {<ees, ees>8 <ees, ees>8 <ees, ees>8} }
BXVIII = { | <f, f>8-. <f, f>8-. r8 <f, f>8-. <f, f>8-. <f, f>8-. \tuplet 3/2 {<f, f>8 <f, f>8 \after 16. \! <f, f>8} \mbreak }

% measures 19 - 21
AXIX = { | <cis'' e'' g'' bes''>2. r8 <fis'' a''>16 <g'' bes''>16 }
AXX = { | <a' fis'' a''>16 <e'' gis''>16 <a' d'' fis'' a''>8~ \after 8 \> <a' d'' fis'' a''>2. }
AXXI = { | << { \voiceOne | <g' d'' g''>2 r2\! } \new Voice { \voiceTwo | r8 <bes d'>8-. <bes d'>8-. <bes d'>16 <bes d'>16 <c' ees'>8-. <c' ees'>4 r8\! } >> \oneVoice \mbreak }
BXIX = { | <e, e>8-. <e, e>8-. r8 <e, e>8-. <e, e>8-. <e, e>8-. \tuplet 3/2 {<e, e>8 <e, e>8 <e, e>8} }
BXX = { | <d, d>8-. <d, d>8-. r8 <d, d>8-.\> <d, d>8-. <d, d>8-. \tuplet 3/2 {<d, d>8 <d, d>8 <d, d>8} }
BXXI = { | <g, g>8-. <g, g>8-. r8 <g, g>8-. <g, g>8-. <g, g>8-.\! \tuplet 3/2 {<g, g>8 <g, g>8 <g, g>8} \mbreak }

% measures 22 - 24
AXXII = { | r8\mp <d' f'>8-. <d' f'>8-. <d' f'>16 <d' f'>16 <c' e'>8-. <c' e'>4 r8 }
AXXIII = { | << { \voiceOne | c''16 g'16 f'16 g'16~ g'2 <g' c''>4 } \new Voice { \voiceTwo | ees'4 r8 <c' ees'>8~ <c' ees'>8 <c' ees'>8-. \tuplet 3/2 {c'8 c'8 c'8} } >> \oneVoice }
AXXIV = { | << { \voiceOne | bes'16 g'16 f'16 g'16~ g'2. } \new Voice { \voiceTwo | d'4 r8 <bes d'>8~ <bes d'>8 <bes d'>8-. \tuplet 3/2 {<bes d'>8 <bes d'>8 <bes d'>8} } >> \oneVoice \mbreak }
BXXII = { | <g, g>8-. <g, g>8-. r8 <g, g>8-. <g, g>8-. <g, g>8-. \tuplet 3/2 {<g, g>8 <g, g>8 <g, g>8} }
BXXIII = { | <aes, aes>4 r8 <aes, aes>8~ <aes, aes>8 <aes, aes>8-. \tuplet 3/2 {<aes, aes>8 <aes, aes>8 <aes, aes>8} }
BXXIV = { | <g, g>4 r8 <g, g>8~ <g, g>8 <g, g>8-. \tuplet 3/2 {<g, g>8 <g, g>8 <g, g>8} \mbreak }

% measures 25 - 27
AXXV = { | << { \voiceOne | bes'16 f'16 ees'16 f'16~ f'2 bes'4 } \new Voice { \voiceTwo | c'4 r8 <a c'>8~ <a c'>8 <a c'>8-. \tuplet 3/2 {c'8 c'8 c'8} } >> \oneVoice }
AXXVI = { | << { \voiceOne | a'16 ees'16 d'16 ees'16~ ees'2. } \new Voice { \voiceTwo | c'4 r8 <aes c'>8~ <aes c'>8 <aes c'>8-. \tuplet 3/2 {<aes c'>8 <aes c'>8 <aes c'>8} } >> \oneVoice }
AXXVII = { | << { \voiceOne | g'16 d'16 cis'16 d'16~ d'2 g'4 } \new Voice { \voiceTwo | b4 r8 <gis b>8~\< <gis b>8 <gis b>8-. \tuplet 3/2 {b8 b8 \after 16 \! b8} } >> \oneVoice \mbreak }
BXXV = { | <fis, fis>4 r8 <fis, fis>8~ <fis, fis>8 <fis, fis>8-. \tuplet 3/2 {<fis, fis>8 <fis, fis>8 <fis, fis>8} }
BXXVI = { | <f, f>4 r8 <f, f>8~ <f, f>8 <f, f>8-. \tuplet 3/2 {<f, f>8 <f, f>8 <f, f>8} }
BXXVII = { | <e, e>4 r8 <e, e>8~\< <e, e>8 <e, e>8-. \tuplet 3/2 {<e, e>8 <e, e>8 <e, e>8} \mbreak }

% measures 28 - 30
AXXVIII = { << { \voiceOne | c''16\< g'16 f'16 g'16~ g'2 \after 8. \! c''4 } \new Voice { \voiceTwo | ees'4\< r8 <bes ees'>8~ <bes ees'>8 <bes ees'>8-. \tuplet 3/2 {<ees' g'>8 <ees' g'>8 \after 16. \! <ees' g'>8} } >> \oneVoice }
AXXIX = { << { \voiceOne | d''1\f } \new Voice { \voiceTwo | <fis' a'>4\f r8 <fis' a'>8~ <fis' a'>8 <fis' a'>8-. \tuplet 3/2 {<fis' a'>8 <fis' a'>8 <fis' a'>8} } >> \oneVoice }
AXXX = { | <f' c''>4 r8 <f' c''>8~ <f' c''>8 <f' b'>8-. \tuplet 3/2 {<f' b'>8 <f' b'>8 <f' b'>8} \mbreak \pgbreak }
BXXVIII = { | <ees, ees>4 r8 <ees, ees>8~ <ees, ees>8 <ees, ees>8-. \tuplet 3/2 {<ees, ees>8 <ees, ees>8 <ees, ees>8} }
BXXIX = { | <d, d>4\!\f r8 <d, d>8~ <d, d>8 <d, d>8-. \tuplet 3/2 {<d, d>8 <d, d>8 <d, d>8} }
BXXX = { | <g, g>4 r8 <g, g>8~ <g, g>8 <g, g>8-. \tuplet 3/2 {<g, g>8 <g, g>8 <g, g>8} \mbreak \pgbreak }


APart = {
  \tempo 4 = 137
  \key ees \major
  \clef treble
  \time 4/4

  \AI \AII \AIII \AIV 
  
  \repeat volta 2 {
    \AV \AVI \AVII \AVIII

    \alternative {
      \volta 1 { \AIX \AX }
      \volta 2 { \AXI }
    }
  }
  
  \AXII 
  \AXIII \AXIV \AXV 
  \AXVI \AXVII \AXVIII 
  \AXIX \AXX \AXXI 
  \AXXII \AXXIII \AXXIV 
  \AXXV \AXXVI \AXXVII 
  \AXXVIII \AXXIX \AXXX 
}

BPart = {
  \key ees \major
  \clef bass
  \time 4/4

  \BI \BII \BIII \BIV 

  \repeat volta 2 {
    \BV \BVI \BVII \BVIII 

    \alternative {
      \volta 1 { \BIX \BX }
      \volta 2 { \BXI }
    }
  }
  
  \BXII 
  \BXIII \BXIV \BXV 
  \BXVI \BXVII \BXVIII 
  \BXIX \BXX \BXXI 
  \BXXII \BXXIII \BXXIV 
  \BXXV \BXXVI \BXXVII 
  \BXXVIII \BXXIX \BXXX 
}

InstrumentAPart = \new Staff \with {midiInstrument = "acoustic guitar (steel)"} {
  \APart
}

InstrumentBPart = \new Staff \with {midiInstrument = "acoustic guitar (steel)"} {
  \BPart
}

InstrumentCombined = {
  \new PianoStaff <<
    \InstrumentAPart
    \InstrumentBPart
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}

