\version "2.24.2"

\header{
  title = "THE BARE NECESSITIES"
  subtitle = "from THE JUNGLE BOOK"
  composer = "Music by Terry Gilkyson"
  source = "Disney Goes Classical"
}

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = { \break }
pgbreak = { \pageBreak }

Voices_AVIII = {
  <<
  { \voiceOne
    | d''2
  }
  \new Voice
  { \voiceTwo
    | fis'4 g'8 gis'8
  }
  >>
  \oneVoice
  a'8 ais'8 b'8 c''8
}

% measures 1 - 5
AI = { | s4\mp g'4 \acciaccatura gis'8 a'4 c''4 }
AII = { | r4 <g' c'' e''>2 r8 d''8 }
AIII = { | <bes' e''>4 d''8 <e' bes' c''>8~ <e' bes' c''>4. c''8 }
AIV = { | <f' a' d''>4 c''4 <f' a' d''>4 c''4 }
AV = { | <f' a' d''>8 c''4 <f' a'>8~ <f' a'>4. g'8 \mbreak }
BI = { | s4 r4 r2  }
BII = { | c,4-. r4 g,4-. r4 }
BIII = { | c,4-. r4 c,4-. r4 }
BIV = { | f,4-. r4 c,4-. r4 }
BV = { | f,4-. r4 f,4-. r4 \mbreak }

% measures 6 - 9
AVI = { | <e' c''>4 g'4 <e' c''>4 e''4 }
AVII = { | <cis'' a''>8 g''4. <a' f''>4 e''4 }
AVIII = \Voices_AVIII
AIX = { | <f' b' d''>4 g''4 \acciaccatura gis''8 a''4 g''4 \mbreak }
BVI = { | c,4-. r4 c,4-. bes,,4-. }
BVII = { | a,,4-. r4 a,,4-. r4 }
BVIII = { | d,4-. r4 d,4-. r4 }
BIX = { | g,4 r4 r2 \mbreak }

% measures 10 - 13
AX = { | r4 <c'' e'' a''>2 g''4 }
AXI = { | <c'' e'' a''>4 g''8 <g' bes' e''>8~ <g' bes' e''>4. c''8 }
AXII = { | <f' a' d''>4 c''4 <f' a' d''>4 c''4 }
AXIII = { | <ees'' a''>8 c''4 <a' c''>8~ <a' c''>4 e''4 \mbreak }
BX = { | c,4-. r4 g,4-. r4 }
BXI = { | c,4-. r4 c,4-. r4 }
BXII = { | f,4-. r4 c,4-. r4 }
BXIII = { | f,4-. r4 f,4-. r4 \mbreak }

% measures 14 - 17
AXIV = { | \acciaccatura {dis''8} <c'' e''>4 g''4 \acciaccatura dis''8 <cis'' e''>4 g''4 }
AXV = { | <fis' e''>4 c''8 <f' a'>8~ <f' a'>4 g'4 }
AXVI = { | <c'' e''>4 g''8 ees''8 r8 c''8 a'8 g'8 }
AXVII = { | c''8 r8 c''8 c''8 r8 b'8 a'4 \mbreak }
BXIV = { | \grace s8 c,4-. r4 a,4-. r4 }
BXV = { | d,4-. r4 g,4-. r4 }
BXVI = { | c,4-. r4 f,4-. f,4-. }
BXVII = { | c,4-. r4 r2 \mbreak }

% measures 18 - 21
AXVIII = { | <f' g'>4\mf r8 ais'8 b'8 d''8 r8 \acciaccatura dis''8 e''8 }
AXIX = { | r8 g'8~ <g' d''>8 d''8 r8 d''8 dis''4 }
AXX = { | <g' e''>4 r4 \acciaccatura dis''8 e''8 g''8 r8 a''8 }
AXXI = { | r4 <e' c''>8 c''8 r8 b'8 a'4 \mbreak \pgbreak }
BXVIII = { | g,4-. r4 d,4-. r4 }
BXIX = { | g,4-. r4 d,4-. r4 }
BXX = { | c,4-. r4 g,4-. r4 }
BXXI = { | c,4-. r4 g,4-. r4 \mbreak \pgbreak }

% measures 22 - 25
AXXII = { | <f' g'>4 r8 ais'8 \tuplet 3/2 {b'4 d''4 dis''4} }
AXXIII = { | e''4 <b' d''>4 <b' d''>4 <b' dis''>4 }
AXXIV = { | <c'' e''>4 c'4 b'2 }
AXXV = { | b'4\< <c'' e''>4 f''4 \after 8. \! <e'' g''>4 \mbreak }
BXXII = { | g,4-. r4 d,4-. r4 }
BXXIII = { | g,4-. r4 d,4-. r4 }
BXXIV = { | c,4-. r4 g,4-. r4 }
BXXV = { | c4 r4 d4 e4 \mbreak }

% measures 26 - 29
AXXVI = { | <<{a''4 a''4 a''4 f''4}\\{c''2. a'4}>> }
AXXVII = { | <<{e''4 d''8 e''8~ e''4 f''4}\\{aes'1}>> }
AXXVIII = { | <<{g''4 g''4 g''4 e''4}\\{g'1}>> }
AXXIX = { | <fis' d''>4 r4 r4 c''4 \mbreak }
BXXVI = { | f4-. r4 c4-. r4 }
BXXVII = { | f4-. r4 f4-. r4 }
BXXVIII = { | c4-. r4 g4-. r4 }
BXXIX = { | d4-. r4 d4-. r4 \mbreak }

% measures 30 - 33
AXXX = { | r1  }
AXXXI = { | r1  }
AXXXII = { | r1  }
AXXXIII = { | r1 \mbreak }
BXXX = { | r1  }
BXXXI = { | r1  }
BXXXII = { | r1  }
BXXXIII = { | r1 \mbreak }

% measures 34 - 37
AXXXIV = { | r1  }
AXXXV = { | r1  }
AXXXVI = { | r1  }
AXXXVII = { | r1 \mbreak }
BXXXIV = { | r1  }
BXXXV = { | r1  }
BXXXVI = { | r1  }
BXXXVII = { | r1 \mbreak }

% measures 38 - 41
AXXXVIII = { | r1  }
AXXXIX = { | r1  }
AXL = { | r1  }
AXLI = { | r1 \mbreak }
BXXXVIII = { | r1  }
BXXXIX = { | r1  }
BXL = { | r1  }
BXLI = { | r1 \mbreak }

% measures 42 - 45
AXLII = { | r1  }
AXLIII = { | r1  }
AXLIV = { | r1  }
AXLV = { | r1 \mbreak \pgbreak }
BXLII = { | r1  }
BXLIII = { | r1  }
BXLIV = { | r1  }
BXLV = { | r1 \mbreak \pgbreak }

% measures 46 - 49
AXLVI = { | r1  }
AXLVII = { | r1  }
AXLVIII = { | r1  }
AXLIX = { | r1 \mbreak }
BXLVI = { | r1  }
BXLVII = { | r1  }
BXLVIII = { | r1  }
BXLIX = { | r1 \mbreak }

% measures 50 - 53
AL = { | r1  }
ALI = { | r1  }
ALII = { | r1  }
ALIII = { | r1 \mbreak }
BL = { | r1  }
BLI = { | r1  }
BLII = { | r1  }
BLIII = { | r1 \mbreak }

% measures 54 - 57
ALIV = { | r1  }
ALV = { | r1  }
ALVI = { | r1  }
ALVII = { | r1 \mbreak }
BLIV = { | r1  }
BLV = { | r1  }
BLVI = { | r1  }
BLVII = { | r1 \mbreak }

% measures 58 - 61
ALVIII = { | r1  }
ALIX = { | r1  }
ALX = { | r1  }
ALXI = { | r1 \mbreak }
BLVIII = { | r1  }
BLIX = { | r1  }
BLX = { | r1  }
BLXI = { | r1 \mbreak }

% measures 62 - 65
ALXII = { | r1  }
ALXIII = { | r1  }
ALXIV = { | r1  }
ALXV = { | r1 \mbreak }
BLXII = { | r1  }
BLXIII = { | r1  }
BLXIV = { | r1  }
BLXV = { | r1 \mbreak }

% measures 66 - 69
ALXVI = { | r1  }
ALXVII = { | r1  }
ALXVIII = { | r1  }
ALXIX = { | r1 \mbreak \pgbreak }
BLXVI = { | r1  }
BLXVII = { | r1  }
BLXVIII = { | r1  }
BLXIX = { | r1 \mbreak \pgbreak }

% measures 70 - 73
ALXX = { | r1  }
ALXXI = { | r1  }
ALXXII = { | r1  }
ALXXIII = { | r1 \mbreak }
BLXX = { | r1  }
BLXXI = { | r1  }
BLXXII = { | r1  }
BLXXIII = { | r1 \mbreak }

% measures 74 - 77
ALXXIV = { | r1  }
ALXXV = { | r1  }
ALXXVI = { | r1  }
ALXXVII = { | r1 \mbreak }
BLXXIV = { | r1  }
BLXXV = { | r1  }
BLXXVI = { | r1  }
BLXXVII = { | r1 \mbreak }

% measures 78 - 81
ALXXVIII = { | r1  }
ALXXIX = { | r1  }
ALXXX = { | r1  }
ALXXXI = { | r1 \mbreak }
BLXXVIII = { | r1  }
BLXXIX = { | r1  }
BLXXX = { | r1  }
BLXXXI = { | r1 \mbreak }

% measures 82 - 85
ALXXXII = { | r1  }
ALXXXIII = { | r1  }
ALXXXIV = { | r1  }
ALXXXV = { | r1 \mbreak }
BLXXXII = { | r1  }
BLXXXIII = { | r1  }
BLXXXIV = { | r1  }
BLXXXV = { | r1 \mbreak }

% measures 86 - 89
ALXXXVI = { | r1  }
ALXXXVII = { | r1  }
ALXXXVIII = { | r1  }
ALXXXIX = { | r1 \mbreak }
BLXXXVI = { | r1  }
BLXXXVII = { | r1  }
BLXXXVIII = { | r1  }
BLXXXIX = { | r1 \mbreak }

% measures 90 - 93
AXC = { | r1  }
AXCI = { | r1  }
AXCII = { | r1  }
AXCIII = { | r1 \mbreak \pgbreak }
BXC = { | r1  }
BXCI = { | r1  }
BXCII = { | r1  }
BXCIII = { | r1 \mbreak \pgbreak }

% measures 94 - 98
AXCIV = { | r1  }
AXCV = { | r1  }
AXCVI = { | r1  }
AXCVII = { | r1  }
AXCVIII = { | r1 \mbreak }
BXCIV = { | r1  }
BXCV = { | r1  }
BXCVI = { | r1  }
BXCVII = { | r1  }
BXCVIII = { | r1 \mbreak }

% measures 99 - 102
AXCIX = { | r1  }
AC = { | r1  }
ACI = { | r1  }
ACII = { | r1 \mbreak }
BXCIX = { | r1  }
BC = { | r1  }
BCI = { | r1  }
BCII = { | r1 \mbreak }

% measures 103 - 106
ACIII = { | r1  }
ACIV = { | r1  }
ACV = { | r1  }
ACVI = { | r1 \mbreak }
BCIII = { | r1  }
BCIV = { | r1  }
BCV = { | r1  }
BCVI = { | r1 \mbreak }

% measures 107 - 110
ACVII = { | r1  }
ACVIII = { | r1  }
ACIX = { | r1  }
ACX = { | r1 \mbreak }
BCVII = { | r1  }
BCVIII = { | r1  }
BCIX = { | r1  }
BCX = { | r1 \mbreak }

% measures 111 - 114
ACXI = { | r1  }
ACXII = { | r1  }
ACXIII = { | r1  }
ACXIV = { | r1 \mbreak }
BCXI = { | r1  }
BCXII = { | r1  }
BCXIII = { | r1  }
BCXIV = { | r1 \mbreak }

% measures 115 - 118
ACXV = { | r1  }
ACXVI = { | r1  }
ACXVII = { | r1  }
ACXVIII = { | r1 \mbreak \pgbreak }
BCXV = { | r1  }
BCXVI = { | r1  }
BCXVII = { | r1  }
BCXVIII = { | r1 \mbreak \pgbreak }

% measures 119 - 122
ACXIX = { | r1  }
ACXX = { | r1  }
ACXXI = { | r1  }
ACXXII = { | r1 \mbreak }
BCXIX = { | r1  }
BCXX = { | r1  }
BCXXI = { | r1  }
BCXXII = { | r1 \mbreak }

% measures 123 - 126
ACXXIII = { | r1  }
ACXXIV = { | r1  }
ACXXV = { | r1  }
ACXXVI = { | r1 \mbreak }
BCXXIII = { | r1  }
BCXXIV = { | r1  }
BCXXV = { | r1  }
BCXXVI = { | r1 \mbreak }

% measures 127 - 130
ACXXVII = { | r1  }
ACXXVIII = { | r1  }
ACXXIX = { | r1  }
ACXXX = { | r1 \mbreak }
BCXXVII = { | r1  }
BCXXVIII = { | r1  }
BCXXIX = { | r1  }
BCXXX = { | r1 \mbreak }

% measures 131 - 134
ACXXXI = { | r1  }
ACXXXII = { | r1  }
ACXXXIII = { | r1  }
ACXXXIV = { | r1 \mbreak }
BCXXXI = { | r1  }
BCXXXII = { | r1  }
BCXXXIII = { | r1  }
BCXXXIV = { | r1 \mbreak }

% measures 135 - 138
ACXXXV = { | r1  }
ACXXXVI = { | r1  }
ACXXXVII = { | r1  }
ACXXXVIII = { | r1 \mbreak }
BCXXXV = { | r1  }
BCXXXVI = { | r1  }
BCXXXVII = { | r1  }
BCXXXVIII = { | r1 \mbreak }

% measures 139 - 142
ACXXXIX = { | r1  }
ACXL = { | r1  }
ACXLI = { | r1  }
ACXLII = { | r1 \mbreak \pgbreak }
BCXXXIX = { | r1  }
BCXL = { | r1  }
BCXLI = { | r1  }
BCXLII = { | r1 \mbreak \pgbreak }


APart = {
  \tempo 4 = 200
  \clef treble
  \time 2/2

  \AI \AII \AIII \AIV \AV 
  \AVI \AVII \AVIII \AIX 
  \AX \AXI \AXII \AXIII 
  \AXIV \AXV \AXVI \AXVII 
  \AXVIII \AXIX \AXX \AXXI 
  \AXXII \AXXIII \AXXIV \AXXV 
  \AXXVI \AXXVII \AXXVIII \AXXIX 
  \AXXX \AXXXI \AXXXII \AXXXIII 
  \AXXXIV \AXXXV \AXXXVI \AXXXVII 
  \AXXXVIII \AXXXIX \AXL \AXLI 
  \AXLII \AXLIII \AXLIV \AXLV 
  \AXLVI \AXLVII \AXLVIII \AXLIX 
  \AL \ALI \ALII \ALIII 
  \ALIV \ALV \ALVI \ALVII 
  \ALVIII \ALIX \ALX \ALXI 
  \ALXII \ALXIII \ALXIV \ALXV 
  \ALXVI \ALXVII \ALXVIII \ALXIX 
  \ALXX \ALXXI \ALXXII \ALXXIII 
  \ALXXIV \ALXXV \ALXXVI \ALXXVII 
  \ALXXVIII \ALXXIX \ALXXX \ALXXXI 
  \ALXXXII \ALXXXIII \ALXXXIV \ALXXXV 
  \ALXXXVI \ALXXXVII \ALXXXVIII \ALXXXIX 
  \AXC \AXCI \AXCII \AXCIII 
  \AXCIV \AXCV \AXCVI \AXCVII \AXCVIII 
  \AXCIX \AC \ACI \ACII 
  \ACIII \ACIV \ACV \ACVI 
  \ACVII \ACVIII \ACIX \ACX 
  \ACXI \ACXII \ACXIII \ACXIV 
  \ACXV \ACXVI \ACXVII \ACXVIII 
  \ACXIX \ACXX \ACXXI \ACXXII 
  \ACXXIII \ACXXIV \ACXXV \ACXXVI 
  \ACXXVII \ACXXVIII \ACXXIX \ACXXX 
  \ACXXXI \ACXXXII \ACXXXIII \ACXXXIV 
  \ACXXXV \ACXXXVI \ACXXXVII \ACXXXVIII 
  \ACXXXIX \ACXL \ACXLI \ACXLII 

}

BPart = {
  \tempo 4 = 200
  \clef bass
  \time 2/2

  \BI \BII \BIII \BIV \BV 
  \BVI \BVII \BVIII \BIX 
  \BX \BXI \BXII \BXIII 
  \BXIV \BXV \BXVI \BXVII 
  \BXVIII \BXIX \BXX \BXXI 
  \BXXII \BXXIII \BXXIV \BXXV 
  \BXXVI \BXXVII \BXXVIII \BXXIX 
  \BXXX \BXXXI \BXXXII \BXXXIII 
  \BXXXIV \BXXXV \BXXXVI \BXXXVII 
  \BXXXVIII \BXXXIX \BXL \BXLI 
  \BXLII \BXLIII \BXLIV \BXLV 
  \BXLVI \BXLVII \BXLVIII \BXLIX 
  \BL \BLI \BLII \BLIII 
  \BLIV \BLV \BLVI \BLVII 
  \BLVIII \BLIX \BLX \BLXI 
  \BLXII \BLXIII \BLXIV \BLXV 
  \BLXVI \BLXVII \BLXVIII \BLXIX 
  \BLXX \BLXXI \BLXXII \BLXXIII 
  \BLXXIV \BLXXV \BLXXVI \BLXXVII 
  \BLXXVIII \BLXXIX \BLXXX \BLXXXI 
  \BLXXXII \BLXXXIII \BLXXXIV \BLXXXV 
  \BLXXXVI \BLXXXVII \BLXXXVIII \BLXXXIX 
  \BXC \BXCI \BXCII \BXCIII 
  \BXCIV \BXCV \BXCVI \BXCVII \BXCVIII 
  \BXCIX \BC \BCI \BCII 
  \BCIII \BCIV \BCV \BCVI 
  \BCVII \BCVIII \BCIX \BCX 
  \BCXI \BCXII \BCXIII \BCXIV 
  \BCXV \BCXVI \BCXVII \BCXVIII 
  \BCXIX \BCXX \BCXXI \BCXXII 
  \BCXXIII \BCXXIV \BCXXV \BCXXVI 
  \BCXXVII \BCXXVIII \BCXXIX \BCXXX 
  \BCXXXI \BCXXXII \BCXXXIII \BCXXXIV 
  \BCXXXV \BCXXXVI \BCXXXVII \BCXXXVIII 
  \BCXXXIX \BCXL \BCXLI \BCXLII 

}

InstrumentAPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \APart
}

InstrumentBPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \BPart
}

InstrumentCombined = {
  \new PianoStaff <<
    \InstrumentAPart
    \InstrumentBPart
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}

