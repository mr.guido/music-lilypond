\version "2.24.2"

\header{
  title = "Ye Banks and Braes"
  composer = "Robert Burns (1791)"
  poet = "Scottish Melody"
  instrument = "For Concertina"
  tagline = "Below = left hand; above = right hand.\nCircles = draw; otherwise push."
}

LeftHand = {\set fingeringOrientations = #'(down) \set stringNumberOrientations = #'(down)}
RightHand = {\set fingeringOrientations = #'(up) \set stringNumberOrientations = #'(up)}

Treble = {
  \tempo 4 = 100
  \clef treble
  \time 3/4
  
  % measures 1-9
  \LeftHand s2 <g'-5>4
  | \RightHand <c''-1>2 <c''-1>4
  | <d''\2>4 <c''-1>4 <d''\2>4
  | <e''-2>4 <g''-3>4 <e''-2>4
  | <d''\2>4 <c''-1>4 <d''\2>4
  | <e''-2>4. <d''\2>8 <c''-1>4
  | <c''-1>4 \LeftHand <a'\5>4 <g'-5>4
  | <g'-5>4 <a'\5>4 \RightHand <c''-1>4
  | <d''\2>2 \LeftHand <g'-5>4

  % measures 10-17
  \break
  | \RightHand <c''-1>2 <c''-1>4
  | <d''\2>4 <c''-1>4 <d''\2>4
  | <e''-2>4 <g''-3>4 <e''-2>4
  | <d''\2>4 <c''-1>4 <d''\2>4
  | <e''-2>4. <d''\2>8 <c''-1>4
  | <c''-1>4 \LeftHand <a'\5>4 <g'-5>4
  | <g'-5>4 <a'\5>4 \RightHand <b'\1>4
  | <c''-1>2 <e''-2>4

  % measures 18-25
  \break
  | <g''-3>2 <a''\4>4
  | <g''-3>4 <e''-2>4 <c''-1>4
  | <g''-3>2 <a''\4>4
  | <g''-3>4 <e''-2>4 <c''-1>4
  | <g''-3>4 <e''-2>4 <c''-1>4
  | <g''-3>4 <e''-2>4 <g''-3>4
  | <a''\4>4. <g''-3>8 <f''\3>8 <e''-2>8
  | <e''-2>4 <d''\2>4\fermata <e''-2>8 <d''\2>8

  % measures 26-33
  \break
  | <c''-1>2 <c''-1>4
  | <d''\2>4 <c''-1>4 <d''\2>4
  | <e''-2>4 <g''-3>4 <e''-2>4
  | <d''\2>4 <c''-1>4 <d''\2>4
  | <e''-2>4. <d''\2>8 <c''-1>4
  | <c''-1>4 \LeftHand <a'\5>4 <g'-5>4
  | <g'-5>4 <a'\5>4 \RightHand <b'\1>4
  | <c''-1>2 r4
}

ConcertinaTreble = \new Staff \with {midiInstrument = "concertina"} { 
  \Treble
}

ConcertinaCombined = {
  \new PianoStaff <<
    \ConcertinaTreble
  >>
}

\score {
  \ConcertinaCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \ConcertinaCombined
    }
  >>
  \midi {
    \tempo 4 = 100
  }
}
