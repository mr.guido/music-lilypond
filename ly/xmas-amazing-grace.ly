\version "2.24.2"

\header{
  title = "Amazing Grace"
}

\score {
  <<
    \new Staff = "staff" \with {midiInstrument = "accordion"} {
      \new Voice = "melody" {
        { \time 3/4
          s2 g'4
          c''2 {e''8 (c''8)}
          e''2 d''4
          c''2 a'4
          g'2 g'4
          \break
          c''2 {e''8 (c''8)}
          e''2 d''4
          g''2.~ g''2 e''4
          \break
          g''2 {g''8 (e''8)}
          c''2 g'4
          a'2 {c''8 (a'8)}
          g'2 g'4
          \break
          c''2 {e''8 (c''8)}
          e''2 d''4
          c''2.~ c''2
          \bar "||"
        }
      }
    }

    \new Lyrics {
      \lyricsto "melody" {
        "A-" "maz-" "ing" "grace,"
        "how" "sweet" "the" "sound,"
        "that" "saved" "a" "wretch" "like" "me."
        "I" "once" "was" "lost" "but" "now" "am" "found,"
        "was" "blind," "but" "now" "I" "see."
      }
    }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 100
  }
}
