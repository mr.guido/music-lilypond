\version "2.24.2"

\header{
  title = "Silent Night"
  subtitle = "For Concertina"
}

\score {
  <<
    \new Staff = "staff" \with {midiInstrument = "acoustic grand"} {
      \new Voice = "melody" {
        \relative {
           \time 3/4
            g'4. a8 g4 e2.
            g4. a8 g4 e2.
            d'2 d4 b2. c2
            c4 g2. \break a2 a4
            c4. b8 a4 g4.
            a8 g4 e2. a2
            a4 c4. b8 a4
            g4. a8 g4 e2. \break
            d'2 d4 f4. d8
            b4 c2. e2. c4
            g4 e4 g4. f8
            d4 c2
         }
      }
    }
    % Lyrics below are for the left hand
    % The horizontal line means draw; if omitted it means push
    \new Lyrics {
      \lyricsto "melody" {
        " "  "_"  " "  " " % g'4. a8 g4 e2.
        " "  "_"  " "  " " % g4. a8 g4 e2.
        " "  " "  " "  " " % d'2 d4 b2. c2
        " "  " "  "_"  "_" % c4 g2. a2 a4
        " "  " "  "_"  " " % c4. b8 a4 g4.
        "_"  " "  " "  "_" % a8 g4 e2. a2
        "_"  " "  " "  "_" % a4 c4. b8 a4
        " "  "_"  " "  " " % g4. a8 g4 e2.
        " "  " "  " "  " " % d'2 d4 f4. d8
        " "  " "  " "  " " % b4 c2. e2. c4
        " "  " "  " "  "_" % g4 e4 g4. f8
        "_"  " " % d4 c2
      }
    }
    % 5 means index finger
    \new Lyrics {
      \lyricsto "melody" {
        "5"  "5"  "5"  "4" % g'4. a8 g4 e2.
        "5"  "5"  "5"  "4" % g4. a8 g4 e2.
        " "  " "  " "  " " % d'2 d4 b2. c2
        " "  "5"  "5"  "5" % c4 g2. a2 a4
        " "  " "  "5"  "5" % c4. b8 a4 g4.
        "5"  "5"  "4"  "5" % a8 g4 e2. a2
        "5"  " "  " "  "5" % a4 c4. b8 a4
        "5"  "5"  "5"  "4" % g4. a8 g4 e2.
        " "  " "  " "  " " % d'2 d4 f4. d8
        " "  " "  " "  " " % b4 c2. e2. c4
        "5"  "4"  "5"  "4" % g4 e4 g4. f8
        "3"  "3" % d4 c2
      }
    }
    % Lyrics above are for the right hand
    % Horizontal bar means draw; if omitted it means push
    \new Lyrics \with { alignAboveContext = "staff" } {
      \lyricsto "melody" {
        " "  " "  " "  " " % g'4. a8 g4 e2.
        " "  " "  " "  " " % g4. a8 g4 e2.
        "_"  "_"  "_"  " " % d'2 d4 b2. c2
        " "  " "  " "  " " % c4 g2. a2 a4
        " "  "_"  " "  " " % c4. b8 a4 g4.
        " "  " "  " "  " " % a8 g4 e2. a2
        " "  " "  "_"  " " % a4 c4. b8 a4
        " "  " "  " "  " " % g4. a8 g4 e2.
        "_"  "_"  "_"  "_" % d'2 d4 f4. d8
        "_"  " "  " "  " " % b4 c2. e2. c4
        " "  " "  " "  " " % g4 e4 g4. f8
        " "  " " % d4 c2
      }
    }
    % 1 means index finger
    \new Lyrics \with { alignAboveContext = "staff" } {
      \lyricsto "melody" {
        " "  " "  " "  " " % g'4. a8 g4 e2.
        " "  " "  " "  " " % g4. a8 g4 e2.
        "2"  "2"  "1"  "1" % d'2 d4 b2. c2
        "1"  " "  " "  " " % c4 g2. a2 a4
        "1"  "1"  " "  " " % c4. b8 a4 g4.
        " "  " "  " "  " " % a8 g4 e2. a2
        " "  "1"  "1"  " " % a4 c4. b8 a4
        " "  " "  " "  " " % g4. a8 g4 e2.
        "2"  "2"  "3"  "2" % d'2 d4 f4. d8
        "1"  "1"  "2"  "1" % b4 c2. e2. c4
        " "  " "  " "  " " % g4 e4 g4. f8
        " "  " " % d4 c2
      }
    }
    
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 95
  }
}
