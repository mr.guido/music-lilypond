\version "2.24.2"

\header{
  title = "Happy Birthday"
}

\score {
  \new Staff \with {midiInstrument = "acoustic guitar (nylon)"} <<
  {
    \time 3/4
    g'8 g'8 a'4 g'4
    c''4 b'2
    g'8 g'8 a'4 g'4
    d''4 c''2
    g'8 g'8 g''4 e''4
    c''4 b'4 a'4
    f''8 f''8 e''4 c''4
    d''4 c''2
  }

  \addlyrics { 
    "Ha-" "ppy" "Birth" "Day"
    "To" "You"
    "Ha-" "ppy" "Birth" "Day"
    "To" "You"
    "Ha-" "ppy" "Birth" "Day" "Dear" "Jeff" "rey"
    "Ha-" "ppy" "Birth" "Day" "To" "You!"
  }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 120
  }
}
