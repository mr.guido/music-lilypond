\version "2.24.2"

\header{
  title = "Twinkle, Twinkle, Little Star"
  subtitle = "Higher Octave"
}

\score {
  <<
    \new Staff = "staff" {
      \new Voice = "melody" {
        {
          c''4 c''4 g''4 g''4
          a''4 a''4 g''2
          f''4 f''4 e''4 e''4
          d''4 d''4 c''2
          \break
          g''4 g''4 f''4 f''4
          e''4 e''4 d''2
          g''4 g''4 f''4 f''4
          e''4 e''4 d''2
          \break
          c''4 c''4 g''4 g''4
          a''4 a''4 g''2
          f''4 f''4 e''4 e''4
          d''4 d''4 c''2
        }
      }
    }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 100
  }
}
