\version "2.24.2"

\header{
  title = "Star Wars"
  subtitle = "Anakin's Theme"
  composer = "Music by John Williams"
  arranger = "Arranged by Dan Coates"
  source = "Star Wars: A Musical Journey"
}

\include "articulate.ly"

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = { }
pgbreak = {  }

Voices_BI_BII = {
  <<
  { \voiceOne
    | r8\p e8[ r8 e8] r8 e8[ r8 e8]
    | r8\< e8[ r8 e8] r8 e8[ r8 \after 16 \! e8]
  }
  \new Voice
  { \voiceTwo
    | a,1\p
    | \after 2.. \! a,1\<
  }
  >>
  \oneVoice
}
Voices_AVI = {
  <<
  { \voiceOne
    | d''4 d'8 a'8 e'2) \mbreak
  }
  \new Voice
  { \voiceTwo
    | s4 s4 d'4 cis'4 \mbreak
  }
  >>
  \oneVoice
}
Voices_BVI = {
  <<
  { \voiceOne
    | aes4 f4 s2 \mbreak
  }
  \new Voice
  { \voiceTwo
    | bes,2 <a, e>2 \mbreak
  }
  >>
  \oneVoice
}
Voices_AX = {
  <<
  { \voiceOne
    | e'4\> d'4 \after 16 \! a'2)
  }
  \new Voice
  { \voiceTwo
    | s2 d'4 cis'4
  }
  >>
  \oneVoice
}
Voices_BIX = {
  <<
  { \voiceOne
    | <f a>2 <e a>2
  }
  \new Voice
  { \voiceTwo
    | e4 d4 s2
  }
  >>
  \oneVoice
}
Voices_BXII = {
  <<
  { \voiceOne
    | c'4 d'4 s2
  }
  \new Voice
  { \voiceTwo
    | f2 <e c'>2
  }
  >>
  \oneVoice
}
Voices_AXIII = {
  <<
  { \voiceOne
    | r8 a'8 b'8[ c''8] b'8 e'8 \tuplet 3/2 {e''8 d''8 c''8}
  }
  \new Voice
  { \voiceTwo
    | e'2 s2
  }
  >>
  \oneVoice
}
Voices_BXVIII = {
  <<
  { \voiceOne
    | f'2 <f c'>2 \mbreak
  }
  \new Voice
  { \voiceTwo
    | e'4 d'4 b4 a4 \mbreak
  }
  >>
  \oneVoice
}
Voices_AXXII = {
  <<
  { \voiceOne
    | <aes' d''>4 c''8 b'8 <e' b'>4 c''4)
  }
  \new Voice
  { \voiceTwo
    | s4 f'4 s2
  }
  >>
  \oneVoice
}
Voices_BXXI = {
  <<
  { \voiceOne
    | r8 e8[ r8 e8] r8 fis8[ r8 fis8]
  }
  \new Voice
  { \voiceTwo
    | a,2 a,2
  }
  >>
  \oneVoice
}
Voices_AXXVIII = {
  <<
  { \voiceOne
    | <cis'' a''>4\mf gis''8 fis''8 dis''4 e''4)
  }
  \new Voice
  { \voiceTwo
    | s2 <a' cis''>2
  }
  >>
  \oneVoice
}
Voices_AXXXII = {
  <<
  { \voiceOne
    | <cis'' a''>4\> gis''8 fis''8 dis''4 \after 8. \! e''4)
  }
  \new Voice
  { \voiceTwo
    | s2 a'2
  }
  >>
  \oneVoice
}
Voices_BXXXV_BXXXVI = {
  <<
  { \voiceOne
    | r8 e8[ r8 e8] r8 e8[ r8 e8]
    | r8 e8[ r8 e8] r8 e8[ r8 e8] \mbreak
  }
  \new Voice
  { \voiceTwo
    | a,1
    | a,1 \mbreak
  }
  >>
  \oneVoice
}
Voices_BXXXIX = {
  <<
  { \voiceOne
    | <f'>2 s2
  }
  \new Voice
  { \voiceTwo
    | e'4 d'4 <c' e'>4 <aes f'>4
  }
  >>
  \oneVoice
}
Voices_BXLI_BXLII = {
  <<
  { \voiceOne
    | s2 r8 e8( a8[ b8]
    | cis'1)\fermata\pp
  }
  \new Voice
  { \voiceTwo
    | <a, e>4 <f, c>4 a,2~
    | a,1\fermata\pp
  }
  >>
  \oneVoice
}

% measures 1 - 3
AI = { | r4\p a4( b4 dis'4) }
AII = { | r4\< a4( b4 fis'8 dis'8) }
AIII = { | e'4(\!\mp <cis' a'>4 <dis' b'>4 fis''4 \mbreak }
BI_BII = \Voices_BI_BII
BIII = { | a,8\mp e8 a4 a,8 fis8 b4 \mbreak }

% measures 4 - 6
AIV = { | <c'' fis''>4 e''8 dis''8 <cis'' dis''>4 e''4) }
AV = { | <a' f''>4( a'8 c''8 ees'4 gis'8 a'8 }
AVI = \Voices_AVI
BIV = { | gis,8 fis8 c'4 a,8 e8 cis'4 }
BV = { | <f ees'>2 f,2 }
BVI = \Voices_BVI

% measures 7 - 9
AVII = { | e'4( <cis' a'>4 <dis' b'>4 fis''4 }
AVIII = { | <c'' gis''>4 fis''8 dis''8 <cis'' dis''>4 e''4) }
AIX = { | a''4( a'8 b'8 c''4 cis''8 e'8 \mbreak }
BVII = { | a,8 e8 a4 a,8 fis8 b4 }
BVIII = { | a,8 fis8 c'4 a,8 e8 cis'4  }
BIX = \Voices_BIX

% measures 10 - 12
AX = \Voices_AX
AXI = { | r8\p\< c''8( b'8[ a'8] e''8 a'8 \tuplet 3/2 {b'8 a'8 g'8} }
AXII = { | a'4\! b'8 e'8 <g' b'>4 c''4) \mbreak \pgbreak }
BX = { | <b, f a>2\> \after 16 \! <a, e>2 }
BXI = { | c'2\p\< b4 \after 8. \! g4 }
BXII = \Voices_BXII

% measures 13 - 15
AXIII = \Voices_AXIII
AXIV = { | <a' d''>4 <fis' a'>2.\arpeggio }
AXV = { | r8\mp fis'8( ais'8[ b'8] c''8 b'8 e''8 b''8 \mbreak }
BXIII = { | c4 b,8 a,8 <g, e>2 }
BXIV = { | <f, d>4 <d, a,>2.\arpeggio }
BXV = { | fis,8\mp dis8 fis4 g,8 e8 g4 \mbreak }

% measures 16 - 18
AXVI = { | a''8\< c''8 <f'' a''>8 <d'' b''>8 <d'' b''>4 <e'' c'''>4) }
AXVII = { | <fis'' d'''>4(\>\f a''4 <f'' c'''>4~ \tuplet 3/2 {<f'' c'''>8 b''8 a''8} }
AXVIII = { | g''8\!\mp f''8 b'8 c''8 gis'8 a'8) r16 b'16( c''16 d''16 \mbreak }
BXVI = { | f,8 c8 a4 c8 g8 c'4 }
BXVII = { | d,8 a,8 fis4 f,8 c8 a4 }
BXVIII = \Voices_BXVIII

% measures 19 - 21
AXIX = { | e''4 a'4 d''4~ \tuplet 3/2 {d''8 c''8 b'8} }
AXX = { | a'8 g'8 c'8 d'8 e'4\< d'4) }
AXXI = { | <cis' e'>4(\!\mf a4 dis'4 b'4 \mbreak }
BXIX = { | <e a c'>2 <d fis a>2 }
BXX = { | <c f a>2 <b, gis>4 <bes, g>4 }
BXXI = \Voices_BXXI

% measures 22 - 24
AXXII = \Voices_AXXII
AXXIII = { | <aes' f''>4( aes'8 c''8 f'8 g'8 g'8 aes'8 }
AXXIV = { | d''4\> c''8 b'8 d'4) e'8( f'8 \mbreak }
BXXII = { | f,8 c8 aes4 c,8 g,8 e4 }
BXXIII = { | f,8 c8 f4 c4 f,4 }
BXXIV = { | <a, f>2 <bes, g>2 \mbreak }

% measures 25 - 27
AXXV = { | e'4\mp\< <cis' a'>4 <dis' b'>4 fis''4 }
AXXVI = { | <d'' b''>4\!\mf a''8 gis''8 <cis'' gis''>4 a''4) }
AXXVII = { | <d'' bes''>4(\f a''8\> g''8 e''4 \after 8. \! f''4 \mbreak \pgbreak }
BXXV = { | a,8 e8 a4 a,8 fis8 b4 }
BXXVI = { | a,8 f8 b4 a,8 e8 a4 }
BXXVII = { | bes,8 f8 bes8 f8 d'4 bes4 \mbreak \pgbreak }

% measures 28 - 30
AXXVIII = \Voices_AXXVIII
AXXIX = { | <c'' aes''>4( g''8 f''8 g'4 aes'4 }
AXXX = { | <cis' a'>4 f'8 c''8 <cis' a'>4) e''8( a''8 \mbreak }
BXXVIII = { | e2 e,2 }
BXXIX = { | f,8 c8 aes4 f4 c4 }
BXXX = { | <a, e>4 <f, c>4 <a, e>2 \mbreak }

% measures 31 - 33
AXXXI = { | <d'' bes''>4 a''8 g''8 e''4 f''4 }
AXXXII = \Voices_AXXXII
AXXXIII = { | <c'' aes''>4(\mf g''8 f''8 g'4 aes'4 \mbreak }
BXXXI = { | bes,8 f8 bes8 f8 d'4 bes4 }
BXXXII = { | a,8 e8 a8 e8 f4 e4 }
BXXXIII = { | f,8 c8 aes4 f4 c4 \mbreak }

% measures 34 - 36
AXXXIV = { | <cis' a'>4 f'8 c''8 <cis' a'>2) }
AXXXV = { | r4\mp a4( b4 dis'4) }
AXXXVI = { | r4 a4(\< b4 fis'8 \after 16. \! dis'8) \mbreak }
BXXXIV = { | <a, e>4 <f, c>4 <a, e>2 }
BXXXV_BXXXVI = \Voices_BXXXV_BXXXVI

% measures 37 - 39
AXXXVII = { | e'4( <cis' a'>4 <dis' b'>4 fis''4 }
AXXXVIII = { | <c'' gis''>4 fis''8 dis''8 <cis'' dis''>4\< \after 8. \! e''4) }
AXXXIX = { | a''4( \tempo 4 = 109 a'8. \tempo 4 = 108 a'16 \tempo 4 = 107 e''8\> \tempo 4 = 106 dis''16 \tempo 4 = 105 e''16 \tempo 4 = 104 f''8 \tempo 4 = 103 c''8 \tempo 4 = 102 \mbreak }
BXXXVII = { | a,8 e8 a4 a,8 fis8 b4 }
BXXXVIII = { | a,8 fis8 c'4 a,8 e8 cis'4  }
BXXXIX = \Voices_BXXXIX

% measures 40 - 42
AXL = { | aes'4\! f'8.\p c''16 }
AXLI = { | a'4) f'8( c''8 \after 4 \> <cis' a'>2) }
AXLII = { | <e'' a''>1\!\pp\fermata \mbreak \pgbreak }
BXL = { | <f c'>2 }
BXLI_BXLII = \Voices_BXLI_BXLII

APart = {
  \tempo 4 = 110
  \key a \major
  \clef treble
  \time 4/4

  \AI \AII \AIII 
  \AIV \AV \AVI 
  \AVII \AVIII \AIX 
  \AX \AXI \AXII 
  \AXIII \AXIV \AXV 
  \AXVI \AXVII \AXVIII 
  \AXIX \tempo 4 = 100 \AXX \tempo 4 = 110 \AXXI 
  \AXXII \AXXIII \AXXIV 
  \AXXV \AXXVI \AXXVII 
  \AXXVIII \AXXIX \AXXX 
  \AXXXI \AXXXII \AXXXIII 
  \AXXXIV \AXXXV \AXXXVI 
  \AXXXVII \AXXXVIII \AXXXIX
  \time 2/4
  \AXL
  \time 4/4
  \AXLI \AXLII 
}

BPart = {
  \clef bass
  \key a \major
  \time 4/4

  \BI_BII \BIII 
  \BIV \BV \BVI 
  \BVII \BVIII \BIX 
  \BX \BXI \BXII 
  \BXIII \BXIV \BXV 
  \BXVI \BXVII \BXVIII 
  \BXIX \BXX \BXXI 
  \BXXII \BXXIII \BXXIV 
  \BXXV \BXXVI \BXXVII 
  \BXXVIII \BXXIX \BXXX 
  \BXXXI \BXXXII \BXXXIII 
  \BXXXIV \BXXXV_BXXXVI
  \BXXXVII \BXXXVIII \BXXXIX 
  \time 2/4
  \BXL
  \time 4/4
  \BXLI_BXLII
}

InstrumentAPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \APart
}

InstrumentBPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \BPart
}

InstrumentCombined = {
  \new PianoStaff <<
    \set PianoStaff.connectArpeggios = ##t
    \InstrumentAPart
    \InstrumentBPart
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats \articulate {
      \InstrumentCombined
    }
  >>
  \midi { }
}

