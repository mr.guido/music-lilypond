\version "2.24.2"

\header{
  title = "Super Mario Bros."
  subtitle = "GROUND THEME"
  composer = "Koji Kondo"
  arranger = "Sakiko Masuda"
}

\score {
  \new PianoStaff <<
    \new Staff \with {midiInstrument = "acoustic grand"} <<
    { \clef treble 
      <a' e''>8 <a' e''>8 r8 <a' e''>8-> r8 <a' c''>8 e''8 <c'' e'' g''>8->
      r4 r8 <b ees' g'>8-> r2
      <e' g' c''>4 r8 <d' g'>8 r4 <des' e'>4
      r8 <e' a'>8 [r8 <g' b'>8~->] <g' b'>8 bes'8 a'8 <d' g'>8->
      \break
      r8 e''8 g''8 <cis'' a''>-> r8 f''8 [r8 <c'' g''>8->]
      r8 e''8 [r8 c''8] d''8 <aes' b'>8-> r4
      r8 g'8 g''8 ges''8 f''8 dis''8 r8 e''8->
      r8 gis'8 a'8 c''8 r8 a'8 c''8 d''8
      \break
      r8 g'8 g''8 ges''8 f''8 dis''8 e''8 c'''8->
      r1
      r8 g'8 g''8 ges''8 f''8 dis'' r8 e''8->
      r8 gis'8 a'8 c''8 r8 a'8 c''8 d''8
      \break

      r4 <g' c'' ees''>4-. r8 <aes' c'' d''>8-> [r8 <g' c''>8->]
      r1
      <ees' g' c''>8 <ees' g' c''>8 r8 <ees' aes' c''>8 r8 <f' aes' d''>4.->
      <g' e''>8 c''8 r8 a'8 g'4 r8 <ees' aes' c''>8->
      \break
      r8 c''8 [r8 <d' aes' c''>8->] r8 c''8 d''8 <g' b' e''>8->
      r1
      <ees' g' c''>8 <ees' g' c''>8 r8 <ees' aes' c''>8 r8 <f' aes' d''>4.->
      <g' e''>8 c''8 r8 a'8 g'4 r8 <a' e''>8->
      \break
      r8 <a' e''>8 [r8 <a' e''>8->] r8 <a' c''>8 e''8 <c'' e'' g''>8->
      r4 r8 <b ees' g'>8-> r2

      <g' e''>8 c''8 r8 <bes e' g'>8~-> <bes e' g'>4 gis'4
      \break
      
      <c' f' a'>8 f''8 r8 f''8 a'4 r8 <d' f' b'>->
      r8 a''8 [r8 a''8] <b' a''>8 g''8 f''8 <b' e''>8->
      r8 c''8 [r8 a'8] g'4 r8 <g' e''>8->
      r8 c''4. <bes e' g'>4-_ gis'-_
      \break
      <c' f' a'>8 f''8 r8 f''8 a'4 r8 <d' f' b'>8->
      r8 f'8 r8 <c'' f''>8~-> <c'' f''>8 e''8 <b' d''>8 c''8->
      r1
    }
    
    \new Staff \with {midiInstrument = "string ensemble 1"} 
    { \clef bass 
      <f c'>8 <f c'>8 r8 <fis c'>8-> r8 <c' fis'>8 [r8 <d' f'>8->]
      r4 r8 <g, f>8-> r2
      <g c'>4\staccato r8 <f bes>8 r4 <e b>4
      r8 <f c'>8 [r8 <a ees'>8->~] <a ees'>4. <e b>8->
      \break
      r4 r8 <g cis' f'>8-> r4 r8 <f a c' e'>8->
      r2 r8 <f b ees'>8-> r4
      c4 r4 r8 <bes e' aes'>8 [r8 <a d' g'>8->]
      r2 r8 <f c' e'>4.
      \break
      <e b d'>4 r4 <d a c'>4. <c g a>8->
      r8 g,4.-> gis,4-_ a,4-_
      c4 r4 r8 <bes e' aes'>8 [r8 <a d' g'>8->]
      r2 r8 <f c' e'>4.
      \break

      r4 <aes ees'>4-. r8 <bes f'>8-> [r8 <c' e'>8->]
      r8 dis8 e8 c8-> r2
      aes,4 r4 r8 bes,4.->
      c4 r4 r8 dis8 e8 f8->
      \break
      r4 r8 <bes, f>8-> r4 r8 <c g>8->
      r8 g,4.-> c4-_ bes,4-_
      aes,4 r4 r8 bes,4.->
      c4 r4 r8 dis8 e8 <f c'>8->
      \break
      r8 <f c'>8 [r8 <fis c'>8->] r8 <c' fis'>8 [r8 <d' f'>8->]
      r4 r8 <g, f>8-> r2

      c4 r4 r8 bes,4.->
      \break

      a,4 r4 r8 f,8 fis,8 g,8->
      r2 <g f'>4. <c' g'>8->
      r2 r8 dis8 e8 c8->
      r2 bes,2
      \break
      a,4 r4 r8 f8 fis8 g8->
      r4 r8 d8~-> d4 g8 c8->
      r2 r8 g,8 gis,8 a,8->
    }
    >>
  >>
  \layout {
    indent = 0.0
  }
  \midi {
    \tempo 4 = 180
  }
}
