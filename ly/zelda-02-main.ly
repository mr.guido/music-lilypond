\version "2.24.2"

\header{
  title = "The Legend of Zelda"
  subtitle = "Main Theme"
  composer = "Composed by Koji Kondo"
  arranger = "Arranged by Shinobu Amayake"
  source = "The Legend of Zelda Series For Easy Piano (Kindle)"
}

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = {  }
pgbreak = {  }

% measures 1 - 3
AI = { | <e' c''>2\mp \tuplet 3/2 {r4 c''8-.} \tuplet 3/2 {c''8-. c''8-. c''8-.} }
AII = { | \tuplet 3/2 {<d' c''>4-. bes'8} <d' c''>4 \tuplet 3/2 {r4 c''8-.} \tuplet 3/2 {c''8-. c''8-. c''8-.} }
AIII = { | \tuplet 3/2 {<ees' c''>4-. bes'8} <ees' c''>4 \tuplet 3/2 {r4 c''8-.} \tuplet 3/2 {c''8-. c''8-. c''8-.} \mbreak }
BI = { | c4-.\mp \tuplet 3/2 {c8-. c8-. c8-.} c4-. \tuplet 3/2 {c8-. c8-. c8-.} }
BII = { | bes,4-. \tuplet 3/2 {bes,8-. bes,8-. bes,8-.} bes,4-. \tuplet 3/2 {bes,8-. bes,8-. bes,8-.} }
BIII = { | aes,4-. \tuplet 3/2 {aes,8-. aes,8-. aes,8-.} aes,4-. \tuplet 3/2 {aes,8-. aes,8-. aes,8-.} \mbreak }

% measures 4 - 6
AIV = { | c''8-. g'16 g'16 g'8 g'16 g'16 g'8 g'16 g'16 g'8-. g'8-. }
AV = { | <e' c''>4-.-- <e' g'>4 r8. c''16 c''16 d''16 e''16 f''16 }
AVI = { | <bes' g''>2 r8 g''8 \tuplet 3/2 {g''8 aes''8 bes''8} \mbreak }
BIV = { | g,4-. g,4-. g,4-. a,8-. b,8-. }
BV = { | c4-. \tuplet 3/2 {c8-. c8-. bes,8-.} c4-. c4-. }
BVI = { | bes,4-. \tuplet 3/2 {bes,8-. bes,8-. aes,8-.} bes,4-. bes,4-. \mbreak }

% measures 7 - 9
AVII = { | <ees'' c'''>2 \tuplet 3/2 {r8 c'''8 c'''8} \tuplet 3/2 {c'''8 bes''8 aes''8} }
AVIII = { | <ees'' bes''>8. aes''16 g''2 g''4 }
AIX = { | f''8 f''16 g''16 aes''2 g''8 f''8 \mbreak }
BVII = { | aes,4-. \tuplet 3/2 {aes,8-. aes,8-. ges,8-.} aes,4-. aes,4-. }
BVIII = { | ees4-. \tuplet 3/2 {ees8-. ees8-. d8-.} ees4-. ees4-. }
BIX = { | des4-. \tuplet 3/2 {des8-. des8-. c8-.} des4-. des8-. des8-. \mbreak }

% measures 10 - 12
AX = { | ees''8 ees''16 f''16 g''2 f''8 ees''8 }
AXI = { | d''8 d''16 e''16 fis''2 <c'' a''>4 }
AXII = { | <b' g''>8-. g'16 g'16 g'8 g'16 g'16 g'8 g'16 g'16 g'8-. g'8-. \mbreak \pgbreak }
BX = { | c4-. \tuplet 3/2 {c8-. c8-. bes,8-.} c4-. c8-. c8-. }
BXI = { | d4-. \tuplet 3/2 {d8-. d8-. cis8-.} d4-. \tuplet 3/2 {d8-. d8-. d8-.} }
BXII = { | g,4-. g,4-. g,4-. a,8-. b,8-. \mbreak \pgbreak }

% measures 13 - 15
AXIII = { | <e' c''>4-.--\< <e' g'>4 r8. c''16 c''16 d''16 e''16 f''16 }
AXIV = { | <bes' g''>2 r8 g''8 \tuplet 3/2 {g''8\! aes''8 bes''8} }
AXV = { | <ees'' c'''>2. <ges'' ees'''>4 \mbreak }
BXIII = { | c4-. \tuplet 3/2 {c8-. c8-. bes,8-.} c4-. c4-. }
BXIV = { | bes,4-. \tuplet 3/2 {bes,8-. bes,8-. aes,8-.} bes,4-. bes,4-. }
BXV = { | aes,4-. \tuplet 3/2 {aes,8-. aes,8-. ges,8-.} aes,4-. aes,4-. \mbreak }

% measures 16 - 18
AXVI = { | <f'' d'''>4-.-- b''4 r4 <b' g''>4\f }
AXVII = { | <c'' aes''>2 r4 <ees'' c'''>4 }
AXVIII = { | <d'' b''>4-.-- g''4 r4 <b' g''>4 \mbreak }
BXVI = { | g,4-. \tuplet 3/2 {g,8-. g,8-. f,8-.} g,4-. g,4-. }
BXVII = { | fis,4 \tuplet 3/2 {fis,8 a,8 c8} ees4 r4 }
BXVIII = { | g4 \tuplet 3/2 {g,8-. g,8-. g,8-.} g,4-. r4 \mbreak }

% measures 19 - 21
AXIX = { | <c'' aes''>2 r4 <ees'' c'''>4 }
AXX = { | <d'' b''>4-.-- g''4 r4 <b' e''>4 }
AXXI = { | <aes' f''>2. <des'' aes''>4 \mbreak }
BXIX = { | fis,4 \tuplet 3/2 {fis,8 a,8 c8} ees4 r4 }
BXX = { | g4-. \tuplet 3/2 {g,8-. g,8-. g,8-.} g,4-. r4 }
BXXI = { | des4-. \tuplet 3/2 {des8-. des8-. c8-.} des4-. \tuplet 3/2 {des8-. des8-. des8-.} \mbreak }

% measures 22 - 24
AXXII = { | <c'' g''>4-.-- ees''4 r4 c''4 }
AXXIII = { | d''8 d''16 e''16 fis2 <c'' a''>4  }
AXXIV = { | <b' g''>8-. g'16 g'16 g'8 g'16 g'16 g'8 g'16 g'16 g'8-. g'8-. \mbreak \pgbreak }
BXXII = { | c4-. \tuplet 3/2 {c8-. c8-. bes,8-.} c4-. \tuplet 3/2 {c8-. c8-. c8-.}}
BXXIII = { | d4-. \tuplet 3/2 {d8-. d8-. cis8-.} d4 \tuplet 3/2 {d8-. d8-. d8-.} }
BXXIV = { | g,4-. g,4-. g,4-. a,8-. b,8-. \mbreak \pgbreak }


APart = {
  \tempo 4 = 100
  \clef treble
  \time 4/4

  \AI \AII \AIII 
  \AIV 
  \repeat volta 2 {
    \AV \AVI 
    \AVII \AVIII \AIX 
    \AX \AXI \AXII 
    \AXIII \AXIV \AXV 
    \AXVI \AXVII \AXVIII 
    \AXIX \AXX \AXXI 
    \AXXII \AXXIII \AXXIV 
  }
}

BPart = {
  \clef bass

  \BI \BII \BIII 
  \BIV 
  \repeat volta 2 {
    \BV \BVI 
    \BVII \BVIII \BIX 
    \BX \BXI \BXII 
    \BXIII \BXIV \BXV 
    \BXVI \BXVII \BXVIII 
    \BXIX \BXX \BXXI 
    \BXXII \BXXIII \BXXIV 
  }
}

InstrumentAPart = \new Staff \with {midiInstrument = "electric grand"} {
  \APart
}

InstrumentBPart = \new Staff \with {midiInstrument = "electric grand"} {
  \BPart
}

InstrumentCombined = {
  \new PianoStaff <<
    \InstrumentAPart
    \InstrumentBPart
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}
