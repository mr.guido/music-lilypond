\version "2.24.2"

\header{
  title = "Itsy Bitsy Spider"
  instrument = "For Concertina"
}

\score {
  <<
    \new Staff = "staff" {
      \new Voice = "melody" {
        { \time 6/8
          r2 r8 g'8
          c''4 c''8 c''4 d''8
          e''4. e''4 e''8
          d''4 c''8 d''4 e''8
          c''2.
          e''4. e''4 f''8
          g''4. g''4.
          \break
          f''4 e''8 f''4 g''8
          e''2.
          c''4. c''4 d''8
          e''4. e''4.
          d''4 c''8 d''4 e''8
          \break
          c''4. g'4 g'8
          c''4 c''8 c''4 d''8
          e''4. e''4 e''8
          d''4 c''8 d''4 e''8
          c''4.~ c''4
        }
      }
    }
    % Lyrics below are for the left hand
    % The horizontal line means draw; if omitted it means push
    \new Lyrics {
      \lyricsto "melody" {
        " "
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "  " "  " "
        " "
        " "  " "  " "
        " "  " "
        " "  " "  " "  " "
        " "
        " "  " "  " "
        " "  " "
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "  " "  " "
        " "
      }
    }
    % 5 means index finger
    \new Lyrics {
      \lyricsto "melody" {
        "5"
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "  " "  " "
        " "
        " "  " "  " "
        " "  " "
        " "  " "  " "  " "
        " "
        " "  " "  " "
        " "  " "
        " "  " "  " "  " "
        " "  "5"  "5"
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "  " "  " "
        " "
      }
    }
    \new Lyrics {
      \lyricsto "melody" {
        The it- sy bit- sy spi- der crawled up the wa- ter spout.
        Down came the rain, and washed the spi- der out.
        Out came the sun, and dried up all the rain,
        and the it- sy bit- sy spi- der went up the spout a gain.
      }
    }
    % Lyrics above are for the right hand
    % Horizontal bar means draw; if omitted it means push
    \new Lyrics \with { alignAboveContext = "staff" } {
      \lyricsto "melody" {
        " "
        " "  " "  " "  "_"
        " "  " "  " "
        "_"  " "  "_"  " "
        " "
        " "  " "  "_"
        " "  " "
        "_"  " "  "_"  " "
        " "
        " "  " "  "_"
        " "  " "
        "_"  " "  "_"  " "
        " "  " "  " "
        " "  " "  " "  "_"
        " "  " "  " "
        "_"  " "  "_"  " "
        " "
      }
    }
    % 1 means index finger
    \new Lyrics \with { alignAboveContext = "staff" } {
      \lyricsto "melody" {
        " "
        "1"  "1"  "1"  "2"
        "2"  "2"  "2"
        "2"  "1"  "2"  "2"
        "1"
        "2"  "2"  "3"
        "3"  "3"
        "3"  "2"  "3"  "3"
        "2"
        "1"  "1"  "2"
        "2"  "2"
        "2"  "1"  "2"  "2"
        "1"  " "  " "
        "1"  "1"  "1"  "2"
        "2"  "2"  "2"
        "2"  "1"  "2"  "2"
        "1"
      }
    }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 120
  }
}
