\version "2.24.2"

\header{
  title = "Blowin' In The Wind"
}

\score {
  <<
    \new Staff = "staff" {
      \new Voice = "melody" {
        { \time 4/4
          g'2 g'4 g'4
          a'2 a'4 g'4
          g'2 {e'8 (d'8)}
          c'2. e'4
          g'2. g'4
          \break
          a'2 g'2 f'4
          g'1~
          g'2 e'4 f'4
          g'2 g'4 g'4
          a'2 a'4 g'4
          \break
          g'2 {e'4 (d'4)}
          c'2. e'4
          g'2. g'4
          f'2 e'4 e'4
          d'1~
          d'2 e'4 f'4
          \break
          g'2 g'4 g'4
          a'2 a'4 g'4
          g'8 g'4. {e'4 (d'4)}
          c'2. e'4
          g'2 g'2
          \break
          a'2 g'4 f'4
          g'1~
          g'2. e'4
          f'2 f'4 e'4
          d'2. f'4
          e'8 e'4. e'4 d'4
          \break
          c'2. e'4
          f'2 f'4 e'4
          d'8 d'4. c'4 b4
          c'1~
          c'1
        }
      }
    }

    \new Lyrics {
      \lyricsto "melody" {
        How man- y roads must a man walk down
        Be- fore you call him a man?
        Yes, n' how man y seas must a white dove sail
        Be fore she sleeeps in the sand?
        Yes n' how man- y times must the can- non balls fly
        Be- before they're for- ev- er banned?
        The an- swer my friend, is blow- in' in the wind.
        The an- swer is blow- in in the wind
      }
    }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 160
  }
}
