\version "2.24.2"

\header{
  title = "Star Wars"
  subtitle = "20th Centry Fox Fanfare"
  composer = "Alfred Newman"
  arranger = "Dan Coates"
}

\score {
  \new Staff \with {midiInstrument = "acoustic grand"} <<
  { \clef treble 
    \key f \major
    \time 6/8
    <a' c''>8.\ff <a' c''>16 <a' c''>8 <a' c''>4.~
    <a' c''>8 <bes' des''>8 <a' c''>8 <bes' des''>8 <a' c''>8 <bes' des''>8
    <a' c''>8. <a' c''>16 <a' c''>8 <a' c''>8 <a' c''>8 <a' c''>8
    \break
    <a' c''>8 <a' c''>8 <a' c''>8 <a' c''>8 <f' c''>8 <g' bes'>8
    <a' c''>8. <a' c''>16 <a' c''>8 <a' c''>4.~
    <a' c''>4. {f'8\f\< (a'8 c''8\!
    \break
    d''4.)} <b d'>8.\> <b d'>16 <b d'>8\!
    <b d'>4. {g'8\< (bes'8 d''8\!
    f''4.)} <b des'>8.\> <b des'>16 <b des'>8\!
    \break
    <b c'>4. {g'8\< (bes'8 des''8\!
    f''4.)}\ff <a' c''>8 <f' a'>8 <g' bes'>8
    <f' a' c''>2.
  }
  
  \new Staff \with {midiInstrument = "acoustic grand"} 
  { \clef bass
    \key f \major
    % c,8 d, e, f, g, a,
    % b, c d e f g
    % a b c' d' e' f'

    <f c'>4. <f c'>4.~
    <f c'>4. <f des'>4.
    <f c'>4. <f c'>4.~
    \break
    <f c'>4. c4.
    <f, f>2.~
    <f, f>4. f8 a8 c'8
    \break
    d'4. <g, f>4.
    <g, f>4. g8 b8 d'8
    f'4. <g, f>4.
    \break
    <c e g>4. g8 b8 des'8
    f'4. <f, f>4.
    <f, f>2.
  }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 130
  }
}
