\version "2.24.2"

\header{
  title = "Star Wars"
  subtitle = "Abbreviated Main Title"
  composer = "Music by John Williams"
  arranger = "Arranged by Dan Coates, Jeffrey Mezic"
  instrument = "For Concertina"
  meter = "122"
}

\score {
  \new Staff \with {midiInstrument = "concertina"} <<
  {
    s2 s4 {\tuplet 3/2 {d'8\f (d'8 d'8}
    g'2-> d''2->
    \tuplet 3/2 {c''8 b'8 a'8} g''2 d''4)}
    {\tuplet 3/2 {c''8 (b'8 a'8} g''2 d''4
    \tuplet 3/2 {c''8\> b'8 c''8} a'2->\!)} {d'8. (d'16
    g'2\mf d''2
    \tuplet 3/2 {c''8 b'8 a'8} g''2 d''4)}
    
    {\tuplet 3/2 {c''8 (b'8 a'8} g''2 d''4
    \tuplet 3/2 {c''8 b'8\> c''8} a'2\!)} {d'8.\mp (d'16
    e'4. e'8 c''8 b'8 a'8 g'8
    \tuplet 3/2 {g'8 a'8 b'8} a'4.)} r8 {d'8. (d'16
    e'4.\< e'8 c''8 b'8 a'8 g'8\!
    <f' d''>4 <d' fis' a'>2\>)} r4\!
  }
  >>
  \layout {
    indent = 0.0
  }
  \midi {
    \tempo 4 = 122
    % \tempo 4 = 80
  }
}
