\version "2.24.2"

\header{
  title = "Frère Jacques"
  subtitle = "Lower Octave"
}

\score {
  <<
    \new Staff = "staff" {
      \new Voice = "melody" {
        {
          c'4 d'4 e'4 c'4
          c'4 d'4 e'4 c'4
          e'4 f'4 g'2
          e'4 f'4 g'2
          \break
          g'8 a'8 g'8 f'8 e'4 c'4
          g'8 a'8 g'8 f'8 e'4 c'4
          c'4 g4 c'2
          c'4 g4 c'2
        }
      }
    }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 116
  }
}
