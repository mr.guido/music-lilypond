\version "2.24.2"

\header{
  title = "This Land Is Your Land"
  composer = "Woody Guthrie"
  source = "First 50 Songs You Should Play On Harmonica (Kindle)"
}

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = { \break }
pgbreak = { \pageBreak }

% measures 1 - 4
AI = { | s4 c'4 d'4 e'4 }
AII = { | f'2 f'2~ }
AIII = { | f'4 f'4 c'4 d'4 }
AIV = { | e'2 e'2~ \mbreak }

% measures 5 - 8
AV = { | e'4 g4 c'4 e'4 }
AVI = { | d'2 d'2~ }
AVII = { | d'4 d'8 d'8 c'4 d'4 }
AVIII = { | e'2 e'2~ \mbreak }

% measures 9 - 12
AIX = { | e'4 c'8 c'8 d'4 e'4 }
AX = { | f'2 f'2~ }
AXI = { | f'4 f'8 f'8 c'4 d'4 }
AXII = { | e'2 e'2~ \mbreak }

% measures 13 - 17
AXIII = { | e'1 }
AXIV = { | d'4 d'2 d'4 }
AXV = { | b4. g8 b4 d'4 }
AXVI = { | c'1~ }
AXVII = { | c'4 \bar "|." \mbreak \pgbreak }

APart = {
  \tempo 4 = 140
  \clef treble
  \time 4/4

  \AI \AII \AIII \AIV 
  \AV \AVI \AVII \AVIII 
  \AIX \AX \AXI \AXII 
  \AXIII \AXIV \AXV \AXVI \AXVII 
}

InstrumentAPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \APart
}

InstrumentCombined = {
  \new PianoStaff <<
    \InstrumentAPart
    \addlyrics { 
      This land is your land, this land is my land, from Cal- i- for- nia to
      the New York is- land, from the red- wood for- est to the gulf- stream
      wat- ers; this land was made for you and me.
    }
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}
