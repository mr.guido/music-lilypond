\version "2.24.2"

\header{
  title = "The Sound of Silence"
}

\score {
  \new Staff \with {midiInstrument = "acoustic guitar (nylon)"} <<
  { \clef treble 
    r4 d'8 d'8 f'8 f'8 a'8 a'8
    g'1
    r8 c'8 c'8 c'8 e'8 e'8 g'8 g'8
    \break
    f'1
    r8
    f'8 f'8 f'8 a'8 a'8 c''8 {c''8
    (d''4)} d''4 c''8~ c''2
    \break
    r4 f'8 f'8 a'8 a'8 c''8 {c''8
    (d''4)} d''8 c''8~ c''2
    \time 2/4 r8 f'8 f'8 \time 4/4 
    \break
    d''8 d''4.~ d''4 d''8 e''8
    f''8 f''4. e''8 d''4.
    c''2. d''8 c''8
    \break
    a'1
    \time 2/4 r8 f'8 f'8 f'8 \time 4/4 
    c''2. r8 e'8
    f'8 d'4.~ d'2
  }
  \addlyrics { 
  "Hel-" "lo" "dark-" "ness," "my" "old" "friend"
  "I've" "come" "to" "talk" "with" "you" "a-"
  "gain" "be-" "cause" "a" "vi-" "sion" "soft-" "ly" "creep-" "ing"
  "left" "its" "seeds" "while" "I" "was" "sleep-" "ing." "And" "the"
  "vi-" "sion" "that" "was" "plant-" "ed" "in" "my" "brain" "still" "re-"
  "mains" "with-" "in" "the" "sound" "of" "si-" "lence."
  }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 130
  }
}
