\version "2.24.2"

\header{
  title = "Movement II: The Wind Waker"
  composer = "Music by Kenta Nagata, Hajime Wakai, Toru Minegishi, and Koji Kondo"
  source = "The Legend of Zelda - Symphony of the Goddesses (Kindle)"
}

% TODO Add dynamics, crescendos to both parts

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = { \break }
pgbreak = { \pageBreak }

% measures 1 - 4
AI = { | r4^"The Legendary Hero:" a32( b32 c'32 d'32 e'8~ e'2~ }
AII = { | e'4 e'4 f'4 d'4 }
AIII = { | e'1) }
AIV = { | r8 e''16 e''16 e''4( d''8) e''16( c''16 d''8-.) b'8-. \mbreak }
BI = { | <a,, a,>1~( }
BII = { | <a,, a,>4 e4 f4 d4 }
BIII = { | e1) }
BIV = { | \clef treble a'2. g'8-. g'8-. \mbreak }

% measures 5 - 8
AV = { | a'1 }
AVI = { | r8 <c'' e''>16 <c'' e''>16 <c'' e''>4 r8 <c'' e''>16( <a' c''>16 <g' b' d''>8)-. <e' g' b'>8-. }
AVII = { | <e' a'>2. d'8-. d'8-. }
AVIII = { | << { \voiceOne | a'8( c''8) \acciaccatura c'' <d' g' b'>8 <b d' g'>8 <g c' e'>2 } \new Voice { \voiceTwo | f'4 s4 s2 } >> \oneVoice \mbreak }
BV = { | e'2. d'8-. d'8-. }
BVI = { | e'1 }
BVII = { | \clef bass c'8 b16( c'16 d'8-.) b8-. g4 <g b>8-. <f b>8-. }
BVIII = { | a2 c4 c8( g8) \mbreak }

% measures 9 - 12
AIX = { | << { \voiceOne | a'8( c''8) \acciaccatura c'' <d' g' b'>8 <b d' g'>8 <g' c'' e''>2 } \new Voice { \voiceTwo | f'4 s4 s2 } >> \oneVoice }
AX = { | << { \voiceOne | a'8 c''8 \acciaccatura c'' <d' g' b'>8 <b d' g'>8 } \new Voice { \voiceTwo | f'4 s4 } >> \oneVoice <g c' e'>4.( <g b d'>8 }
AXI = { | <gis b e'>2) <gis b>2\arpeggio }
AXII = { | d'8( e'8 f'8 gis'8) b'2 \mbreak }
BIX = { | f2 c'8 c16( f16) g8( c'8) }
BX = { | a2 c8 c8 d8( f8) }
BXI = { | e2 <e, b,>2\arpeggio }
BXII = { | r2 r4 d'4 \mbreak }

% measures 13 - 16
AXIII = { | d'8( e'8 f'8 d''8) <e' gis' b'>2\arpeggio }
AXIV = { | d'8( e'8 f'8 gis'8) f'8( gis'8 b'8 d''8) }
AXV = { | gis'8( b'8 d''8 f''8) d''8 f''8 gis''8 b''8 }
AXVI = { | <e'' gis'' b'' d'''>2\arpeggio r2 \mbreak \pgbreak }
BXIII = { | b2 d'2 }
BXIV = { | <e b>2 <e d'>2 }
BXV = { | <e d' f'>2\arpeggio \clef treble <b gis'>2 }
BXVI = { | <e' b'>2\arpeggio r2 \clef bass \mbreak \pgbreak }

% measures 17 - 20
AXVII = { \bar "||" \key des \major r1 }
AXVIII = { | r2 r4 r8 aes'8-. }
AXIX = { | f'8-. <aes des' f'>8-. <aes des' f'>8-. ees'8-. f'8-. <des' f' aes'>8-. <des' f' aes'>8-. aes'8-. }
AXX = { | f'8-. <des' ges' bes'>8-. <des' ges' bes'>8-. ees'8-. f'8-. <ees' aes' c''>8-. <ees' aes' c''>8-. <c' aes'>8( \mbreak }
BXVII = { \bar "||" \key des \major des,8-. des8-. r8 des8-. des,8-. des8-. r8 des8-. }
BXVIII = { | des,8-. des8-. r8 des8-. des,8-. des8-. r8 des8-. }
BXIX = { | des,8-. des8-. r8 aes,8-. f,8-. f8-. r8 des8-. }
BXX = { | ges,8-. ges8-. r8 des8-. aes,8-. aes8-. r8 ges,8-. \mbreak }

% measures 21 - 24
AXXI = { | <ees' c''>4. <des' bes'>8 <c' aes'>4. <bes ges'>8 }
AXXII = { | <aes f'>4. des'8 <aes des' f'>4) <aes c' ees'>8 aes'8-. }
AXXIII = { | f'8-. <aes des' f'>8-. <aes des' f'>8-. ees'8-. f'8-. <des' f' aes'>8-. <des' f' aes'>8-. aes'8-. }
AXXIV = { | f'8-. <des' ges' bes'>8-. <des' ges' bes'>8-. ees'8-. f'8-. <ees' aes' c''>8-. <ees' aes' c''>8-. <c' aes'>8( \mbreak }
BXXI = { | f,8-. f8-. <f aes>8-. des8 bes,8 bes8-. <bes des'>8-. bes,8-. }
BXXII = { | ees,8-. ees8-. <bes, des>8-. ees,8-. aes,8-. \parenthesize aes8-. r8 aes,8-. }
BXXIII = { | des,8-. des8-. r8 aes,8-. f,8-. f8-. r8 des8-. }
BXXIV = { | ges,8-. ges8-. r8 des8-. aes,8-. aes8-. r8 ges,8-. \mbreak }

% measures 25 - 28
AXXV = { | <ees' c''>4. <des' bes'>8 <f' des''>4.) f'16( ges'16) }
AXXVI = { | aes'8-. <bes des' ges' aes'>8-. <bes des' ges' aes'>8-. <des' ges' bes'>8( <c' ees' aes'>4) <bes des' ges'>4 }
AXXVII = { | << { \voiceOne \bar "||" des''2 aes''8-. aes''8-. aes''8-. aes''8-. } \new Voice { \voiceTwo \bar "||" aes'8-. aes'8-. aes'8-. aes'8-. bes'4. ees''8 } >> \oneVoice }
AXXVIII = { | <des'' f'' aes''>8-. des''8-. aes'8-. f'8-. aes'4. aes'16 bes'16 \mbreak }
BXXV = { | f,8-. f8-. <f aes>8-. f,8-. bes,8-. bes8-. <bes des'>8-. f8-. }
BXXVI = { | ees,8-. ees8-. r8 bes,8-. aes,8-. aes8-. r8 aes,8-. }
BXXVII = { \bar "||"  ges,8-. ges8-. r8 des8-. ges,8-. ges8-. r8 des8-. }
BXXVIII = { | b,8-. b,8-. r8 ges,8-. b,8-. b,8-. r8 ges,8-. \mbreak }

% measures 29 - 32
AXXIX = { | aes'8-. aes'8-. aes'8-. aes'8-. aes''8-. aes''8-. aes''8-. aes''8-. }
AXXX = { | <f'' aes''>8-. c''8-. aes'8-. f'8-. r2 }
AXXXI = { << { \voiceOne | <des'' aes''>2 <bes' f''>4. <ees'' bes''>8 } \new Voice { \voiceTwo | r8 bes'8-. bes'4-. r8 aes'8-. aes'4-. } >> \oneVoice }
AXXXII = { | << { \voiceOne | <des'' aes''>2 <aes' des''>4. des''16 ees''16 } \new Voice { \voiceTwo | r8 bes'8-. bes'4-. r8 ees'8-. ees'4-. } >> \oneVoice \mbreak }
BXXIX = { | f,8-. f8-. r8 des8-. f,8-. f8-. r8 des8-. }
BXXX = { | bes,8-. bes,8-. r8 f,8-. bes,8-. bes,8-. r8 f,8-. }
BXXXI = { | ees,8-. ees8-. ges8-. bes,8-. ees,8-. ees8-. f8-. bes,8-. }
BXXXII = { | ees,8-. ees8-. ges8-. bes,8-. ees,8-. ees8-. aes8-. bes,8-. \mbreak }

% measures 33 - 36
AXXXIII = { | << { \voiceOne | f''16 ees''8.~ ees''4 } \new Voice { \voiceTwo | r8 bes'8-. bes'4-. } >> \oneVoice <des' bes'>8( <c' aes'>8-.) <des' bes'>8-. <f' des''>8-. }
AXXXIV = { | <ees' c''>4. aes8~ <aes des' f'>4( <ges c' ees'>4) }
AXXXV = { \bar "||" r8 <aes' c'' ees''>8-. <aes' c'' ees''>8-. r8 r8 <f' aes' c''>8-. <f' aes' c''>8-. r8 }
AXXXVI = { | r8 <ges' bes' des''>8-. <ges' bes' des''>8-. r8 r8 <ees' ges' bes'>8-. <ees' ges' bes'>8-. r8 \mbreak \pgbreak }
BXXXIII = { | aes,8-. aes8-. r8 ees8-. aes,8-. aes8-. r8 ees8-. }
BXXXIV = { | aes,8-. aes,8-. r8 ees,8-. aes,8-. aes,8-. r8 ees,8-. }
BXXXV = { \bar "||" des,8-. des8-. r8 aes,8-. des,8-. des8-. r8 aes,8-. }
BXXXVI = { | des,8-. des8-. r8 aes,8-. des,8-. des8-. r8 aes,8-. \mbreak \pgbreak }

% measures 37 - 40
AXXXVII = { | r8 <f' aes' c''>8-. <f' aes' c''>8-. r8 r8 <des' f' aes'>8-. <des' f' aes'>8-. r8 }
AXXXVIII = { | r8 <ees' ges' bes'>8-. <ees' ges' bes'>8-. r8 r8 <c'' ees'' ges''>8-. <c'' ees'' ges'' aes''>4 }
AXXXIX = { | << { \voiceOne | f''2. } \new Voice { \voiceTwo | aes'8 des''8 aes'16 bes'16 aes'8 des''8[ aes'8] } >> \oneVoice <ges' ees''>4 }
AXL = { | << { \voiceOne | f''2. } \new Voice { \voiceTwo | aes'8 des''8 aes'16 bes'16 aes'8 des''8[ aes'8] } >> \oneVoice <c'' ees'' aes''>4 \mbreak }
BXXXVII = { | des,8-. des8-. r8 aes,8-. des,8-. des8-. r8 aes,8-. }
BXXXVIII = { | des,8-. des8-. r8 aes,8-. des,8-. des8-. r8 aes,8-. }
BXXXIX = { | des,8-. des8-. r8 aes,8-. des,8-. des8-. r8 aes,8-. }
BXL = { | des,8-. des8-. r8 aes,8-. des,8-. des8-. r8 aes,8-. \mbreak }

% measures 41 - 44
AXLI = { | << { \voiceOne | f''2. } \new Voice { \voiceTwo | aes'8 des''8 aes'16 bes'16 aes'8 des''8 aes'8 } >> \oneVoice <ges' ees''>4 }
AXLII = { | << { \voiceOne | f''2 } \new Voice { \voiceTwo | aes'8 des''8 aes'16 bes'16 aes'8 } >> \oneVoice des''8 aes'8 ees'8 des'8 }
AXLIII = { | aes'8-. <f' des''>8-. <f' aes'>8-. des'8-. des'8-. <aes des'>8-. <aes des'>8-. des'16( ees'16 }
AXLIV = { | f'8-.) <bes des'>8-. <bes des'>8-. ees'8-. f'8( <aes c'>8-.) <aes c' ees'>8-. r8 \mbreak }
BXLI = { | des,8-. des8-. r8 aes,8-. des,8-. des8-. r8 aes,8-. }
BXLII = { | des,8-. des8-. r8 aes,8-. des,8-. des8-. r8 aes,8-. }
BXLIII = { | des,8-. des8-. r8 aes,8-. f,8-. f8-. r8 des8-. }
BXLIV = { | ges,8-. ges8-. r8 des8-. aes,8-. aes8-. r8 ges,8-. \mbreak }

% measures 45 - 48
AXLV = { | aes'8-. <c' f' aes'>8-. <c' f' aes'>8-. bes'8-. des'8-. des'8-. <bes des'>8-. des'16( ees'16 }
AXLVI = { | f'8-.) <ges bes des' f'>8-. <ges bes des' f'>8-. ges'8-. <aes des' f'>4~ <aes c' ees'>8 r8 }
AXLVII = { | aes''8-. <f'' des'''>8-. <f'' aes''>8-. f''8-. des''8-. <f' aes' des''>8-. <f' aes' des''>8-. d''16( e''16 }
AXLVIII = { | f''8-.) <f' bes' des''>8-. <f' bes' des''>8-. ees''8-. f''8 <aes' c''>8-. <aes' c'' ees''>8-. r8 \mbreak }
BXLV = { | f,8-. f8-. r8 des8-. bes,8-. bes8-. r8 bes,8-. }
BXLVI = { | ees,8-. ees8-. r8 ees,8-. aes,8-. aes8-. r8 aes,8-. }
BXLVII = { | des,8-. des8-. r8 aes,8-. f,8-. f8-. r8 des8-. }
BXLVIII = { | ges,8-. ges8-. r8 des8-. aes,8-. aes8-. r8 ges,8-. \mbreak }

% measures 49 - 52
AXLIX = { | aes''8-. <c'' f'' bes''>8-. <c'' f'' aes''>8-. f''8-. des''8-. <f' bes' des''>8-. <f' bes' des''>8-. f''16( ges''16 }
AL = { | aes''8-.) <bes' des' ges'' aes''>8-. <bes' des' ges'' aes''>8-. bes''8-. <aes' des'' f'' aes''>4( <aes' c'' ees'' ges''>8) r8 }
ALI = { | aes'8-. aes'8-. aes'8-. aes'8-. aes'8-. des'8-. aes'8-. des''8-. }
ALII = { | f''8-. des''8-. aes'8-. f'8-. r2 \mbreak }
BXLIX = { | f,8-. f8-. r8 f,8-. bes,8-. bes8-. r8 f8-. }
BL = { | ees,8-. ees8-. r8 bes,8-. aes,8-. aes8-. r8 aes,8-. }
BLI = { | ges,8-. ges8-. r8 des8-. ges,8-. ges8-. r8 des8-. }
BLII = { | b,8-. b,8-. r8 ges,8-. b,8-. b,8-. r8 ges,8-. \mbreak }

% measures 53 - 55
ALIII = { | aes'8-. aes'8-. aes'8-. aes'8-. aes'8-. des'8-. aes'8-. des''8-. }
ALIV = { | f''8-. c''8-. aes'8-. f'8-. r2 }
ALV = { | aes''8( <ees'' ges'' des'''>8-.) <ees'' ges'' aes''>8-. f''8-. \tuplet 3/2 { <aes' c'' ees''>4( <aes' des'' f''>4) <aes' ees'' ges''>4 } \mbreak \pgbreak }
BLIII = { | f,8-. f8-. r8 des8-. f,8-. des8-. r8 des8-. }
BLIV = { | bes,8-. bes,8-. r8 f,8-. bes,8-. bes,8-. r8 f,8-. }
BLV = { | ees,8-. ees8-. r8 bes,8-. ees,8-. ees8-. r8 bes,8-. \mbreak \pgbreak }

% measures 56 - 58
ALVI = { | << { \voiceOne | aes''4. ges''8~ ges''4 } \new Voice { \voiceTwo | r8 <bes' des''>8-. <bes' des''>8-. r8 r8 <aes' c''>8-. } >> \oneVoice <aes' c'' f''>8-. ees''8-. }
ALVII = { | f''8-. <f' bes' des''>8-. <f' bes' des''>8-. r8 <bes des'>8( <aes c'>8-.) <bes des'>8-. <des' f'>8-. }
ALVIII = { | <c' ees'>4. aes'8~ <aes' des'' f''>4( <aes' c'' ees''>4) \mbreak }
BLVI = { | ees,8-. ees8-. r8 bes,8-. ees,8-. ees8-. r8 bes,8-. }
BLVII = { | aes,8-. aes8-. r8 ees8-. aes,8-. aes8-. r8 ees8-. }
BLVIII = { | aes,8-. aes,8-. r8 ees,8-. aes,8-. aes,8-. r4 \mbreak }

% measures 59 - 62
ALIX = { | <aes' des'' f'' aes''>1^"Ocean:" }
ALX = { | <aes' c'' ees'' aes''>1 }
ALXI = { | <aes' b' ees'' aes''>1 }
ALXII = { | <g' bes' des'' ges''>1 \mbreak }
BLIX = { | des,8( aes,8 des8 ees8~ ees8 des4 aes,8) }
BLX = { | des,8( aes,8 des8 ees8~ ees8 des4 aes,8) }
BLXI = { | des,8( aes,8 des8 ees8~ ees8 des4 aes,8) }
BLXII = { | des,8( aes,8 des8 ees8~ ees8 des4 aes,8) \mbreak }

% measures 63 - 66
ALXIII = { | <aes' des'' f'' aes''>1 }
ALXIV = { | <aes' c'' ees'' aes''>1 }
ALXV = { | <aes' b' ees'' aes''>1\< }
ALXVI = { | <g' bes' des'' ges''>1 \mbreak }
BLXIII = { | des,8( aes,8 des8 ees8~ ees8 des4 aes,8) }
BLXIV = { | des,8( aes,8 des8 ees8~ ees8 des4 aes,8) }
BLXV = { | des,8(\< aes,8 des8 ees8~ ees8 des4 aes,8) }
BLXVI = { | des,8( aes,8 des8 ees8~ ees8 des4 aes,8) \mbreak }

% measures 67 - 70
ALXVII = { | f'8\!\f r8 bes4 f''8 r8 bes16 c'16 des'16 ees'16 }
ALXVIII = { | f'8 r8 bes'16 c''16 des''16 ees''16 f''8 r8 bes'4 }
ALXIX = { | f'8 r8 bes4 f''8 r8 bes16 c'16 des'16 ees'16 }
ALXX = { | f'8 r8 bes'16 c''16 des''16 ees''16 f''8 r8 bes'4 \mbreak }
BLXVII = { | <bes, des f>4-.\!\f <bes, des f>4-. <bes, des f>4-. <bes, des f>4-. }
BLXVIII = { | <bes, des f>4-. <bes, des f>4-. <bes, des f>4-. <bes, des f>4-. }
BLXIX = { | <ges, des f>4-. <ges, des f>4-. <ges, des f>4-. <ges, des f>4-. }
BLXX = { | <ges, des f>4-. <ges, des f>4-. <ges, des f>4-. <ges, des f>4-. \mbreak }

% measures 71 - 74
ALXXI = { | f'8 r8 bes4 f''8 r8 bes16 c'16 des'16 ees'16 }
ALXXII = { | f'8 r8 bes'16 c''16 des''16 ees''16 f''8 r8 bes'4 }
ALXXIII = { | f'8 r8 bes4 f''8 r8 bes16 c'16 des'16 ees'16 }
ALXXIV = { | f'8 r8 bes'16 c''16 des''16 ees''16 f''8 r8 bes'4~ \mbreak \pgbreak }
BLXXI = { | << { \voiceOne | f2. ees4 } \new Voice { \voiceTwo | des4-. des4-. des4-. des4-. } >> \oneVoice }
BLXXII = { | << { \voiceOne | f2. aes4 } \new Voice { \voiceTwo | des4-. des4-. des4-. des4-. } >> \oneVoice }
BLXXIII = { | << { \voiceOne | f2. ees4 } \new Voice { \voiceTwo | f,4-. f,4-. f,4-. f,4-. } >> \oneVoice }
BLXXIV = { | << { \voiceOne | f1 } \new Voice { \voiceTwo | f,4-. f,4-. f,4-. f,4-. } >> \oneVoice \mbreak \pgbreak }

% measures 75 - 77
ALXXV = { \bar "||" \key c \major \tempo 4 = 130 bes'2 \tuplet 3/2 { c'8( f'8 a'8) } \tuplet 3/2 { c''8( f''8 a''8) } }
ALXXVI = { | \tuplet 3/2 { f'4 f'8 } \tuplet 3/2 { f'8 c'8 a8 } \tuplet 3/2 { bes8 c'8 f'8 } a'4 }
ALXXVII = { | \tuplet 3/2 { f''4 f''8 } \tuplet 3/2 { f''8 c''8 a'8 } \tuplet 3/2 { bes'8 c''8 f''8 } a''4 \mbreak }
BLXXV = { \bar "||" \key c \major r2 r4 \tuplet 3/2 {r4 <f, a, c>8} }
BLXXVI = { | <f, a, c>1~ }
BLXXVII = { | <f, a, c>2. r8 <a, c f>16 <a, c f>16 \mbreak }

% measures 78 - 81
ALXXVIII = { | \tuplet 3/2 { f'4 f'8 } \tuplet 3/2 { f'8 c'8 a8 } \tuplet 3/2 { bes c' f' } a'4 }
ALXXIX = { | \tuplet 3/2 { f''4 f''8 } \tuplet 3/2 { f''8 c''8 a'8 } \tuplet 3/2 { bes'8 c''8 f''8 } a''4 }
ALXXX = { | c''2 f'4 \tuplet 3/2 { f'4 f'8 } }
ALXXXI = { | c''4. <a' c'' f''>16 <a' c'' f''>16 <a' c'' f''>4 \tuplet 3/2 { f'4 f'8 } \mbreak }
BLXXVIII = { | <bes, d f>1~ }
BLXXIX = { | <bes, d f>1 }
BLXXX = { | <a, f>2. \tuplet 3/2 { <a, c f>4 <a, c f>8 } }
BLXXXI = { | <bes, d f>1 \mbreak }

% measures 82 - 85
ALXXXII = { | c''4. <a' c'' f''>16 <a' c'' f''>16 <a' c'' f''>8 <a' c'' f''>8 \tuplet 3/2 { <a' c'' f''>8 <a' c'' f''>8 <a' c'' f''>8 } }
ALXXXIII = { << { \voiceOne | <c'' f''>1 } \new Voice { \voiceTwo | a'2 g'2 } >> \oneVoice }
ALXXXIV = { | \tempo 4 = 100 <a' d'' f''>8( <c'' e'' g''>8) <c'' e'' g''>2. }
ALXXXV = { | e''16 c''16 f''16 des''16 fis''16 des''16 g''16 ees''16 aes''8-> r8 r4 \mbreak }
BLXXXII = { | <a, f>2. \tuplet 3/2 {<a, c f>4 <a, c f>8} }
BLXXXIII = { | <bes, d f>2 <c e g>2 }
BLXXXIV = { | r2 g4 c4 }
BLXXXV = { | c,8-. des,8-. des,8-. ees,8-. ees,8-.-> r8 r8 c,8 \mbreak }

% measures 86 - 88
ALXXXVI = { | \clef bass r8\mp^"Pirate Ship Theme:" <f a c'>16 <f a c'>16 <f a c'>8 <f a c'>8 r8 <aes c' ees'>8 <aes c' ees'>8 r8 }
ALXXXVII = { | r8 <f a c'>16 <f a c'>16 <f a c'>8 <f a c'>8 r8 <f aes b>8 <f aes b>8 r8 }
ALXXXVIII = { | a8.( gis16 a8. gis16 a8-.) a8-. f8-. c8-. \mbreak }
BLXXXVI = { | f,4\mp r8 f,8 c,4 r8 c,8 }
BLXXXVII = { | f,4 r8 f,8 c,4 r8 c,8 }
BLXXXVIII = { | f,4 r8 f,8 c,4 r8 c,8 \mbreak }

% measures 89 - 92
ALXXXIX = { | c'4 <f a c'>8 <f a c'>8 r8 <f aes b>8 <f aes b>8[ r16 d'16]( }
AXC = { | ees'8. d'16 ees'8. d'16 ees'8-.) ees'8-. d'8-. c'8-. }
AXCI = { | gis2 a8-.-> f8-.-> c8-.-> r8 }
AXCII = { | a8.( gis16 a8. gis16 a8-.) a8-. c8-. c8-. \mbreak \pgbreak }
BLXXXIX = { | f,4 r8 f,8 c,4 r8 c,8 }
BXC = { | f,4 r8 f,8 c,4 r8 c,8 }
BXCI = { | f,4 r8 f,8 c,4 r8 c,8 }
BXCII = { | f,4 r8 f,8 c,4 r8 c,8 \mbreak \pgbreak }

% measures 93 - 95
AXCIII = { | c'4 <f a c'>8 <f a c'>8 r8 <f aes b>8 <f aes b>8[ r16 d'16]( }
AXCIV = { | ees'8. d'16 c'8. gis16 a8-.) c'8-. a8-. c8-. }
AXCV = { | f4 <f a c'>8 <f a c'>8 r8 <f aes b>8 <f aes b>8 r8 \mbreak }
BXCIII = { | f,4 r8 f,8 c,4 r8 c,8 }
BXCIV = { | f,4 r8 f,8 c,4 r8 c,8 }
BXCV = { | f,4 r8 f,8 c,4 r8 c,8 \mbreak }

% measures 96 - 99
AXCVI = { | \clef treble r1 }
AXCVII = { | r1 }
AXCVIII = { | r1 }
AXCIX = { | r1 \mbreak }
BXCVI = { | r1 }
BXCVII = { | r1 }
BXCVIII = { | r1 }
BXCIX = { | r1 \mbreak }

% measures 100 - 103
AC = { | r1 }
ACI = { | r1 }
ACII = { | r1 }
ACIII = { | r1 \mbreak }
BC = { | r1 }
BCI = { | r1 }
BCII = { | r1 }
BCIII = { | r1 \mbreak }

% measures 104 - 107
ACIV = { | r1 }
ACV = { | r1 }
ACVI = { | r1 }
ACVII = { | r1 \mbreak }
BCIV = { | r1 }
BCV = { | r1 }
BCVI = { | r1 }
BCVII = { | r1 \mbreak }

% measures 108 - 111
ACVIII = { | r1 }
ACIX = { | r1 }
ACX = { | r1 }
ACXI = { | r1 \mbreak \pgbreak }
BCVIII = { | r1 }
BCIX = { | r1 }
BCX = { | r1 }
BCXI = { | r1 \mbreak \pgbreak }

% measures 112 - 116
ACXII = { | r1 }
ACXIII = { | r1 }
ACXIV = { | r1 }
ACXV = { | r1 }
ACXVI = { | r1 \mbreak }
BCXII = { | r1 }
BCXIII = { | r1 }
BCXIV = { | r1 }
BCXV = { | r1 }
BCXVI = { | r1 \mbreak }

% measures 117 - 120
ACXVII = { | r1 }
ACXVIII = { | r1 }
ACXIX = { | r1 }
ACXX = { | r1 \mbreak }
BCXVII = { | r1 }
BCXVIII = { | r1 }
BCXIX = { | r1 }
BCXX = { | r1 \mbreak }

% measures 121 - 124
ACXXI = { | r1 }
ACXXII = { | r1 }
ACXXIII = { | r1 }
ACXXIV = { | r1 \mbreak }
BCXXI = { | r1 }
BCXXII = { | r1 }
BCXXIII = { | r1 }
BCXXIV = { | r1 \mbreak }

% measures 125 - 128
ACXXV = { | r1 }
ACXXVI = { | r1 }
ACXXVII = { | r1 }
ACXXVIII = { | r1 \mbreak }
BCXXV = { | r1 }
BCXXVI = { | r1 }
BCXXVII = { | r1 }
BCXXVIII = { | r1 \mbreak }

% measures 129 - 132
ACXXIX = { | r1 }
ACXXX = { | r1 }
ACXXXI = { | r1 }
ACXXXII = { | r1 \mbreak \pgbreak }
BCXXIX = { | r1 }
BCXXX = { | r1 }
BCXXXI = { | r1 }
BCXXXII = { | r1 \mbreak \pgbreak }

% measures 133 - 136
ACXXXIII = { | r1 }
ACXXXIV = { | r1 }
ACXXXV = { | r1 }
ACXXXVI = { | r1 \mbreak }
BCXXXIII = { | r1 }
BCXXXIV = { | r1 }
BCXXXV = { | r1 }
BCXXXVI = { | r1 \mbreak }

% measures 137 - 140
ACXXXVII = { | r1 }
ACXXXVIII = { | r1 }
ACXXXIX = { | r1 }
ACXL = { | r1 \mbreak }
BCXXXVII = { | r1 }
BCXXXVIII = { | r1 }
BCXXXIX = { | r1 }
BCXL = { | r1 \mbreak }

% measures 141 - 144
ACXLI = { | r1 }
ACXLII = { | r1 }
ACXLIII = { | r1 }
ACXLIV = { | r1 \mbreak }
BCXLI = { | r1 }
BCXLII = { | r1 }
BCXLIII = { | r1 }
BCXLIV = { | r1 \mbreak }

% measures 145 - 148
ACXLV = { | r1 }
ACXLVI = { | r1 }
ACXLVII = { | r1 }
ACXLVIII = { | r1 \mbreak }
BCXLV = { | r1 }
BCXLVI = { | r1 }
BCXLVII = { | r1 }
BCXLVIII = { | r1 \mbreak }

% measures 149 - 152
ACXLIX = { | r1 }
ACL = { | r1 }
ACLI = { | r1 }
ACLII = { | r1 \mbreak \pgbreak }
BCXLIX = { | r1 }
BCL = { | r1 }
BCLI = { | r1 }
BCLII = { | r1 \mbreak \pgbreak }

% measures 153 - 156
ACLIII = { | r1 }
ACLIV = { | r1 }
ACLV = { | r1 }
ACLVI = { | r1 \mbreak }
BCLIII = { | r1 }
BCLIV = { | r1 }
BCLV = { | r1 }
BCLVI = { | r1 \mbreak }

% measures 157 - 160
ACLVII = { | r1 }
ACLVIII = { | r1 }
ACLIX = { | r1 }
ACLX = { | r1 \mbreak }
BCLVII = { | r1 }
BCLVIII = { | r1 }
BCLIX = { | r1 }
BCLX = { | r1 \mbreak }

% measures 161 - 164
ACLXI = { | r1 }
ACLXII = { | r1 }
ACLXIII = { | r1 }
ACLXIV = { | r1 \mbreak }
BCLXI = { | r1 }
BCLXII = { | r1 }
BCLXIII = { | r1 }
BCLXIV = { | r1 \mbreak }

% measures 165 - 168
ACLXV = { | r1 }
ACLXVI = { | r1 }
ACLXVII = { | r1 }
ACLXVIII = { | r1 \mbreak }
BCLXV = { | r1 }
BCLXVI = { | r1 }
BCLXVII = { | r1 }
BCLXVIII = { | r1 \mbreak }

% measures 169 - 172
ACLXIX = { | r1 }
ACLXX = { | r1 }
ACLXXI = { | r1 }
ACLXXII = { | r1 \mbreak \pgbreak }
BCLXIX = { | r1 }
BCLXX = { | r1 }
BCLXXI = { | r1 }
BCLXXII = { | r1 \mbreak \pgbreak }

% measures 173 - 176
ACLXXIII = { | r1 }
ACLXXIV = { | r1 }
ACLXXV = { | r1 }
ACLXXVI = { | r1 \mbreak }
BCLXXIII = { | r1 }
BCLXXIV = { | r1 }
BCLXXV = { | r1 }
BCLXXVI = { | r1 \mbreak }

% measures 177 - 180
ACLXXVII = { | r1 }
ACLXXVIII = { | r1 }
ACLXXIX = { | r1 }
ACLXXX = { | r1 \mbreak }
BCLXXVII = { | r1 }
BCLXXVIII = { | r1 }
BCLXXIX = { | r1 }
BCLXXX = { | r1 \mbreak }

% measures 181 - 184
ACLXXXI = { | r1 }
ACLXXXII = { | r1 }
ACLXXXIII = { | r1 }
ACLXXXIV = { | r1 \mbreak }
BCLXXXI = { | r1 }
BCLXXXII = { | r1 }
BCLXXXIII = { | r1 }
BCLXXXIV = { | r1 \mbreak }

% measures 185 - 188
ACLXXXV = { | r1 }
ACLXXXVI = { | r1 }
ACLXXXVII = { | r1 }
ACLXXXVIII = { | r1 \mbreak }
BCLXXXV = { | r1 }
BCLXXXVI = { | r1 }
BCLXXXVII = { | r1 }
BCLXXXVIII = { | r1 \mbreak }

% measures 189 - 192
ACLXXXIX = { | r1 }
ACXC = { | r1 }
ACXCI = { | r1 }
ACXCII = { | r1 \mbreak \pgbreak }
BCLXXXIX = { | r1 }
BCXC = { | r1 }
BCXCI = { | r1 }
BCXCII = { | r1 \mbreak \pgbreak }

% measures 193 - 195
ACXCIII = { | r1 }
ACXCIV = { | r1 }
ACXCV = { | r1 \mbreak }
BCXCIII = { | r1 }
BCXCIV = { | r1 }
BCXCV = { | r1 \mbreak }

% measures 196 - 198
ACXCVI = { | r1 }
ACXCVII = { | r1 }
ACXCVIII = { | r1 \mbreak }
BCXCVI = { | r1 }
BCXCVII = { | r1 }
BCXCVIII = { | r1 \mbreak }

% measures 199 - 201
ACXCIX = { | r1 }
ACC = { | r1 }
ACCI = { | r1 \mbreak }
BCXCIX = { | r1 }
BCC = { | r1 }
BCCI = { | r1 \mbreak }

% measures 202 - 204
ACCII = { | r1 }
ACCIII = { | r1 }
ACCIV = { | r1 \mbreak }
BCCII = { | r1 }
BCCIII = { | r1 }
BCCIV = { | r1 \mbreak }

% measures 205 - 207
ACCV = { | r1 }
ACCVI = { | r1 }
ACCVII = { | r1 \mbreak \pgbreak }
BCCV = { | r1 }
BCCVI = { | r1 }
BCCVII = { | r1 \mbreak \pgbreak }

% measures 208 - 210
ACCVIII = { | r1 }
ACCIX = { | r1 }
ACCX = { | r1 \mbreak }
BCCVIII = { | r1 }
BCCIX = { | r1 }
BCCX = { | r1 \mbreak }

% measures 211 - 214
ACCXI = { | r1 }
ACCXII = { | r1 }
ACCXIII = { | r1 }
ACCXIV = { | r1 \mbreak }
BCCXI = { | r1 }
BCCXII = { | r1 }
BCCXIII = { | r1 }
BCCXIV = { | r1 \mbreak }

% measures 215 - 217
ACCXV = { | r1 }
ACCXVI = { | r1 }
ACCXVII = { | r1 \mbreak }
BCCXV = { | r1 }
BCCXVI = { | r1 }
BCCXVII = { | r1 \mbreak }

% measures 218 - 221
ACCXVIII = { | r1 }
ACCXIX = { | r1 }
ACCXX = { | r1 }
ACCXXI = { | r1 \mbreak }
BCCXVIII = { | r1 }
BCCXIX = { | r1 }
BCCXX = { | r1 }
BCCXXI = { | r1 \mbreak }

% measures 222 - 225
ACCXXII = { | r1 }
ACCXXIII = { | r1 }
ACCXXIV = { | r1 }
ACCXXV = { | r1 \mbreak \pgbreak }
BCCXXII = { | r1 }
BCCXXIII = { | r1 }
BCCXXIV = { | r1 }
BCCXXV = { | r1 \mbreak \pgbreak }

% measures 226 - 229
ACCXXVI = { | r1 }
ACCXXVII = { | r1 }
ACCXXVIII = { | r1 }
ACCXXIX = { | r1 \mbreak }
BCCXXVI = { | r1 }
BCCXXVII = { | r1 }
BCCXXVIII = { | r1 }
BCCXXIX = { | r1 \mbreak }

% measures 230 - 233
ACCXXX = { | r1 }
ACCXXXI = { | r1 }
ACCXXXII = { | r1 }
ACCXXXIII = { | r1 \mbreak }
BCCXXX = { | r1 }
BCCXXXI = { | r1 }
BCCXXXII = { | r1 }
BCCXXXIII = { | r1 \mbreak }

% measures 234 - 237
ACCXXXIV = { | r1 }
ACCXXXV = { | r1 }
ACCXXXVI = { | r1 }
ACCXXXVII = { | r1 \mbreak }
BCCXXXIV = { | r1 }
BCCXXXV = { | r1 }
BCCXXXVI = { | r1 }
BCCXXXVII = { | r1 \mbreak }

% measures 238 - 241
ACCXXXVIII = { | r1 }
ACCXXXIX = { | r1 }
ACCXL = { | r1 }
ACCXLI = { | r1 \mbreak }
BCCXXXVIII = { | r1 }
BCCXXXIX = { | r1 }
BCCXL = { | r1 }
BCCXLI = { | r1 \mbreak }

% measures 242 - 245
ACCXLII = { | r1 }
ACCXLIII = { | r1 }
ACCXLIV = { | r1 }
ACCXLV = { | r1 \mbreak \pgbreak }
BCCXLII = { | r1 }
BCCXLIII = { | r1 }
BCCXLIV = { | r1 }
BCCXLV = { | r1 \mbreak \pgbreak }

% measures 246 - 248
ACCXLVI = { | r1 }
ACCXLVII = { | r1 }
ACCXLVIII = { | r1 \mbreak }
BCCXLVI = { | r1 }
BCCXLVII = { | r1 }
BCCXLVIII = { | r1 \mbreak }

% measures 249 - 251
ACCXLIX = { | r1 }
ACCL = { | r1 }
ACCLI = { | r1 \mbreak }
BCCXLIX = { | r1 }
BCCL = { | r1 }
BCCLI = { | r1 \mbreak }

% measures 252 - 254
ACCLII = { | r1 }
ACCLIII = { | r1 }
ACCLIV = { | r1 \mbreak }
BCCLII = { | r1 }
BCCLIII = { | r1 }
BCCLIV = { | r1 \mbreak }

% measures 255 - 258
ACCLV = { | r1 }
ACCLVI = { | r1 }
ACCLVII = { | r1 }
ACCLVIII = { | r1 \mbreak }
BCCLV = { | r1 }
BCCLVI = { | r1 }
BCCLVII = { | r1 }
BCCLVIII = { | r1 \mbreak }

% measures 259 - 262
ACCLIX = { | r1 }
ACCLX = { | r1 }
ACCLXI = { | r1 }
ACCLXII = { | r1 \mbreak \pgbreak }
BCCLIX = { | r1 }
BCCLX = { | r1 }
BCCLXI = { | r1 }
BCCLXII = { | r1 \mbreak \pgbreak }

% measures 263 - 266
ACCLXIII = { | r1 }
ACCLXIV = { | r1 }
ACCLXV = { | r1 }
ACCLXVI = { | r1 \mbreak }
BCCLXIII = { | r1 }
BCCLXIV = { | r1 }
BCCLXV = { | r1 }
BCCLXVI = { | r1 \mbreak }

% measures 267 - 270
ACCLXVII = { | r1 }
ACCLXVIII = { | r1 }
ACCLXIX = { | r1 }
ACCLXX = { | r1 \mbreak }
BCCLXVII = { | r1 }
BCCLXVIII = { | r1 }
BCCLXIX = { | r1 }
BCCLXX = { | r1 \mbreak }

% measures 271 - 274
ACCLXXI = { | r1 }
ACCLXXII = { | r1 }
ACCLXXIII = { | r1 }
ACCLXXIV = { | r1 \mbreak }
BCCLXXI = { | r1 }
BCCLXXII = { | r1 }
BCCLXXIII = { | r1 }
BCCLXXIV = { | r1 \mbreak }

% measures 275 - 278
ACCLXXV = { | r1 }
ACCLXXVI = { | r1 }
ACCLXXVII = { | r1 }
ACCLXXVIII = { | r1 \mbreak }
BCCLXXV = { | r1 }
BCCLXXVI = { | r1 }
BCCLXXVII = { | r1 }
BCCLXXVIII = { | r1 \mbreak }

% measures 279 - 282
ACCLXXIX = { | r1 }
ACCLXXX = { | r1 }
ACCLXXXI = { | r1 }
ACCLXXXII = { | r1 \mbreak \pgbreak }
BCCLXXIX = { | r1 }
BCCLXXX = { | r1 }
BCCLXXXI = { | r1 }
BCCLXXXII = { | r1 \mbreak \pgbreak }

% measures 283 - 286
ACCLXXXIII = { | r1 }
ACCLXXXIV = { | r1 }
ACCLXXXV = { | r1 }
ACCLXXXVI = { | r1 \mbreak }
BCCLXXXIII = { | r1 }
BCCLXXXIV = { | r1 }
BCCLXXXV = { | r1 }
BCCLXXXVI = { | r1 \mbreak }

% measures 287 - 290
ACCLXXXVII = { | r1 }
ACCLXXXVIII = { | r1 }
ACCLXXXIX = { | r1 }
ACCXC = { | r1 \mbreak }
BCCLXXXVII = { | r1 }
BCCLXXXVIII = { | r1 }
BCCLXXXIX = { | r1 }
BCCXC = { | r1 \mbreak }

% measures 291 - 294
ACCXCI = { | r1 }
ACCXCII = { | r1 }
ACCXCIII = { | r1 }
ACCXCIV = { | r1 \mbreak }
BCCXCI = { | r1 }
BCCXCII = { | r1 }
BCCXCIII = { | r1 }
BCCXCIV = { | r1 \mbreak }

% measures 295 - 298
ACCXCV = { | r1 }
ACCXCVI = { | r1 }
ACCXCVII = { | r1 }
ACCXCVIII = { | r1 \mbreak }
BCCXCV = { | r1 }
BCCXCVI = { | r1 }
BCCXCVII = { | r1 }
BCCXCVIII = { | r1 \mbreak }

% measures 299 - 302
ACCXCIX = { | r1 }
ACCC = { | r1 }
ACCCI = { | r1 }
ACCCII = { | r1 \mbreak \pgbreak }
BCCXCIX = { | r1 }
BCCC = { | r1 }
BCCCI = { | r1 }
BCCCII = { | r1 \mbreak \pgbreak }

% measures 303 - 306
ACCCIII = { | r1 }
ACCCIV = { | r1 }
ACCCV = { | r1 }
ACCCVI = { | r1 \mbreak }
BCCCIII = { | r1 }
BCCCIV = { | r1 }
BCCCV = { | r1 }
BCCCVI = { | r1 \mbreak }

% measures 307 - 310
ACCCVII = { | r1 }
ACCCVIII = { | r1 }
ACCCIX = { | r1 }
ACCCX = { | r1 \mbreak }
BCCCVII = { | r1 }
BCCCVIII = { | r1 }
BCCCIX = { | r1 }
BCCCX = { | r1 \mbreak }

% measures 311 - 314
ACCCXI = { | r1 }
ACCCXII = { | r1 }
ACCCXIII = { | r1 }
ACCCXIV = { | r1 \mbreak }
BCCCXI = { | r1 }
BCCCXII = { | r1 }
BCCCXIII = { | r1 }
BCCCXIV = { | r1 \mbreak }

% measures 315 - 317
ACCCXV = { | r1 }
ACCCXVI = { | r1 }
ACCCXVII = { | r1 \mbreak }
BCCCXV = { | r1 }
BCCCXVI = { | r1 }
BCCCXVII = { | r1 \mbreak }

% measures 318 - 321
ACCCXVIII = { | r1 }
ACCCXIX = { | r1 }
ACCCXX = { | r1 }
ACCCXXI = { | r1 \mbreak \pgbreak }
BCCCXVIII = { | r1 }
BCCCXIX = { | r1 }
BCCCXX = { | r1 }
BCCCXXI = { | r1 \mbreak \pgbreak }


APart = {
  \tempo 4 = 77
  \clef treble
  \time 4/4

  \AI \AII \AIII \AIV 
  \AV \AVI \AVII \AVIII 
  \AIX \AX \AXI \AXII 
  \AXIII \AXIV \AXV \AXVI 
  \AXVII \AXVIII \AXIX \AXX 
  \AXXI \AXXII \AXXIII \AXXIV 
  \AXXV \AXXVI \AXXVII \AXXVIII 
  \AXXIX \AXXX \AXXXI \AXXXII 
  \AXXXIII \AXXXIV \AXXXV \AXXXVI 
  \AXXXVII \AXXXVIII \AXXXIX \AXL 
  \AXLI \AXLII \AXLIII \AXLIV 
  \AXLV \AXLVI \AXLVII \AXLVIII 
  \AXLIX \AL \ALI \ALII 
  \ALIII \ALIV \ALV 
  \ALVI \ALVII \ALVIII 
  \ALIX \ALX \ALXI \ALXII 
  \ALXIII \ALXIV \ALXV \ALXVI 
  \ALXVII \ALXVIII \ALXIX \ALXX 
  \ALXXI \ALXXII \ALXXIII \ALXXIV 
  \ALXXV \ALXXVI \ALXXVII 
  \ALXXVIII \ALXXIX \ALXXX \ALXXXI 
  \ALXXXII \ALXXXIII \ALXXXIV \ALXXXV 
  \ALXXXVI \ALXXXVII \ALXXXVIII 
  \ALXXXIX \AXC \AXCI \AXCII 
  \AXCIII \AXCIV \AXCV 
  \AXCVI \AXCVII \AXCVIII \AXCIX 
  \AC \ACI \ACII \ACIII 
  \ACIV \ACV \ACVI \ACVII 
  \ACVIII \ACIX \ACX \ACXI 
  \ACXII \ACXIII \ACXIV \ACXV \ACXVI 
  \ACXVII \ACXVIII \ACXIX \ACXX 
  \ACXXI \ACXXII \ACXXIII \ACXXIV 
  \ACXXV \ACXXVI \ACXXVII \ACXXVIII 
  \ACXXIX \ACXXX \ACXXXI \ACXXXII 
  \ACXXXIII \ACXXXIV \ACXXXV \ACXXXVI 
  \ACXXXVII \ACXXXVIII \ACXXXIX \ACXL 
  \ACXLI \ACXLII \ACXLIII \ACXLIV 
  \ACXLV \ACXLVI \ACXLVII \ACXLVIII 
  \ACXLIX \ACL \ACLI \ACLII 
  \ACLIII \ACLIV \ACLV \ACLVI 
  \ACLVII \ACLVIII \ACLIX \ACLX 
  \ACLXI \ACLXII \ACLXIII \ACLXIV 
  \ACLXV \ACLXVI \ACLXVII \ACLXVIII 
  \ACLXIX \ACLXX \ACLXXI \ACLXXII 
  \ACLXXIII \ACLXXIV \ACLXXV \ACLXXVI 
  \ACLXXVII \ACLXXVIII \ACLXXIX \ACLXXX 
  \ACLXXXI \ACLXXXII \ACLXXXIII \ACLXXXIV 
  \ACLXXXV \ACLXXXVI \ACLXXXVII \ACLXXXVIII 
  \ACLXXXIX \ACXC \ACXCI \ACXCII 
  \ACXCIII \ACXCIV \ACXCV 
  \ACXCVI \ACXCVII \ACXCVIII 
  \ACXCIX \ACC \ACCI 
  \ACCII \ACCIII \ACCIV 
  \ACCV \ACCVI \ACCVII 
  \ACCVIII \ACCIX \ACCX 
  \ACCXI \ACCXII \ACCXIII \ACCXIV 
  \ACCXV \ACCXVI \ACCXVII 
  \ACCXVIII \ACCXIX \ACCXX \ACCXXI 
  \ACCXXII \ACCXXIII \ACCXXIV \ACCXXV 
  \ACCXXVI \ACCXXVII \ACCXXVIII \ACCXXIX 
  \ACCXXX \ACCXXXI \ACCXXXII \ACCXXXIII 
  \ACCXXXIV \ACCXXXV \ACCXXXVI \ACCXXXVII 
  \ACCXXXVIII \ACCXXXIX \ACCXL \ACCXLI 
  \ACCXLII \ACCXLIII \ACCXLIV \ACCXLV 
  \ACCXLVI \ACCXLVII \ACCXLVIII 
  \ACCXLIX \ACCL \ACCLI 
  \ACCLII \ACCLIII \ACCLIV 
  \ACCLV \ACCLVI \ACCLVII \ACCLVIII 
  \ACCLIX \ACCLX \ACCLXI \ACCLXII 
  \ACCLXIII \ACCLXIV \ACCLXV \ACCLXVI 
  \ACCLXVII \ACCLXVIII \ACCLXIX \ACCLXX 
  \ACCLXXI \ACCLXXII \ACCLXXIII \ACCLXXIV 
  \ACCLXXV \ACCLXXVI \ACCLXXVII \ACCLXXVIII 
  \ACCLXXIX \ACCLXXX \ACCLXXXI \ACCLXXXII 
  \ACCLXXXIII \ACCLXXXIV \ACCLXXXV \ACCLXXXVI 
  \ACCLXXXVII \ACCLXXXVIII \ACCLXXXIX \ACCXC 
  \ACCXCI \ACCXCII \ACCXCIII \ACCXCIV 
  \ACCXCV \ACCXCVI \ACCXCVII \ACCXCVIII 
  \ACCXCIX \ACCC \ACCCI \ACCCII 
  \ACCCIII \ACCCIV \ACCCV \ACCCVI 
  \ACCCVII \ACCCVIII \ACCCIX \ACCCX 
  \ACCCXI \ACCCXII \ACCCXIII \ACCCXIV 
  \ACCCXV \ACCCXVI \ACCCXVII 
  \ACCCXVIII \ACCCXIX \ACCCXX \ACCCXXI 
}

BPart = {
  \clef bass
  \time 4/4

  \BI \BII \BIII \BIV 
  \BV \BVI \BVII \BVIII 
  \BIX \BX \BXI \BXII 
  \BXIII \BXIV \BXV \BXVI 
  \BXVII \BXVIII \BXIX \BXX 
  \BXXI \BXXII \BXXIII \BXXIV 
  \BXXV \BXXVI \BXXVII \BXXVIII 
  \BXXIX \BXXX \BXXXI \BXXXII 
  \BXXXIII \BXXXIV \BXXXV \BXXXVI 
  \BXXXVII \BXXXVIII \BXXXIX \BXL 
  \BXLI \BXLII \BXLIII \BXLIV 
  \BXLV \BXLVI \BXLVII \BXLVIII 
  \BXLIX \BL \BLI \BLII 
  \BLIII \BLIV \BLV 
  \BLVI \BLVII \BLVIII 
  \BLIX \BLX \BLXI \BLXII 
  \BLXIII \BLXIV \BLXV \BLXVI 
  \BLXVII \BLXVIII \BLXIX \BLXX 
  \BLXXI \BLXXII \BLXXIII \BLXXIV 
  \BLXXV \BLXXVI \BLXXVII 
  \BLXXVIII \BLXXIX \BLXXX \BLXXXI 
  \BLXXXII \BLXXXIII \BLXXXIV \BLXXXV 
  \BLXXXVI \BLXXXVII \BLXXXVIII 
  \BLXXXIX \BXC \BXCI \BXCII 
  \BXCIII \BXCIV \BXCV 
  \BXCVI \BXCVII \BXCVIII \BXCIX 
  \BC \BCI \BCII \BCIII 
  \BCIV \BCV \BCVI \BCVII 
  \BCVIII \BCIX \BCX \BCXI 
  \BCXII \BCXIII \BCXIV \BCXV \BCXVI 
  \BCXVII \BCXVIII \BCXIX \BCXX 
  \BCXXI \BCXXII \BCXXIII \BCXXIV 
  \BCXXV \BCXXVI \BCXXVII \BCXXVIII 
  \BCXXIX \BCXXX \BCXXXI \BCXXXII 
  \BCXXXIII \BCXXXIV \BCXXXV \BCXXXVI 
  \BCXXXVII \BCXXXVIII \BCXXXIX \BCXL 
  \BCXLI \BCXLII \BCXLIII \BCXLIV 
  \BCXLV \BCXLVI \BCXLVII \BCXLVIII 
  \BCXLIX \BCL \BCLI \BCLII 
  \BCLIII \BCLIV \BCLV \BCLVI 
  \BCLVII \BCLVIII \BCLIX \BCLX 
  \BCLXI \BCLXII \BCLXIII \BCLXIV 
  \BCLXV \BCLXVI \BCLXVII \BCLXVIII 
  \BCLXIX \BCLXX \BCLXXI \BCLXXII 
  \BCLXXIII \BCLXXIV \BCLXXV \BCLXXVI 
  \BCLXXVII \BCLXXVIII \BCLXXIX \BCLXXX 
  \BCLXXXI \BCLXXXII \BCLXXXIII \BCLXXXIV 
  \BCLXXXV \BCLXXXVI \BCLXXXVII \BCLXXXVIII 
  \BCLXXXIX \BCXC \BCXCI \BCXCII 
  \BCXCIII \BCXCIV \BCXCV 
  \BCXCVI \BCXCVII \BCXCVIII 
  \BCXCIX \BCC \BCCI 
  \BCCII \BCCIII \BCCIV 
  \BCCV \BCCVI \BCCVII 
  \BCCVIII \BCCIX \BCCX 
  \BCCXI \BCCXII \BCCXIII \BCCXIV 
  \BCCXV \BCCXVI \BCCXVII 
  \BCCXVIII \BCCXIX \BCCXX \BCCXXI 
  \BCCXXII \BCCXXIII \BCCXXIV \BCCXXV 
  \BCCXXVI \BCCXXVII \BCCXXVIII \BCCXXIX 
  \BCCXXX \BCCXXXI \BCCXXXII \BCCXXXIII 
  \BCCXXXIV \BCCXXXV \BCCXXXVI \BCCXXXVII 
  \BCCXXXVIII \BCCXXXIX \BCCXL \BCCXLI 
  \BCCXLII \BCCXLIII \BCCXLIV \BCCXLV 
  \BCCXLVI \BCCXLVII \BCCXLVIII 
  \BCCXLIX \BCCL \BCCLI 
  \BCCLII \BCCLIII \BCCLIV 
  \BCCLV \BCCLVI \BCCLVII \BCCLVIII 
  \BCCLIX \BCCLX \BCCLXI \BCCLXII 
  \BCCLXIII \BCCLXIV \BCCLXV \BCCLXVI 
  \BCCLXVII \BCCLXVIII \BCCLXIX \BCCLXX 
  \BCCLXXI \BCCLXXII \BCCLXXIII \BCCLXXIV 
  \BCCLXXV \BCCLXXVI \BCCLXXVII \BCCLXXVIII 
  \BCCLXXIX \BCCLXXX \BCCLXXXI \BCCLXXXII 
  \BCCLXXXIII \BCCLXXXIV \BCCLXXXV \BCCLXXXVI 
  \BCCLXXXVII \BCCLXXXVIII \BCCLXXXIX \BCCXC 
  \BCCXCI \BCCXCII \BCCXCIII \BCCXCIV 
  \BCCXCV \BCCXCVI \BCCXCVII \BCCXCVIII 
  \BCCXCIX \BCCC \BCCCI \BCCCII 
  \BCCCIII \BCCCIV \BCCCV \BCCCVI 
  \BCCCVII \BCCCVIII \BCCCIX \BCCCX 
  \BCCCXI \BCCCXII \BCCCXIII \BCCCXIV 
  \BCCCXV \BCCCXVI \BCCCXVII 
  \BCCCXVIII \BCCCXIX \BCCCXX \BCCCXXI 
}

InstrumentAPart = \new Staff \with {midiInstrument = "acoustic guitar (steel)"} {
  \APart
}

InstrumentBPart = \new Staff \with {midiInstrument = "acoustic guitar (steel)"} {
  \BPart
}

InstrumentCombined = {
  \new PianoStaff <<
    \set PianoStaff.connectArpeggios = ##t
    \InstrumentAPart
    \InstrumentBPart
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}
