\version "2.24.2"

\header{
  title = "London Bridge is Falling Down"
  instrument = "For Concertina"
}

\score {
  <<
    \new Staff = "staff" {
      \new Voice = "melody" {
        { \time 2/4
          g'8. a'16 g'8 f'8
          e'8 f'8 g'4
          d'8 e'8 f'4
          e'8 f'8 g'4
          % \break
          g'8. a'16 g'8 f'8
          e'8 f'8 g'4
          d'4 g'4
          e'8 c'4.
        }
      }
    }
    % Lyrics below are for the left hand
    % The horizontal line means draw; if omitted it means push
    \new Lyrics {
      \lyricsto "melody" {
        " "  "_"  " "  "_"
        " "  "_"  " "
        "_"  " "  "_"
        " "  "_"  " "
        " "  "_"  " "  "_"
        " "  "_"  " "
        "_"  " "
        " "  " "
      }
    }
    % 5 means index finger
    \new Lyrics {
      \lyricsto "melody" {
        "5"  "5"  "5"  "4"
        "4"  "4"  "5"
        "3"  "4"  "4"
        "4"  "4"  "5"
        "5"  "5"  "5"  "4"
        "4"  "4"  "5"
        "3"  "5"
        "4"  "3"
      }
    }
    \new Lyrics {
      \lyricsto "melody" {
        Lon don bridge is fall- ing down
        fall- ing down, fall- ing down
        Lon don bridge is fall- ing down
        My fair la- dy
      }
    }
    % Lyrics above are for the right hand
    % Horizontal bar means draw; if omitted it means push
    \new Lyrics \with { alignAboveContext = "staff" } {
      \lyricsto "melody" {
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "  " "
        " "  " "  " "
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "
        " "  " "
      }
    }
    % 1 means index finger
    \new Lyrics \with { alignAboveContext = "staff" } {
      \lyricsto "melody" {
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "  " "
        " "  " "  " "
        " "  " "  " "  " "
        " "  " "  " "
        " "  " "
        " "  " "
      }
    }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 90
  }
}
