\version "2.24.2"

\header{
  title = "The Legend of Zelda™: Ocarina of Time™"
  subtitle = "GERUDO VALLEY"
  composer = "Composed by Koji Kondo"
  arranger = "Piano arrangement by Shinobu Amayake"
}

% TODO need to go back and add repeats

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% To just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = { \break }
pgbreak = { \pageBreak }

% measures 1 - 6
AI = { | r8 \mf d'16 f'16~ f'8 a'8 r8 gis'16 d''16 e''4 }
AII = { | r1 }
AIII = { | r1 }
AIV = { | r1 }
AV = { | r1 }
AVI = { | r1 \mbreak }
BI = { | <e a>8\mf r8 r4 r4 e4 }
BII = { | e4 e4 e8. e16~ e8 e8 }
BIII = { | e4 e4 e8. e16~ e8 e8 }
BIV = { | <a c'>4\p <a c'>4 <a c'>4 <a c'>8 <a c'>8 }
BV = { | <f a>4 <f a>4 <f a>4 <f a>8 <f a>8 }
BVI = { | <g b>4 <g b>4 <g b>4 <g b>8 <g b>8 \mbreak }

% measures 7 - 10
AVII = { | d'4 d'4 d'4 d'8 d'8 }
AVIII = { | r16 e'16[ a'16 b'16] c''8.[ e'16] a'16[ b'16 c''8~] c''4 }
AIX = { | r16 f'16[ a'16 b'16] c''8. f'16 a'16[ b'16 c''8]~ c''4 }
AX = { | r16 d'16[ g'16 a'16] b'8.[ d'16] g'16[ a'16 b'8]~ b'4 \mbreak }
BVII = { | <e gis>4 <e gis>4 <e gis>4 <e gis>8 <e gis>8 }
BVIII = { | <a c'>4 <a c'>4 <a c'>4 <a c'>8 <a c'>8 }
BIX = { | <f a>4 <f a>4 <f a>4 <f a>8 <f a>8 }
BX = { | <g b>4 <g b>4 <g b>4 <g b>8 <g b>8 \mbreak }

% measures 11 - 15
AXI = { | r16 a'16 b'16 a'16 gis'2. }
AXII = { | r16 c''16 d''16 c''16 b'2. }
AXIII = { | r8\mf e'8~ e'16 c''8. b'8. a'16~ a'8 e'8 }
AXIV = { | g'4 g'16 a'16 g'16 f'16~ f'2 }
AXV = { | r8 d'8~ d'16 b'8. a'8. g'16~ g'8 f'8 \mbreak }
BXI = { | <e d'>4 <e d'>4 <e d'>4 <e d'>8 <e d'>8 }
BXII = { | <e gis>4 <e gis>4 <e gis>4 <e gis>8 <e gis>8 }
BXIII = { | <a c'>4\mf <a c'>4 <a c'>4 <a c'>8 <a c'>8 }
BXIV = { | <f a>4 <f a>4 <f a>4 <f a>8 <f a>8 }
BXV = { | <g b>4 <g b>4 <g b>4 <g b>8 <g b>8 \mbreak }

% measures 16 - 20
AXVI = { | e'4 f'16 g'16 f'16 e'16~ e'2 }
AXVII = { | r8 e'8~ e'16 c''8. b'8. a'16~ a'8 e'8 }
AXVIII = { | g'4 g'16 a'16 g'16 f'16~ f'4 r8 c'8 }
AXIX = { | d'4. a'8 g'4. f'8 }
AXX = { | <d' e'>1 \mbreak \pgbreak }
BXVI = { | <e gis>4 <e gis>4 <e gis>4 <e gis>8 <e gis>8 }
BXVII = { | <a c'>4 <a c'>4 <a c'>4 <a c'>8 <a c'>8 }
BXVIII = { | <f a>4 <f a>4 <f a>4 <f a>8 <f a>8 }
BXIX = { | <g b>4 <g b>4 <g b>4 <g b>8 <g b>8 }
BXX = { | <e gis>4 <e gis>4 <e gis>4 <e gis>8 <e gis>8 \mbreak \pgbreak }

% measures 21 - 24
AXXI = { | r8 c''8~ c''16 b'8. c''8. b'16~ b'8 e'8 }
AXXII = { | g'4~ g'16 f'16 e'16 f'16~ f'2 }
AXXIII = { | r8 b'8~ b'16 a'8. b'8. g'16~ g'8 f'8 }
AXXIV = { | e'4 f'16 g'16 f'16 e'16~ e'2 \mbreak }
BXXI = { | <a c'>4 <a c'>4 <a c'>4 <a c'>8 <a c'>8 }
BXXII = { | <f a>4 <f a>4 <f a>4 <f a>8 <f a>8 }
BXXIII = { | <g b>4 <g b>4 <g b>4 <g b>8 <g b>8 }
BXXIV = { | <e gis>4 <e gis>4 <e gis>4 <e gis>8 <e gis>8 \mbreak }

% measures 25 - 28
AXXV = { | r8 a16 b16 c'16 e'8. a16 b16 c'16 e'16~ e'4 }
AXXVI = { | r8 a16 b16 c'16 f'8. a16 b16 c'16 f'16~ f'8 f'8 }
AXXVII = { | d'4. <g' b'>8 <f' a'>4. d'8 }
AXXVIII = { | <d' e'>1 \mbreak }
BXXV = { | <a, e>4 <a, e>4 <a, e>4 <a, e>8 <a, e>8 }
BXXVI = { | <f, c>4 <f, c>4 <f, c>4 <f, c>8 <f, c>8 }
BXXVII = { | <g, f>4 <g, f>4 <g, f>4 <g, f>8 <g, f>8 }
BXXVIII = { | <e gis>4 <e gis>4 <e gis>4 <e gis>8 <e gis>8 \mbreak }

% measures 29 - 32
AXXIX = { | c''16 b'16 c''8~ c''4 r8 a'16 b'16 c''16 d''16 c''16 b'16 }
AXXX = { | c''16 b'16 c''8~ c''2. }
AXXXI = { | b'16 a'16 b'8~ b'4 r8 g'16 a'16 b'16 c''16 b'16 a'16 }
AXXXII = { | b'16 a'16 gis'8~ gis'2. \mbreak }
BXXIX = { | <a c'>4 <a c'>4 <a c'>4 <a c'>8 <a c'>8 }
BXXX = { | <f a>4 <f a>4 <f a>4 <f a>8 <f a>8 }
BXXXI = { | <g b>4 <g b>4 <g b>4 <g b>8 <g b>8 }
BXXXII = { | <e d'>4 <e d'>4 <e d'>4 <e d'>8 <e d'>8 \mbreak }

% measures 33 - 36
AXXXIII = { | c''16 b'16 c''8~ c''4 r8 a'16 b'16 c''16 d''16 c''16 b'16 }
AXXXIV = { | c''16 b'16 c''8~ c''2. }
AXXXV = { | d''16 c''16 d''8~ d''4 r8 b'16 c''16 d''16 e''16 f''16 g''16 }
AXXXVI = { | e''16 f''16 e''8~ e''2. \mbreak }
BXXXIII = { | <a c'>4 <a c'>4 <a c'>4 <a c'>8 <a c'>8 }
BXXXIV = { | <f a>4 <f a>4 <f a>4 <f a>8 <f a>8 }
BXXXV = { | <g b>4 <g b>4 <g b>4 <g b>8 <g b>8 }
BXXXVI = { | <e gis>4 <e gis>4 <e gis>4 <e gis>8 <e gis>8 \mbreak }

% measures 37 - 40
AXXXVII = { | a8 c'16 b16~ b8 c'8 a8 c'16 b16~ b8 c'8 }
AXXXVIII = { | f8 c'16 b16~ b8 c'8 f8 c'16 b16~ b8 c'8 }
AXXXIX = { | g8 d'16 cis'16~ cis'8 d'8 g8 d'16 cis'16~ cis'8 d'8 }
AXL = { | f'16 g'16 f'16 e'16~ e'2. \mbreak }
BXXXVII = { | <a, e>4 <a, e>4 <a, e>4 <a, e>8 <a, e>8 }
BXXXVIII = { | <f, c>4 <f, c>4 <f, c>4 <f, c>8 <f, c>8 }
BXXXIX = { | <g, d>4 <g, d>4 <g, d>4 <g, d>8 <g, d>8 }
BXL = { | <e gis>4 <e gis>4 <e gis>4 <e gis>8 <e gis>8 \mbreak }

AHalf = {
  \tempo 4 = 114
  \clef treble
  \time 4/4
  
  \AI \AII \AIII \AIV \AV \AVI
  \AVII 
  \repeat volta 2 {
    \AVIII \AIX \AX
    \alternative {
      \volta 1 { \AXI }
      \volta 2 { \AXII }
    }
  }
  \AXIII \AXIV \AXV
  \AXVI \AXVII \AXVIII \AXIX \AXX
  \AXXI \AXXII \AXXIII \AXXIV
  \AXXV \AXXVI \AXXVII \AXXVIII
  \AXXIX \AXXX \AXXXI \AXXXII
  \AXXXIII \AXXXIV \AXXXV \AXXXVI
  \AXXXVII \AXXXVIII \AXXXIX \AXL
}

BHalf = {
  \clef bass
  \time 4/4

  \BI \BII \BIII \BIV \BV \BVI
  \BVII 
  \repeat volta 2 {
    \BVIII \BIX \BX
    \alternative {
      \volta 1 { \BXI }
      \volta 2 { \BXII }
    }
  }
  \BXIII \BXIV \BXV
  \BXVI \BXVII \BXVIII \BXIX \BXX
  \BXXI \BXXII \BXXIII \BXXIV
  \BXXV \BXXVI \BXXVII \BXXVIII
  \BXXIX \BXXX \BXXXI \BXXXII
  \BXXXIII \BXXXIV \BXXXV \BXXXVI
  \BXXXVII \BXXXVIII \BXXXIX \BXL
}

InstrumentAHalf = \new Staff \with {midiInstrument = "acoustic guitar (steel)"} { 
  \AHalf
}

InstrumentBHalf = \new Staff \with {midiInstrument = "acoustic guitar (steel)"} { 
  \BHalf
}

InstrumentCombined = {
  \new PianoStaff <<
    \InstrumentAHalf
    \InstrumentBHalf
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}
