\version "2.24.2"

\header{
  title = "Let It Be"
}

\score {
  <<
    \new Staff = "staff" {
      \new Voice = "melody" {
        { \time 4/4
          s2. s8 g'16 g'16
          g'8 g'16 a'16~ a'8 e'8 g'8 g'8 c''16 d''8.
          e''16 e''8 e''16~ e''8 d''8 d''8 c''8 c''4
          \break
          e''16 e''8. f''8 e''16 e''16~ e''8 d''8 r8 e''16 d''16
          {d''8 (c''4.~ c''4)} r8. g'16
          \break
          g'16 g'8. a'8 c''16 g'16~ g'8 g'8 c''16 e''8.
          d''16 e''8. e''8 d''16 d''16~ d''16 c''8 c''16~ c''4
          \break
          e''16 e''8. f''8 e''16 e''16~ e''8 d''8 r8 e''16 d''16
          {d''16 (c''8.~ c''2)} e''16 d''8 c''16~
          \break
          c''4 e''16 g''8 {a''16~ (a''8 g''8)} g''16 g''8 {e''16~
          (e''16 d''16 c''8)} a'16 g'8 e''16~ e''4 r4
          \break
          e''16 e''8 f''16~ f''8 e''16 e''16~ e''8 d''8 r16 e''16 d''16 {d''16~
          (d''8 c''4. c''4~)} r8
        }
      }
    }

    \new Lyrics {
      \lyricsto "melody" {
        When I find my self in times of trou- ble,
        Moth- er Mar- y comes to me
        speak- ing words of wis- dom; let it be.
        And in my hour of dark- ness she is stand- ing right in front of me
        speak- ing words of wis- dom; let it be. Let it be,
        let it be, let it be, let it be.
        Whis- per words of wis- dom; let it be.
      }
    }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 60
  }
}
