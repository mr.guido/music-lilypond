\version "2.24.2"

\header{
  title = "The Legend of Zelda: The Wind Waker"
  subtitle = "Ocean Theme"
  composer = "Composed by Kenta Nagata"
  arranger = "Arranged by Shinobu Amayake"
  source = "The Legend of Zelda Series for Piano (Kindle)"
}

% D major means c & f are sharp

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = { \break }
pgbreak = { \pageBreak }

Voices_AXVII_AXIX = {
  <<
  { \voiceOne
    | s1\<
    | b'8-. b'16 cis''16 d''2 r4
    | r2 r4 e'4
  }
  \new Voice
  { \voiceTwo
    | b8-. b16 cis'16 d'2.~
    | d'2. e'4
    | d'1
  }
  >>
  \oneVoice
}
Voices_AXXI_AXXIV = {
  <<
  { \voiceOne
    | s1\<
    | r1
    | s1\>
    | \after 2 \! e'1 \mbreak
  }
  \new Voice
  { \voiceTwo
    | b8-.\< b16 cis'16 d'2.~
    | d'2. <d' e'>4
    | <b d'>1\>
    | cis'2 r8 cis'8-. cis'8-. \after 16 \! cis'8-. \mbreak
  }
  >>
  \oneVoice
}

% measures 1 - 4
AI = { | <d' fis' a'>1\mf\< }
AII = { | <cis' e' a'>1 }
AIII = { | <c' e' a'>1\> }
AIV = { | <b d' g'>1 \mbreak }
BI = { | d,8( a,8 d8 e8~ e8 d4 a,8) }
BII = { | d,8( a,8 d8 e8~ e8 d4 a,8) }
BIII = { | d,8( a,8 d8 e8~ e8 d4 a,8) }
BIV = { | d,8( a,8 d8 e8~ e8 d4 a,8) \mbreak }

% measures 5 - 8
AV = { | <a d' fis'>1\< }
AVI = { | <a cis' e'>1 }
AVII = { | <g bes e'>1\> }
AVIII = { | <c' e' g'>1 \mbreak }
BV = { | d,8( a,8 d8 e8~ e8 d4 a,8) }
BVI = { | d,8( a,8 d8 e8~ e8 d4 a,8) }
BVII = { | d,8( a,8 d8 e8~ e8 d4 a,8) }
BVIII = { | d,8( a,8 d8 e8~ e8 d4 a,8) \mbreak }

% measures 9 - 12
AIX = { | fis'8-.->\![ r16 d'16] a2.~ }
AX = { | a2. d'16 a16 d'16 fis'16 }
AXI = { << { \voiceOne | a'1~ | a'2. a'8 g'16 fis'16 } \new Voice { \voiceTwo | e'1 | d'1 } >> \oneVoice }
BIX = { | d,4-.-> r8 <d fis>8-. r8 <d fis>8-. <d fis>8-. a,8-.-> }
BX = { | d,4-.-> r8 <d fis>8-. r8 <d fis>8-. <d fis>8-. a,8-.-> }
BXI = { | fis,4-.-> r8 <fis a>8-. r8 <fis a>8-. <fis a>8-. d,8-.-> }
BXII = { | fis,4-.-> r8 <fis a>8-. r8 <fis a>8-. <fis a>8-. d,8-.-> \mbreak }

% measures 13 - 16
AXIII = { | <d' g'>1~ }
AXIV = { | <d' g'>1 }
AXV = { | << { \voiceOne | r2 r4 \tuplet 3/2 { a'8-. a'8-. a'8-. } | a'1 } \new Voice { \voiceTwo | <d' e'>1~ | <cis' e'>1 } >> \oneVoice }
BXIII = { | g,4-.-> r8 <g b>8-. r8 <g b>8-. <g b>8-. d,8-.-> }
BXIV = { | g,4-.-> r8 <g b>8-. r8 <g b>8-. <g b>8-. g,8-.-> }
BXV = { | a,4-.-> r8 <e a>8-. r8 <e a>8-. <e a>8-. e,8-.-> }
BXVI = { | a,4-.-> r8 <e a>8-. r8 <e a>8-. <e a>8-. e,8-.-> \mbreak \pgbreak }

% measures 17 - 20
AXVII_AXIX = \Voices_AXVII_AXIX
AXX = { | <a d'>1 \mbreak }
BXVII = { | g,4-.-> r8 <d g>8-. r8 <d g>8-. <d g>8-. d,8-.-> }
BXVIII = { | g,4-.-> r8 <g b>8-. r8 <g b>8-. <g b>8-. d,8-.-> }
BXIX = { | g,4-.-> r8 <fis a>8-. r8 <fis a>8-. <fis a>8-. d,8-.-> }
BXX = { | fis,4-.-> r8 <d fis>8-. r8 <d fis>8-. <d fis>8-. fis,8-.-> \mbreak }

% measures 21 - 24
AXXI_AXXIV = \Voices_AXXI_AXXIV
BXXI = { | e,4-.-> r8 <e g>8-. r8 <e g>8-. <e g>8-. b,8-.-> }
BXXII = { | e,4-.-> r8 <e g>8-. r8 <e g>8-. <e g>8-. <e g>8-. }
BXXIII = { | a,4-.-> r8 <e a>8-. r8 <e a>8-. <e a>8-. e,8-.-> }
BXXIV = { | a,4-.-> r8 <e a>8-. r8 <e a>8-. <e a>8-. <e a>8-. \mbreak }

% measures 25 - 28
AXXV_AXXVI = { << { \voiceOne | s1 | b'8-. b'16 cis''16 d''2 r4 } \new Voice { \voiceTwo | b8-. b16 cis'16 d'2.~ | d'2. e'4 } >> \oneVoice }
AXXVII_AXXVIII = { << { \voiceOne | s2 fis'2~ | fis'1 } \new Voice { \voiceTwo | d'2 r4 e'4 | d'1 } >> \oneVoice \mbreak }
BXXV = { | g,4-.-> r8 <d g>8-. r8 <d g>8-. <d g>8-. d,8-.-> }
BXXVI = { | g,4-.-> r8 <g b>8-. r8 <g b>8-. <g b>8-. d,8-.-> }
BXXVII = { | fis,4-.-> r8 <fis a>8-. r8 <fis a>8-. <fis a>8-. d,8-.-> }
BXXVIII = { | b,4-.-> r8 <fis b>8-. r8 <fis b>8-. <fis b>8-. fis,8-.-> \mbreak }

% measures 29 - 32
AXXIX = { | d'8-. d'16 e'16 \after 8 \< <d' f'>2.~ }
AXXX = { << { \voiceOne | f'1 } \new Voice { \voiceTwo | bes8-. bes16 c'16 d'2. } >> \oneVoice }
AXXXI = { | <bes d'>8 <bes d'>16 <c' e'>16 <d' f'>2. }
AXXXII = { | <e' g'>1\! \mbreak \pgbreak }
BXXIX = { | bes,4-.-> r8 <f bes>8-.\< r8 <f bes>8-. <f bes>8-. f,8-.-> }
BXXX = { | bes,4-.-> r8 <f bes>8-. r8 <f bes>8-. <f bes>8-. bes,8-.-> }
BXXXI = { | c4-.-> r8 <f bes>8-. r8 <f bes>8-. <f bes>8 -. g,8-.-> }
BXXXII = { | c4-.->\! r8 <e g>8 r8 <e g>8-. <e g>8-. g,8-.-> \mbreak \pgbreak }

% measures 33 - 36
AXXXIII = { | <a d' fis'>1\mf\< }
AXXXIV = { | <b e' gis'> }
AXXXV = { | <a d' fis'>1\> }
AXXXVI = { | <cis' d' a'>1 \mbreak }
BXXXIII = { | d,8(\mf\< a,8 d8 e8~ e8 d4 a,8) }
BXXXIV = { | d,8( a,8 d8 e8~ e8 d4 a,8) }
BXXXV = { | d,8(\> a,8 d8 e8~ e8 d4 a,8) }
BXXXVI = { | d,8( a,8 d8 e8~ e8 d4 a,8) \mbreak }

% measures 37 - 40
Voices_AXXXVII_AXL = {
  <<
  { \voiceOne
    | s1\!
    | fis''8[-.-> r16 d''16] a'2.
    | r2 r4 d''8 fis''8
    | fis''8 e''16 dis''16 e''2 r4
  }
  \new Voice
  { \voiceTwo
    | f'8-.->[\! r16 d'16] a2.~
    | a2. d'8 fis'8
  }
  >>
  \oneVoice
}
BXXXVII = { | r1\! }
BXXXVIII = { | r1 }
BXXXIX = { | r1 }
BXL = { | r1 \mbreak }

% measures 41 - 44
AXLI = { | r1 }
AXLII = { | r1 }
AXLIII = { | r1 }
AXLIV = { | r1 \mbreak }
BXLI = { | r1 }
BXLII = { | r1 }
BXLIII = { | r1 }
BXLIV = { | r1 \mbreak }

% measures 45 - 48
AXLV = { | r1 }
AXLVI = { | r1 }
AXLVII = { | r1 }
AXLVIII = { | r1 \mbreak \pgbreak }
BXLV = { | r1 }
BXLVI = { | r1 }
BXLVII = { | r1 }
BXLVIII = { | r1 \mbreak \pgbreak }

% measures 49 - 52
AXLIX = { | r1 }
AL = { | r1 }
ALI = { | r1 }
ALII = { | r1 \mbreak }
BXLIX = { | r1 }
BL = { | r1 }
BLI = { | r1 }
BLII = { | r1 \mbreak }

% measures 53 - 55
ALIII = { | r1 }
ALIV = { | r1 }
ALV = { | r1 \mbreak }
BLIII = { | r1 }
BLIV = { | r1 }
BLV = { | r1 \mbreak }

% measures 56 - 58
ALVI = { | r1 }
ALVII = { | r1 }
ALVIII = { | r1 \mbreak }
BLVI = { | r1 }
BLVII = { | r1 }
BLVIII = { | r1 \mbreak }

% measures 59 - 62
ALIX = { | r1 }
ALX = { | r1 }
ALXI = { | r1 }
ALXII = { | r1 \mbreak \pgbreak }
BLIX = { | r1 }
BLX = { | r1 }
BLXI = { | r1 }
BLXII = { | r1 \mbreak \pgbreak }

APart = {
  \tempo 4 = 140
  \clef treble
  \time 4/4
  \key d \major

  \AI \AII \AIII \AIV 
  \AV \AVI \AVII \AVIII 
  \AIX \AX \AXI 
  \AXIII \AXIV \AXV 
  \AXVII_AXIX \AXX 
  \AXXI_AXXIV 
  \AXXV_AXXVI \AXXVII_AXXVIII 
  \AXXIX \AXXX \AXXXI \AXXXII 
  \AXXXIII \AXXXIV \AXXXV \AXXXVI 
  \Voices_AXXXVII_AXL
  \AXLI \AXLII \AXLIII \AXLIV 
  \AXLV \AXLVI \AXLVII \AXLVIII 
  \AXLIX \AL \ALI \ALII 
  \ALIII \ALIV \ALV 
  \ALVI \ALVII \ALVIII 
  \ALIX \ALX \ALXI \ALXII 
}

BPart = {
  \clef bass
  \key d \major

  \BI \BII \BIII \BIV 
  \BV \BVI \BVII \BVIII 
  \BIX \BX \BXI \BXII 
  \BXIII \BXIV \BXV \BXVI 
  \BXVII \BXVIII \BXIX \BXX 
  \BXXI \BXXII \BXXIII \BXXIV 
  \BXXV \BXXVI \BXXVII \BXXVIII 
  \BXXIX \BXXX \BXXXI \BXXXII 
  \BXXXIII \BXXXIV \BXXXV \BXXXVI 
  \BXXXVII \BXXXVIII \BXXXIX \BXL 
  \BXLI \BXLII \BXLIII \BXLIV 
  \BXLV \BXLVI \BXLVII \BXLVIII 
  \BXLIX \BL \BLI \BLII 
  \BLIII \BLIV \BLV 
  \BLVI \BLVII \BLVIII 
  \BLIX \BLX \BLXI \BLXII 
}

InstrumentAPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \APart
}

InstrumentBPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \BPart
}

InstrumentCombined = {
  \new PianoStaff <<
    \InstrumentAPart
    \InstrumentBPart
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
 }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
   }
  >>
  \midi { }
}
