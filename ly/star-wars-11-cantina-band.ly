\version "2.24.2"

\header{
  title = "Star Wars"
  subtitle = "Cantina Band"
  composer = "Music by John Williams"
  arranger = "Arranged by Dan Coates"
}

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% To just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = { }

AHalf = {
  \tempo 4 = 220
  \key f \major
  \clef treble
  \time 2/2

  \repeat volta 2 {
    | a'4-.\mf d''4-. a'4-. d''4-.
    | a'8 d''4 a'8~ a'8[ gis'8] a'4
    | a'8 gis'8 a'8 g'8 r8 fis'8 g'8 f'8
    \mbreak

    | f'4. d'8~ d'4 r4
    | a'4-. d''4-. a'4-. d''4-.
    | a'8 d''4 a'8~ a'8[ gis'8] a'4
    \mbreak

    | g'4\< g'4~ g'8 fis' g'4\!
    | c''4-.\> bes'4-. a'4-. g'4-.\!
    | a'4-. d''4-. a'4-. d''4-.
    \mbreak

    | a'8 d''4 a'8~ a'8[ gis'8] a'4
    | c''4 c''4~ c''8 a'8 g'4
    | f'4.\> d'8~ d'2\!
    \mbreak

    | {d'2\mp\cresc (f'2\! a'2\< \after 4 \! c''2
    | ees''4\f d''4 gis'8 a'4.
    | f'1\>)}
    \mbreak

    | r8\! {a'4 (f'8 a'4-.)} r4
    | r8 {a'4 (f'8 a'4-.)} r4
    | r8 {a'4 (f'8 gis'4 a'4
    \mbreak
    
    | f'4. d'8~ d'2)}
    | r8 {a'4 (f'8 a'4-.)} r4
    | r8 {a'4 (f'8 a'4-.)} r4
    \mbreak

    | r8 {a'4 (f'8 gis'4 a'4
    | g'2 c'2)}
    | r8\mf {a'4 (f'8 a'4-.)} r4
    \mbreak

    | r8 {a'4 (f'8 a'4-.)} r4
    | r8 {a'4 (f'8 gis'4 a'4
    | f'4.\> d'8~ \after 4. \! d'2)}
    \mbreak

    | {bes8\mp (d'8 f'4-.)} {b8 (d'8 f'4-.)}
    | {gis'8\< (a'4 d'8~ \after 4. \! d'2)}
    | d'8\f( f'8 bes'8 d''8 gis'8 a'4. <>)
    \mbreak

    \alternative {
      \volta 1 { | f'1\> }
      \volta 2 { | f'2\! <a' f''>4->-.\sfz r4 \bar "|." }
    }
  }

  | c'4.\mf e'8~ e'4 g'4~
  \mbreak
  
  | g'8 g'4 g'8 g'4 e'8 c'8
  | f'4 f'4 f'8 <e' g'>4.
  | <f' a'>1\>
  \mbreak

  | e'4\mp\! e'4 g'8 g'4 c'8~
  | c'8 e'4 c'8 e'4 g'4
  | f'4 f'4 f'8 g'4.
  \mbreak

  | \after 4 \< <d' fis' a'>1
  | bes'4\!\mf\cresc bes'4\! d''4 bes'8 des''8~
  | des''8 des''8 bes'4 des''4 bes'4
  \mbreak

  | f'4\< f'4 f'8 a'4.\!
  | <fis' a' d''>1\f\>
  | r8\! bes4 d'8 g'4 g'4
  \mbreak

  | r8 e'4 e'8 e'4 e'4
  | r8 f'4 f'8 f'4 f'4
  | \after 4 \> \after 2. \! f'1

  \fine
}

BHalf = {
  \key f \major
  \clef bass
  \time 2/2
  
  \repeat volta 2 {
    d4-. <f a>4-. a,4-. <f a>4-.
    | d4-. <f a>4-. a,4-. <f a>4-.
    | d4 <f a>4 c4 <e g>4
    \mbreak

    | d4 c4 bes,4 a,4
    | d4 <f a>4 a,4 <f a>4
    | d4 <f a>4 a,4 <f a>4
    \mbreak

    | g,4 <f g>4 b,4 <f g>4
    | c4 d4 dis4 e4
    | d4 <f a>4 a,4 <f a>4
    \mbreak

    | d4 <f a>4 a,4 <f a>4
    | c4 <e g>4 g,4 <e g>4
    | d4 <f a>4 a,4 <f a>4
    \mbreak

    | bes,4 <d f>4 b,4 <f aes>4
    | c4 <f a>4 d4 <fis a>4
    | <g, f>4 r4 <c bes>4 r4
    | <f a>4 e4 d4 c4
    \mbreak

    | d4 <f a>4 a,4 <f a>4
    | d4 <f a>4 a,4 <f a>4
    | d4 <f a>4 a,4 <f a>4
    \mbreak

    | d4 c4 bes,4 a,4
    | d4 <f a>4 a,4 <f a>4
    | d4 <f a>4 a,4 <f a>4
    \mbreak

    | d4 <f a>4 a,4 <f a>4
    | <ees g>2 <e g>4 c4
    | d4 <f a>4 a,4 <f a>4
    \mbreak

    | d4 <f a>4 a,4 <f a>4
    | d4 <f a>4 a,4 <f a>4
    | d4 c4 bes,4 a,4
    \mbreak

    | bes,4 <d f>4 b,4 <f aes>4
    | c4 <f a>4 d4 <fis c'>4
    | <f g bes>4 r4 <c bes>4 r4

    \alternative {
      \volta 1 { | <f a>4 e4 d4 c4 }
      \volta 2 { | <f a>4 c4 f,4-.-> r4 \bar "|." }
    }
  }

  | c4 <e g>4 g,4 <e g>4
  \mbreak

  | c4 <e g>4 g,4 <e g>4
  | f4 <a c'>4 c4 <bes c'>4
  | <f c'>4 f4 e4 d4
  \mbreak

  | c4 <e g>4 g,4 <e g>4
  | c4 <e g>4 g,4 <e g>4
  | f4 <a c'>4 c4 <bes c'>4
  \mbreak

  | r4 c'4 bes4 a4
  | <f bes d'>4 r4 <f bes d'>4 r4
  | <f bes des'>4 r4 <f bes des'>4 r4
  \mbreak

  | <f a c'>4 r4 <f a c'>4 r4
  | d'4 c'4 bes4 a4
  | g4 r4 d4 r4
  \mbreak

  | <c bes>4 r4 <e bes c'>4 r4
  | <f a c'>4 r4 <f aes des'>4 r4
  | <f a d'>2 f4 e4

  \fine
}

InstrumentA = \new Staff \with {midiInstrument = "acoustic grand"} { 
  \AHalf
}

InstrumentB = \new Staff \with {midiInstrument = "acoustic grand"} { 
  \BHalf
}

InstrumentCombined = {
  \new PianoStaff <<
    \InstrumentA
    \InstrumentB
  >>
}

\score {
  \InstrumentCombined
  \layout {
    % indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}
