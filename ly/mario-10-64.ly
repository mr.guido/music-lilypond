\version "2.24.2"

\header{
  title = "Super Mario 64"
  subtitle = "MAIN THEME"
  composer = "Composed by Koji Kondo"
  arranger = "Jazz Piano arrangement by Sakiko Masuda"
  source = "Super Mario Jazz Piano Arrangements"
}

% TODO need to go back and add chords like C7, F7, etc.

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% To just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = { \break }
pgbreak = { \pageBreak }

% measures 1 - 5
AI = { | <g' c''>4-- <g' a'>4-- <g' c''>4-- <aes' d''>8 <g' c''>8-> }
AII = { | r8 e'4.-> f'4-- fis'4-- }
AIII = { | <ees' f' g'>4-> r4 g''8 g'8 r8 g''8-> }
AIV = { | r2 r4 d''8 dis''8 }
AV = { | e''8 dis''8 e''8 g''8 r8 a''8 g''4-. \mbreak }
BI = { | <bes e'>4-- <a ees'>4-- <bes e'>4-- <b f'>8 <bes e'>8-> }
BII = { | r8 e4.-> f4-- fis4-- }
BIII = { | <g b>4-> r4 g8 g,8 r8 g8-> }
BIV = { | r2 g,,4-> r4 }
BV = { | <c g b>4 r4 r2 \mbreak }
CI = { | r1 }
CII = { | r1 }
CIII = { | r1 }
CIV = { | r1 }
CV = { | r1 }

% measures 6 - 10
AVI = { | c''4 r4 r4 g'8 gis'8 }
AVII = { | a'8 gis'8 a'8 c''8 r8 d''8 c''4-. }
AVIII = { | a'4 r4 r4 r8 aes'8 }
AIX = { | g'8-> fis'8 g'8 g''8-> r4 g'8 fis'8 }
AX = { | g'8 g''8-> r4 g'8-> fis'8 g'8 g''8-> }
BVI = { | r8 <g bes d' f'>8 r4 <g bes c' e'>2 }
BVII = { | <f a c' d'>4 r4 r2 }
BVIII = { | r8 <fis a c' e'>8 r4 <f aes c' ees'>2 }
BIX = { | <e b d'>4.-> <e b d'>8-> r2 }
BX = { | r8 <ees g des'>8-> r4 <ees g des'>4.-> <d f a c'>8-> \mbreak }
CVI = { | }
CVII = { | }
CVIII = { | }
CIX = { | }
CX = { | }

% measures 11 - 15
AXI = { | r4 r8 g''8->~ g''8 a''8 g''8 d''8-> }
AXII = { | r2 r4 d''8 dis''8 }
AXIII = { | e''8 dis''8 e''8 g''8 r8 a''8 g''4-. }
AXIV = { | bes''4 r4 r4 g''8 gis''8 }
AXV = { | a''8 gis''8 a''8 c'''8 r8 d'''8 c'''4-. \mbreak }
BXI = { | r4 r8 <d f a c'>8->~ <d f a c'>4. <g b f'>8-> }
BXII = { | r2 g,4-> r4 }
BXIII = { | <c g b>4 r4 r8 <b d' e'>8 r4 }
BXIV = { | <g bes d' f'>4. <g bes cis' e'>8 r2 }
BXV = { | r8 <f a c' d'>8 r4 r8 <f a c' d'>8 r4 \mbreak }
CXI = { | }
CXII = { | }
CXIII = { | }
CXIV = { | }
CXV = { | }

% measures 16 - 20
AXVI = { | a''4 r4 r4 r8 aes''8 }
AXVII = { | g''8-> fis''8 g''8 g'8-> r4 g''8 fis''8 }
AXVIII = { | g''8 g'8-> r4 <f' a' c'' e''>4-- <f' aes' b' e''>4-- }
AXIX = { | <e' g' c''>4-> r4 g''8 gis''8 a''8 c'''8-.-> }
AXX = { | r2 r4 d''8 dis''8 \mbreak \pgbreak }
BXVI = { | r8 <fis a c' d'>8 r4 <f aes c' d'>2 }
BXVII = { | <e g b d'>4.-> <e g b d'>8-> r2 }
BXVIII = { | r8 <ees g bes des'>8 r4 d4-- g4-- }
BXIX = { | <c a>4-> r4 r2 }
BXX = { | r8 g,8 r4 g,2 \mbreak \pgbreak }
CXVI = { | }
CXVII = { | }
CXVIII = { | }
CXIX = { | }
CXX = { | }

% measures 21 - 25
AXXI = { | <e' g' c''>4-> c'''4 <d'' f'' bes''>8 <dis'' fis'' b''>8 r8 <e'' g'' c'''>8~ }
AXXII = { | <e'' g'' c'''>2 e''8 f''8 g''8 gis''8 }
AXXIII = { | <c'' e'' a''>2. <c'' ees'' aes''>4 }
AXXIV = { | <b' d'' g''>2 r8 <bes' des'' ges''>4.-> }
AXXV = { | <a' c'' f''>4.-> <ces'' ees'' g''>8-> r4 <g' b' e''>4~-> \mbreak }
BXXI = { | <c a>4-> r4 bes,8 b,8 r8 c8~ }
BXXII = { | c2 g,2 }
BXXIII = { | f,4. f8 r2 }
BXXIV = { | e4. a,8 r8 ees4.-> }
BXXV = { | <d c'>4.-> <des ces'>8 r4 c4-> \mbreak }
CXXI = { | }
CXXII = { | }
CXXIII = { | }
CXXIV = { | }
CXXV = { | }

% measures 26 - 30
AXXVI = { | <g' b' e''>2 e''8 f''8 g''8 gis''8 }
AXXVII = { | <c'' e'' a''>2. <c'' ees'' aes''>4 }
AXXVIII = { | <b' d'' g''>2 r8 <bes' des'' ges''>4.-> }
AXXIX = { | <a' c'' f''>4.-> <aes' b' e''>8 r4 <e' g' c''>4->~ }
AXXX = { | <e' g' c''>1 \mbreak }
BXXVI = { | r4 g,4-- c,-> r4 }
BXXVII = { | f4. f,8 r2 }
BXXVIII = { | e4. a,8 r8 ees4.-> }
BXXIX = { | d4.-> g8-> r4 c4-> }
BXXX = { | r8 c,4.-> d,4-- e,4-- \mbreak }
CXXVI = { | }
CXXVII = { | }
CXXVIII = { | }
CXXIX = { | }
CXXX = { | }

% measures 31 - 34
AXXXI = { | <e' g' c''>1 }
AXXXII = { | c''8 e'8 r8 bes'8 r2 }
AXXXIII = { | c''8 e'8 r8 bes'8 r2 }
AXXXIV = { | a'8 c'8 r8 ees'8 r2 \mbreak }
BXXXI = { | r8 g,4.-> bes,4-- b,4-- }
BXXXII = { | c4 r4 r8 g,8 bes,8 b,8 }
BXXXIII = { | c4 r4 r8 c,8 ees,8 e,8 }
BXXXIV = { | f4 r4 r8 c,8 ees,8 e,8 \mbreak }
CXXXI = { | }
CXXXII = { | }
CXXXIII = { | }
CXXXIV = { | }

% measures 35 - 39
AXXXV = { | a'8 c'8 r8 ees'8 r2 }
AXXXVI = { | c''8 e'8 r8 bes'8 r2 }
AXXXVII = { | c''8 e'8 r8 bes'8 r2 }
AXXXVIII = { | <c' ees' a'>4 <c' ees' a'>4 <c' ees' a'>4 <c' ees' a'>8 <b ees' aes'>8-> }
AXXXIX = { | r8 fis''8 g''4 g'''2->\glissando \mbreak }
BXXXV = { | f,4 r4 r8 g,8 bes,8 b,8 }
BXXXVI = { | c4 r4 r8 g,8 bes,8 b,8 }
BXXXVII = { | c4 r4 r8 c,8 ees,8 e,8 }
BXXXVIII = { | f,4 r4 f,4. g,8-> }
BXXXIX = { | r1 \mbreak }
CXXXV = { | }
CXXXVI = { | }
CXXXVII = { | }
CXXXVIII = { | }
CXXXIX = { | }

% measures 40 - 43
AXL = { | <c' ees' a'>4-- <c' ees' a'>4-- <c' ees' aes'>4-- <c' ees' aes'>4-- }
AXLI = { | <b d' g'>4-- <b d' g'>4-- <bes des' ges'>4-- <bes des' ges'>4-- }
AXLII = { | <a c' f'>4-> r4 e'8 dis'8 e'8 c'8-> }
AXLIII = { | r2 <c'' c'''>4-> r4 \bar "|." } % TODO add gva ---|
BXL = { | f,4-- r4 f,4-> r4 }
BXLI = { | e,4-> r4 ees,4-> r4 }
BXLII = { | d,4-> r4 g,4. c,8-> }
BXLIII = { r4 g,4-.-> c,4-> r4 \bar "|." } % TODO add gva, bassa ----|
CXL = { | }
CXLI = { | }
CXLII = { | }
CXLIII = { | }

AHalf = {
  \tempo 4 = 220
  \clef treble
  \time 4/4
  \override Glissando.style = #'zigzag
  
  \AI \AII \AIII \AIV

  \repeat volta 2 {
    \AV
    \AVI \AVII \AVIII \AIX \AX
    \AXI \AXII \AXIII \AXIV \AXV
    \AXVI \AXVII \AXVIII

    \alternative {
      \volta 1 { \AXIX \AXX }
      \volta 2 { \AXXI }
    }
  }

  \AXXII 

  \repeat volta 2 {
    \AXXIII \AXXIV \AXXV
    \AXXVI \AXXVII \AXXVIII \AXXIX
    \alternative {
      \volta 1 { \AXXX }
      \volta 2 { \AXXXI }
    }
  }
  
  \repeat volta 2 {
    \AXXXII \AXXXIII \AXXXIV
    \AXXXV \AXXXVI \AXXXVII
    \alternative {
      \volta 1 { \AXXXVIII \AXXXIX }
      \volta 2 { \AXL \AXLI \AXLII \AXLIII }
    }
  }
}

BHalf = {
  \clef bass
  \time 4/4

  \BI \BII \BIII \BIV

  \repeat volta 2 {
    \BV
    \BVI \BVII \BVIII \BIX \BX
    \BXI \BXII \BXIII \BXIV \BXV
    \BXVI \BXVII \BXVIII

    \alternative {
      \volta 1 { \BXIX \BXX }
      \volta 2 { \BXXI }
    }
  }

  \BXXII 

  \repeat volta 2 {
    \BXXIII \BXXIV \BXXV
    \BXXVI \BXXVII \BXXVIII \BXXIX
    \alternative {
      \volta 1 { \BXXX }
      \volta 2 { \BXXXI }
    }
  }
  
  \repeat volta 2 {
    \BXXXII \BXXXIII \BXXXIV
    \BXXXV \BXXXVI \BXXXVII
    \alternative {
      \volta 1 { \BXXXVIII \BXXXIX }
      \volta 2 { \BXL \BXLI \BXLII \BXLIII }
    }
  }
}

Chords = {
  \chords \with {midiInstrument = "acoustic grand"} { 
    \set noChordSymbol = "(N.C.)"
  
    % | c4:7 f4:7 c4:7 r4 
    % | r1 
    % | g4:7 r4 r2 
    % | r1

    % \repeat volta 2 {
    %   | c4:maj7 r4 r2 
    %   | g4:m7 r4 c4:7 r4 
    %   | f4:6 r4 r2 
    %   | fis4:m7 r4 f4:m7 r4 
    %   | e:m7 r4 r2 
    %   | r8 ees8:7 r4 r4 r8 d8:m7
    %   | r2 r4 r8 g8:7
    %   | r1
    %   | c4:maj7 r4 r2
    %   | g4:m7 r4 g4:dim
    %   | f4:6 r4 r2
      
      % \alternative {
      %   \volta 1 { \CXIX \CXX }
      %   \volta 2 { \CXXI }
      % }
    % }

    % \CXXII 

    % \repeat volta 2 {
    %   \CXXIII \CXXIV \CXXV
    %   \CXXVI \CXXVII \CXXVIII \CXXIX
    %   \alternative {
    %     \volta 1 { \CXXX }
    %     \volta 2 { \CXXXI }
    %   }
    % }
    
    % \repeat volta 2 {
    %   \CXXXII \CXXXIII \CXXXIV
    %   \CXXXV \CXXXVI \CXXXVII
    %   \alternative {
    %     \volta 1 { \CXXXVIII \CXXXIX }
    %     \volta 2 { \CXL \CXLI \CXLII \CXLIII }
    %   }
    % }
  }
}

InstrumentAHalf = \new Staff \with {midiInstrument = "acoustic grand"} { 
  \AHalf
}

InstrumentBHalf = \new Staff \with {midiInstrument = "acoustic grand"} { 
  \BHalf
}

InstrumentCombined = {
  \new PianoStaff <<
    \Chords
    \InstrumentAHalf
    \InstrumentBHalf
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}
