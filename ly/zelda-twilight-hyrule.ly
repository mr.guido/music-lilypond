\version "2.24.2"

\header{
  title = "The Legend of Zelda: Twilight Princess"
  subtitle = "Hyrule Field Main Theme"
  composer = "Composed by TORU MINEGISHI"
  arranger = "Piano arrangement by SHINOBU AMAYAKE"
  source = "Kindle e-book"
}

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = { }
pgbreak = { }

% Voices
Voices_BII = {
  <<
  { \voiceOne
    | r4 <g b e'>4 <a cis' e'>2
  }
  \new Voice
  { \voiceTwo
    | e,2->( e,4.) e,8->
  }
  >>
  \oneVoice
}
Voices_BIII = {
  <<
  { \voiceOne
    | r4 <b d' g'>4 <a cis' fis'>2
  }
  \new Voice
  { \voiceTwo
    | e,1
  }
  >>
  \oneVoice
}
Voices_BXXVIII = {
  <<
  { \voiceOne
    | <d g b>1
  }
  \new Voice
  { \voiceTwo
    | b,8\sustainOn b,4 b,8~ b,4. b,8\sustainOff
  }
  >>
  \oneVoice
}
Voices_BXXIX_BXXXIV = {
  <<
  { \voiceOne
    | <e a cis'>1 | <fis a>1~ | fis1
    | <d g b>1 | <e a cis'>1 | <fis a>1
  }
  \new Voice
  { \voiceTwo
    |cis8 cis4 cis8~ cis2 | d8 d4 d8~ d8 d4. | d8 d4 d8~ d8 d4 d8
    | b,8 b,4 b,8~ b,4. b,8 | cis8 cis4 cis8~ cis8 cis4. | d8 d4 d8~ d8 d4 d8
  }
  >>
  \oneVoice
}

% measures 1 - 4
AI = { | s2 s4 s8 r8  }
AII = { | r4 e''16 fis''16 g''16 a''16 <cis'' fis'' b''>2 }
AIII = { | r4 e''16 fis''16 g''16 a''16 <cis'' fis'' b''>2 }
AIV = { | r8 <g b d'>8[\mf r8 <g b d'>8] r8 <g b d'>8[ r8 <g b d'>8] \mbreak }
BI = { | s2 s4 s8 e,16-> e,16 }
BII = \Voices_BII
BIII = \Voices_BIII
BIV = { | e,8\mf e,8 r4 e,8 e,8 r4 \mbreak }

% measures 5 - 7
AV = { | <g b d'>8[ <g b d'>8 r8 <g b d'>8] r8 <g b d'>8[ r8 <g b d'>8] }
AVI = { | b4-. g'4( fis'4-.) d'4 }
AVII = { | cis'4. b16 a16 cis'2 \mbreak }
BV = { | e,8 e,8 r4 e,8 e,8 r4 }
BVI = { | e,8[\sustainOn <e g>8\sustainOff r8 <e g>8] e,8[\sustainOn <e g>8\sustainOff r8 <e g>8] }
BVII = { | e,8[\sustainOn <e a>8\sustainOff r8 <e a>8] e,8[\sustainOn <e a>8\sustainOff r8 <e a>8] \mbreak }

% measures 8 - 10
AVIII = { | b4-. g'4( fis'4-.) d'4 }
AIX = { | <cis' e'>4. fis'16 g'16 <cis' e' a'>2 }
AX = { | <e' g' b'>1 \mbreak }
BVIII = { | e,8[\sustainOn <e g>8\sustainOff r8 <e g>8] e,8[\sustainOn <e g>8\sustainOff r8 <e g>8] }
BIX = { | e,8[\sustainOn <e a>8\sustainOff r8 <e a>8] e,8[\sustainOn <e a>8\sustainOff r8 <e a>8] }
BX = { | e,8[\sustainOn <e g>8\sustainOff r8 <e g>8] e,8[\sustainOn <e g>8\sustainOff r8 <e g>8] \mbreak }

% measures 11 - 13
AXI = { | f'4 c''4( b'4-.) g'4 }
AXII = { | <d' f' a'>2.( <d' f' a'>8) g'16 a'16 }
AXIII = { | <b dis' fis'>4 b''16( a''16 fis''16 dis''16) a''16( fis''16 dis''16 b'16) fis''16( dis''16 b'8) \mbreak \pgbreak }
BXI = { | f,8[\sustainOn <f a>8\sustainOff r8 <f a>8] f,8[\sustainOn <f a>8\sustainOff r8 <f a>8] }
BXII = { | bes,8[\sustainOn <f bes>8\sustainOff r8 <f bes>8] bes,8[\sustainOn <f bes>8\sustainOff r8 <f bes>8] }
BXIII = { | b,8[\sustainOn <dis fis>8\sustainOff r8 <dis fis>8] b,8[\sustainOn <dis fis>8\sustainOff r8 <dis fis>8] \mbreak \pgbreak }

% measures 14 - 16
AXIV = { | <g b>4-. <e' g'>4( <d' fis'>4)-. <b d'>4 }
AXV = { | <a cis'>4. b16 a16 <a cis'>2 }
AXVI = { | <g b>4-. <e' g'>4( <d' fis'>4-.) <b d'>4 \mbreak }
BXIV = { | e,8[\sustainOn <e g>8\sustainOff r8 <e g>8] e,8[\sustainOn <e g>8\sustainOff r8 <e g>8] }
BXV = { | e,8[\sustainOn <e g>8\sustainOff r8 <e g>8] e,8[\sustainOn <e g>8\sustainOff r8 <e g>8] }
BXVI = { | e,8[\sustainOn <e g>8\sustainOff r8 <e g>8] e,8[\sustainOn <e g>8\sustainOff r8 <e g>8] \mbreak }

% measures 17 - 19
AXVII = { | <cis' e'>4. fis'16 g'16 <cis' fis' a'>4. b'16\< cis''16 }
AXVIII = { | <fis' b' d''>2.\!\f <b' d'' fis''>4 }
AXIX = { | <a' cis'' e''>4-. <cis' e' a'>2. \mbreak }
BXVII = { | e,8[\sustainOn <e a>8\sustainOff r8 <e a>8] e,8[\sustainOn <e a>8\sustainOff r8 <e a>8] }
BXVIII = { | b,8[-> <fis b d'>8 r8 <fis b d'>8] r8 <fis b d'>8[ r8 b,8] }
BXIX = { | a,8[-> <e a cis'>8 r8 <e a>8] r8 <e a>8[ r8 <e a>8] \mbreak }

% measures 20 - 22
AXX = { | <b d' g'>2.( <g' b' d''>4 }
AXXI = { | <fis' a' cis''>4-.) <a cis' fis'>2. }
AXXII = { | g'8-. g'16( a'16 <d' b'>2.) \mbreak }
BXX = { | g,8[-> <d g>8 r8 <d g>8] r8 <d g>8[ r8 g,8] }
BXXI = { | fis,8[-> <cis fis a>8 r8 <cis fis>8] r8 <cis fis>8[ r8 <cis fis>8] }
BXXII = { | g,8[-> <g b>8 r8 <g b>8] r8 <g b>8[ r8 <g b>8] \mbreak }

% measures 23 - 25
AXXIII = { | <gis' b'>8-. <gis' b'>16( <a' cis''>16 <b' d''>2.) }
AXXIV = { | a'8-. a'16( b'16 <e' cis''>2.) }
AXXV = { | <ais' cis''>8-. <ais' cis''>16( <b' d''>16 <cis'' e''>2) r8 b'16 <ais' cis''>16 \mbreak }
BXXIII = { | gis,8->[ <f b>8 r8 <f b>8] r8 <f b>8[ r8 <f b>8] }
BXXIV = { | a,8->[ <a cis'>8 r8 <a cis'>8] r8 <a cis'>8[ r8 <a cis'>8] }
BXXV = { | ais,8->[ <g cis'>8 r8 <g cis'>8] r8 <g cis'>8[ r8 <g cis'>8] \mbreak }

% measures 26 - 28
AXXVI = { | <b' d''>1( }
AXXVII = { | <b' d''>1)  }
AXXVIII = { | d'2 e'4 fis'4 \mbreak \pgbreak }
BXXVI = { | <b, fis b>8->[ <fis b d'>8 r8 <fis b d'>8] r8 <fis b d'>8[ r8 <fis b d'>8] }
BXXVII = { | b,8->[ <fis b d'>8 r8 <fis b d'>8] \tuplet 3/2 {fis,4-> g,4-> a,4->} }
BXXVIII = \Voices_BXXVIII

% measures 29 - 31
AXXIX = { | a'2~ \tuplet 3/2 {a'4 e'4 d'4} }
AXXX = { | cis'2.~ cis'8 c'16 b16 }
AXXXI = { | a2. r4 \mbreak }
% BXXIX = { | r1  }
% BXXX = { | r1  }
% BXXXI = { | r1 \mbreak }

% measures 32 - 34
AXXXII = { | d'2 e'4 fis'4 }
AXXXIII = { | a'2. cis''4 }
AXXXIV = { | cis''2.~ cis''8 b'16 cis''16 \mbreak }
% BXXXII = { | r1  }
% BXXXIII = { | r1  }
% BXXXIV = { | r1 \mbreak }

% measures 35 - 37
AXXXV = { | d''1 }
AXXXVI = { | b4-.\mf <b g'>4( <b fis'>4) <b d'>4 }
AXXXVII = { | cis'2. r4 \mbreak }
BXXXV = {
  <<
  {
    | <d fis>1
  }
  \\
  {
    | \stemUp a2 a8 a8 r8 a8
  }
  \\
  {
    | \stemDown b,8\sustainOn b,8\sustainOff r4 b,8\sustainOn b,8\sustainOff r4
  }
  >>
  \oneVoice
}
BXXXVI = { | <g, b, d g>8[\sustainOn <b, d g>8\sustainOff r8 <b, d g>8] r8 <g, b, d g>8[ r8 <b, d g>8]  }
BXXXVII = { | <a, cis e a>8[\sustainOn <cis e a>8\sustainOff r8 <cis e a>8] r8 <a, cis e a>8[ r8 <cis e a>8] \mbreak }

% measures 38 - 40
AXXXVIII = { | d'4-. <d' bes'>4( <d' a'>4-.) f'4 }
AXXXIX = { | e'2 c'16 d'16 e'16 f'16 g'16 a'16 bes'16 c''16 }
AXL = { | d''4-.\f a'2 d''16 e''16 f''16 g''16 \mbreak }
BXXXVIII = { | <bes, d f bes>8[\sustainOn <d f bes>8\sustainOff r8 <d f bes>8] r8 <bes, d f bes>8[ r8 <d f bes>8] }
BXXXIX = { | <c e g c'>8[\sustainOn <e g c'>8\sustainOff r8 <e g c'>8] r8 <c e g c'>8[ r8 <e g c'>8] }
BXL = { | <d g a d'>8[\sustainOn <d g a d'>8\sustainOff r8 <g a d'>8] d8[ <d g a d'>8 r8 <g a d'>8] \mbreak }

% measures 41 - 43
AXLI = { | a''2. r4 }
AXLII = { | d''4-. a'2 d''16 e''16 f''16 g''16 }
AXLIII = { | <f'' a''>2. r8 e'16->( eis'16 \mbreak \pgbreak }
BXLI = { | <d g a d'>8[\sustainOn <d g a d'>8\sustainOff r8 <g a d'>8] d8[ <d g a d'>8 r8 <g a d'>8] }
BXLII = { | <d f a d'>8[\sustainOn <d f a d'>8\sustainOff r8 <f a d'>8] d8[ <d f a d'>8 r8 <f a d'>8] }
BXLIII = { | <d f a d'>8[\sustainOn <d f a d'>8\sustainOff r8 <f a d'>8] d8[ <f a d'>8 r8 <a, e g>8->]( \mbreak \pgbreak }

% measures 44 - 46
AXLIV = { | <d' fis'>8) e'8-.  d'8-. e'8-. fis'8-. gis'8-. a'4 }
AXLV = { | gis'8-. fis'8-. e'8-. fis'8-. gis'8-. a'8-. b'4 }
AXLVI = { | fis'8-. e'8-. d'8-. e'8-. fis'8-. gis'8-. a'8-. b'8-. \mbreak }
BXLIV = { | <b, d fis>8)[\sustainOn <d fis>8\sustainOff r8 <d fis>8] b,8[ <d fis>8 r8 <fis, d fis>8] }
BXLV = { | <b, e gis>8[\sustainOn <e gis>8\sustainOff r8 <e gis>8] b,8[ <e gis>8 r8 <e gis>8] }
BXLVI = { | <b, d fis>8[\sustainOn <d fis>8\sustainOff r8 <d fis>8] b,8[ <d fis>8 r8 <fis, d fis>8] \mbreak }

% measures 47 - 49
AXLVII = { | gis'16 a'16 gis'8~ gis'2. }
AXLVIII = { | b'8-. a'8-. g'8-. a'8-. b'8-. cis''8-. d''4 }
AXLIX = { | cis''8-. b'8-. a'8-. b'8-. cis''8-. d''8-. e''4 \mbreak }
BXLVII = { | <b, e gis>8[\sustainOn <e gis>8\sustainOff r8 <e gis>8] b,8[ <e gis>8 r8 <b, e gis>8] }
BXLVIII = { | <e g b>8[\sustainOn <g b>8\sustainOff r8 <g b>8] e8[ <g b>8 r8 <b, g b>8] }
BXLIX = { | <e fis a>8[\sustainOn <fis a>8\sustainOff r8 <fis a>8] e8[ <fis a>8 r8 <fis a>8] \mbreak }

% measures 50 - 52
AL = { | b'8-. a'8-. g'8-. a'8-. b'8-. cis''8-. d''8-. e''8-. }
ALI = { | cis''16 d''16 cis''8~ cis''2. }
ALII = { | <fis' a'>8-. <e' g'>8-. <d' fis'>8-. <e' g'>8-. <fis' a'>8-. <g' b'>8-. <a' cis''>4~ \mbreak }
BL = { | <e g b>8[\sustainOn <g b>8\sustainOff r8 <g b>8] e8[ <g b>8 r8 <b, g b>8] }
BLI = { | <e a cis'>8[\sustainOn <a cis'>8\sustainOff r8 <a cis'>8] e8[ <a cis'>8 r8 <a cis'>8] }
BLII = { | g,8[\sustainOn <d g b>8\sustainOff r8 <d g b>8] g,8[ <d g b>8 r8 <d g b>8] \mbreak }

% measures 53 - 56
ALIII = { | <a' cis''>1 }
ALIV = { | <fis' a'>8-. <e' g'>8-. <d' fis'>8-. <e' g'>8-. <fis' a'>8-. <g' b'>8-. <a' cis''>4~ }
ALV = { | <a' cis''>1 }
ALVI = { | <f' a'>8-. <e' g'>8-. <d' f'>8-. <e' g'>8-. <f' a'>8-. <g' bes'>8-. <a' c''>4~ \mbreak }
BLIII = {
  <<
  { \voiceOne
    | <a cis'>4 <fis' a'>4 <e' gis'>4 <cis' e'>4
  }
  \new Voice
  { \voiceTwo
    | d8 d8 r4 r8 d8 a8 a8
  }
  >>
  \oneVoice
}
BLIV = { | g,8[ <d g b>8 r8 <d g b>8] g,8[ <d g b>8 r8 <d g b>8]  }
BLV = {
  <<
  { \voiceOne
    | <a cis'>4 <fis' a'>4 <e' gis'>4 <cis' e'>4
  }
  \new Voice
  { \voiceTwo
    | d8 d8 r4 r8 d8 a8 a8
  }
  >>
  \oneVoice
}
BLVI = { | <bes, d f bes>8[ <bes, d f bes>8 r8 <d f bes>8] bes,8[ <bes, d f bes>8 r8 <d f bes>8] \mbreak }

% measures 57 - 59
ALVII = {
  <<
  { \voiceOne
    | <a' c''>1
  }
  \new Voice
  { \voiceTwo
    | <c' e'>4 <a' c''>4 <g' b'>4 <e' g'>4
  }
  >>
  \oneVoice
}
ALVIII = { | fis'2.~ fis'8 e'16 d'16 }
ALIX = { | cis'1 \mbreak \pgbreak }
BLVII = { | f,8[ <f a>8 r8 <f a>8] r8 f,8 c8-> c8-> }
BLVIII = {
  <<
  { \voiceOne
    | <g b d'>8[ <g b d'>8 r8 <g b d'>8] r8 <g b d'>8[ r8 b8]
  }
  \new Voice
  { \voiceTwo
    | e,4 r4 e,8[ b,8 r8 b,8]
  }
  >>
  \oneVoice
}
BLIX = {
  <<
  { \voiceOne
    | <e a>8[ <e a>8 r8 <e a>8] r8 <e a>8[ r8 <e a>8]
  }
  \new Voice
  { \voiceTwo
    | <a, cis>8 <a, cis>8 r4 a,8[ a,8 r8 a,8]
  }
  >>
  \oneVoice
}


APart = {
  \tempo 4 = 144
  \clef treble
  \time 4/4
  \key g \major

  % Causes \mark \default to have a square box arround it, lettered. Ex:
  % [A] [B] [C] [D] [E] [F] [G]
  \set Score.rehearsalMarkFormatter = #format-mark-box-alphabet

  \AI \AII \AIII \AIV 
  \AV \mark \default \AVI \AVII 
  \AVIII \AIX \AX 
  \AXI \AXII \AXIII 

  \mark \default \AXIV \AXV \AXVI 
  \AXVII \AXVIII \AXIX 
  \AXX \AXXI \mark \default \AXXII 
  \AXXIII \AXXIV \AXXV 
  \AXXVI \AXXVII \mark \default \AXXVIII 

  \AXXIX \AXXX \AXXXI 
  \AXXXII \AXXXIII \AXXXIV 
  \AXXXV \mark \default \AXXXVI \AXXXVII 
  \AXXXVIII \AXXXIX \AXL 
  \AXLI \AXLII \AXLIII 

  \mark \default \AXLIV \AXLV \AXLVI 
  \AXLVII \AXLVIII \AXLIX 
  \AL \ALI \mark \default \ALII 
  \ALIII \ALIV \ALV \ALVI 
  \ALVII \ALVIII \ALIX 
}

BPart = {
  \tempo 4 = 144
  \clef bass
  \time 4/4
  \key g \major

  \BI \BII \BIII \BIV 
  \BV \BVI \BVII 
  \BVIII \BIX \BX 
  \BXI \BXII \BXIII 
  \BXIV \BXV \BXVI 
  \BXVII \BXVIII \BXIX 
  \BXX \BXXI \BXXII 
  \BXXIII \BXXIV \BXXV 
  \BXXVI \BXXVII \BXXVIII 
  \Voices_BXXIX_BXXXIV
  \BXXXV \BXXXVI \BXXXVII 
  \BXXXVIII \BXXXIX \BXL 
  \BXLI \BXLII \BXLIII 
  \BXLIV \BXLV \BXLVI 
  \BXLVII \BXLVIII \BXLIX 
  \BL \BLI \BLII 
  \BLIII \BLIV \BLV \BLVI 
  \BLVII \BLVIII \BLIX 

}

InstrumentAPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \APart
}

InstrumentBPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \BPart
}

InstrumentCombined = {
  \new PianoStaff <<
    \InstrumentAPart
    \InstrumentBPart
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}

