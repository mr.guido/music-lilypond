\version "2.24.2"

\header{
  title = "O Come, All Ye Faithful"
  poet = "Below = left hand; above = right hand.\nCircles = draw; otherwise push."
}

LeftHand = {\set fingeringOrientations = #'(down) \set stringNumberOrientations = #'(down)}
RightHand = {\set fingeringOrientations = #'(up) \set stringNumberOrientations = #'(up)}


\score {
  \new PianoStaff <<
    <<
      \new Staff \with {midiInstrument = "acoustic grand"} {
        \time 4/4
        \RightHand
        s2. <c''-1>4
        <c''-1>2 \LeftHand <g'-1>4 \RightHand <c''-1>4
        <d''\2>2 \LeftHand <g'-1>2
        \break
        \RightHand
        <e''-2>4 <d''\2>4 <e''-2>4 <f''\3>4
        <e''-2>2 <d''\2>4 <c''-1>4
        <c''-1>2 <b'\1>4 \LeftHand <a'\1>4
        \break
        \RightHand <b'\1>4( <c''-1>4) <d''\2>4 <e''-2>4
        <b'\1>2( <a'\1>4.) <g'-1>8
        <g'-1>2. r4
        \break
        <g''-3>2 <f''\3>4 <e''-2>4
        <f''\3>2 <e''-2>2
        <d''\2>4 <e''-2>4 <c''-1>4 <d''\2>4
        \break
        <b'\1>2 \LeftHand <g'-1>4 \RightHand <c''-1>4
        <c''-1>4 <b'\1>4 <c''-1>4 <d''\2>4
        <c''-1>2 \LeftHand <g'-1>4 \RightHand <e''-2>4
        \break
        <e''-2>4 <d''\2>4 <e''-2>4 <f''\3>4
        <e''-2>2 <d''\2>4 <e''-2>4
        <f''\3>4 <e''-2>4 <d''\2>4 <c''-1>4
        \break
        <b'\1>2 <c''-1>4( <f''\3>4)
        <e''-2>2( <d''\2>4.) <c''-1>8
        <c''-1>2. r4
      }
      \addlyrics {
        Oh, come, all ye faith- ful, Joy- ful and tri- um- phant!
        Oh, come, ye, oh come ye to Beth- le- hem.
        Come and be- hold him, Born the King of an- gels;
        Oh, come, let us a- dore him;
        Oh, come, let us a- dore him;
        Oh, come, let us a- dore him, Christ, the Lord.
      }
    >>
  >>
  \layout {
    indent = 0.0
  }
  \midi {
    \tempo 4 = 130
  }
}
