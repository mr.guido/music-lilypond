\version "2.24.2"

% The long notes and rests feel too long.

\header{
  title = "Scarborough Fair"
}

\score {
  <<
    \new Staff = "staff" \with {midiInstrument = "harmonica"} {
      \new Voice = "melody" {
        { \time 3/4
          d'2 d'4
          a'8 a'4. a'4
          e'4. f'8 e'4
          d'2.~ d'2.
          \break
          r4 a'4 c''4
          d''2 c''4
          a'4 b'4 g'4
          a'2.~ a'2.
          \break
          r2.
          r4 r4 d''4
          d''2 d''4
          c''2 a'4
          a'4 g'4 f'4
          \break
          {e'4 (c'2~ c'2.)}
          d'2 a'4
          g'2 f'4
          \break
          e'4 d'4 c'4
          d'2.~ d'2.
          r2.
          r2.
        }
      }
    }

    \new Lyrics {
      \lyricsto "melody" {
        Are you go- ing to Scar- bo- rough Fair?
        Pars- ley, sage, rose- mar- y and thyme.
        Re- mem- ber me to one who lives there.
        She once was a true love of mine.
      }
    }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 130
  }
}
