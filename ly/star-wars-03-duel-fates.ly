\version "2.20.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Star Wars"
  subtitle = "Duel of the Fates"
  composer = "Music by John Williams"
  arranger = "Arranged by Dan Coates"
}

\score {
  \new Staff \with {midiInstrument = "acoustic guitar (nylon)"} <<
    { \clef treble 
      \time 4/4
      <e' g' b'>8\f <e' g' b'>4.~ <e' g' b'>2
      <f' a' b'>8 <f' a' b'>4.~ <f' a' b'>2
      <g' b' e''>8 <g' b' e''>4. <g' b' e''>2
      <a' c'' f''>8 <g' c'' ees''>8 <g' c'' ees''>2.
      \break % 5
      r4\mp {e'8 (f'8 g'4 a'4
      b4 a'4 g'8 f'8 e'4)}
      r4 {e'8 (f'8 g'4 a'4
      \break % 8
      b4 a'4 g'8 f'8 e'4)}
      r4 {dis'8\< (e'8 f'4 g'4
      a4 b'4 a'8 g'8 f'4\!)}
      \break % 11
      r4 {dis'8 (e'8 f'4 g'4
      a4 g'4 f'8 e'8 dis'4~
      dis'1~
      dis'1)}
      \break %15

      {a4 (a'4 g'8 f'8 e'4~
      e'1~
      e'1)}
      \break % 18
      <g' b'>8\mf <g' b'>4.~ <g' b'>2~
      <g' b'>1
      <g' b'>8 <f' a'>8 <f' a'>2.~
      \break % 21
      <f' a'>1
      <g' b'>4 <g' b'>2.~
      <g' b'>1
      \break % 24
      <g' b'>4
      <f' a'>2.~
      <f' a'>1
      r4\f <e'' g'' b''>4 r2
      \break % 27

      r2 <a'' c'''>8 <fis'' a''>8 r4
      r4 <e'' g'' b''>4 r2
      r4 r8 <g'' b''>8 <a'' c'''>8 <fis'' a''>8 r4
      \break % 30
      <g' b' e''>4\f <g' b' e''>2.~
      <g' b' e''>1
      <a' fis''>4 <c'' ees''>2.~
      \break % 33
      <c'' ees''>1
      <g' b' e''>8 <g' b' e''>8 <g' b' e''>2.~
      <g' b' e''>1
      \break % 36
      <a' fis''>4 <c'' ees''>2.~
      <c'' ees''>1
      <b' e'' g''>4 <b' e'' g''>2.
      \break % 39

      <b' e'' g''>1
      <c'' ees'' g''>4 <b' e'' g''>2.~
      <b' e'' g''>1
      \break % 42
      <c'' e'' a''>4 <a' c'' fis''>2.
      <g' b'>8\mf <g' b'>8 r4 <g' b'>8 <g' b'>8 r4
      <g' b'>8 <g' b'>8 r4 <a' b'>8 <f' b'>8 <f' b'>4
      \break % 45
      <g' b'>8 <g' b'>8 r4 <g' b'>8 <g' b'>8 r4
      <g' b'>8 <g' b'>8 r4 <a' b'>8 <f' b'>8 <f' b'>4
      <g' bes' d''>4\ff <g' bes' d''>2.~
      \break % 48
      <g' bes' d''>1
      <g' c'' d''>4 <g' a' d''>2.~
      <g' a' d''>1
      \break % 51

      <bes' d'' g''>4 <bes' d'' g''>2.~
      <bes' d'' g''>1
      <c'' f'' g''>8 <c'' ees'' g''>8 <c'' ees'' g''>2.~
      \break % 54
      <c'' ees'' g''>1
      \clef bass r4\mp {g8 (a8 b4 c'4
      d4 c'4 b8 a8 g4)}
      r4 {g8 (a8 b4 c'4
      \break % 58
      c4 d'4 c'8 b8 a4)}
      \clef treble
      <g' b' d''>4\p <g' b' d''>2.~
      <g' b' d''>1
      \break % 61
      <g' c'' d''>4 <g' a' d''>2.~
      <g' a' d''>1
      <bes' d'' g''>4\mf <bes' d'' g''>2.~
      \break % 64

      <bes' d'' g''>1
      <c'' ees'' a''>8 <c'' ees'' ges''>8 <c'' ees'' ges''>2.~
      <c'' ees'' ges''>1
      \break % 67
      <bes' g''>8\ff <bes' g''>8 r4 <bes' g''>8 <bes' g''>8 r4
      <bes' g''>8 <bes' g''>8 r4 <c'' a''>8 <c'' fis''>8 <c'' fis''>4
      <bes' g''>8 <bes' g''>8 r4 <bes' g''>8 <bes' g''>8 r4
      \break % 70
      <bes' g''>8 <bes' g''>8 r4 <c'' a''>8 <c'' fis''>8 <c'' fis''>4
      <bes' d'' g''>8 <bes' d'' g''>4.~ <bes' d'' g''>2
      bes'8\staccato\mf c''8\staccato {bes'16 (a'16 g'8)} b''8\staccato c'''8\staccato {b''16 (a''16 g''8)}
      \break % 73
      c'''16\f b''16 a''8 r4 r2
      r4 <ges'' b''>8\staccato <ges'' c'''>8\staccato <ges'' b''>8\staccato <ges'' c'''>8\staccato <ges'' b''>8\staccato <ges'' c'''>8\staccato
      r2 r4 <ges'' b''>8\staccato\ff <ges'' c'''>8\staccato
      b''16 a''16 g''8 r4 r2
    }
    
    \new Staff \with {midiInstrument = "acoustic guitar (nylon)"} 
    { \clef bass 
      g8 g4.~ g2
      a8 f4.~ f2
      b8 b4.~ b2
      a8 c'8 c'2.
      \break
      <e, b,>8\staccato <e, b,>8\staccato r4 r2
      r1
      <e, b,>8\staccato <e, b,>8\staccato r4 r2
      \break
      r1
      <e, b,>8\staccato <e, b,>8\staccato r4 r2
      r1
      \break
      <e, b,>8\staccato <e, b,>8\staccato r4 r2
      r1
      <e, b,>8\staccato <e, b,>8\staccato r4 <e, b,>8\staccato <e, b,>8\staccato r4
      <e, b,>8\staccato <e, b,>8\staccato r4 <e, b,>8\staccato <e, b,>8\staccato r4
      \break
      r1
      <e, b,>8\staccato <e, b,>8\staccato r4 <e, b,>8\staccato <e, b,>8\staccato r4
      <e, b,>8\staccato <e, b,>8\staccato r4 <e, b,>8\staccato <e, b,>8\staccato r4
      \break
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      \break
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      \break
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      \break

      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      \break
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      c,8\staccato d,8\staccato c,8\staccato g,8\staccato c,8\staccato d,8\staccato c,8\staccato g,8\staccato
      \break
      c,8\staccato d,8\staccato c,8\staccato g,8\staccato c,8\staccato d,8\staccato c,8\staccato g,8\staccato
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      \break
      c,8\staccato d,8\staccato c,8\staccato g,8\staccato c,8\staccato d,8\staccato c,8\staccato g,8\staccato
      c,8\staccato d,8\staccato c,8\staccato g,8\staccato c,8\staccato d,8\staccato c,8\staccato g,8\staccato
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      \break

      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      \break
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      \break
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      e,8\staccato f,8\staccato e,8\staccato g,8\staccato e,8\staccato f,8\staccato e,8\staccato g,8\staccato
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      \break
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      \break

      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      \break
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      g,8\staccato g,8\staccato r4 r2
      r1
      {c,2 (d,2
      \break
      e,2 f,4 fis,4)}
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      \break
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      \break

      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      \break
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      \break
      g,8\staccato a,8\staccato g,8\staccato bes,8\staccato g,8\staccato a,8\staccato g,8\staccato bes,8\staccato
      g,8 d8 b8 a8 b8\staccato c'8\staccato {b16 (a16 g8~
      g1)}
      \break
      fis16 g16 a8 r4 r2
      r4 {bes,4 (e4 ges4)}
      r2 r4 ees8\staccato ees8\staccato
      <g, d>8 <g, d>8 r4 r2
    }
  >>
  \layout {

  }
  \midi {
    \tempo 4 = 152
  }
}
