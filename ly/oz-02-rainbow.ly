\version "2.24.2"

% File created using this command:
% python src/generate_lilypond.py A,B 1 4,3,3,3.3,3,3,3,3.4,3,3,3,4.

\header{
  title = "Over the Rainbow"
  subtitle = "From: The Wizard of Oz"
  composer = "Music by Harold Arlen"
  arranger = "Arranged by Dan Coates"
  source = "online"
}

% Breaks as in the score I'm transcribing from, but when I'm done I'll set this
% to just the empty curly braces {} so lilypond can figure out the breaks.
mbreak = {  }
pgbreak = {  }

% Voices
Voices_BI_BIII = {
  <<
  { \voiceOne
    | r4\p f'4( g'4 f'4)
    | r4 f'4( g'4 f'4)
    | r4 f'4( g'4 d'4)
  }
  \new Voice
  { \voiceTwo
    | bes1\p
    | bes1
    | bes1
  }
  >>
  \oneVoice
}
Voices_AVIII_AX = {
  <<
  { \voiceOne
    | f'1
    | g2 ees'2
    | <bes d'>4 bes8 c'8 d'4 ees'4 \mbreak
  }
  \new Voice
  { \voiceTwo
    | c'4( bes4 a4 aes4)
    | s2 c'4 bes4
    | s2 <aes b>2 \mbreak
  }
  >>
  \oneVoice
}
Voices_AXVI_AXVIII = {
  <<
  { \voiceOne
    | f''1 \mbreak
    | g'2 ees''2
    | s2 d''4 ees''4
  }
  \new Voice
  { \voiceTwo
    | c''( bes' a' aes') \mbreak
    | s2 c''4 bes'4
    | <bes' d''>4 bes'8 c''8 <aes' b'>2
  }
  >>
  \oneVoice
}
Voices_AXXII_AXXIII = {
  <<
  { \voiceOne
    | ees'8 f'8 ees'8 f'8 ees'8 f'8 ees'8 f'8 \mbreak
    | g'2 g'2
  }
  \new Voice
  { \voiceTwo
    | c'1 \mbreak
    | <d' f'>1
  }
  >>
  \oneVoice
}
Voices_BXXI_BXXIII = {
  <<
  { \voiceOne
    | r4 f4 g4 f4
    | r4 g4 bes4 a4 \mbreak
    | r4 f4 c'4 bes4
  }
  \new Voice
  { \voiceTwo
    | bes,1
    | bes,1 \mbreak
    | bes,1
  }
  >>
  \oneVoice
}
Voices_BXXV_BXXVII = {
  <<
  { \voiceOne
    | r4\mf f4 g4 f4 \mbreak
    | r4 g4 bes4 g4
    | c'4 a4 bes4 g4
  }
  \new Voice
  { \voiceTwo
    | bes,1\mf \mbreak
    | des1
    | d2 des2
  }
  >>
  \oneVoice
}
Voices_AXXXII_AXXXIV = {
  <<
  { \voiceOne
    | f''1 \mbreak
    | g'2\mf ees''2
    | <bes' d''>4 bes'8 c''8 d''4 ees''4
  }
  \new Voice
  { \voiceTwo
    | c''4(\> bes'4 a'4 \after 8 \! aes'4) \mbreak
    | s2\mf c''4 bes'4
    | s2 <aes' b'>2
  }
  >>
  \oneVoice
}
Voices_BXXXVIII_BXL = {
  <<
  { \voiceOne
    | r4 f'4( g'4 f'4) \mbreak
    | r4 f'4( g'4 f'4)
    | r4 f'4( g'4 d'4)
  }
  \new Voice
  { \voiceTwo
    | bes1 \mbreak
    | bes1
    | bes1
  }
  >>
  \oneVoice
}
Voices_BXLII = {
  <<
  { \voiceOne
    | r4 f4( g4 f4)
  }
  \new Voice
  { \voiceTwo
    | bes,1
  }
  >>
  \oneVoice
}

% measures 1 - 4
AI = { | d''8\p( f''8 d''8 f''8 d''8 f''8 d''8 f''8) }
AII = { | ees''8( f''8\< ees''8 f''8 ees''8 f''8 ees''8 f''8) }
AIII = { | <d'' g''>2\! <d'' g''>2 }
AIV = { | f''8 c''8 ees''8 bes'8 d''8\> a'8 c''4 \mbreak }
BI_BIII = \Voices_BI_BIII
BIV = { | <f ees'>2 \clef bass f,2\> \mbreak }

% measures 5 - 7
AV = { | bes2\!\mp <d' bes'>2 }
AVI = { | <f' a'>4 <d' f'>8 <e' g'>8 <f' a'>4 <d' bes'>4 }
AVII = { | <g bes>2 <bes g'>2 \mbreak }
BV = { | bes,,8\!\mp f,8 bes,8 a,8 g,4 d4 }
BVI = { | <d a>2. <bes, aes>4 }
BVII = { | ees,8 bes,8 f8 ees8 d4 cis4 \mbreak }

% measures 8 - 10
AVIII_AX = \Voices_AVIII_AX
BVIII = { | d2 bes,,2 }
BIX = { | ees,8 bes,8 f8 ees8 <ees ges>2 }
BX = { | f4 f,4 <g, f>2 \mbreak }

% measures 11 - 13
AXI = { | <a c'>4 a8 bes8 <g c'>4 <ges d'>4 }
AXII = { | <f g bes>2 r8\< c'16( d'16 e'16 f'16 g'16 a'16) }
AXIII = { | <d' bes'>2\!\mf <d'' bes''>2 \mbreak \pgbreak }
BXI = { | <c e>2 <f, ees>2 }
BXII = { | bes,,8 f,8 d8 bes,8 <c ees>4 f,4 }
BXIII = { | bes,,8 f,8 bes,8 a,8 g,8 d8 a8 g8 \mbreak \pgbreak }

% measures 14 - 16
AXIV = { | <f'' a''>4 <d'' f''>8 <e'' g''>8 <f'' a''>4 <d'' bes''>4 }
AXV = { | <g' bes'>2 <bes' g''>2 }
AXVI_AXVIII = \Voices_AXVI_AXVIII
BXIV = { | <d a>2. <bes, aes>4 }
BXV = { | ees,8 bes,8 f8 ees8 d4 des4 }
BXVI = { | <d f>2 bes,,2 \mbreak }

% measures 17 - 19
AXIX = { | <a' c''>4 a'8 bes'8 <g' c''>4 <fis' d''>4 \mbreak }
BXVII = { | ees,8 bes,8 f8 ees8 <ees ges>2 }
BXVIII = { | f4 f,4 <g, f>2 }
BXIX = { | <c e bes>2 <f, ees>2 \mbreak }

% measures 20 - 22
AXX = { | <f' g' bes'>2.\> r8 f'8 }
AXXI = { | d'8\!\mp f'8 d'8 f'8 d'8 f'8 d'8 f'8 }
AXXII_AXXIII = \Voices_AXXII_AXXIII
BXX = { | bes,8( f8 bes8 c'8 d'4 f4) }
BXXI_BXXIII = \Voices_BXXI_BXXIII
BXXII = { | r1 \mbreak }

% measures 24 - 25
AXXIV = { | f''8\< a'8 ees''8 g'8 d''8 f'8 c''8 f'8 }
AXXV = { | d'8\!\mf f'8 d'8 f'8 d'8 f'8 d'8 f'8 \mbreak }
BXXIV = { | <f ees'>2\< \after 4. \! f,2 }
BXXV_BXXVII = \Voices_BXXV_BXXVII

% measures 26 - 28
AXXVI = { | e'8 g'8 e'8 g'8 e'8 g'8 e'8 g'8 }
AXXVII = { | <f' a'>2 <e' a'>2 }
AXXVIII = { | <ees' bes' c''>2\< \after 4. \! <cis' g'>2 \mbreak \pgbreak }
BXXVIII = { | c8\< ees8 g8 bes8 <ees a>4 \after 8 \! f,4 \mbreak \pgbreak }

% measures 29 - 32
AXXIX = { | <d' f' bes'>2\f <d'' g'' bes''>2 }
AXXX = { | <d'' f'' a''>4 <d'' f''>8 <e'' g''>8 <f'' a''>4 <d'' bes''>4 }
AXXXI = { | <g' bes'>2 <bes' g''>2 }
AXXXII_AXXXIV = \Voices_AXXXII_AXXXIV
BXXIX = { | bes,,8\f f,8 bes,8 a,8 g,8 d8 a8 g8 }
BXXX = { | <d a>2. <bes, aes>4 }
BXXXI = { | ees,8 bes,8 f8 ees8 d4 des4 }
BXXXII = { | <d f>2\> \after 4 \! bes,,2 \mbreak }

% measures 33 - 35
AXXXV = { | <a' c''>4 a'8 bes'8 <g' c''>4 <fis' d''>4 \mbreak }
BXXXIII = { | ees,8 bes,8 f8 ees8 <ees ges>2 }
BXXXIV = { | f4 f,4 <g, f>2 }
BXXXV = { | <c e bes>2 <f, ees>2 \mbreak }

% measures 36 - 38
AXXXVI = { | <f' g' bes'>2 g'8( e'8\> c'8 \after 16 \! a8) }
AXXXVII = { | <f' g' bes'>2. r8 f'8 }
AXXXVIII = { | d''8\p f''8 d''8 f''8 d''8 f''8 d''8 f''8 \mbreak }
BXXXVI = { | bes,,8( f,8 d4) <f, ees>2 }
BXXXVII = { | bes,,8( f,8 bes,8 c8 d8 f8 bes8) r8  }
BXXXVIII_BXL = \Voices_BXXXVIII_BXL

% measures 39 - 41
AXXXIX = { | ees''8 f''8 ees''8 f''8 ees''8 f''8 ees''8 f''8 }
AXL = { | <d'' g''>2 <d'' g''>2 }
AXLI = { | f''8(\< a'8 ees''8 g'8 d''8 a'8 c''8) f'8\! \mbreak }
BXLI = { | <f ees'>2 \clef bass f,2 \mbreak }

% measures 42 - 45
AXLII = { | d'8\mp f'8 d'8 f'8 d'8 f'8 d'8 f'8 }
AXLIII = { | ees'8 f'8 ees'8 f'8 ees'8 <d' f'>8\> <ees' g'>8 <c' a'>8 }
AXLIV = { | <d' bes'>2\!\p bes'8(\> c''8 d''8 f''8 }
AXLV = { | <d'' f'' bes''>1)\!\pp\arpeggio\fermata \mbreak \pgbreak }
BXLII = \Voices_BXLII
BXLIII = { | <c bes>4 g4 <f a>4 f,4 }
BXLIV = { | bes,,8( f,8 bes,8 c8 d4 f4 }
BXLV = { | bes1)\fermata \mbreak \pgbreak }

APart = {
  \tempo 4 = 140
  \key bes \major
  \clef treble
  \time 2/2

  \AI \AII \AIII \AIV 
  \repeat volta 2 {
    \AV \AVI \AVII 
    \AVIII_AX
    \AXI \AXII \AXIII 
    \AXIV \AXV \AXVI_AXVIII
    \AXIX 
    \AXX \AXXI \AXXII_AXXIII
    \AXXIV \AXXV 
    \AXXVI \AXXVII \AXXVIII 
    \AXXIX \AXXX \AXXXI \AXXXII_AXXXIV
    \AXXXV

    \alternative {
      \volta 1 { \AXXXVI }
      \volta 2 { \AXXXVII }
    }  
  }
  \AXXXVIII 
  \AXXXIX \AXL \AXLI 
  \AXLII \AXLIII \AXLIV \AXLV 
}

BPart = {
  \tempo 4 = 140
  \key bes \major
  \clef treble
  \time 2/2

  \BI_BIII \BIV
  \repeat volta 2 {
    \BV \BVI \BVII 
    \BVIII \BIX \BX 
    \BXI \BXII \BXIII 
    \BXIV \BXV \BXVI 
    \BXVII \BXVIII \BXIX 
    \BXX \BXXI_BXXIII
    \BXXIV \BXXV_BXXVII
    \BXXVIII 
    \BXXIX \BXXX \BXXXI \BXXXII 
    \BXXXIII \BXXXIV \BXXXV
    \alternative {
      \volta 1 { \BXXXVI }
      \volta 2 { \BXXXVII }
    }
  }
  \clef treble \BXXXVIII_BXL 
  \BXLI 
  \BXLII \BXLIII \BXLIV \BXLV 
}

InstrumentAPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \APart
}

InstrumentBPart = \new Staff \with {midiInstrument = "acoustic grand"} {
  \BPart
}

InstrumentCombined = {
  \new PianoStaff <<
    \chords \with {midiInstrument = "acoustic grand"} { 
      \set noChordSymbol = "(N.C.)"
      | r1 | r1 | r1 | r1
      \repeat volta 2 {
        | bes2 g2:m | d:m bes:7 | ees1
        | bes2./d bes4:7 | ees2 ees2:m | bes2/f g2:7
        | c2:7 f2:7 | bes2:6 c4:m f4:7 | bes2 g2:m 

        | d4:m r2 bes4:7 | ees2 r2 | bes4/d r2 bes4:7
        | ees2 ees2:m | bes4/f r4 g2:7 | c2:7 f2:7
        | bes2.:6 r4 | bes2 r2 | ees2/bes r4 f4:7/bes
        | bes1:6 | f1:7 | bes1
        | des1:dim | d2:m7 des2:dim | c2:m7 f2:7

        | bes2 g2:m | d2:m r4 bes4:7 | ees2 r2 | bes2/d r4 bes4:7
        | ees2 ees2:m | bes2/f g2:7 | c2:7 f2:7

        \alternative {
          \volta 1 { | bes2:6 f2:7 }
          \volta 2 { | bes2.:6 r4 }
        }
      } 
      | bes2 r2
      | f2:7/bes r2 | bes2:6 r2 | f2:7 r2
      | bes2 r2 | c2:m7 f2:7 | bes2 r2 | r1
    }
    \InstrumentAPart
    \addlyrics { 
      " " " " " " " " " " " " " " " " " " " " " "
      "Some" "where" "o-" "ver" "the" "rain-" "bow" "way" "up"
      "high" " " " " " " "there's" "a" " " "land" "that" "I" "heard" "of"
      "once" "in" "a" "lull-" "a-" "by." " "
      "Some" "where" "o-" "ver" "the" "rain-" "bow" "skies" "are" "blue,"
      " " " " " "
      "and" "the" " " "dreams" "that" "you" "dare" "to" "dream" "real-" "ly" "do" "come"
      "true." "Some" "day" "I'll" "wish" "up-" "on" "a" "star" "and" "wake" "up"
      "where" "the" "clouds" "are" "far" "be-" "hind" "me," " " " " " " " " " " " " " "
      "where" "trou-" "bles" "melt" "like" "lem-" "on" "drops" "a-" "way" "a-" "bove"
      "the" "chim-" "ney" "tops" "that's" "where" "you'll" "find" "me."
      "Some" "where" "o-" "ver" "the" "rain-" "bow" "blue" "birds" "fly," " " " " " "
      "birds" "fly" "_" "o-" "ver" "the" "rain-" "bow," "why" "then," "oh" "why" "can't"
      "I?" " " "I?" " " " " " " " " " " " " " " " " " "
      " " " " " " " " " " " " " " " " " " " " " "
      "If" "hap-" "py" "lit-" "tle" "blue" "birds" "fly" "be-" "yond" "the" "rain-" "bow"
      "why" "oh" "why" "can't" "I?"

    }
    \InstrumentBPart
  >>
}

\score {
  \InstrumentCombined
  \layout {
    indent = 0.0
  }
}

\score {
  <<
    \unfoldRepeats {
      \InstrumentCombined
    }
  >>
  \midi { }
}
